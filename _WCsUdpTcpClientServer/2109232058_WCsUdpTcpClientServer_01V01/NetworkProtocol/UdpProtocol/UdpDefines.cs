﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDProtocol
{
  public delegate void DUdpOnMessage(String message);

  public class Defines
  {
    public const String NAME_TASK_UDPCLIENT = "TaskUdpClient";
    public const String NAME_TASK_UDPSERVER = "TaskUdpServer";
  }

}
