﻿
namespace WCsUdpServer
{
  partial class FormUdpServer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpClient = new System.Windows.Forms.TabPage();
      this.lbxInfo = new System.Windows.Forms.ListBox();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.SuspendLayout();
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemToolStripMenuItem});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(484, 24);
      this.mstMain.TabIndex = 2;
      this.mstMain.Text = "menuStrip1";
      // 
      // systemToolStripMenuItem
      // 
      this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitQuit});
      this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
      this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
      this.systemToolStripMenuItem.Text = "&System";
      // 
      // mitQuit
      // 
      this.mitQuit.Name = "mitQuit";
      this.mitQuit.Size = new System.Drawing.Size(97, 22);
      this.mitQuit.Text = "&Quit";
      // 
      // tbcMain
      // 
      this.tbcMain.Controls.Add(this.tbpClient);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Top;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(484, 209);
      this.tbcMain.TabIndex = 3;
      // 
      // tbpClient
      // 
      this.tbpClient.Location = new System.Drawing.Point(4, 22);
      this.tbpClient.Name = "tbpClient";
      this.tbpClient.Padding = new System.Windows.Forms.Padding(3);
      this.tbpClient.Size = new System.Drawing.Size(476, 183);
      this.tbpClient.TabIndex = 0;
      this.tbpClient.Text = "Server";
      this.tbpClient.UseVisualStyleBackColor = true;
      // 
      // lbxInfo
      // 
      this.lbxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxInfo.FormattingEnabled = true;
      this.lbxInfo.Location = new System.Drawing.Point(0, 233);
      this.lbxInfo.Name = "lbxInfo";
      this.lbxInfo.Size = new System.Drawing.Size(484, 428);
      this.lbxInfo.TabIndex = 4;
      // 
      // FormUdpServer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(484, 661);
      this.Controls.Add(this.lbxInfo);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Name = "FormUdpServer";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "WCsUdpServer";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUdpServer_FormClosing);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitQuit;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpClient;
    private System.Windows.Forms.ListBox lbxInfo;
  }
}

