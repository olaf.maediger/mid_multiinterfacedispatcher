﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
//
using Task;
using UDProtocol;
//
namespace WCsUdpServer
{
  public partial class FormUdpServer : Form
  {
    private Process FProcessClient;
    private CUdpServer FUdpServer;
    //
    public FormUdpServer()
    {
      InitializeComponent();
      //
      Left = 2000 + 520;
      Top = 10;
      //
      FProcessClient = null;
      if (0 == Process.GetProcessesByName(Defines.NAME_APPLICATION_CLIENT).Length)
      {
        FProcessClient = Process.Start(@".\" + Defines.NAME_APPLICATION_CLIENT + ".exe");
      }
      //
      FUdpServer = new CUdpServer(IPAddress.Parse("127.0.0.1"), Defines.IPPORT_COMMAND);
      FUdpServer.SetOnMessage(Info);
      FUdpServer.Open();
    }
    //
    //------------------------------------------------------------------------------
    //  Callback
    //------------------------------------------------------------------------------
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Event
    //------------------------------------------------------------------------------
    private void FormUdpServer_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        FUdpServer.Close();
        if (FProcessClient is Process)
        {
          FProcessClient.CloseMainWindow();
          FProcessClient.Close();
        }
      }
      catch (Exception)
      {
        return;
      }
    }
    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }


  }
}
