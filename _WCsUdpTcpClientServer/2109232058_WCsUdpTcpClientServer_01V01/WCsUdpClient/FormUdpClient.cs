﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
//
using Task;
using UDProtocol;
//
namespace WCsUdpClient
{
  public partial class FormUdpClient : Form
  {
    private Process FProcessServer;
    private CTask FTask;
    private CUdpClient FUdpClient;
    //
    public FormUdpClient()
    {
      InitializeComponent();
      //
      Left = 2000 + 10;
      Top = 10;
      //
      FProcessServer = null;
      if (0 == Process.GetProcessesByName(Defines.NAME_APPLICATION_SERVER).Length)
      {
        FProcessServer = Process.Start(@".\" + Defines.NAME_APPLICATION_SERVER + ".exe");
      }
      //
      FUdpClient = new CUdpClient(IPAddress.Parse("127.0.0.1"), Defines.IPPORT_COMMAND);
      FUdpClient.SetOnMessage(Info);
      FUdpClient.Open();
    }
    //
    //------------------------------------------------------------------------------
    //  Callback
    //------------------------------------------------------------------------------
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Event
    //------------------------------------------------------------------------------
    private void FormUdpClient_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        FUdpClient.Close();
        if (FProcessServer is Process)
        {
          FProcessServer.CloseMainWindow();
          FProcessServer.Close();
        }
      }
      catch (Exception)
      {
        return;
      }
    }
    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void btnSend_Click(object sender, EventArgs e)
    {
      Info("UdpClient-Send: Message to Server<" + tbxSend.Text + ">");
      FUdpClient.Send(tbxSend.Text);
    }
  }
}
