﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
//
using Task;
using UDProtocol;
//
namespace WCsUdpClient
{
  public partial class FormUdpClient : Form
  {
    // private Process FProcessServer;
    private CUdpServer FUdpServer;  // Request from Server
    private CUdpClient FUdpClient;  // Message to Server
    //
    public FormUdpClient()
    {
      InitializeComponent();
      //
      Left = 0 * 2000 + 520;
      Top = 10;
      //
      //FProcessServer = null;
      //if (0 == Process.GetProcessesByName(Defines.NAME_APPLICATION_SERVER).Length)
      //{
      //  FProcessServer = Process.Start(@".\" + Defines.NAME_APPLICATION_SERVER + ".exe");
      //}
      // 
      String HostName = Dns.GetHostName();
      Info("HostName[" + HostName + "]");
      String HostIPAddress = "";
      IPHostEntry HostIPEntries = Dns.GetHostEntry(HostName);
      for (int I = 0; I < HostIPEntries.AddressList.Length; I++)
      {
        String IPA = HostIPEntries.AddressList[I].ToString();
        if (IPA.Length < 16)
        {
          if (0 == HostIPAddress.Length)
          {
            HostIPAddress = IPA;
            Info(">>>HostIPAddress[" + HostIPAddress + "]");
          }
          else
          {
            Info("---(ip4:" + IPA + ")");
          }
        }
        else
        {
          Info("---(ip6:" + IPA + ")");
        }
      }
      //
      FUdpServer = new CUdpServer(IPAddress.Parse(HostIPAddress), UdpDefines.IPPORT_CLIENT);
      FUdpServer.SetOnMessage(Info);
      FUdpServer.Open();
      //
      FUdpClient = new CUdpClient(IPAddress.Parse(UdpDefines.SIPADDRESS_BROADCAST), UdpDefines.IPPORT_SERVER);
      FUdpClient.SetOnMessage(Info);
      FUdpClient.Open();
    }
    //
    //------------------------------------------------------------------------------
    //  Callback
    //------------------------------------------------------------------------------
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Event
    //------------------------------------------------------------------------------
    private void FormUdpClient_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        FUdpServer.Close();
        FUdpClient.Close();
        //if (FProcessServer is Process)
        //{
        //  FProcessServer.CloseMainWindow();
        //  FProcessServer.Close();
        //}
      }
      catch (Exception)
      {
        return;
      }
    }
    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void btnSend_Click(object sender, EventArgs e)
    {
      Info("UdpClient-Send: Message to Server<" + tbxSend.Text + ">");
      FUdpClient.Send(tbxSend.Text);
    }
  }
}
