﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
//
using Task;
using UDProtocol;
//
namespace WCsUdpServer
{
  public partial class FormUdpServer : Form
  {
    // private Process FProcessClient;
    private CUdpServer FUdpServer;  // Request from Client
    private CUdpClient FUdpClient;  // Message to Client
    //
    public FormUdpServer()
    {
      InitializeComponent();
      //
      Left = 0 * 2000 + 10;
      Top = 10;
      //
      //FProcessClient = null;
      //if (0 == Process.GetProcessesByName(Defines.NAME_APPLICATION_CLIENT).Length)
      //{
      //  FProcessClient = Process.Start(@".\" + Defines.NAME_APPLICATION_CLIENT + ".exe");
      //}
      //
      String HostName = Dns.GetHostName();
      Info("HostName[" + HostName + "]");
      String HostIPAddress = "";
      IPHostEntry HostIPEntries = Dns.GetHostEntry(HostName);
      for (int I = 0; I < HostIPEntries.AddressList.Length; I++)
      {
        String IPA = HostIPEntries.AddressList[I].ToString();
        if (IPA.Length < 16)
        {
          if (0 == HostIPAddress.Length)
          {
            HostIPAddress = IPA;
            Info(">>>HostIPAddress[" + HostIPAddress + "]");
          }
          else
          {
            Info("---(ip4:" + IPA + ")");
          }
        }
        else
        {
          Info("---(ip6:" + IPA + ")");
        }
      }
      FUdpServer = new CUdpServer(IPAddress.Parse(HostIPAddress), UdpDefines.IPPORT_SERVER);
      FUdpServer.SetOnMessage(Info);
      FUdpServer.Open();
      //
      FUdpClient = new CUdpClient(IPAddress.Parse(UdpDefines.SIPADDRESS_BROADCAST), UdpDefines.IPPORT_CLIENT);
      FUdpClient.SetOnMessage(Info);
      FUdpClient.Open();
    }
    //
    //------------------------------------------------------------------------------
    //  Callback
    //------------------------------------------------------------------------------
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Event
    //------------------------------------------------------------------------------
    private void FormUdpServer_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        FUdpServer.Close();
        FUdpClient.Close();
        //if (FProcessClient is Process)
        //{
        //  FProcessClient.CloseMainWindow();
        //  FProcessClient.Close();
        //}
      }
      catch (Exception)
      {
        return;
      }
    }
    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void btnSend_Click(object sender, EventArgs e)
    {
      Info("UdpServer-Send: Message to Client<" + tbxSend.Text + ">");
      FUdpClient.Send(tbxSend.Text);
    }
  }
}
