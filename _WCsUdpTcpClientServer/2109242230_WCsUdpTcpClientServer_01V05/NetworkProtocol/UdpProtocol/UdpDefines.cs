﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDProtocol
{
  public delegate void DUdpOnMessage(String message);

  public class UdpDefines
  {
    public const String NAME_TASK_UDPCLIENT = "TaskUdpClient";
    public const String NAME_TASK_UDPSERVER = "TaskUdpServer";
    //
    // HOst !!! public const String SIPADDRESS_SERVER = "192.168.178.36";
    public const String SIPADDRESS_BROADCAST = "192.168.178.255";
    // 
    public const int IPPORT_SERVER = 20010;
    public const int IPPORT_CLIENT = 20012;
    //
  }

}
