﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Net.Sockets;
//
using Task;
//
namespace UDProtocol
{
  public class CUdpClient
  {
    private IPAddress FIPAddress;
    private int FIPPort;
    private DUdpOnMessage FOnMessage;
    private CTask FTask;
    private Socket FSocket;
    private String FTXText;
    //
    //------------------------------------------------------------------------------
    //  Constructor
    //------------------------------------------------------------------------------
    public CUdpClient(IPAddress ipaddress, int ipport)
    {
      FIPAddress = ipaddress;
      FIPPort = ipport;
      FTXText = "";
      FOnMessage = null;
    }
    //
    //------------------------------------------------------------------------------
    //  Property
    //------------------------------------------------------------------------------
    public void SetOnMessage(DUdpOnMessage onmessage)
    {
      FOnMessage = onmessage;
    }
    //
    //------------------------------------------------------------------------------
    //  Helper
    //------------------------------------------------------------------------------
    private void Info(String info)
    {
      if (FOnMessage is DUdpOnMessage)
      {
        FOnMessage(info);
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private void OnExecutionStart(RTaskData data)
    {
      Info("UdpClient-Start: Initialising Client");
      FSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      FSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
    }
    private Boolean OnExecutionBusy(RTaskData data)
    {
      try
      {
        if (0 == FTXText.Length)
        { // Check-Frequency: 20Hz
          Thread.Sleep(50);
        }
        else
        {
          Info("UdpClient-Busy: Try Connection to Server...");
          FSocket.Connect(FIPAddress, FIPPort);
          Info("UdpClient-Busy: Server connected");
          Info("UdpClient-Busy: Send Message to Server...");
          byte[] TXData = Encoding.ASCII.GetBytes(FTXText);
          FSocket.Send(TXData);
          FTXText = "";
        }
      }
      catch (Exception)
      {
        return true;
      }
      return true;
    }
    private void OnExecutionEnd(RTaskData data)
    {
    }
    private void OnExecutionAbort(RTaskData data)
    {
    }
    //
    //------------------------------------------------------------------------------
    //  Management
    //------------------------------------------------------------------------------
    public void Open()
    {
      FTXText = "";
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
      }
      FTask = new CTask(UdpDefines.NAME_TASK_UDPCLIENT, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      Info("UdpClient-Open: Starting Task");
      FTask.Start();
    }
    public void Close()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
      }
    }
    public Boolean Send(String text)
    {
      if (0 == FTXText.Length)
      {
        FTXText = text;
        return true;
      }
      return false;
    }
  //
  }
}
//
