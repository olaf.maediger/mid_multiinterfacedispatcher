﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
//
using Task;
//
namespace UDProtocol
{
  public class CUdpServer
  {
    private IPAddress FIPAddress;
    private int FIPPort;
    private DUdpOnMessage FOnMessage;
    private CTask FTask;
    private Socket FSocket;
    //
    //------------------------------------------------------------------------------
    //  Constructor
    //------------------------------------------------------------------------------
    public CUdpServer(IPAddress ipaddress, int ipport)
    {
      FIPAddress = ipaddress;
      FIPPort = ipport;
      FOnMessage = null;
    }
    //
    //------------------------------------------------------------------------------
    //  Property
    //------------------------------------------------------------------------------
    public void SetOnMessage(DUdpOnMessage onmessage)
    {
      FOnMessage = onmessage;
    }
    //
    //------------------------------------------------------------------------------
    //  Helper
    //------------------------------------------------------------------------------
    private void Info(String info)
    {
      if (FOnMessage is DUdpOnMessage)
      {
        FOnMessage(info);
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private void OnExecutionStart(RTaskData data)
    {
    }
    private Boolean OnExecutionBusy(RTaskData data)
    {
      Info("UdpServer-Busy: Create Connection");
      FSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      FSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
      Info("UdpServer-Busy: Wait for Request from Client...");
      EndPoint EndPointClient = new IPEndPoint(FIPAddress, FIPPort);
      FSocket.Bind(EndPointClient);
      Info("UdpServer-Busy: Binding to Client successful");
      byte[] RXData = new byte[1024];
      int RXSize = FSocket.ReceiveFrom(RXData, SocketFlags.None, ref EndPointClient);
      String RXText = "";
      for (int CI = 0; CI < RXSize; CI++)
      {
        RXText += (char)RXData[CI];
      }
      Info("UdpServer-Busy: Data from Client received: <" + RXText + ">");     
      Info("UdpServer-Busy: Dispose Connection");
      FSocket.Close();
      FSocket = null;
      // Thread.Sleep(1000);
      return true;
    }
    private void OnExecutionEnd(RTaskData data)
    {
    }
    private void OnExecutionAbort(RTaskData data)
    {
    }
    //
    //------------------------------------------------------------------------------
    //  Management
    //------------------------------------------------------------------------------
    public void Open()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
      }
      FTask = new CTask(UdpDefines.NAME_TASK_UDPCLIENT, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      Info("UdpServer-Open: Starting Task");
      FTask.Start();
    }
    public void Close()
    {
      if (FSocket is Socket)
      {
        // debug ok ! Console.WriteLine("!!! Socket Closed");
        FSocket.Close();
        FSocket = null;
      }
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
      }
    }

  }

}
