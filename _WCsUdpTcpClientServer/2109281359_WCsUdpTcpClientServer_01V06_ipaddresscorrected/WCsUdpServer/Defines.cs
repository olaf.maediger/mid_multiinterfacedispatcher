﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCsUdpServer
{
  public class Defines
  {
    public const String NAME_APPLICATION_CLIENT = "WCsUdpClient";
    //
    public const String NAME_TASK = "TaskServer";
    //
    public const String RESPONSE_ACKNOWLEDGE = "ACK";
  }
}
