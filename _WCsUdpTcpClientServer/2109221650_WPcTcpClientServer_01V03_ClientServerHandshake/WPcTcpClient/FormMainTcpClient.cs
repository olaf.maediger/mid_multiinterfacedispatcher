﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Text;
//
using Task;
using System.Net;
using System.Net.Sockets;
//
namespace WPcTcpClient
{
  public partial class FormMainTcpClient : Form
  {
    const String NAME_APPLICATION_CLIENT = "WPcTcpClient";
    const String NAME_APPLICATION_SERVER = "WPcTcpServer";
    const String NAME_TASK = "Task";
    const String COMMAND_SETMOTORAXIS = "SET MotorAxis 1 2 3";
    //
    private Process FProcess;
    private CTask FTask;
    private TcpClient FTcpClient;
    private IPAddress FIPAddress;
    private int FPort = 1234;
    //
    public FormMainTcpClient()
    {
      InitializeComponent();
      //
      FProcess = null;
      if (0 == Process.GetProcessesByName(NAME_APPLICATION_SERVER).Length)
      {
        FProcess = Process.Start(@".\" + NAME_APPLICATION_SERVER + ".exe");
      }
      //
      Info("Task start...");
      FTask = new CTask(NAME_TASK, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      FTask.Start();
    }
    //
    //------------------------------------------------------------------------------
    //  Helper
    //------------------------------------------------------------------------------

    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private void OnExecutionStart(RTaskData data)
    {
      // Info("OnExecutionStart");
      Info("Start: Initialising / try to connect with Server...");
      FIPAddress = IPAddress.Parse("127.0.0.1");
    }
    private Boolean OnExecutionBusy(RTaskData data)
    {
      try
      { // Info("OnExecutionBusy[" + data.Counter.ToString() + "]");
        if (FTcpClient is TcpClient)
        {
          FTcpClient.Close();
          FTcpClient = null;
        }
        Info("Busy: TcpClient: Create");
        FTcpClient = new TcpClient();
        FTcpClient.Connect(FIPAddress, FPort);
      }
      catch (Exception ex)
      {
        Info("Error: Cannot connect to Server!");
        Info("Busy: TcpClient: Destroy");
        FTcpClient.Close();
        FTcpClient = null;
        Thread.Sleep(5000);
        return true;
      }
      Info("Busy: Connected to Server");
      NetworkStream NWStream = FTcpClient.GetStream();
      // TXD
      Info("Busy: Send Data to Server: <" + COMMAND_SETMOTORAXIS + ">");
      byte[] TXData = ASCIIEncoding.ASCII.GetBytes(COMMAND_SETMOTORAXIS);
      NWStream.Write(TXData, 0, TXData.Length);
      // RXD
      Info("Busy: Waiting for Response from Server...");
      Thread.Sleep(100);
      byte[] RXData = new byte[128];
      int RXSize = NWStream.Read(RXData, 0, RXData.Length);
      Info("Busy: Receive Data from Server: <" + ASCIIEncoding.ASCII.GetString(RXData) + ">");
      //
      Info("Busy: Close Connection from Server");
      NWStream.Close();
      Info("Busy: TcpClient: Destroy");
      FTcpClient.Close();
      FTcpClient = null;
      //
      Thread.Sleep(300000);
      return true;
    }
    private void OnExecutionEnd(RTaskData data)
    {
      Info("End: ");
      if (FTcpClient is TcpClient)
      {
        Info("End: TcpClient: Destroy");
        FTcpClient.Close();
        FTcpClient = null;
      }
      FTask = null;
    }
    private void OnExecutionAbort(RTaskData data)
    {
      if (FTcpClient is TcpClient)
      {
        Info("Abort: TcpClient: Destroy");
        FTcpClient.Close();
        FTcpClient = null;
      }
      FTask = null;
    }
    //
    private void FormMainTcpClient_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (FTcpClient is TcpClient)
        {
          Info("FormClosing: TcpClient: Destroy");
          FTcpClient.Close();
          FTcpClient = null;
        }
        if (FTask is CTask)
        {
          FTask.Abort();
        }
        if (FProcess is Process)
        {
          FProcess.CloseMainWindow();
          FProcess.Close();
        }
      }
      catch (Exception)
      {
        return;
      }
    }

    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
  }
}
