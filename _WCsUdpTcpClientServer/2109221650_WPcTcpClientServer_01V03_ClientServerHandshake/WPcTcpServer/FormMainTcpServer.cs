﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Net.Sockets;
//
using Task;
//
namespace WPcTcpServer
{
  public partial class FormMainTcpServer : Form
  {
    const String NAME_APPLICATION_SERVER = "WPcTcpServer";
    const String NAME_APPLICATION_CLIENT = "WPcTcpClient";
    const String NAME_TASK = "Task";
    const int PORT_TCP = 1234;
    //
    //private Process FProcess;
    private CTask FTask;
    private IPAddress FIPAddress;
    private TcpListener FTcpListener;
    private TcpClient FTcpClient;
    //
    public FormMainTcpServer()
    {
      InitializeComponent();
      //
      //FProcess = null;
      if (0 == Process.GetProcessesByName(NAME_APPLICATION_CLIENT).Length)
      {
        // FProcess = Process.Start(@".\" + NAME_APPLICATION_CLIENT + ".exe");
      }
      //
      FTcpListener = null;
      FTcpClient = null;
      Info("Task start...");
      FTask = new CTask(NAME_TASK, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      FTask.Start();
    }
    
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new CBInfo(Info), new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private void OnExecutionStart(RTaskData data)
    {
      // Info("*** OnExecutionStart");
      FIPAddress = IPAddress.Parse("127.0.0.1");
      Info("Start: TCPListener start");
      FTcpListener = new TcpListener(FIPAddress, PORT_TCP);
      FTcpListener.Start();
    }
    private Boolean OnExecutionBusy(RTaskData data)
    {
      try
      {
        //Info("*** OnExecutionBusy[" + data.Counter.ToString() + "]");
        // Info("Server is listening on " + FTcpListener.LocalEndpoint);
        //Info("Waiting for a connection...");
        if (FTcpListener is TcpListener)
        {
          if (!FTcpListener.Pending())
          {
            Thread.Sleep(100);
            return true;
          }
          // wait for Client-Request:
          FTcpClient = FTcpListener.AcceptTcpClient();
          Info("Busy: Connection to TcpClient accepted.");
          NetworkStream NWStream = FTcpClient.GetStream();
          // RXD: Reading request
          Info("Busy: Reading request from Client");
          Byte[] RXData = new Byte[128];
          int RXSize = NWStream.Read(RXData, 0, RXData.Length);
          String RXText = "";
          for (int CI = 0; CI < RXSize; CI++)
          {
            RXText += Convert.ToChar(RXData[CI]);
          }
          Info("Busy: Request from Client: <" + RXText + ">");
          // TXD: Send handshake to Client
          String TXText = "ACCEPT " + RXText;
          Info("Busy: Sending Acknowledge to Client: <" + TXText + ">");
          Byte[] TXData = new Byte[128];
          int TXSize = TXText.Length;
          for (int CI = 0; CI < TXSize; CI++)
          {
            TXData[CI] = Convert.ToByte(TXText[CI]);
          }
          for (int CI = TXSize; CI < TXData.Length; CI++)
          {
            TXData[CI] = 0x00;
          }          
          NWStream.Write(TXData, 0, TXData.Length);
          //
          Info("Busy: Close Connection to Client");
          NWStream.Close();
          FTcpClient.Close();
          FTcpClient = null;
          //
          Thread.Sleep(30000);
        }
      }
      catch (Exception e)
      {
        Info(e.Message);
      }
      return true;
    }
    private void OnExecutionEnd(RTaskData data)
    {
      // Info("*** OnExecutionEnd");
      if (FTcpListener is TcpListener)
      {
        Info("End: TcpListener stop");
        FTcpListener.Stop();
        FTcpListener = null;
      }
      FTask = null;
    }
    private void OnExecutionAbort(RTaskData data)
    {
      Info("*** OnExecutionAbort");
      if (FTcpListener is TcpListener)
      {
        Info("Abort: TcpListener stop");
        FTcpListener.Stop();
        FTcpListener = null;
      }
      FTask = null;
    }
    //
    private void FormMainTcpServer_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
        }
        if (FTcpListener is TcpListener)
        {
          Info("FormClosing: TcpListener stop");
          FTcpListener.Stop();
          FTcpListener = null;
        }
        //if (FProcess is Process)
        //{
        //  FProcess.CloseMainWindow();
        //  FProcess.Close();
        //  FProcess = null;
        //}
      }
      catch (Exception)
      {
        return;
      }
    }

    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
  }
}
