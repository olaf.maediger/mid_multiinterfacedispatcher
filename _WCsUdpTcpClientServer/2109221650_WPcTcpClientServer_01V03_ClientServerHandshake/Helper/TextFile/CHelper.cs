using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Threading;

namespace TextFile
{
	static class CHelper
	{
		static public String CreateDateTimeFileName(String title)
		{
			DateTime DateTime = DateTime.Now;
			Int32 Year = DateTime.Year % 100;
			String Filename = Year.ToString("00");
			Filename += DateTime.Month.ToString("00");
			Filename += DateTime.Day.ToString("00");
			Filename += DateTime.Hour.ToString("00");
			Filename += DateTime.Minute.ToString("00");
			Filename += DateTime.Second.ToString("00");
			Filename += DateTime.Millisecond.ToString();
			Filename += title;
			return Filename;
		}

		static public String CreateDateTimeFileName()
		{
			return CreateDateTimeFileName("TextFile");
		}
	}
}
