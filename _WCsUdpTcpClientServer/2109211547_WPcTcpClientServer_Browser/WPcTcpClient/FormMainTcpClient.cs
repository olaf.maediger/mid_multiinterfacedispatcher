﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
//
using Task;
//
namespace WPcTcpClient
{
  public partial class FormMainTcpClient : Form
  {
    const String NAME_APPLICATION_SERVER = "WPcTcpServer";
    const String NAME_APPLICATION_CLIENT = "WPcTcpClient";
    const String NAME_TASK = "Task";
    //
    private Process FProcess;
    private CTask FTask;
    //
    public FormMainTcpClient()
    {
      InitializeComponent();
      //
      FProcess = null;
      if (0 == Process.GetProcessesByName(NAME_APPLICATION_SERVER).Length)
      {
        FProcess = Process.Start(@".\WPcTcpServer.exe");
      }
      //
      FTask = new CTask(NAME_TASK, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private void OnExecutionStart(RTaskData data)
    {
    }
    private Boolean OnExecutionBusy(RTaskData data)
    {
      return false;
    }
    private void OnExecutionEnd(RTaskData data)
    {

    }
    private void OnExecutionAbort(RTaskData data)
    {

    }
    //
    private void FormMainTcpClient_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
        }
        if (FProcess is Process)
        {
          FProcess.CloseMainWindow();
          FProcess.Close();
        }
      }
      catch (Exception)
      {
        return;
      }
    }

    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
  }
}
