﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
//
using Task;
//
namespace WPcTcpServer
{
  public partial class FormMainTcpServer : Form
  {
    const String NAME_APPLICATION_SERVER = "WPcTcpServer";
    const String NAME_APPLICATION_CLIENT = "WPcTcpClient";
    const String NAME_TASK = "Task";
    const int PORT_TCP = 80;
    //
    private Process FProcess;
    private CTask FTask;
    private IPAddress FIPAddress;
    private TcpListener FTcpListener;
    //
    public FormMainTcpServer()
    {
      InitializeComponent();
      //
      FProcess = null;
      if (0 == Process.GetProcessesByName(NAME_APPLICATION_SERVER).Length)
      {
        // FProcess = Process.Start(@".\WPcTcpServer.exe");
      }
      //
      FTask = new CTask(NAME_TASK, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      FTask.Start();
    }
    private delegate void CBInfo(String line);
    private void Info(String line)
    {
      if (this.InvokeRequired)
      {
        CBInfo CB = new CBInfo(Info);
        Invoke(CB, new object[] { line });
      }
      else
      {
        lbxInfo.Items.Add(line);
        lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
        lbxInfo.Refresh();
      }
    }
    //
    //------------------------------------------------------------------------------
    //  Callback - Task
    //------------------------------------------------------------------------------
    private delegate void CBOnExecutionStart(RTaskData data);
    private void OnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBOnExecutionStart CB = new CBOnExecutionStart(OnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FIPAddress = IPAddress.Parse("127.0.0.1");
        Info("Starting TCP listener...");
        FTcpListener = new TcpListener(FIPAddress, PORT_TCP);
        FTcpListener.Start();
      }
    }
    private delegate Boolean CBOnExecutionBusy(RTaskData data);
    private Boolean OnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBOnExecutionBusy CB = new CBOnExecutionBusy(OnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        Info("Server is listening on " + FTcpListener.LocalEndpoint);
        Info("Waiting for a connection...");
        Socket Client = FTcpListener.AcceptSocket();
        Info("Connection accepted.");
        Info("Reading data...");
        byte[] Data = new byte[100];
        int Size = Client.Receive(Data);
        String SData = "Received data[";
        for (int i = 0; i < Size; i++)
        {
          SData += Convert.ToChar(Data[i]);
        }
        Info(SData);
        Client.Close();
        return true;
      }
      return true;
    }
    private delegate void CBOnExecutionEnd(RTaskData data);
    private void OnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBOnExecutionEnd CB = new CBOnExecutionEnd(OnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (FTcpListener is TcpListener)
        {
          FTcpListener.Stop();
          FTcpListener = null;
        }
      }
    }
    private delegate void CBOnExecutionAbort(RTaskData data);
    private void OnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBOnExecutionAbort CB = new CBOnExecutionAbort(OnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (FTcpListener is TcpListener)
        {
          FTcpListener.Stop();
          FTcpListener = null;
        }
      }
    }
    //
    private void FormMainTcpServer_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (FTcpListener is TcpListener)
        {
          FTcpListener.Stop();
          FTcpListener = null;
        }
        if (FTask is CTask)
        {
          FTask.Abort();
        }
        if (FProcess is Process)
        {
          FProcess.CloseMainWindow();
          FProcess.Close();
          FProcess = null;
        }
      }
      catch (Exception)
      {
        return;
      }
    }

    private void mitQuit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
  }
}
