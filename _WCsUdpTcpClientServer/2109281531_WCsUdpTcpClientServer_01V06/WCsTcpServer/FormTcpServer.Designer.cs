﻿
namespace WPcTcpServer
{
  partial class FormTcpServer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpServer = new System.Windows.Forms.TabPage();
      this.pnlInfo = new System.Windows.Forms.Panel();
      this.lbxInfo = new System.Windows.Forms.ListBox();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.pnlInfo.SuspendLayout();
      this.SuspendLayout();
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemToolStripMenuItem});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(484, 24);
      this.mstMain.TabIndex = 1;
      this.mstMain.Text = "menuStrip1";
      // 
      // systemToolStripMenuItem
      // 
      this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitQuit});
      this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
      this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
      this.systemToolStripMenuItem.Text = "&System";
      // 
      // mitQuit
      // 
      this.mitQuit.Name = "mitQuit";
      this.mitQuit.Size = new System.Drawing.Size(97, 22);
      this.mitQuit.Text = "&Quit";
      this.mitQuit.Click += new System.EventHandler(this.mitQuit_Click);
      // 
      // tbcMain
      // 
      this.tbcMain.Controls.Add(this.tbpServer);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Top;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(484, 100);
      this.tbcMain.TabIndex = 2;
      // 
      // tbpServer
      // 
      this.tbpServer.Location = new System.Drawing.Point(4, 22);
      this.tbpServer.Name = "tbpServer";
      this.tbpServer.Padding = new System.Windows.Forms.Padding(3);
      this.tbpServer.Size = new System.Drawing.Size(476, 74);
      this.tbpServer.TabIndex = 0;
      this.tbpServer.Text = "Server";
      this.tbpServer.UseVisualStyleBackColor = true;
      // 
      // pnlInfo
      // 
      this.pnlInfo.Controls.Add(this.lbxInfo);
      this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlInfo.Location = new System.Drawing.Point(0, 124);
      this.pnlInfo.Name = "pnlInfo";
      this.pnlInfo.Size = new System.Drawing.Size(484, 537);
      this.pnlInfo.TabIndex = 3;
      // 
      // lbxInfo
      // 
      this.lbxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxInfo.FormattingEnabled = true;
      this.lbxInfo.Location = new System.Drawing.Point(0, 0);
      this.lbxInfo.Name = "lbxInfo";
      this.lbxInfo.Size = new System.Drawing.Size(484, 537);
      this.lbxInfo.TabIndex = 0;
      // 
      // FormTcpServer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(484, 661);
      this.Controls.Add(this.pnlInfo);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Name = "FormTcpServer";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "WPcTcpServer";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMainTcpServer_FormClosing);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.pnlInfo.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitQuit;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpServer;
    private System.Windows.Forms.Panel pnlInfo;
    private System.Windows.Forms.ListBox lbxInfo;
  }
}

