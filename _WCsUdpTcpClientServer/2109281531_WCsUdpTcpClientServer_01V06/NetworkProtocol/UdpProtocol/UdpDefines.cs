﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDProtocol
{
  public delegate void DUdpOnMessage(String message);

  public class UdpDefines
  {
    public const String NAME_TASK_UDPCLIENT = "TaskUdpClient";
    public const String NAME_TASK_UDPSERVER = "TaskUdpServer";
    //
    // HOst !!! public const String SIPADDRESS_SERVER = "192.168.178.36";
    // SA7 public const String SIPADDRESS_BROADCAST = "192.168.178.255";
    // IFN
    // wifi ??? public const String SIPADDRESS_BROADCAST = "192.168.15.25";    
    //public const String SIPADDRESS_BROADCAST = "192.168.1.19";
    // public const String SIPADDRESS_BROADCAST = "127.0.0.1";
    //public const String SIPADDRESS_BROADCAST = "169.254.141.214";
    //public const String SIPADDRESS_BROADCAST = "169.254.171.132";
    // IFN LAN : public const String SIPADDRESS_BROADCAST = "192.168.1.255";
    // IFN WLAN :
    //public const String SIPADDRESS_BROADCAST = "192.168.0.255";
    public const String SIPADDRESS_BROADCAST = "192.168.15.255";
    // 
    public const int IPPORT_SERVER = 20010;
    public const int IPPORT_CLIENT = 20012;
    //
  }

}
