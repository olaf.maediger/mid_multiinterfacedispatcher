//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_BT)
//
#ifndef DispatcherBT_h
#define DispatcherBT_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - BT - SHORT
//----------------------------------------------------------------
#define SHORT_WCB   "WCB"       // WriteCommandBt <c> <p0>[..<p3>]
//
//----------------------------------------------------------------
// Dispatcher - Bt - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_BT       " Help (Bt-Bluetooth):"
#define MASK_WCB              " %-3s <c> [<p0>..<p3>]: Write <c>ommand Bt <p>arameter"
//
//----------------------------------------------------------------
// Dispatcher - Bt
//----------------------------------------------------------------
class CDispatcherBt : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherBt(void);
  //
  bool ExecuteWriteCommandBt(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool virtual Execute(void);
};
//
#endif // DispatcherBt_h
//
#endif // DISPATCHER_BT
//
