//
#include <Arduino.h>
//
const char* HEADER[] = 
{
  "\r\n",
  "*** Esp32PointerPointer",
  "    Version: 01V01",
  "    Date   : 210910",
  "    Time   : 0951",
  "    Author : OMdevelop"
};
//
typedef char  Character;
typedef char* PCharacter;
typedef char** VPCharacter;
//
const int SIZE_VECTOR = 5;
//
void setup() 
{
  Serial.begin(115200);
  delay(333);
  for (int H = 0; H < sizeof(HEADER) / sizeof(char*); H++)
  {
    Serial.println(HEADER[H]);
  }
  //
  PCharacter PName = new Character[10];
  strcpy(PName, "abc");
  Serial.print("PName[");
  Serial.print(PName);
  Serial.println("]");
  //
  VPCharacter VNames = new PCharacter[SIZE_VECTOR];
  Serial.print("sizeof(VNames)[");
  Serial.print(sizeof(VNames));
  Serial.println("]");
  //
  for (int I = 0; I < SIZE_VECTOR; I++)
  {
    VNames[I] = new char[10];
    sprintf(VNames[I], "Names[%i][%d]", I, I * I);
    Serial.println(VNames[I]);
  }
}

void loop() 
{
}