<h2>PC-MID - PCMultiInterfaceDispatcher</h2>


<br><h3>2106060850 - Wiederaufnahme MID nach Auslieferung Glamma</h3>

<b>O</b> Vorbereitung Esp32MultiInterfaceDispatcher (zuerst mit Uart, dann Bt, Lan und Wlan).
<b>X</b> Interface: Abstrakte Methoden ReadLine/WriteLine, Überladen in Bt, Lan, Wlan und Uart.
<b>X</b> UC-Reduktion auf UCLaserAreaScanner.




---



<br><h3>2106060850 - Übersicht</h3>

* Kommunikation zwischen uC(Arduino-Derivat) und PC mit Win10.
* Exe mit C# erstellt unter VS19.
* "Hardware"-Command-Transfer über Uart, Bt, Lan und Wlan.



<br><h3>2106060850 - Gliederung der VS19-Solution</h3>

<img src="./image/2106061011_VSCode.png" alt="" width="80%">

<br><h3>2106050750 - Erstes TopDown-Design</h3>


<br><h3>Definitionen</h3>
MID - MultiInterfaceDispatcher
uC - MicroController
Lan - LocalAreaNetwork
Wlan - WirelessLocalAreaNetwork
Bt - Bluetooth
Uart - UniversialAsynchronousReceiverTransmitter
VS19 - VisualStudio2019

<!--
<h3>2106060850 - </h3>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<b></b>
<img src=".png" alt="">
-->
