<h2>MID_Esp32MultiInterfaceDispatcher</h2>


<h3>2109100430</h3>

* <b>O</b>
* <b>O</b>
* <b>O</b>
* <b></b>
* Boolean CInterfaceUartHS::ReadLine(char* prxline, int &rxsize)
  reads a whole line with CR/LF,
  if <line then rxdata is buffered in FRXBlock,
  return true: found whole line
  prxline points to extern buffer with copy of line

<h3>2109090700</h3>

* <b>O</b> Help-Ausgabe main.cpp von uart auf Mqtt erweitern!
* <b>O</b> Bearbeitung der Command-Dispatcher-Files
  DispatcherMqtt.h/.cpp entsprechend DispatcherUart.h/.cpp
* <b>X</b> Basis-Interface-Files: Uart.h/.cpp // Mqtt.h/.cpp
  (Um)Benennung auf InterfaceUart.h/.cpp mit CUart -> CInterfaceUart
  (Um)Benennung auf Mqtt.h/.cpp mit CMqtt -> CProtocolMqtt
* <b>O</b> Topic-Buffer: Reduction auf 32 Byte
* <b>O</b> Mqtt-Instanz in main.cpp
* <b>O</b>
* <b>///</b> Integration von CProtocolMqtt MqttCommand in main.cpp
* <b>X</b> ReadCommandUart/Mqtt geben keinen Sinn, da automatische Hintergrund-Bearbeitung
  (Receiving und Dispatching) der eingehenden RXData
* <b>O</b> Integration von OOP-Version <b>2109081625_Esp32PubSubClient_02V01</b>,
  speziell (Protocol)Mqtt.h, (Protocol)Mqtt.cpp, Definition.h

<h3>2109081800</h3>

  * Restart Projekt <b>Esp32MultiInterfceDispatcher</b> auf der Grundlage von   <b>2108311537_Esp32MultiInterfaceDispatcher_NoErrorRXBuffer</b>
  * Fertigstellung Version: 2109081625_Esp32PubSubClient_02V01




<!--
<a href="https://pubsubclient.knolleary.net/api">Api PubSubClient</a>
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<h8></h8>
<b></b>
<i></i>
<></>
<></>
-->
