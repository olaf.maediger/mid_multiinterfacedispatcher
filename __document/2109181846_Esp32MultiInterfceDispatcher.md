<h2>MID_Esp32MultiInterfaceDispatcher</h2>

<h3>ToDo</h3>

* <b>O</b> Lan-CommandDispatcher

 <b></b>

<h3>2109180800 : WLan-CommandDispatcher</h3>

* Einbau <b>CConnectionWifi.cpp/.h</b>, Reconfigure <b>Wlan</b> in MID


<h3>2109180800 : Wlan OOP</h3>

* Namensänderung <b>CWifi</b> nach <b>CConnectionWifi</b> notwendig, sonst Compile-Error
* <b>2109181851_Esp32WlanBasicClass_01V02</b> mit WiFi-Reference, CConnectionWifi
* <b>2109181842_Esp32WlanBasicClass_01V01</b>
* Basisprogramm mit OOP


<h3>2109171834 : Wlan</h3>

Einfache Basisprogramme ohne OOP:
* erweiterte <b>platformio.ini</b> mit 3MB und 115200bps
* Wlan-Version <b>2109172136_Esp32WlanBasic_01V01</b>
* Wlan-Version <b>2109171920_Esp32WlanScan_01V01</b>

<h3>2109170700 : Speichererweiterung ESP32</h3>

* Minimalprojekt <b>2109170906_Esp32Flash4MB_01V01</b>
* modifizierte Datei <b>default_4MB.csv</b> erzeugt bei Compilierung 3MB Flash!!!
<code># Name,   Type, SubType, Offset,  Size, Flags
nvs,      data, nvs,     0x009000,0x005000,
otadata,  data, ota,     0x00e000,0x002000,
app0,     app,  ota_0,   0x010000,0x2E0000,
app1,     app,  ota_1,   0x3E0000,0x010000,
spiffs,   data, spiffs,  0x3F0000,0x010000,</code>

* Erweiterung <b>platformio.ini</b>:
<code>board_upload.flash_size = 4MB
board_upload.maximum_size = 4194304
board_build.partitions = default_4MB.csv</code>

* Original Partition-Files unter:
<a href="https://github.com/espressif/arduino-esp32/tree/master/tools/partitions">Espressif Esp32 Partition</a>

<h3>2109160800 : Speichererweiterung ESP32</h3>

* Stichwort: Partition-Table (als <b>default.csv</b> Datei unter x:/)
* default Flash-Memory: 1.3MB, obwohl 4MB vorhanden sind


<h3>210915xxxx : InterfaceBt ohne Mqtt</h3>

* <b>XX</b> Bt-CommandDispatcher verhält sich identisch zu Uart-/Mqtt-Dispatcher
* <b>X</b> Bluetooth-Version: <b>2109161457_Esp32MultiInterfaceDispatcher_00V35_UartBtorMqtt</b>

<h3>210913xxxx : InterfaceBt Exception</h3>

* Bt erst einmal mit Mqtt disabled programmieren
* <b>X</b> Exception liegt an dem zu geringen Flash-Speicher

<h3>2109121018</h3>

* <b>???</b> Instanzierung der CInterfaceBt-Class löst Exception aus
* <b>X</b> <b>DefinitionSystem.h</b> für Bluetooth freischalten
* <b>...</b> Mqtt-Ausgaben identisch zu Uart-Ausgaben (auch Hilfe...) erweitern!
* <b>X</b> Eigene Bt-Class-Library suchen, Reste gefunden und neu formuliert

<h3>2109111002</h3>

* <b>X</b> <b>2109111648_Esp32MultiInterfaceDispatcher_00V33</b>
* <b>X</b> Übertragung von <b>Definition.h</b>(Mqtt) nach <b>DefinitionSystem.h</b>
* <b>X</b> Jede Device erhält eigene <b>Definition<device>.h</b> Datei
* <b>X</b> Mqtt-Uart-Ausgaben normalisieren
* <b>X</b> Mqtt-Commands werden bearbeitet
* <b>X</b> Exception in <b>Command.cpp</b> (aktuell unkommentiert)
> for (int MI = 0; MI < MQTT_PARAMETERCOUNT; MI++)
> {
> // &nbsp;&nbsp;MqttParameters[0] = 0x00;
> &nbsp;&nbsp;MqttParameters[MI][0] = 0x00;
> }

* <b>X</b> main.cpp: <b>void InterfaceProtocolDispatcherPeriodic(void)</b>
* <b>X</b> main.cpp: Dispatcher Erweiterung mit Mqtt, neue Command-Interface/Protocol/Dispatcher Function: <b>bool InterfaceProtocolDispatcher(char* command, int parametercount, char** parameters)</b>
* <b>X</b> main.cpp: <b>void CommandDispatcherPeriodic(void)</b>
* <b>X</b> Entschlackung von main-<b>setup()</b> und -<b>loop()</b>:
  <b>SetupInterfaceProtocol()</b>
  <b>SetupDispatcher()</b>


<h3>2109100430</h3>

* <b>X</b> bool CCommand::AnalyseInterfaceBlock(void) mit Mqtt erweitern
* <b>O</b> Erweiterung DISPATCHER_MQTT
* <b>X</b> <b>2109101607_Esp32MultiInterfaceDispatcher_00V32</b>
  mit Uart- und (teilweiser)Mqtt-Kommunikation(ohne Dispatcher)
* <b>X</b> Zyklisches Reconnecten funktioniert
* <b>X</b> Integration von <b>2109100958_Esp32PointerPointer_01V01</b> und <b>CProtocolMqtt</b>
* <b>X</b> Demo-Programm zur Lösung des PointerPointerProblems:
  <b>2109100958_Esp32PointerPointer_01V01</b>,
  Übertragung der Pointer PCharacter, VPCharacter in <b>DefinitionSystem.h</b>
* Boolean CInterfaceUartHS::ReadLine(char* prxline, int &rxsize)
  reads a whole line with CR/LF,
  if <line then rxdata is buffered in FRXBlock,
  return true: found whole line
  prxline points to extern buffer with copy of line

<h3>2109090700</h3>

* <b>O</b> Help-Ausgabe main.cpp von uart auf Mqtt erweitern!
* <b>O</b> Bearbeitung der Command-Dispatcher-Files
  DispatcherMqtt.h/.cpp entsprechend DispatcherUart.h/.cpp
* <b>X</b> Basis-Interface-Files: Uart.h/.cpp // Mqtt.h/.cpp
  (Um)Benennung auf InterfaceUart.h/.cpp mit CUart -> CInterfaceUart
  (Um)Benennung auf Mqtt.h/.cpp mit CMqtt -> CProtocolMqtt
* <b>O</b> Topic-Buffer: Reduction auf 32 Byte
* <b>O</b> Mqtt-Instanz in main.cpp
* <b>O</b>
* <b>///</b> Integration von CProtocolMqtt MqttCommand in main.cpp
* <b>X</b> ReadCommandUart/Mqtt geben keinen Sinn, da automatische Hintergrund-Bearbeitung
  (Receiving und Dispatching) der eingehenden RXData
* <b>O</b> Integration von OOP-Version <b>2109081625_Esp32PubSubClient_02V01</b>,
  speziell (Protocol)Mqtt.h, (Protocol)Mqtt.cpp, Definition.h

<h3>2109081800</h3>

  * Restart Projekt <b>Esp32MultiInterfceDispatcher</b> auf der Grundlage von   <b>2108311537_Esp32MultiInterfaceDispatcher_NoErrorRXBuffer</b>
  * Fertigstellung Version: 2109081625_Esp32PubSubClient_02V01




<!--
<a href="https://pubsubclient.knolleary.net/api">Api PubSubClient</a>
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<h8></h8>
<b></b>
<i></i>
<></>
<></>
-->
