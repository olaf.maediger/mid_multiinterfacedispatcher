<h2>MID - MultiInterfaceDispatcher</h2>

<br><h3>ToDo</h3>

<b>XOO</b> mit Command (neben Command-Response) auch Device-State-FSM<b>s!!!</b> starten und abarbeiten.


<br><h3>2106290430 - Ergänzung</h3>

<b>X</b> 2106291651_Esp32MultiInterfaceDispatcher_00V06_WTR_FSM mit
WaitTimeRelative timems und preemptiver FSM.
<b>X</b> TimeRelative: Klasse für Command WaitTimeRelative überarbeitet.
<b>X</b> 2106291530_Esp32MultiInterfaceDispatcher_00V06beta
Response &<command>, !SC <state>.
<b>X</b> 2106291220_Esp32MultiInterfaceDispatcher_00V06alpha
StateDispatcher $\rarr$ StateCommand, StateEvent von Esp32.
<b>X</b> WriteHelp über Callback in main von DispatcherCommon aufrufen.
<b>X</b> Command-Callback OnResponseBegin/Busy/End definieren.
<b>X</b> 2106290944_Esp32MultiInterfaceDispatcher_00V05_commandsystem
<b>X</b> TimeRelative neu definiert, aber noch nicht verallgemeinert!
<b>X</b> DispatcherSystem


<br><h3>2106280430 - Ergänzung</h3>

<b>O</b>
<b>X</b> DispatcherCommon


<br><h3>2106250430 - Ergänzung</h3>

<b>O</b>
<b>O</b>


<br><h3>2106220430 - Ergänzung</h3>

<b>O</b>
<b>O</b>
<b>X</b> Vorbereitung Esp32MultiInterfaceDispatcher (zuerst mit Uart, dann Bt, Lan und Wlan).


<br><h3>2106060850 - Wiederaufnahme MID nach Auslieferung Glamma</h3>

<b>X</b> Vorbereitung Esp32MultiInterfaceDispatcher (zuerst mit Uart, dann Bt, Lan und Wlan).
<b>X</b> Interface: Abstrakte Methoden ReadLine/WriteLine, Überladen in Bt, Lan, Wlan und Uart.
<b>X</b> UC-Reduktion auf UCLaserAreaScanner.




---



<br><h3>2106060850 - Übersicht</h3>

* Kommunikation zwischen uC(Arduino-Derivat) und PC mit Win10.
* Exe mit C# erstellt unter VS19.
* "Hardware"-Command-Transfer über Uart, Bt, Lan und Wlan.



<br><h3>2106060850 - Gliederung der VS19-Solution</h3>

<img src="./image/2106061011_VSCode.png" alt="" width="80%">

<br><h3>2106050750 - Erstes TopDown-Design</h3>


<br><h3>Definitionen</h3>
MID - MultiInterfaceDispatcher
uC - MicroController
Lan - LocalAreaNetwork
Wlan - WirelessLocalAreaNetwork
Bt - Bluetooth
Uart - UniversialAsynchronousReceiverTransmitter
VS19 - VisualStudio2019

<!--
<h3>2106060850 - </h3>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<b></b>
<img src=".png" alt="">
-->
