//
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//
#include "Defines.h"
#include "Error.h"
#include "Automation.h"
// #include "Utilities.h"
// #include "TimeRelative.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#include "Dispatcher.h"
#if defined(COMMAND_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(COMMAND_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#include "Dispatcher.h"
//-------------------------------
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
//-----------------------------------------------------------
// Segment - Global Variables - Interface
//-----------------------------------------------------------
#if defined(INTERFACE_UART)
CUart UartDispatcher(&Serial); // or: &Serial1
// CUart UartProgram(&Serial1, PIN_UART0_RXD, PIN_UART0_TXD);
#endif
#if defined(INTERFACE_WLAN)
CWlan WlanDispatcher;
#endif
#if defined(INTERFACE_LAN)
CLan LanDispatcher;
#endif
#if defined(INTERFACE_BT)
CBt BtDispatcher;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Dispatcher
//-----------------------------------------------------------
CDispatcher Dispatcher;
#if defined(COMMAND_COMMON)
CDispatcherCommon DispatcherCommon;
#endif
#if defined(COMMAND_SYSTEM)
CDispatcherSystem DispatcherSystem;
#endif

// !!!!!!!!!! CTimeRelative TimeRelativeSystem("TRSM", INIT_MESSAGE_ON);

#if defined(COMMAND_LEDSYSTEM)
CDispatcherLedSystem DispatcherLedSystem;
CLed LedSystem(PIN_LEDSYSTEM, "LEDS", LEDSYSTEM_INVERTED); 
#endif

#if defined(COMMAND_RTC)
CDispatcherRtc DispatcherRtc;
CRTCInternal RTCInternal;
#endif

#if defined(COMMAND_NTP)
CDispatcherNtp DispatcherNtp;
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
#endif

#if defined(COMMAND_WATCHDOG)
CDispatcherWatchDog DispatcherWatchDog;
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif

#if defined(COMMAND_I2CDISPLAY)
CDispatcherI2CDisplay DispatcherI2CDisplay;
C2CDisplay I2CDisplay;
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
CMenu MenuSystem("MS");                       
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Protocol
//-----------------------------------------------------------
CError        Error;
CAutomation   Automation;
//
#if defined(PROTOCOL_SDCARD)
CSDCard SDCard;
// ??? CDispatcherFile  DispatcherFile;
CSDCard SDCard(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
#if defined(PROTOCOL_MQTT)
CMqtt MqttClient;
#endif
//
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
//
//
//#################################################################
//  Global - Setup - Main
//#################################################################
void setup() 
{ 
//####################################################
  Error.Open();
//----------------------------------------------------
// Interface - Uart
//----------------------------------------------------
#if defined(INTERFACE_UART)
  UartDispatcher.Open(115200);
  delay(300);
  UartDispatcher.SetRXEcho(UART_INIT_RXECHO);
  UartDispatcher.WriteLine("\r\n# Serial-Connection: [...]");
#endif
//
//----------------------------------------------------
// Interface - Wlan
//----------------------------------------------------
#if defined(INTERFACE_WLAN)
#endif
//
//----------------------------------------------------
// Interface - Lan
//----------------------------------------------------
#if defined(INTERFACE_LAN)
#endif
//
//----------------------------------------------------
// Interface - Bt
//----------------------------------------------------
#if defined(INTERFACE_BT)
#endif
//####################################################
//----------------------------------------------------
// Protocol - SDCard
//----------------------------------------------------
#if defined(PROTOCOL_SDCARD)
  SerialProgram.WriteLine("# Initializing SDCARD ...");    
  while (SDCard.Open(&SD) < 1)
  {
    SerialProgram.WriteLine(": Initializing SDCARD failed - Insert correct SDCARD[FAT32, initfile]!");
    delay(5000);
  }
  SerialProgram.WriteLine("# Initializing SD card done.");
  //
  Boolean Result = XmlFile.Open(&SD);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Initfile Not Found!");
    return;
  }
  //  
  SerialProgram.Write("# Reading Initfile: ");
  SerialProgram.WriteLine(XML_INITFILE);
  Result = XmlFile.ReadFile(XML_INITFILE);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Reading File!");
    return;
  }
  // SerialProgram.WriteLine("# Content XmlFile:");
  // Serial.println(XmlFile.GetText());
  //
  SerialProgram.WriteLine("# Parsing Initfile");
  Result = XmlFile.Parse();
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Parsing Xml!");
    return;
  }
  //
  SerialProgram.WriteLine("# Result Xml Query:");
  XMLNode* XmlRoot = XmlFile.GetRoot();
  if (!XmlRoot) return;
  //----------------------------------------------------------------------
  XMLElement* XmlWifi = XmlFile.GetElement(XmlRoot, "wifi");
  if (!XmlWifi) return;
  //
  XMLElement* XmlSSID = XmlFile.GetElement(XmlWifi, "ssid");
  if (!XmlSSID) return;
  SerialProgram.Write("# WifiSSID[");
  SerialProgram.Write(XmlSSID->GetText());
  SerialProgram.WriteLine("]");
  WifiSSID = XmlSSID->GetText();
  //
  XMLElement* XmlPASSWORD = XmlFile.GetElement(XmlWifi, "password");
  if (!XmlPASSWORD) return;
  SerialProgram.Write("# WifiPASSWORD[");
  SerialProgram.Write(XmlPASSWORD->GetText());
  SerialProgram.WriteLine("]");
  WifiPASSWORD = XmlPASSWORD->GetText();
  //----------------------------------------------------------------------
  XMLElement* XmlMQTTBroker = XmlFile.GetElement(XmlRoot, "mqttbroker");
  if (!XmlMQTTBroker) return;
  //
  XMLElement* XmlIPAddress = XmlFile.GetElement(XmlMQTTBroker, "ipaddress");
  if (!XmlIPAddress) return;
  SerialProgram.Write("# BrokerIPAddress[");
  SerialProgram.Write(XmlIPAddress->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPAddress = XmlIPAddress->GetText();
  //
  XMLElement* XmlIPPort = XmlFile.GetElement(XmlMQTTBroker, "ipport");
  if (!XmlIPPort) return;
  SerialProgram.Write("# BrokerIPPort[");
  SerialProgram.Write(XmlIPPort->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPPort = atoi(XmlIPPort->GetText());
  //----------------------------------------------------------------------
  // !!! Free Ram !!!
  XmlFile.Close();
  //----------------------------------------------------------------------
  // !!! !!! !!! !!! !!! !!!
  DispatcherFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!DispatcherFile.ParseFile(INIT_COMMANDFILE))
  {
    SerialProgram.WriteLine(": Error: Cannot Read Dispatcherfile!");
    return;
  }
  SerialProgram.Write("# DispatcherFile[");
  SerialProgram.Write(INIT_COMMANDFILE);
  SerialProgram.WriteLine("]:");
  bool DispatcherLoop = true;
  while (DispatcherLoop)
  {
    String Dispatcher = DispatcherFile.GetDispatcher();
    DispatcherLoop = (0 < Dispatcher.length());
    if (DispatcherLoop)
    {
      SerialProgram.Write("# Dispatcher[");
      SerialProgram.Write(Dispatcher.c_str());
      SerialProgram.WriteLine("]");
    }
  }
  DispatcherFile.Close();  
#endif
//
//----------------------------------------------------
// Protocol - Mqtt
//----------------------------------------------------
#if defined(PROTOCOL_MQTT)
  MQTTClient.Initialise(WifiSSID.c_str(), WifiPASSWORD.c_str());
  MQTTClient.Open(MQTTBrokerIPAddress.c_str(), MQTTBrokerIPPort); 
#endif
//####################################################
//----------------------------------------------------
// Dispatcher - Common
//----------------------------------------------------
  Dispatcher.Open();
#if defined(COMMAND_COMMON)
  DispatcherCommon.Open();
#endif
#if defined(COMMAND_SYSTEM)
  DispatcherSystem.Open();
#endif
//
//----------------------------------------------------
// Dispatcher - System
//----------------------------------------------------
//
//----------------------------------------------------
// Dispatcher - LedSystem
//----------------------------------------------------
#if defined(COMMAND_LEDSYSTEM)
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(100);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff(); 
#endif
//
//----------------------------------------------------
// Dispatcher - RTC
//----------------------------------------------------
#if defined(COMMAND_RTC)
  TimeRelativeSystem.Open();
  RTCInternal.Open();
#endif
//
//----------------------------------------------------
// Dispatcher - NTP
//----------------------------------------------------
#if defined(COMMAND_NTP)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  TimeAbsoluteSystem.Open();  
#endif
//
//----------------------------------------------------
// Dispatcher - WatchDog
//----------------------------------------------------
#if defined(COMMAND_WATCHDOG)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
//
//----------------------------------------------------
// Dispatcher - I2CDisplay
//----------------------------------------------------
#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  MenuSystem.Open();
#endif
  //
  //##############################################################
  // Init Application...
  //##############################################################
#if defined(COMMAND_COMMON)
  DispatcherCommon.WriteProgramHeader();
  // DispatcherCommon.WriteHelp();
#endif
#if defined(COMMAND_SYSTEM)
#endif
  //
  Automation.Open();

  // debug:
  pinMode(2, OUTPUT);
}  
//
//#################################################################
//  Global - Loop - Main
//#################################################################
void loop()
{
  digitalWrite(2, HIGH);
  delay(1);
  digitalWrite(2, LOW);
  delay(50);
  Error.Handle();

  Dispatcher.AnalyseInterfaceBlock();
#if defined(COMMAND_COMMON)
  // !!! DispatcherCommon.HandleInterface();
  // !!! Error.Handle();
#endif
#if defined(COMMAND_SYSTEM)
  DispatcherSystem.HandleInterface();
  Error.Handle();
#endif
#if defined(COMMAND_LEDSYSTEM)
  Error.Handle();
#endif
#if defined(COMMAND_RTC)
  Error.Handle();
#endif
#if defined(COMMAND_NTP)
  Error.Handle();
#endif
#if defined(COMMAND_WATCHDOG)
  Error.Handle();
#endif
#if defined(COMMAND_I2CDISPLAY)
  Error.Handle();
#endif
// // #if defined(PROTOCOL_SDCARD)
// //   if (ecNone == Error.GetCode())
// //   {
// //     SDCard.Handle();
// //   }
// // #endif
// // #if defined(PROTOCOL_MQTT)
// //   if (ecNone == Error.GetCode())
// //   {
// //     Mqtt.Handle();
// //   }
// // #endif
  if (ecNone == Error.GetCode())
  {
    // Automation.Handle(); 
  }
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/* --> Automation:

  SerialProgram, MQTTClient);
// #if defined(COMMAND_SDCARD)
//   Dispatcher.Handle(SerialProgram, SDCard);
// #endif // SDCARD_ISPLUGGED
//   Dispatcher.Handle(SerialProgram);
//   
// #endif // COMMAND_UART
//   // Preemptive Multitasking:
//   //------------------------------------------------------------------------
//   // NC TimeRelativeSystemus.Wait_Execute();
// #if defined(COMMAND_NTP)  
//   TimeRelativeSystem.Wait_Execute();
//   TimeAbsoluteSystem.Wait_Execute();
// #endif
// #if defined(COMMAND_I2CDISPLAY)
//   MenuSystem.Display_Execute();
// #endif
//   //
//   // ???? LedSystem.Blink_Execute(GlobalBuffer);  
*/


