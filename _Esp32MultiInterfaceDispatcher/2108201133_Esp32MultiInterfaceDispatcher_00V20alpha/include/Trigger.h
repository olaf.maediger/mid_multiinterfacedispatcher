//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef Trigger_h
#define Trigger_h
//
#include "TimeSpan.h"
//
//--------------------------------
//  Trigger - Constant
//--------------------------------
const int TRIGGER_SIZE_ID                               = 4;
const int TRIGGER_SIZE_BUFFER                           = 64;
//
//--------------------------------
//  Type - TriggerBase
//--------------------------------
enum ETriggerLevel
{
  tlUndefined = -1,
  tlPassive = 0,
  tlActive = 1
};
//
class CTriggerBase
{
  protected:
  char FID[TRIGGER_SIZE_ID];
  char FBuffer[TRIGGER_SIZE_BUFFER];
  int FPin;
  bool FInverted;
  ETriggerLevel FLevel;
  //  
  public:
  CTriggerBase(const char* pid, int pin,
               bool inverted = false);
  //
  ETriggerLevel virtual GetLevel(void);
  void virtual SetLevel(ETriggerLevel level);
  //
  Boolean virtual Open(void) = 0;
  Boolean virtual Close(void) = 0;
  //
  void virtual Execute(void) = 0;
};
//
//--------------------------------
//  Type - TriggerOut
//--------------------------------
//
const char* const TRIGGEROUT_LEVEL_ACTIVE               = "TriggerOut[%s] Level Active";
const char* const TRIGGEROUT_LEVEL_PASSIVE              = "TriggerOut[%s] Level Passive";
//
const char* const TRIGGEROUT_STATE_MASK                 = "TriggerOut[%s] State %s";
const char* const TRIGGEROUT_STATE_UNDEFINED            = "Undefined";
const char* const TRIGGEROUT_STATE_ACTIVE               = "Active";
const char* const TRIGGEROUT_STATE_PASSIVE              = "Passive";
const char* const TRIGGEROUT_STATE_PULSEACTIVE          = "PulseActive";
const char* const TRIGGEROUT_STATE_PULSEPASSIVE         = "PulsePassive";
const char* const TRIGGEROUT_STATE_SEQUENCEACTIVE       = "SequenceActive";
const char* const TRIGGEROUT_STATE_SEQUENCEPASSIVE      = "SequencePassive";
//
class CTriggerOut;
typedef void (*DOnEventTriggerOut)(CTriggerOut* ptrigger, const char* pevent);
//
const char* const STATE_TRIGGEROUT[] = {TRIGGEROUT_STATE_UNDEFINED, 
                                        TRIGGEROUT_STATE_ACTIVE, 
                                        TRIGGEROUT_STATE_PASSIVE,
                                        TRIGGEROUT_STATE_PULSEACTIVE, 
                                        TRIGGEROUT_STATE_PULSEPASSIVE,
                                        TRIGGEROUT_STATE_SEQUENCEACTIVE, 
                                        TRIGGEROUT_STATE_SEQUENCEPASSIVE};
//
enum EStateTriggerOut
{
  stoUndefined = 0,
  stoActive = 1,              // ->A
  stoPassive = 2,             // ->P
  stoPulseActive = 3,         // P->A->P
  stoPulsePassive = 4,        // A->P->A
  stoSequenceActive = 5,      // P->A-> .. P->A->P
  stoSequencePassive = 6      // A->P-> .. A->P->A
};
class CTriggerOut : public CTriggerBase
{
  protected:
  EStateTriggerOut FState;
  DOnEventTriggerOut FOnEvent;
  UInt32 FPulsePeriod;
  UInt32 FPulseWidth;
  UInt32 FPulseCount;
  UInt32 FPulseIndex;
  CTimeSpan* FPTimePeriod;
  CTimeSpan* FPTimeWidth;
  //  
  public:
  CTriggerOut(const char* pid, 
              int pin,
              DOnEventTriggerOut ponevent,
              bool inverted = false);
  //
  void SetState(EStateTriggerOut state);
  //
  void WriteLevel(ETriggerLevel level);
  ETriggerLevel GetLevel(void);
  void SetLevel(ETriggerLevel level);
  //
  bool Open(void);
  bool Close(void);
  //
  bool SetActive(void);
  bool SetPassive(void);
  bool PulseActive(UInt32 period);
  bool PulsePassive(UInt32 period);
  bool SequenceActive(UInt32 period, UInt32 width, UInt32 count);
  bool SequencePassive(UInt32 period, UInt32 width, UInt32 count);
  //
  void Execute(void);
};
//
//--------------------------------
//  Type - TriggerIn
//--------------------------------
//
const char* const TRIGGERIN_LEVEL_ACTIVE               = "TriggerIn[%s] Level Active";
const char* const TRIGGERIN_LEVEL_PASSIVE              = "TriggerIn[%s] Level Passive";
//
const char* const TRIGGERIN_STATE_MASK                  = "TriggerIn[%s] State %s";
const char* const TRIGGERIN_STATE_UNDEFINED             = "Undefined";
const char* const TRIGGERIN_STATE_ACTIVE                = "Active";
const char* const TRIGGERIN_STATE_PASSIVE               = "Passive";
const char* const TRIGGERIN_STATE_WAITFORACTIVE         = "WaitForActive";
const char* const TRIGGERIN_STATE_WAITFORPASSIVE        = "WaitForPassive";
const char* const TRIGGERIN_STATE_TIMEOUT               = "TimeOut";
//
class CTriggerIn;
typedef void (*DOnEventTriggerIn)(CTriggerIn* ptrigger, const char* pevent);
//
const char* const STATE_TRIGGERIN[] = {TRIGGERIN_STATE_UNDEFINED, 
                                       TRIGGERIN_STATE_ACTIVE, 
                                       TRIGGERIN_STATE_PASSIVE,
                                       TRIGGERIN_STATE_WAITFORACTIVE, 
                                       TRIGGERIN_STATE_WAITFORPASSIVE,
                                       TRIGGERIN_STATE_TIMEOUT};
//
enum EStateTriggerIn
{
  stiUndefined = 0,
  stiActive = 1,
  stiPassive = 2,
  stiWaitForActive = 3,   // IsPassive
  stiWaitForPassive = 4,  // IsActive
  stiWaitTimeOut = 5
};
//
class CTriggerIn : public CTriggerBase
{
  protected:
  EStateTriggerIn FState;
  CTimeSpan* FPTimeOut;
  DOnEventTriggerIn FOnEvent;
  //  
  public:
  CTriggerIn(const char* pid, int pin,
             DOnEventTriggerIn ponevent,             
             bool inverted = false);
  //
  void SetState(EStateTriggerIn state);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  ETriggerLevel ReadLevel(void);
  ETriggerLevel GetLevel(void);
  void SetLevel(ETriggerLevel level);
  //
  bool WaitForActive(long unsigned timeout);
  bool WaitForPassive(long unsigned timeout);
  //
  void Execute(void);
};
//
#endif // Trigger_h
//
#endif // DISPATCHER_TRIGGER
//
