//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "Trigger.h"

#include "InterfaceUart.h"

extern CUart UartCommand;
//
//############################################################
//  TriggerBase
//############################################################
CTriggerBase::CTriggerBase(const char* pid, int pin,
                           bool inverted)
{
  strcpy(FID, pid);
  FPin = pin;
  FInverted = inverted;
  FLevel = tlUndefined;
}
ETriggerLevel CTriggerBase::GetLevel(void)
{
  return FLevel;
}
void CTriggerBase::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
//############################################################
//  TriggerOut
//############################################################
void OnEventTimePeriod(CTimeSpan* ptimespan, const char* pevent)
{
  // debug Serial.print("OETP:");
  // debug Serial.println(pevent);
}
void OnEventTimeWidth(CTimeSpan* ptimespan, const char* pevent)
{
  // debug Serial.print("OETW:");
  // debug Serial.println(pevent);  
}
//
CTriggerOut::CTriggerOut(const char* pid, int pin,
                         DOnEventTriggerOut ponevent,
                         bool inverted)
  : CTriggerBase(pid, pin, inverted)
{ 
  FState = stoUndefined;
  FOnEvent = ponevent;
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
  FPulseIndex = 0;
  FPTimePeriod = new CTimeSpan("TOTP", OnEventTimePeriod);
  FPTimeWidth = new CTimeSpan("TOTW", OnEventTimeWidth);
}
//
void CTriggerOut::WriteLevel(ETriggerLevel level)
{
  if (FInverted)
  { // Inverted:
    if (level != FLevel)
    {
      FLevel = level;
      if (tlActive == level)
      {
        digitalWrite(FPin, LOW);
        if (FOnEvent) 
        {
          sprintf(FBuffer, TRIGGEROUT_LEVEL_ACTIVE, FID);
          FOnEvent(this, FBuffer);
        }
      }
      else
      {
        digitalWrite(FPin, HIGH);
        if (FOnEvent) 
        {
          sprintf(FBuffer, TRIGGEROUT_LEVEL_PASSIVE, FID);
          FOnEvent(this, FBuffer);
        }
      }
    }
  }
  else 
  { // not Inverted:
    if (level != FLevel)
    {
      FLevel = level;
      if (tlActive == level)
      {
        digitalWrite(FPin, HIGH);
        if (FOnEvent) 
        {
          sprintf(FBuffer, TRIGGEROUT_LEVEL_ACTIVE, FID);
          FOnEvent(this, FBuffer);
        }
      }
      else
      {
        digitalWrite(FPin, LOW);
        if (FOnEvent) 
        {
          sprintf(FBuffer, TRIGGEROUT_LEVEL_PASSIVE, FID);
          FOnEvent(this, FBuffer);
        }
      }    
    }
  }
}  
//
ETriggerLevel CTriggerOut::GetLevel(void)
{
  return FLevel;
}
void CTriggerOut::SetLevel(ETriggerLevel level)
{
  CTriggerBase::SetLevel(level);
}
//
void CTriggerOut::SetState(EStateTriggerOut state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent) 
    {
      sprintf(FBuffer, TRIGGEROUT_STATE_MASK, FID, STATE_TRIGGEROUT[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
Boolean CTriggerOut::Open()
{
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
  FPulseIndex = 0;
  pinMode(FPin, OUTPUT);
  WriteLevel(tlPassive);
  return true;
}
Boolean CTriggerOut::Close()
{
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
  FPulseIndex = 0;
  WriteLevel(tlPassive);
  pinMode(FPin, INPUT);
  return true;
}
//--------------------------------------------------
bool CTriggerOut::SetActive(void)
{  
  SetState(stoActive);
  WriteLevel(tlActive);
  return true;
}
bool CTriggerOut::SetPassive(void)
{  
  SetState(stoPassive);
  WriteLevel(tlPassive);
  return true;
}
//
bool CTriggerOut::PulseActive(UInt32 pulseperiod)
{  
  WriteLevel(tlPassive);
  FPulsePeriod = pulseperiod;
  SetState(stoPulseActive);
  WriteLevel(tlActive);
  FPTimePeriod->StartPeriod(FPulsePeriod);  
  return true;
}
//
bool CTriggerOut::PulsePassive(UInt32 pulseperiod)
{
  WriteLevel(tlActive);
  FPulsePeriod = pulseperiod;
  SetState(stoPulsePassive);
  WriteLevel(tlPassive);
  FPTimePeriod->StartPeriod(FPulsePeriod);  
  return true;
}
//
bool CTriggerOut::SequenceActive(UInt32 period, UInt32 width, UInt32 count)
{
  WriteLevel(tlPassive);
  FPulsePeriod = period;
  FPulseWidth = width;
  FPulseCount = count;
  FPulseIndex = 0;
  SetState(stoSequenceActive);
  WriteLevel(tlActive);
  FPTimePeriod->StartPeriod(FPulsePeriod);  
  FPTimeWidth->StartPeriod(FPulseWidth);  
  return true;
}
bool CTriggerOut::SequencePassive(UInt32 period, UInt32 width, UInt32 count)
{
  WriteLevel(tlActive);
  FPulsePeriod = period;
  FPulseWidth = width;
  FPulseCount = count;
  FPulseIndex = 0;
  SetState(stoSequencePassive);
  WriteLevel(tlPassive);
  FPTimePeriod->StartPeriod(FPulsePeriod);  
  FPTimeWidth->StartPeriod(FPulseWidth);  
  return true;
}
//
//------------------------------------------------------
//  TriggerOut - Execute
//------------------------------------------------------
void CTriggerOut::Execute(void)
{  
  FPTimePeriod->Execute();
  FPTimeWidth->Execute();
  switch (FState)
  {
    case stoActive:
      break;
    case stoPassive:
      break;
    case stoPulseActive:
      if (FPTimePeriod->IsIdle())
      {
        WriteLevel(tlPassive);
        SetState(stoPassive);
      }
      break;
    case stoPulsePassive:
      if (FPTimePeriod->IsIdle())
      {
        WriteLevel(tlActive);
        SetState(stoActive);
      }
      break;
    case stoSequenceActive:      
      if (FPTimeWidth->IsIdle())
      {
        WriteLevel(tlPassive);
      }
      if (FPTimePeriod->IsIdle())
      {
        FPulseIndex++;
        if (FPulseIndex < FPulseCount)
        {
          WriteLevel(tlActive);
          FPTimePeriod->StartPeriod(FPulsePeriod);  
          FPTimeWidth->StartPeriod(FPulseWidth);            
        }
        else
        {
          WriteLevel(tlPassive);
          SetState(stoPassive);
        }
      }
      break;
    case stoSequencePassive:
      if (FPTimeWidth->IsIdle())
      {
        WriteLevel(tlActive);
      }
      if (FPTimePeriod->IsIdle())
      {
        FPulseIndex++;
        if (FPulseIndex < FPulseCount)
        {
          WriteLevel(tlPassive);
          FPTimePeriod->StartPeriod(FPulsePeriod);  
          FPTimeWidth->StartPeriod(FPulseWidth);            
        }
        else
        {
          WriteLevel(tlActive);
          SetState(stoActive);
        }
      }
      break;
    default:
      SetState(stoUndefined);
      break;
  }
}
//
//############################################################
//  TriggerIn
//############################################################

void OnEventTimeOut(CTimeSpan* ptimespan, const char* pevent)
{
  // !!!!! Serial.print("OETO:");
  // !!!!! Serial.println(pevent);
}

CTriggerIn::CTriggerIn(const char* pid, int pin, 
                       DOnEventTriggerIn ponevent,
                       bool inverted)
  : CTriggerBase(pid, pin, inverted)
{
  FOnEvent = 0;
  FState = stiUndefined;
  FOnEvent = ponevent;
  FPTimeOut = new CTimeSpan("TOTO", OnEventTimeOut);
}
//
ETriggerLevel CTriggerIn::ReadLevel(void)
{
  ETriggerLevel TLR = tlPassive;
  if (FInverted)
  {
    if (0 < digitalRead(FPin)) 
    {
      TLR = tlPassive;
    }
    if (TLR != FLevel)
    {
      FLevel = TLR;
      if (FOnEvent) 
      {
        if (tlActive == FLevel)
        {
          sprintf(FBuffer, TRIGGERIN_LEVEL_ACTIVE, FID);
        }
        else
        {
          sprintf(FBuffer, TRIGGERIN_LEVEL_PASSIVE, FID);
        }
        FOnEvent(this, FBuffer);
      }
    }
    return FLevel;
  } // not Inverted:
  if (0 < digitalRead(FPin))
  {
    TLR = tlActive;
  }
  if (TLR != FLevel)
  {
    FLevel = TLR;
    if (FOnEvent) 
    {
      if (tlActive == FLevel)
      {
        sprintf(FBuffer, TRIGGERIN_LEVEL_ACTIVE, FID);
      }
      else
      {
        sprintf(FBuffer, TRIGGERIN_LEVEL_PASSIVE, FID);
      }
      FOnEvent(this, FBuffer);
    }
  }
  return FLevel;
}
//
ETriggerLevel CTriggerIn::GetLevel(void)
{
  return FLevel;
}
void CTriggerIn::SetLevel(ETriggerLevel level)
{
  CTriggerBase::SetLevel(level);
}
//
void CTriggerIn::SetState(EStateTriggerIn state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGERIN_STATE_MASK, FID, STATE_TRIGGERIN[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
Boolean CTriggerIn::Open()
{
  pinMode(FPin, INPUT);
  SetState(stiPassive);
  return true;
}
Boolean CTriggerIn::Close()
{
  pinMode(FPin, INPUT);
  SetLevel(tlPassive);
  SetState(stiPassive);
  return true;
}
//
bool CTriggerIn::WaitForActive(long unsigned timeout)
{
  if ((stiPassive == FState) || (stiActive == FState))
  {
    FPTimeOut->StartPeriod(timeout);
    SetState(stiWaitForActive);
    return true;
  }
  return false;
}
bool CTriggerIn::WaitForPassive(long unsigned timeout)
{
  if ((stiPassive == FState) || (stiActive == FState))
  {
    FPTimeOut->StartPeriod(timeout);
    SetState(stiWaitForPassive);
    return true;
  }
  return false;
}
//
//------------------------------------------------------
//  TriggerIn - Execute
//------------------------------------------------------
void CTriggerIn::Execute(void)
{
  switch (FState)
  {
    case stiActive:
      if (tlPassive == ReadLevel())
      {
        SetState(stiPassive);
      }
      break;
    case stiPassive:
      if (tlActive == ReadLevel())
      {
        SetState(stiActive);
      }
      break;
    case stiWaitForActive:
      if (tlActive == ReadLevel())
      {
        FPTimeOut->Abort();
        SetState(stiActive);
      }
      break;
    case stiWaitForPassive:
      if (tlPassive == ReadLevel())
      {
        FPTimeOut->Abort();
        SetState(stiPassive);
      }
      break;
    case stiWaitTimeOut:
      break;
    default: // stiUndefined
      break;
  }
}
//
#endif // DISPATCHER_TRIGGER
//
//####################################################################
//####################################################################
//####################################################################


















/*




void CTriggerOut::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//





    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stoActive);
      }
      else
        if (FTimeWait <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stoActive);
          }
          else
          {
            SetState(stoPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;



//
void CTrigger::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
bool CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
  return true;
}
//
bool CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
  return true;
}
//
bool CTrigger::PulseActive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlPassive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulseActive);
      return true;
    default:
      return false;
  }
}
//
bool CTrigger::PulsePassive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlActive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulsePassive);
      return true;
    default:
      return false;
  }
  return false;
}
//

void CTriggerBase::SetState(EStateTrigger state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, STATE_TRIGGER[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}
*/



/*
Boolean override CTriggerBase::Open()
{
  base.Open();
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  SetLevel(tlPassive);
  return true;
}

Boolean CTriggerBase::Close()
{
  SetPassive();
  SetLevel(tlPassive);
  pinMode(FPin, INPUT);
  return true;
}




//
void CTriggerIn::Execute(void)
{  
  switch (FState)
  {
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    default:
      SetState(stiUndefined);
  }
}


Boolean CTrigger::Open()
{
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  SetLevel(tlPassive);
  SetState(stPassive);
  return true;
}

Boolean CTrigger::Close()
{
  SetPassive();
  SetLevel(tlPassive);
  pinMode(FPin, INPUT);
  SetState(stPassive);
  return true;
}



ool CTrigger::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
//
bool CTrigger::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}

//
void CTrigger::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
bool CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
  return true;
}
//
bool CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
  return true;
}
//
bool CTrigger::PulseActive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlPassive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulseActive);
      return true;
    default:
      return false;
  }
}
//
bool CTrigger::PulsePassive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlActive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulsePassive);
      return true;
    default:
      return false;
  }
  return false;
}
//
bool CTrigger::PulseSequence(UInt32 period, UInt32 count)
{

}
bool CTrigger::PulseSequence(UInt32 period, UInt32 width, UInt32 count);
{

}
//
void CTrigger::Execute(void)
{  
  switch (FState)
  {
    case stActive:
      if ((tdInput == FDirection) && (tlPassive == GetLevel()))
      {
        SetState(stPassive);
      }
      break;
    case stPassive:
      if ((tdInput == FDirection) && (tlActive == GetLevel()))
      {
        SetState(stActive);
      }
      break;
    case stPulseActive:
      if (FTimeOut <= millis())
      {
        SetPassive();
      }
      break;
    case stPulsePassive:
      if (FTimeOut <= millis())
      {
        SetActive();
      }
      break;
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    default:
      SetState(stUndefined);
  }
}

*/

/*
ool CTriggerOut::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
//
bool CTriggerOut::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}



ETriggerLevel CTriggerOut::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

*/