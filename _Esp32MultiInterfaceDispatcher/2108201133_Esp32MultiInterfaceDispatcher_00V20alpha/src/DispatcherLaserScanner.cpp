//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#include "DispatcherLaserScanner.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
extern CError Error;
extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
// !!!!!!!!!!!!!! extern CLaserScanner LaserScanner;
//
//#########################################################
//  Dispatcher - LaserScanner - Constructor
//#########################################################
CDispatcherLaserScanner::CDispatcherLaserScanner(void)
{
}
//
//#########################################################
//  Dispatcher - LaserScanner - Execution
//#########################################################
bool CDispatcherLaserScanner::ExecutePulseLaserPosition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x> <y> <p> <c> 
  if (parametercount < 4)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PX = atoi(parameters[0]);
  int PY = atoi(parameters[1]);
  int PP = atoi(parameters[2]);
  int PC = atoi(parameters[3]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i %i", command, PX, PY, PP, PC);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // !!!!!!!!!!!!!! LaserScanner.PulseLaserPosition(PX, PY, PP, PC);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserScanner::ExecuteAbortLaserPosition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserScanner::ExecutePulseLaserRow(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <p> <c>
  if (parametercount < 2)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PP = atoi(parameters[0]);
  int PC = atoi(parameters[1]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i", command, PP, PC);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserScanner::ExecuteAbortLaserRow(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserScanner::ExecutePulseLaserColumn(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <p> <c>
  if (parametercount < 2)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PP = atoi(parameters[0]);
  int PC = atoi(parameters[1]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i", command, PP, PC);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserScanner::ExecuteAbortLaserColumn(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - LaserScanner - Handler
//#########################################################
bool CDispatcherLaserScanner::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_PLP, command))
  {
    return ExecutePulseLaserPosition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALP, command))
  {
    return ExecuteAbortLaserPosition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PLR, command))
  {
    return ExecutePulseLaserRow(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALR, command))
  {
    return ExecuteAbortLaserRow(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PLC, command))
  {
    return ExecutePulseLaserColumn(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALC, command))
  {
    return ExecuteAbortLaserColumn(command, parametercount, parameters);
  }
  return false;
}
//
#endif // DISPATCHER_LASERSCANNER
//
