// //
#include "DefinitionSystem.h"
//
#include "Error.h"
#include "Dispatcher.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
extern CError     Error;
//
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CInterfaceBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanCommand;
#endif
//
//#########################################################
//  Dispatcher - Constructor
//#########################################################
CDispatcher::CDispatcher(void)
{
}
CDispatcher::~CDispatcher(void)
{
}
//
//#########################################################
//  Dispatcher - Property
//#########################################################
//
//#########################################################
//  Dispatcher - Handler
//#########################################################
bool CDispatcher::Open(void)
{
  return true;
}
bool CDispatcher::Close(void)
{
  return true;
}
//
void CDispatcher::WriteEvent(char* line)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteEvent(line);
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MQTTClient.WriteEvent(line);
#endif
#if defined(PROTOCOL_SDCARD)
  SDCard.WriteEvent(line);
#endif 
#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.WriteEvent(line);
#endif
}
//
void CDispatcher::WriteResponse(char* line)
{
#if defined(INTERFACE_UART)
// !!!!!!!!!!!!!!!   UartCommand.WriteResponse(line);
#endif

#if defined(INTERFACE_BT)
#endif

#if defined(INTERFACE_WLAN)
#endif

#if defined(INTERFACE_LAN)
#endif

#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!  MQTTClient.WriteResponse(line);
#endif

#if defined(PROTOCOL_SDCARD)
  SDCard.WriteAnswer(line);
#endif 

#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.WriteAnswer(line);
#endif
}
//
void CDispatcher::WriteComment(char* line)
{
#ifdef INTERFACE_UART
// !!!!!!!!!!!!!!!   UartCommand.WriteComment(line);
// !!!!!!!!!!!!!!!   UartCommand.WritePrompt();
#endif
#if defined(PROTOCOL_MQTT)
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MQTTClient.WriteComment(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteComment(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteComment(line);
//#endif 
}
//
void CDispatcher::WriteComment(char* mask, char* line)
{
  //!!!!!!!!!!!!!!!!!!sprintf(GetTxdBuffer(), mask.c_str(), line.c_str());
  //!!!!!!!!!!!!!!!!!!!!WriteComment(GetTxdBuffer());
}

//
//##################################################################
// Dispatcher - Analysis & Execution
//##################################################################
Boolean CDispatcher::ExecuteBegin(void)
{
#if defined(INTERFACE_UART)
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
  return true;
}

Boolean CDispatcher::ExecuteResponse(char* response)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteResponse(response);
#endif
#if defined(INTERFACE_BT)
  BtCommand.WriteResponse(response);
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_MQTT)
  MqttCommand.WriteResponse(response);
#endif
  return true;
}

Boolean CDispatcher::ExecuteEnd(void)
{
#if defined(INTERFACE_UART)
  return true;
#endif
#if defined(INTERFACE_BT)
  return true;
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_MQTT)
  return true;
#endif
}
// //
// //##################################################################
// // Dispatcher - Write - Helper
// //##################################################################
// #if defined(COMMAND_COMMON)
// bool CDispatcher::WriteProgramHeader(void)
// {
// #if defined(INTERFACE_UART)
//   // //  UartCommand.WriteComment();
//   // UartCommand.WriteLine(); UartCommand.WriteComment();
//   // UartCommand.WriteLine(TITLE_LINE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PROJECT, ARGUMENT_PROJECT);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_HARDWARE, ARGUMENT_HARDWARE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_DATE, ARGUMENT_DATE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_TIME, ARGUMENT_TIME);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_AUTHOR, ARGUMENT_AUTHOR);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PORT, ARGUMENT_PORT);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PARAMETER, ARGUMENT_PARAMETER);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(TITLE_LINE);
//   // UartCommand.WriteComment();
//   // // UartCommand.WriteNewLine();
//   // // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_ENDLINE);
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
//   MQTTClient.WriteComment(TITLE_LINE);
//   MQTTClient.WriteComment(MASK_PROJECT, ARGUMENT_PROJECT);
//   MQTTClient.WriteComment(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
//   MQTTClient.WriteComment(MASK_HARDWARE, ARGUMENT_HARDWARE);
//   MQTTClient.WriteComment(MASK_DATE, ARGUMENT_DATE);
//   MQTTClient.WriteComment(MASK_TIME, ARGUMENT_TIME);
//   MQTTClient.WriteComment(MASK_AUTHOR, ARGUMENT_AUTHOR);
//   MQTTClient.WriteComment(MASK_PORT, ARGUMENT_PORT);
//   MQTTClient.WriteComment(MASK_PARAMETER, ARGUMENT_PARAMETER);
//   MQTTClient.WriteComment(TITLE_LINE);
//   MQTTClient.WriteComment(MASK_ENDLINE);
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
//   return true;
// }
// #endif // COMMAND_COMMON
// //
// #if defined(COMMAND_COMMON)
// void CDispatcher::WriteHardwareVersion(void)
// {
// #if defined(INTERFACE_UART)
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
// }
// #endif // COMMAND_COMMON
// //   sprintf(GetTxdBuffer(), MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// //   serial.WriteComment(GetTxdBuffer());
// // #endif
// // //######################################################
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteComment(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// // #endif
// //
// #if defined(COMMAND_COMMON)
// void CDispatcher::WriteSoftwareVersion(void)
// {
// #if defined(INTERFACE_UART)
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
// }
// #endif // COMMAND_COMMON
// //   sprintf(GetTxdBuffer(), MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// //   serial.WriteComment(GetTxdBuffer());
// // #endif
// // //######################################################
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteComment(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// // #endif
// // }
// // 
// //
// #if defined(COMMAND_COMMON)
// bool CDispatcher::WriteHelp(void)
// {
// #if defined(INTERFACE_UART) 
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
//   return false;
// }
// #endif // COMMAND_COMMON


// bool CDispatcher::Execute(void)
// { 
// #if defined(COMMAND_COMMON)  
//   if (!strcmp(SHORT_GPH, GetPDispatcher()))
//   {
//     return ExecuteGetProgramHeader();
//   }
//   if (!strcmp(SHORT_GHV, GetPDispatcher()))
//   {
//     return ExecuteGetHardwareVersion();
//   }
//   if (!strcmp(SHORT_GSV, GetPDispatcher()))
//   {
//     return ExecuteGetSoftwareVersion();
//   }
//   if (!strcmp(SHORT_H, GetPDispatcher()))
//   {
//     return ExecuteGetHelp();
//   }
// #endif  
//   return false;
// }

// bool CDispatcher::HandleInterface(void)
// {
//   if (AnalyseInterfaceBlock())
//   {
//     return Execute();
//   }
//   return false;
// }

// #if defined(SDCARD_ISPLUGGED)
// bool CDispatcher::DetectSDCardLine(CSDCard &sdcard)
// {
//   if (scIdle == GetState())
//   { // ready for next Dispatcher
//     if (DispatcherFile.IsExecuting())
//     { // more Dispatchers exist and no Wait-Dispatcher:
//       if (TimeRelativeSystem.Wait_Execute()) return false;
// #if defined(NTPCLIENT_ISPLUGGED)
//       if (TimeAbsoluteSystem.Wait_Execute()) return false;
// #endif      
// #if defined(TRIGGERINPUT_ISPLUGGED)
//       if (TriggerInputSystem.Wait_Execute()) return false;
// #endif
// #if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
//       if (MotorPositionReached.Wait_Execute()) return false;
// #endif  
//       String Dispatcher = DispatcherFile.GetDispatcher();
//       if (0 < Dispatcher.length())
//       { // copy DispatcherFile-Line (next Dispatcher) to DispatcherBuffer:
//         strcpy(FDispatcherText, Dispatcher.c_str());
//         return true;
//       }
//     }
//   }
//   return false;
// }
// #endif
/*
ecf /sdc.cmd
*/

