#include "DefinitionSystem.h"
//
#include "Error.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(INTERFACE_BT)
//!!!!!!!!!!!#include "InterfaceBt.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(COMMAND_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#include "Dispatcher.h"
//-------------------------------
//
//
//--------------------------------
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanDispatcher;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanDispatcher;
#endif
#if defined(INTERFACE_BT)
//!!!!!!!!!!!!!!!!!!!!extern CInterfaceBt BtCommand;
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
extern CSDCard SDCard;
#endif
#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!extern CMqtt Mqtt;
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
extern CDispatcherCommon DispatcherCommon;
#endif
#if defined(COMMAND_SYSTEM)
//extern CDispatcherSystem DispatcherSystem;
#endif
#if defined(COMMAND_LEDSYSTEM)
extern CLed LedSystem;
#endif
#if defined(COMMAND_RTC)
extern CRtc RtcLocal;
#endif
#if defined(COMMAND_NTP)
extern CNtp NtpClient;
#endif
#if defined(COMMAND_WATCHDOG)
extern CWatchDog WatchDog;
#endif
#if defined(COMMAND_I2CDISPLAY)
extern 
#endif
//-------------------------------
//
CError::CError(void)
{
  SetCode(INIT_ERRORCODE);
}

Boolean CError::IsActive(void)
{
  return (ecNone != FCode);
}

EErrorCode CError::GetCode(void)
{
  return FCode;  
}
void CError::SetCode(EErrorCode code)
{
  FCode = code;
}
  
Boolean CError::Open(void)
{
  FCode = ecNone;
  return true; 
}
  
Boolean CError::Close(void)
{
  FCode = ecNone;
  return true; 
}

const char* CError::ErrorCodeText(EErrorCode code)
{
  switch (code)
  {
    case ecNone:
      return ERROR_NONE;
    case ecUnknownCommand:
      return ERROR_UNKNOWNCOMMAND;
    case ecInvalidParameter:
      return ERROR_INVALIDPARAMETER;
    case ecMissingParameter:
      return ERROR_MISSINGPARAMETER;
    case ecToManyParameters:
      return ERROR_TOMANYPARAMETERS;
    case ecNotEnoughParameters:
      return ERROR_NOTENOUGHPARAMETERS;
    case ecTimingFailure:
      return ERROR_TIMINGFAILURE;
    case ecMountSDCard:
      return ERROR_MOUNTSDCARD;
    case ecOpenCommandFile:
      return ERROR_OPENCOMMANDFILE;
    case ecParseCommandFile:
      return ERROR_PARSECOMMANDFILE;
    case ecWriteCommandFile:
      return ERROR_WRITECOMMANDFILE;
    case ecCloseCommandFile:
      return ERROR_CLOSECOMMANDFILE;
    case ecUnmountSDCard:
      return ERROR_UNMOUNTSDCARD;
    case ecFileNotOpened:
      return ERROR_FILENOTOPENED;
    case ecReceiveBufferOverflow:
      return ERROR_RECEIVEBUFFEROVERFLOW;
    case ecCommandFailed:
      return ERROR_COMMANDFAILED;
    case ecUndefinedState:
      return ERROR_UNDEFINEDSTATE;
    default: // ecUnknown:
      return ERROR_UNKNOWN;
  }
}

Boolean CError::Handle(void)
{
  if (IsActive())
  {  
    sprintf(FBuffer, "%sError[%i]: %s!", TERMINAL_ERROR, FCode, ErrorCodeText(FCode));
#ifdef INTERFACE_UART
    UartCommand.WriteText(FBuffer);
    UartCommand.WriteLine();
#endif
#ifdef INTERFACE_BT
#endif
#ifdef INTERFACE_WLAN
#endif
#ifdef INTERFACE_LAN
#endif
#ifdef PROTOCOL_MQTT
//!!!!!!!!!!!!!!!!!!!!!!!!!!    MQTTClient.Publish("/Error", Buffer);
#endif
#ifdef PROTOCOL_SDCARD
#endif
    FCode = ecNone;
    return true;
  }
  else
  { // no Error, only preparing for input:
#ifdef INTERFACE_UART
    // ???? UartCommand.WritePrompt();
#endif
#ifdef INTERFACE_BT
#endif
#ifdef INTERFACE_WLAN
#endif
#ifdef INTERFACE_LAN
#endif
#ifdef PROTOCOL_MQTT
#endif
#ifdef PROTOCOL_SDCARD
    MQTTClient.Publish("/Error", Buffer);
#endif
  }
  return true;
}
