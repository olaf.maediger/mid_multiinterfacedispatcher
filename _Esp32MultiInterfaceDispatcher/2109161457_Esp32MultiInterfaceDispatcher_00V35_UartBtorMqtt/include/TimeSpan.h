//
//--------------------------------
//  Library TimeSpan
//--------------------------------
//
#include "DefinitionSystem.h"
//
// no dispatcher dirctive!
//
#ifndef TimeSpan_h
#define TimeSpan_h
//
#include <Arduino.h>
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const int SIZE_TIMESPAN_ID = 5;
const int SIZE_TIMESPAN_BUFFER = 32;
const Float32 INIT_TIMESPAN_MS = 5000.0f;  // [ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
class CTimeSpan;
typedef void (*DOnEventTimeSpan)(CTimeSpan* timespan, const char* event);
//
enum EStateTimeSpan
{
  stsUnknown = 0,
  stsIdle = 1,
  stsBusy = 2
};
//
class CTimeSpan
{
  private:
  char FID[SIZE_TIMESPAN_ID];
  EStateTimeSpan FState;
  Float32 FTimeStart;  // [mm.uu]
  Float32 FTimeEnd; // [mm.uu]
  DOnEventTimeSpan FOnEvent;
  char FBuffer[SIZE_TIMESPAN_BUFFER];
  //
  public:
  CTimeSpan(const char* id);
  CTimeSpan(const char* id, DOnEventTimeSpan oneventtimespan);
  //
  const char* StateText(EStateTimeSpan state);
  EStateTimeSpan GetState(void);
  void SetState(EStateTimeSpan state);
  Boolean IsIdle(void);
  Boolean IsBusy(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  Float32 GetPeriod(void); // [mm.uu]
  // void SetPeriod(Float32 period); // [mm.uu]
  // void StartPeriod(void); // [mm.uu]
  void StartPeriod(Float32 timeperiod); // [mm.uu]
  void Abort(void);
  //
  Boolean Execute(void); // True:Active False:Passive
};
//
#endif // TimeSpan_h
//
