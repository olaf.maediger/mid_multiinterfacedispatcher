//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "DefinitionSystem.h"
//
#ifndef Automation_h
#define Automation_h
//
#include <Arduino.h>
//
//--------------------------------
//  Section - Definition
//--------------------------------
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  saReset = 1
};
//
class CAutomation
{
  private:
  EStateAutomation FState;
  long unsigned FTimePreset;
  //
  void WriteEvent(String text);
  void WriteEvent(String mask, int value);
  //
  void HandleUndefined(void);
  void HandleIdle(void);
  void HandleReset(void);
  //
  public:
  CAutomation(void);
  //
  EStateAutomation GetState(void);
  void SetState(EStateAutomation state);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  void Handle(void);
};
//
#endif
