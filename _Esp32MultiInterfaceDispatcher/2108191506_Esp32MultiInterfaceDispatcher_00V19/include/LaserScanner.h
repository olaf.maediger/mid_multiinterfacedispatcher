//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Trigger.h"
#include "DacMcp4725.h"
#include "TimeSpan.h"
//
//--------------------------------
//  LaserScanner - Constant
//--------------------------------
const int LASERSCANNER_SIZE_ID                   = 6;
const int LASERSCANNER_SIZE_BUFFER               = 48;
//
const char* const LASERSCANNER_EVENT_MASK        = "LaserScanner[%s]=%s";
//
const char* const LASERSCANNER_EVENT_UNDEFINED   = "Undefined";
const char* const LASERSCANNER_EVENT_IDLE        = "Idle";
const char* const LASERSCANNER_EVENT_PLPMOVE     = "PLPMove";
const char* const LASERSCANNER_EVENT_PLPWAIT     = "PLPWait";
const char* const LASERSCANNER_EVENT_PLPPULSE    = "PLPPulse";
//
const char* const STATE_LASERSCANNER[] = {LASERSCANNER_EVENT_UNDEFINED, 
                                          LASERSCANNER_EVENT_IDLE, 
                                          LASERSCANNER_EVENT_PLPMOVE,
                                          LASERSCANNER_EVENT_PLPWAIT, 
                                          LASERSCANNER_EVENT_PLPPULSE};
//
//--------------------------------
//  LaserScanner - Type
//--------------------------------
// class CLaserScanner;
// typedef void (*DOnEventLaserScanner)(CLaserScanner* plaserscanner, const char* pevent);
//
// enum EStateLaserScanner
// {
//   slsUndefined = 0,
//   slsIdle = 1,
//   slsPLPMove = 2,
//   slsPLPWait = 3,
//   slsPLPPulse = 4
// };
//
// class CLaserScanner
// {
//   // private:
//   // EStateLaserScanner FState;
//   // char FID[LASERSCANNER_SIZE_ID];
//   // char FBuffer[LASERSCANNER_SIZE_BUFFER];
//   // CTriggerIn* FPTriggerIn;
//   // CTriggerOut* FPTriggerOut;
//   // CDacMcp4725* FPDacX;
//   // CDacMcp4725* FPDacY;
//   // CTimeSpan* FPTimeSpan;
//   // DOnEventLaserScanner FOnEvent;
//   // UInt32 FPositionX, FPositionY;
//   // Float32 FDelay;
//   // UInt32 FPulsePeriod, FPulseCount;
//   // //  
//   public:
//   // Constructor
//   CLaserScanner(void)
//   {

//   }
//   // CLaserScanner(const char* pid, 
//   //               CTriggerIn* ptriggerin, 
//   //               CTriggerOut* ptriggerout,
//   //               CDacMcp4725* pdacx, 
//   //               CDacMcp4725* pdacy,
//   //               DOnEventLaserScanner oneventlaserscanner,
//   //               DOnEventTimeSpan oneventtimespan);
//   // // Property
//   // const char* StateText(EStateLaserScanner state);
//   // EStateLaserScanner GetState(void);
//   // void SetState(EStateLaserScanner state);
//   // // Method
//   // Boolean Open(void);
//   // Boolean Close(void);
//   // //
//   // void Move(void);
//   // void Wait(void);
//   // void Pulse(void);
//   // //
//   // Boolean PulseLaserPosition(UInt32 positionx, UInt32 positiony,
//   //                            UInt32 pulseperiod, UInt32 pulsecount);
//   // //
//   // void Execute(void);
// };
//
#endif // LaserScanner_h
//
#endif // DISPATCHER_LASERSCANNER
//
