//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_SCANNER)
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
#include "Error.h"
#include "Scanner.h"
#include "DispatcherScanner.h"
// #if (defined(DACX_INTERNAL) | defined(DACY_INTERNAL))
// #include "DacInternal.h"
// #endif
// #if (defined(DACX_MCP4725) | defined(DACY_MCP4725))
// #include "DacMCP4725.h"
// #endif
// #if defined(DISPATCHER_TRIGGER)
// #include "TimeSpan.h"
// #endif
//
extern CError Error;
extern CCommand Command;
// 
#if defined(DISPATCHER_TRIGGER)
  // #if (defined(DACX_INTERNAL) || defined(DACY_INTERNAL)) 
  // extern CDacInternal ScannerInternalXY;
  // #endif
  // #ifdef DACX_MCP4725
  // extern CDacMcp4725 DacMcp4725X;
  // #endif
  // #ifdef DACY_MCP4725
  // extern CDacMcp4725 DacMcp4725Y;
  // #endif
  // extern CTimeSpan ScannerDelay;
extern CScanner ScannerXY;
#endif
//
//#########################################################
//  Dispatcher - Scanner - Constructor
//#########################################################
CDispatcherScanner::CDispatcherScanner(void)
{
}
//
//#########################################################
//  Dispatcher - Scanner - Execution
//#########################################################
bool CDispatcherScanner::ExecuteGetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  UInt32 PX = ScannerXY.GetPositionX();
  sprintf(Command.GetBuffer(), "%s %u", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  UInt32 PX = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %u", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.SetPositionX(PX);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  UInt32 PY = ScannerXY.GetPositionY();
  sprintf(Command.GetBuffer(), "%s %u", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  UInt32 PY = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %u", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.SetPositionY(PY);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  UInt32 XL, XH, DX;
  ScannerXY.GetRangeX(XL, XH, DX);
  sprintf(Command.GetBuffer(), "%s %u %u %u", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
bool CDispatcherScanner::ExecuteSetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <xl> <xh> <dx>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  UInt32 XL = atol(parameters[0]);
  UInt32 XH = atol(parameters[1]);
  UInt32 DX = atol(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %u %u %u", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.SetRangeX(XL, XH, DX);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  UInt32 YL, YH, DY;
  ScannerXY.GetRangeY(YL, YH, DY);
  sprintf(Command.GetBuffer(), "%s %u %u %u", command, YL, YH, DY);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <yl> <yh> <dy>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  UInt32 YL = atol(parameters[0]);
  UInt32 YH = atol(parameters[1]);
  UInt32 DY = atol(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %u %u %u", command, YL, YH, DY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.SetRangeY(YL, YH, DY);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  Float32 DM = ScannerXY.GetDelayMotion();
  sprintf(Command.GetBuffer(), "%s %f", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tw>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  Float32 DM = atof(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %f", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.SetDelayMotion(DM);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteMovePosition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <px> <py>
  UInt32 PX = ScannerXY.GetPositionX();
  UInt32 PY = ScannerXY.GetPositionY();
  if (2 == parametercount)
  {
    PX = atol(parameters[0]);
    PY = atol(parameters[1]);
  } 
  else
    if (1 == parametercount)
    {
      PX = atol(parameters[0]);
    } 
  // Response:
  sprintf(Command.GetBuffer(), "%s %u %u", command, PX, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.Move(PX, PY);
  ExecuteEnd();
  return true;
}
bool CDispatcherScanner::ExecuteWaitTime(char* command, int parametercount, char** parameters)
{ 
  ExecuteBegin();
  // Analyse parameters: <tw>
  Float32 DM = ScannerXY.GetDelayMotion();
  if (1 == parametercount)
  {
    DM = atof(parameters[0]);
  } 
  // Response:
  sprintf(Command.GetBuffer(), "%s %f", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.Wait(DM);
  ExecuteEnd();
  return true;
}
bool CDispatcherScanner::ExecuteMoveWaitPosition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <px> <py> <wt>
  UInt32 PX = ScannerXY.GetPositionX();
  UInt32 PY = ScannerXY.GetPositionY();
  Float32 DM = ScannerXY.GetDelayMotion();
  if (3 == parametercount)
  {
    PX = atol(parameters[0]);
    PY = atol(parameters[1]);
    DM = atof(parameters[2]);
  } 
  else
    if (2 == parametercount)
    {
      PX = atol(parameters[0]);
      PY = atol(parameters[1]);
    } 
    else
      if (1 == parametercount)
      {
        PX = atol(parameters[0]);
      } 
  // Response:
  sprintf(Command.GetBuffer(), "%s %u %u %f", command, PX, PY, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.MoveWait(PX, PY, DM);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteScanX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <-> 
  // Response:
  UInt32 XL = ScannerXY.GetPositionXL();
  UInt32 XH = ScannerXY.GetPositionXH();
  UInt32 DX = ScannerXY.GetPositionDX();
  Float32 DM = ScannerXY.GetDelayMotion();
  sprintf(Command.GetBuffer(), "%s %u %u %u %f", command, XL, XH, DX, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.ScanX();
  ExecuteEnd();
  return true;
}
bool CDispatcherScanner::ExecuteScanY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <-> 
  // Response:
  UInt32 YL = ScannerXY.GetPositionYL();
  UInt32 YH = ScannerXY.GetPositionYH();
  UInt32 DY = ScannerXY.GetPositionDY();
  Float32 DM = ScannerXY.GetDelayMotion();
  ExecuteResponse(Command.GetBuffer());
  sprintf(Command.GetBuffer(), "%s %u %u %u %f", command, YL, YH, DY, DM);
  // Action:
  ScannerXY.ScanY();
  ExecuteEnd();
  return true;
}
bool CDispatcherScanner::ExecuteScanXY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <-> 
  // Response:
  UInt32 XL = ScannerXY.GetPositionXL();
  UInt32 XH = ScannerXY.GetPositionXH();
  UInt32 DX = ScannerXY.GetPositionDX();
  UInt32 YL = ScannerXY.GetPositionYL();
  UInt32 YH = ScannerXY.GetPositionYH();
  UInt32 DY = ScannerXY.GetPositionDY();
  Float32 DM = ScannerXY.GetDelayMotion();
  sprintf(Command.GetBuffer(), "%s %u %u %u %f", command, XL, XH, DX, DM);
  ExecuteResponse(Command.GetBuffer());
  sprintf(Command.GetBuffer(), "%s %u %u %u %f", command, YL, YH, DY, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerXY.ScanXY();
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Scanner - Handler
//#########################################################
bool CDispatcherScanner::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_GPX, command))
  {
    return ExecuteGetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPX, command))
  {
    return ExecuteSetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GPY, command))
  {
    return ExecuteGetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPY, command))
  {
    return ExecuteSetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRX, command))
  {
    return ExecuteGetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRX, command))
  {
    return ExecuteSetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRY, command))
  {
    return ExecuteGetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRY, command))
  {
    return ExecuteSetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GDM, command))
  {
    return ExecuteGetDelayMotion(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SDM, command))
  {
    return ExecuteSetDelayMotion(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_MVP, command))
  {
    return ExecuteMovePosition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_WTT, command))
  {
    return ExecuteWaitTime(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_MWP, command))
  {
    return ExecuteMoveWaitPosition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SCX, command))
  {
    return ExecuteScanX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SCY, command))
  {
    return ExecuteScanY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SXY, command))
  {
    return ExecuteScanXY(command, parametercount, parameters);
  }
  return false;
}
//
#endif // DISPATCHER_SCANNER
//
