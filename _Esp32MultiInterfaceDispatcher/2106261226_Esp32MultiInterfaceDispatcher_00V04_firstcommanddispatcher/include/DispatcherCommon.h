//
#include "Defines.h"
//
#if defined(COMMAND_COMMON)
//
#ifndef DispatcherCommon_h
#define DispatcherCommon_h
//
#include "Command.h"
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - SHORT
//----------------------------------------------------------------
#define SHORT_GPH   "GPH"                   // GPH - GetProgramHeader
#define SHORT_GHV   "GHV"                   // GHV - GetHardwareVersion
#define SHORT_GSV   "GSV"                   // GSV - GetSoftwareVersion
#define SHORT_H     "H"                     // H - (this) Help
//
//----------------------------------------------------------------
// Dispatcher - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_COMMON       " Help (Common):"
#define MASK_H                    " %-3s                  : This Help"
//
#define TITLE_LINE                "--------------------------------------------------"
#define MASK_PROJECT              "- Project:   %-35s -"
#define MASK_SOFTWARE             "- Version:   %-35s -"
#define MASK_HARDWARE             "- Hardware:  %-35s -"
#define MASK_DATE                 "- Date:      %-35s -"
#define MASK_TIME                 "- Time:      %-35s -"
#define MASK_AUTHOR               "- Author:    %-35s -"
#define MASK_PORT                 "- Port:      %-35s -"
#define MASK_PARAMETER            "- Parameter: %-35s -"
//
const int COUNT_SOFTWAREVERSION   = 1;
const int COUNT_HARDWAREVERSION   = 1;
//
#define MASK_SOFTWAREVERSION      "IRQ-Version %s"
#define MASK_HARDWAREVERSION      "Hardware-Version %s"
//----------------------------------------------------------------
// DispatcherCommon
//----------------------------------------------------------------
class CDispatcherCommon : public CDispatcher
{
  public:
  bool WriteProgramHeader(void);
  void WriteHardwareVersion(void);
  void WriteSoftwareVersion(void);
  bool WriteHelp(void);
  //
  public:
  CDispatcherCommon(void);
  //
  // bool ExecuteGetProgramHeader(CCommand &command);
  // bool ExecuteGetHardwareVersion(CCommand &command);
  // bool ExecuteGetSoftwareVersion(CCommand &command);
  bool ExecuteGetHelp(Byte parametercount, char** pparameters);
  //
  bool virtual ExecuteCommand(char* pcommand, int parametercount, char** pparameters);
  bool virtual HandleInterface(CCommand* pcommand);
};
//
#endif
//
#endif // COMMAND_COMMON
//
//