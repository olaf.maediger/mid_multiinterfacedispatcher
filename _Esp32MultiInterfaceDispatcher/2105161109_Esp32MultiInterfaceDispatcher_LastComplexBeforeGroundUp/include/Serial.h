//
#include "Defines.h"
//
#ifdef COMMAND_UART
//
#ifndef Serial_h
#define Serial_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <HardwareSerial.h>
#include "Pinout.h"
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
#define RXDECHO_ON  true 
#define RXDECHO_OFF false
//
#define SIZE_TXDBUFFER      64
#define SIZE_RXDBUFFER      64
//
#define SIZE_FORMATBUFFER   96
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  bool FIsOpen = false;
  bool FRxdEcho = false;
  
  public:
#if ((defined PROCESSOR_NANOR3)||(defined PROCESSOR_UNOR3)||(defined PROCESSOR_MEGA2560)||(defined PROCESSOR_DUEM3)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_STM32F407VG)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36)||(defined PROCESSOR_ESP8266)||(defined PROCESSOR_ESP32))
  //CSerialBase(void);
  virtual bool Open(UInt32 baudrate) = 0;
#endif
//#if ((defined PROCESSOR_ESP8266)||(defined PROCESSOR_ESP32))
//  CSerialBase(void);
//  virtual bool Open(UInt32 baudrate, int pinrxd, int pintxd) = 0;
//#endif
  virtual bool Close() = 0;
  void SetRxdEcho(bool rxdecho);
  //
  // -> Utilities!  String BuildTime(long unsigned milliseconds);
  //
  void WriteNewLine();
  void WritePrompt();
  void WriteComment();
  void WriteResponse(void);
  //
  virtual int GetRxdByteCount() = 0;
  virtual char ReadCharacter() = 0;
  virtual String ReadLine() = 0;
  virtual void Write(const char* text) = 0;
  virtual void Write(String text) = 0;
};
//
//----------------------------------------------------
//  Segment - CSerialH (^=HardwareSerial)
//----------------------------------------------------
//
#if ((defined PROCESSOR_NANOR3)||(defined PROCESSOR_UNOR3)||(defined PROCESSOR_MEGA2560)||(defined PROCESSOR_DUEM3)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_STM32F407VG)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36)||(defined PROCESSOR_ESP8266))
class CSerialH : public CSerialBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CSerialH(HardwareSerial *serial);
  bool Open(UInt32 baudrate);
  bool Close();
  //
  int GetRxdByteCount();
  char ReadCharacter();
  String ReadLine();
  void Write(const char* text);
  void Write(String text);
};
#endif
//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
#if ((defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36))
class CSerialU : public CSerialBase
{
  protected:
  usb_serial_class* FPSerial;
  //  
  public:
  CSerialU(usb_serial_class *serial)
  {
    FPSerial = serial;
  } 

  bool Open(UInt32 baudrate);
  bool Close();
  //
  int GetRxdByteCount();
  char ReadCharacter();
  String ReadLine();
  void Write(const char* text);
  void Write(String text);
};
#endif
//
//----------------------------------------------------
//  Segment - CSerialPS
//----------------------------------------------------
//
#if (defined PROCESSOR_ESP32)
class CSerialPS : public CSerialBase
{
  protected:
  HardwareSerial* FPSerial;
  int FPinRXD, FPinTXD;
  //  
  public:
  CSerialPS(HardwareSerial *serial, int pinrxd, int pintxd);  
  bool Open(UInt32 baudrate);
  bool Close();
  int GetRxdByteCount();
  char ReadCharacter();
  String ReadLine();
  void Write(const char* text);
  void Write(String text);
};
#endif // PROCESSOR_ESP32
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  CSerialBase* FPSerial;
  //  
  public:
#if (defined PROCESSOR_NANOR3)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_UNOR3)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_MEGA2560)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_DUEM3)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_STM32F103C8)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_STM32F407VG)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_TEENSY32)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
  CSerial(usb_serial_class *serial);
  CSerial(usb_serial_class &serial);
#elif (defined PROCESSOR_TEENSY36)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
  CSerial(usb_serial_class *serial);
  CSerial(usb_serial_class &serial);
#elif (defined PROCESSOR_ESP8266)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_ESP32)
  CSerial(HardwareSerial *serial, int rxdpin, int txdpin);
#else
#error Undefined Processor-Type!
#endif
  //
#if ((defined PROCESSOR_NANOR3)||(defined PROCESSOR_UNOR3)||(defined PROCESSOR_MEGA2560)||(defined PROCESSOR_DUEM3)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_STM32F407VG))
  bool Open(UInt32 baudrate);
#endif
#if ((defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36))
  bool Open(UInt32 baudrate);
#endif
#if (defined PROCESSOR_ESP8266)
  bool Open(UInt32 baudrate);
#endif  
#if (defined PROCESSOR_ESP32)
  bool Open(UInt32 baudrate);
#endif  
  bool Close();
  void SetRxdEcho(bool rxdecho);
  //  
  int GetRxdByteCount();
  char ReadCharacter();
  String ReadLine();
  //
  void WriteTime();
  //
  void WriteNewLine(void);
  void WritePrompt(void);
  void WriteEvent(String line);
  void WriteCommand(String command, Character index, String parameter1, String parameter2);
  void WriteResponse(void);
  void WriteResponse(String line);
  void WriteComment(void);
  void WriteComment(String line);
  void WriteDebug(String line);
  //
  void Write(char character);
  void Write(const char* text);
  void Write(String text);
  void Write(UInt8 number);
  void Write(UInt16 number);
  void Write(UInt32 number);
  void Write(Int8 number);
  void Write(Int16 number);
  void Write(Int32 number);
  void Write(Float32 number);
  void Write(Double64 number);
  void WriteByte(UInt8 value);
  void WriteWord(UInt16 value);
  void WriteQuad(UInt32 value);
  //
  void WriteLine(void);
  void WriteLine(const char *text);
  void WriteLine(String text);
  void WriteLine(UInt8 number);
  void WriteLine(UInt16 number);
  void WriteLine(UInt32 number);
  void WriteLine(Int8 number);
  void WriteLine(Int16 number);
  void WriteLine(Int32 number);
  void WriteLine(Float32 number);
  void WriteLine(Double64 number);
  //
  void Write(const char* mask, char character);
  void Write(const char* mask, const char* text);
  void Write(const char* mask, String text);
  void Write(const char* mask, UInt8 number);
  void Write(const char* mask, UInt16 number);
  void Write(const char* mask, UInt32 number);
  void Write(const char* mask, Int8 number);
  void Write(const char* mask, Int16 number);
  void Write(const char* mask, Int32 number);
  void Write(const char* mask, Float32 number);
  void Write(const char* mask, Double64 number);
  //
  void WriteLine(const char* mask, char character);
  void WriteLine(const char* mask, const char* text);
  void WriteLine(const char* mask, String text);
  void WriteLine(const char* mask, UInt8 number);
  void WriteLine(const char* mask, UInt16 number);
  void WriteLine(const char* mask, UInt32 number);
  void WriteLine(const char* mask, Int8 number);
  void WriteLine(const char* mask, Int16 number);
  void WriteLine(const char* mask, Int32 number);
  void WriteLine(const char* mask, Float32 number);
  void WriteLine(const char* mask, Double64 number);
};
//
#endif // Serial_h
//
#endif // COMMAND_UART
//
