#include "DefinitionSystem.h"
//
#ifdef PROTOCOL_MQTT
//
#ifndef ProtocolMqtt_h
#define ProtocolMqtt_h
//
//----------------------------------------------------------------------
//  Include
//----------------------------------------------------------------------
////!!! exact here !!!!!!!!!!
#define USE_WIFI_NINA false
////!!! exact here !!!!!!!!!!
#include <WiFi.h>
#include <PubSubClient.h>
#include "DefinitionMqtt.h"
//
//
//--------------------------------
//  Type
//--------------------------------
class CProtocolMqtt;
typedef void (*DMqttOnSetup)(CProtocolMqtt* mqtt);
typedef void (*DMqttOnTryConnection)(CProtocolMqtt* mqtt);
typedef void (*DMqttOnConnectionSuccess)(CProtocolMqtt* mqtt);
typedef void (*DMqttOnConnectionFailed)(CProtocolMqtt* mqtt);
typedef bool (*DMqttOnReceive)(CProtocolMqtt* pmqtt, const char* ptopic, const char* pmessage);
//
static const char* const ERROR_MQTT[] = 
{
  "None",
  "Connection failed",
  "Subscribe failed",
  "Publish failed",
  "Invalid Topic",
  "Invalid Command"
};

enum EErrorCodeMqtt
{
  ecmqNone = 0,
  ecmqConnectionFailed = 1,
  ecmqSubscribeFailed = 2,
  ecmqPublishFailed = 3,
  ecmqInvalidTopic = 4,
  ecmqInvalidCommand = 5
};


class CProtocolMqtt
{
  private:
  EErrorCodeMqtt FErrorCode;
  WiFiClient*   FPWifiClient;
  PubSubClient* FPMqttClient;
  char FTopic[MQTT_SIZE_TOPIC];
  char FMessage[MQTT_SIZE_MESSAGE];
  //
  public:
  void OnReceiveTopic(char* topic, byte* payload, unsigned int payloadsize);
  //
  private:
  DMqttOnSetup FOnSetup;
  DMqttOnTryConnection FOnTryConnection;
  DMqttOnConnectionSuccess FOnConnectionSuccess;
  DMqttOnConnectionFailed FOnConnectionFailed;
  DMqttOnReceive FOnReceive;
  //
  public:
  CProtocolMqtt(WiFiClient* pwificlient, 
                DMqttOnSetup onsetup, 
                DMqttOnTryConnection ontryconnection, 
                DMqttOnConnectionSuccess onconnectionsuccess, 
                DMqttOnConnectionFailed onconnectionfailed, 
                DMqttOnReceive onreceive);
  EErrorCodeMqtt GetErrorCode(void);
  bool IsConnected(void);
  int GetState(void);
  // 
  void Setup();
  void Connect(void);
  //
  bool Subscribe(const char* ptopic);
  bool Publish(const char* ptopic, const char* pmessage);
  // 
  bool LineDetected(void);
  bool ReadLine(char* buffer, unsigned size);
  //
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteResponse(String text);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  //
  void Execute(void);
};
//
#endif // ProtocolMqtt_h
//
#endif // PROTOCOL_MQTT
//

