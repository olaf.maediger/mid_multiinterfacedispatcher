//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_MQTT)
//
#ifndef DispatcherMqtt_h
#define DispatcherMqtt_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Mqtt - SHORT
//----------------------------------------------------------------
#define SHORT_WCM   "WCM"                  // WCM - Write Command Mqtt
#define SHORT_STM   "STM"                  // STM - Subsribe Topic Mqtt
//
//----------------------------------------------------------------
// Dispatcher - Mqtt - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_MQTT  " Help (Mqtt):"
#define MASK_WCM    " %-3s <c> <p>         : Write Mqtt <c>ommand <p>arameter"
#define MASK_STM    " %-3s <t>             : Subscribe Mqtt <t>opic"
//
//----------------------------------------------------------------
// Dispatcher - Mqtt
//----------------------------------------------------------------
class CDispatcherMqtt : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherMqtt(void);
  //
  bool ExecuteWriteCommandMqtt(char* command, int parametercount, char** parameters);
  bool ExecuteSubscribeTopicMqtt(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool virtual Execute(void);
};
//
#endif // DispatcherMqtt_h
//
#endif // DISPATCHER_MQTT
//
