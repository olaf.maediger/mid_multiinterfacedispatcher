// //
#include "DefinitionSystem.h"
//
#include "Error.h"
#include "Command.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BTTT)
#include "InterfaceBttt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
extern CError     Error;
//
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_BTTT)
extern CInterfaceBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CInterfaceWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CInterfaceLan LanCommand;
#endif
#if defined(PROTOCOL_MQTT)
extern CProtocolMqtt MqttCommand;
#endif
//
//#########################################################
//  Command - Constructor
//#########################################################
CCommand::CCommand(void)
{
  FState = scUndefined;
#if defined(INTERFACE_UART)
  FCommandUart = new Character[UART_COMMANDSIZE];
  FParametersUart = new PCharacter[UART_PARAMETERCOUNT];
  for (int IUP = 0; IUP < UART_PARAMETERCOUNT; IUP++)
  {
    FParametersUart[IUP] = new Character[UART_PARAMETERSIZE];
  }
  ResetCommandUart();
#endif
#if defined(INTERFACE_BTTT)
  FCommandBt = new Character[BT_COMMANDSIZE];
  FParametersBt = new PCharacter[BT_PARAMETERCOUNT];
  for (int IBP = 0; IBP < BT_PARAMETERCOUNT; IBP++)
  {
    FParametersBt[IBP] = new Character[BT_PARAMETERSIZE];
  }
  ResetCommandBt();
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
  FMqttCommand = new Character[MQTT_COMMANDSIZE];
  FMqttParameters = new PCharacter[MQTT_PARAMETERCOUNT];
  for (int IUP = 0; IUP < MQTT_PARAMETERCOUNT; IUP++)
  {
    FMqttParameters[IUP] = new Character[MQTT_PARAMETERSIZE];
  }
  ResetMqttCommand();
#endif
}
CCommand::~CCommand(void)
{
  FState = scUndefined;
}
//
//#########################################################
//  Command - Property
//#########################################################
EStateCommand CCommand::GetState(void)
{
  return FState;
}
//
void CCommand::SetState(EStateCommand state)
{
  if (state != FState)
  {
    FState = state;
#ifdef INTERFACE_UART
    UartCommand.WriteEvent(MASK_STATECOMMAND, StateText(FState));
#endif
// #ifdef INTERFACE_BT
// #endif
// #ifdef INTERFACE_WLAN
// #endif
// #ifdef INTERFACE_LAN
// #endif
#if defined(PROTOCOL_MQTT)
    MqttCommand.WriteEvent(MASK_STATECOMMAND, StateText(FState));
#endif
// #if defined(PROTOCOL_SDCARD)
// #endif
  }
}
//
//#########################################################
//  Command - Helper
//#########################################################
const char* CCommand::StateText(EStateCommand state)
{
  switch (state)
  {
    case scError:
      return (const char*)"Error";
    case scUndefined:
      return (const char*)"Undefined";
    case scInit:
      return (const char*)"Init";
    case scIdle:
      return (const char*)"Idle";
    case scBusy:
      return (const char*)"Busy";
    default: // not in list -> Undefined
      return (const char*)"Unknown";
  }
}
//
#if defined(INTERFACE_UART)
char* CCommand::GetCommandUart(void)
{
  return FCommandUart;
}
Int16 CCommand::GetParameterCountUart(void)
{
  return FParameterCountUart;
}
VPCharacter CCommand::GetParametersUart(void)
{
  return FParametersUart;
}
void CCommand::ResetCommandUart(void)
{
  FCommandUart[0] = 0x00;
  FParameterCountUart = 0;
  for (int UI = 0; UI < UART_PARAMETERCOUNT; UI++)
  {
    FParametersUart[UI][0] = 0x00;
  }
}
#endif
#if defined(INTERFACE_BTTT) 
char* CCommand::GetCommandBt(void)
{
  return FCommandBt;
}
Int16 CCommand::GetParameterCountBt(void)
{
  return FParameterCountBt;
}
VPCharacter CCommand::GetParametersBt(void)
{
  return FParametersBt;
}
void CCommand::ResetCommandBt(void)
{
  FCommandBt[0] = 0x00;
  FParameterCountBt = 0;
  for (int BI = 0; BI < BT_PARAMETERCOUNT; BI++)
  {
    FParametersBt[BI][0] = 0x00;
  }
}
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
char* CCommand::GetMqttCommand(void)
{
  return FMqttCommand;
}
Int16 CCommand::GetMqttParameterCount(void)
{
  return FMqttParameterCount;
}
VPCharacter CCommand::GetMqttParameters(void)
{
  return FMqttParameters;
}
void CCommand::ResetMqttCommand(void)
{
  FMqttCommand[0] = 0x00;
  FMqttParameterCount = 0;
  for (int MI = 0; MI < MQTT_PARAMETERCOUNT; MI++)
  {
    FMqttParameters[MI][0] = 0x00;
  }
}
#endif
//
//#########################################################
//  Command - Helper
//#########################################################
//
//#########################################################
//  Command - Handler
//#########################################################
bool CCommand::Open(void)
{
  SetState(scIdle);
  return true;
}
bool CCommand::Close(void)
{
  SetState(scUndefined);
  return true;
}
//
//##################################################################
// Command - Analysis & Execution
//##################################################################
// InterfaceText -> Command/COMMAND, Parameters
bool CCommand::AnalyseInterfaceBlock(void)
{
#if defined(INTERFACE_UART)
  if (scIdle != GetState())
  {
    Error.SetCode(ecTimingFailure);
    return false;
  }
  int SizeBlockUart = UART_BLOCKSIZE - 1;
  // check of whole line received
  if (UartCommand.ReadLine(FBlockUart, SizeBlockUart))
  { // Block has copy of whole line
    SetState(scBusy);
    char *PTerminal = (char*)" \t\r\n";
    char* PCommand = strtok(FBlockUart, PTerminal);
    if (PCommand)
    { 
      strupr(PCommand);
      strcpy(FCommandUart, PCommand);
      // NC UartCommand.WriteLine();
      // debug UartCommand.WriteText((char*)"FUartCommand[");
      // debug UartCommand.WriteText(FUartCommand);
      // debug UartCommand.WriteLine((char*)"]");
      FParameterCountUart = 0;
      char *PParameter;
      while (0 != (PParameter = strtok(0, PTerminal)))
      {
        strcpy(FParametersUart[FParameterCountUart], PParameter);
        // debug UartCommand.WriteInt32("P[%i]", FUartParameterCount);
        // debug UartCommand.WriteLine("<%s>", FUartParameters[FUartParameterCount]);
        FParameterCountUart++;
        if (UART_PARAMETERCOUNT < FParameterCountUart)
        {
          Error.SetCode(ecToManyParameters);
          return false;
        }
      }
      return true;
    }
    return false;
  }
#endif
#if defined(INTERFACE_BTTT)
  if (scIdle != GetState())
  {
    Error.SetCode(ecTimingFailure);
    return false;
  }
  int SizeBlockBt = BT_BLOCKSIZE - 1;
  // check of whole line received
  if (BtCommand.ReadLine(FBlockBt, SizeBlockBt))
  { // Block has copy of whole line
    SetState(scBusy);
    char *PTerminal = (char*)" \t\r\n";
    char* PCommand = strtok(FBlockBt, PTerminal);
    if (PCommand)
    { 
       strupr(PCommand);
       strcpy(FCommandBt, PCommand);
  //     // debug UartCommand.WriteText((char*)"FBtCommand[");
  //     // debug UartCommand.WriteText(FBtCommand);
  //     // debug UartCommand.WriteLine((char*)"]");
       FParameterCountBt = 0;
       char *PParameter;
       while (0 != (PParameter = strtok(0, PTerminal)))
       {
         strcpy(FParametersBt[FParameterCountBt], PParameter);
  //       // debug BtCommand.WriteInt32("P[%i]", FBtParameterCount);
  //       // debug BtCommand.WriteLine("<%s>", FBtParameters[FBtParameterCount]);
         FParameterCountBt++;
         if (BT_PARAMETERCOUNT < FParameterCountBt)
         {
           Error.SetCode(ecToManyParameters);
           return false;
         }
      }
      return true;
    }
    return false;
  }
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
// access to Mqtt Received Block
// analyse Topic, Command, Parameters
  if (MqttCommand.LineDetected())
  { // Topic & Message received
    if (scIdle != GetState())
    {
      Error.SetCode(ecTimingFailure);
      return false;
    }
    char Block[INTERFACE_SIZE_RXBLOCK];
    // check of whole line received
    if (MqttCommand.ReadLine(Block, INTERFACE_SIZE_RXBLOCK - 1))
    {
      SetState(scBusy);
      char *PTerminal = (char*)" \t\r\n";
      char* PCommand = strtok(Block, PTerminal);
      if (PCommand)
      { 
        strupr(PCommand);
        strcpy(FMqttCommand, PCommand);
        // NC UartCommand.WriteLine();
        // debug UartCommand.WriteText((char*)"FMqttCommand[");
        // debug UartCommand.WriteText(FMqttCommand);
        // debug UartCommand.WriteLine((char*)"]");
        FMqttParameterCount = 0;
        char *PParameter;
        while (0 != (PParameter = strtok(0, PTerminal)))
        {
          strcpy(FMqttParameters[FMqttParameterCount], PParameter);
          // debug UartCommand.WriteInt32("P[%i]", FMqttParameterCount);
          // debug UartCommand.WriteLine("<%s>", FMqttParameters[FMqttParameterCount]);
          FMqttParameterCount++;
          if (MQTT_PARAMETERCOUNT < FMqttParameterCount)
          {
            Error.SetCode(ecToManyParameters);
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }  
#endif
  return false;
}
//
