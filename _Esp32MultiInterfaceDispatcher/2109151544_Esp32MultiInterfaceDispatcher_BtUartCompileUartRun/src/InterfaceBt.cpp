//
#include "DefinitionSystem.h"
//
#if defined(INTERFACE_BTTT)
//
#include "Error.h"
#include "InterfaceBttt.h"
//
extern CError Error;
//
//####################################################
//  CInterfaceBtBase
//####################################################
//----------------------------------------------------
//  CInterfaceBtBase - Constructor
//----------------------------------------------------
// CInterfaceBtBase(abstract!) - CInterfaceBase(abstract!)
CInterfaceBtBase::CInterfaceBtBase(const char* deviceid)
  : CInterfaceBase(deviceid)
{
  FRXEcho = BT_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBtBase - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Read
//----------------------------------------------------
//
//####################################################
//  CInterfaceBtUart
//####################################################
//----------------------------------------------------
//  CInterfaceBtUart - Constructor
//----------------------------------------------------
CInterfaceBtUart::CInterfaceBtUart(const char* deviceid, 
                                   BluetoothSerial* pserial)
  : CInterfaceBtBase(deviceid)
{
  FPSerial = pserial;
} 
//
//----------------------------------------------------
//  CInterfaceBtUart - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtUart - Handler
//----------------------------------------------------
Boolean CInterfaceBtUart::Open(int parameter)
{
  FPSerial->begin(FDeviceID);
  return false;
}
Boolean CInterfaceBtUart::Close(void)
{
  FPSerial->end();
  return false;
}
//
//----------------------------------------------------
//  CInterfaceUartHS - Read
//----------------------------------------------------
UInt8 CInterfaceBtUart::GetRXCount(void)
{
  return FPSerial->available();
}
//
Boolean CInterfaceBtUart::Read(char &character)
{
  character = (char)0x00;
  if (0 < FPSerial->available())
  {
    character = FPSerial->read();
    if (FRXEcho)
    {
      switch (character)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(character);
          break;
      }
    }
    return true;
   }
   return false;
}
// reads a whole line with CR/LF, 
// if <line then rxdata is buffered in FRXBlock,
// return true: found whole line
// prxline points to extern buffer with copy of line 
Boolean CInterfaceBtUart::ReadLine(char* prxline, int &rxsize)
{ // Separator: CR or LF
  // !!! stay rxsize, use as limit !!!
  while (0 < FPSerial->available())
  {
    char RXC = (char)FPSerial->read();
    if (FRXEcho)
    {
      switch (RXC)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(RXC);
          break;
      }
    }
    switch (RXC)
    {
      case (char)SEPARATOR_CR:
      case (char)SEPARATOR_LF:
        if (0 < FRXIndex)
        {          
          FRXBlock[FRXIndex] = SEPARATOR_ZERO;
          if (0 < strlen(FRXBlock))
          { // secure-copy to targetbuffer
            strcpy(prxline, FRXBlock);
            // zero FRXBlock
            rxsize = 1 + FRXIndex;
            FRXIndex = 0;
            FRXBlock[FRXIndex] = SEPARATOR_ZERO;

Serial.println("!!!");
delay(1000);

            return true;
          }
        }
Serial.println("---");
delay(1000);
        return false;
      default:
        FRXBlock[FRXIndex] = RXC;
        FRXIndex++;
        if (rxsize <= (1 + FRXIndex))
        {
          Error.SetCode(ecReceiveBufferOverflow);
Serial.println("???");
delay(1000);
          return false;
        }
    }
  }
  return false;
} 
//
//----------------------------------------------------
//  CInterfaceBtUart - Write
//----------------------------------------------------
Boolean CInterfaceBtUart::Write(char character)
{
  FPSerial->print(character);
  return false;
}

Boolean CInterfaceBtUart::WriteText(char* ptext)
{
  FPSerial->print(ptext);
  return false;
}

Boolean CInterfaceBtUart::WriteLine(void)
{
  FPSerial->write(TOKEN_NEWLINE[0]);
  FPSerial->write(TOKEN_NEWLINE[1]);
  return false;
} 

Boolean CInterfaceBtUart::WriteLine(char* pline)
{
  FPSerial->write((const uint8_t *)pline, strlen(pline));
  FPSerial->write(TOKEN_NEWLINE[0]);
  FPSerial->write(TOKEN_NEWLINE[1]);
  return false;
} 
//
//####################################################
//  CInterfaceBt
//####################################################
//----------------------------------------------------
//  CInterfaceBt - Constructor
//----------------------------------------------------
CInterfaceBt::CInterfaceBt(const char* devicename, BluetoothSerial* pserial)
{
    FPBtBase = new CInterfaceBtUart(devicename, pserial);
}
//
//----------------------------------------------------
//  CInterfaceBt - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBt - Handler
//----------------------------------------------------
Boolean CInterfaceBt::Open(int baudrate)
{
  return FPBtBase->Open(baudrate);
}
Boolean CInterfaceBt::Close(void)
{
  return FPBtBase->Close();
}
//
//----------------------------------------------------
//  CInterfaceBt - Read
//----------------------------------------------------
UInt8 CInterfaceBt::GetRXCount(void)
{
  return FPBtBase->GetRXCount();
}
Boolean CInterfaceBt::Read(char &character)
{
  return FPBtBase->Read(character);
}
// true: whole line with cr/lf found
Boolean CInterfaceBt::ReadLine(char* prxline, int &rxsize)
{
  return FPBtBase->ReadLine(prxline, rxsize);
}
//
//----------------------------------------------------
// CInterfaceUart - Write
//----------------------------------------------------
Boolean CInterfaceBt::WriteLine(void)
{
  return FPBtBase->WriteLine();
}
Boolean CInterfaceBt::WriteText(const char* ptext)
{
  return FPBtBase->WriteText((char*)ptext);
}
Boolean CInterfaceBt::WriteText(const char* mask, const char* ptext)
{
  if (0 != ptext)
  {
    return FPBtBase->WriteText(FPBtBase->ConvertPChar(mask, (char*)ptext));
  }  
  return FPBtBase->WriteText(FPBtBase->ConvertPChar(mask, (char*)""));
}
Boolean CInterfaceBt::WriteLine(const char* pline)
{
  return FPBtBase->WriteLine((char*)pline);
}
Boolean CInterfaceBt::WriteLine(const char* mask, const char* pline)
{
  if (0 != pline)
  {
    return FPBtBase->WriteLine(FPBtBase->ConvertPChar(mask, (char*)pline));
  }  
  return FPBtBase->WriteLine(FPBtBase->ConvertPChar(mask, (char*)""));
}  
Boolean CInterfaceBt::WritePChar(const char* mask, char* value)
{
  if (0 != value)
  {    
    return FPBtBase->WriteText(FPBtBase->ConvertPChar(mask, value));
  }
  return FPBtBase->WriteText((char*)mask);
}
Boolean CInterfaceBt::WriteString(const char* mask, String value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertString(mask, value));
}
Boolean CInterfaceBt::WriteByte(const char* mask, Byte value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertByte(mask, value));
}
Boolean CInterfaceBt::WriteDual(const char* mask, UInt16 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertDual(mask, value));
}
Boolean CInterfaceBt::WriteQuad(const char* mask, UInt32 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertQuad(mask, value));
}
Boolean CInterfaceBt::WriteInt16(const char* mask, Int16 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertInt16(mask, value));
}
Boolean CInterfaceBt::WriteUInt16(const char* mask, UInt16 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertUInt16(mask, value));
}
Boolean CInterfaceBt::WriteInt32(const char* mask, Int32 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertInt32(mask, value));
}
Boolean CInterfaceBt::WriteUInt32(const char* mask, UInt32 value)
{
  return FPBtBase->WriteText(FPBtBase->ConvertUInt32(mask, value));
}
Boolean CInterfaceBt::WriteFloat32(const char* mask, Float32 value)
{  
  return FPBtBase->WriteText(FPBtBase->ConvertFloat32(mask, value));
}
Boolean CInterfaceBt::WriteDouble64(const char* mask, Double64 value)
{  
  return FPBtBase->WriteText(FPBtBase->ConvertDouble64(mask, value));
}
//
//------------------------------------------------------------------------
// Bt - Write - Specials
//------------------------------------------------------------------------
Boolean CInterfaceBt::WriteComment(void)
{
  return FPBtBase->Write(TERMINAL_COMMENT[0]);
}
Boolean CInterfaceBt::WriteComment(String comment)
{
  sprintf(FPBtBase->GetTXBlock(), "%s%s", TERMINAL_COMMENT, comment.c_str());
  return FPBtBase->WriteText(FPBtBase->GetTXBlock());
}
Boolean CInterfaceBt::WriteResponse(String text)
{  
  sprintf(FPBtBase->GetTXBlock(), "%s%s", TERMINAL_RESPONSE, text.c_str());
  return FPBtBase->WriteLine(FPBtBase->GetTXBlock());
}

Boolean CInterfaceBt::WriteEvent(const char* event)
{
  sprintf(FPBtBase->GetTXBlock(), "%s%s", TERMINAL_EVENT, event);
  return FPBtBase->WriteLine(FPBtBase->GetTXBlock());
}
Boolean CInterfaceBt::WriteEvent(const char* mask, const char* event)
{  
  sprintf(FPBtBase->GetTXBlock(), mask, event);
  FPBtBase->WriteText((char*)TERMINAL_EVENT);
  return FPBtBase->WriteLine(FPBtBase->GetTXBlock());
}
//
//-------------------------------------------------------------------
//  Bt - Execute
//-------------------------------------------------------------------
// Boolean CInterfaceBt::Execute(void)
// {
//   // while (0 < GetRxCount())
//   // {
//   //   Char C = F
//   //   switch (C)
//   //   {
//   //     case (byte)SEPARATOR_CR:
//   //     case (byte)SEPARATOR_LF:
//   //       FRxdBufferIndex, SEPARATOR_ZERO);
//   //       FRxdBufferIndex = 0; // restart
//   //       strupr(FRxdBuffer);
//   //       strcpy(FCommandText, FRxdBuffer);
//   //       return true;
//   //     default: 
//   //       FRxdBuffer[FRxdBufferIndex] = C;
//   //       FRxdBufferIndex++;
//   //       break;
//   //   }
//   // }  
// }
//
#endif // INTERFACE_BT
//