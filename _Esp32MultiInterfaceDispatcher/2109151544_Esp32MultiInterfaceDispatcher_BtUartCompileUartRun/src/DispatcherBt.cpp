//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_BT)
//
#include "Error.h"
#include "DispatcherMqtt.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BTTT)
#include "InterfaceBttt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
#include "DispatcherBt.h"
//
extern CError Error;
extern CCommand Command;
// 
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_BTTT)
extern CInterfaceBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CInterfaceWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CInterfaceLan LanCommand;
#endif
#if defined(PROTOCOL_MQTT)
extern CProtocolMqtt MqttCommand;
#endif
//
//#########################################################
//  Dispatcher - Bt - Constructor
//#########################################################
CDispatcherBt::CDispatcherBt(void)
{
}
//
//#########################################################
//  Dispatcher - Mqtt - Execution
//#########################################################
bool CDispatcherBt::ExecuteWriteCommandBt(char* command, 
                                          int parametercount, 
                                          char** parameters)
{ //  <command(used<WCM>)> <parameter0> <parameter1> <parameter2> <parameter3>
  ExecuteBegin();
  // Analyse parameters: p0, p1, p2, p3
  if (4 <= parametercount)
  {
    sprintf(Command.GetBuffer(), 
            "%s %s %s %s", parameters[0], parameters[1], parameters[2], parameters[3]);
  }
  else
  if (3 <= parametercount)
  {
    sprintf(Command.GetBuffer(), 
            "%s %s %s", parameters[0], parameters[1], parameters[2]);
  }
  else
  if (2 == parametercount)
  {
    sprintf(Command.GetBuffer(), "%s %s", parameters[0], parameters[1]);
  }
  else
  if (1 == parametercount)
  {
    sprintf(Command.GetBuffer(), "%s", parameters[0]);
  }
  else
  {
    sprintf(Command.GetBuffer(), "Missing Parameter!");
  }
  // Response:
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (0 < strlen(Command.GetBuffer()))
  {
    BtCommand.WriteLine(Command.GetBuffer());
  }
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Bt - Handler
//#########################################################
bool CDispatcherBt::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_WCB, command))
  {
    return ExecuteWriteCommandBt(command, parametercount, parameters);
  }
  return false;
}
//
bool CDispatcherBt::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_MQTT
