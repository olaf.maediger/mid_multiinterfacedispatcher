#include "DefinitionSystem.h"
//
#ifndef InterfaceBase_h
#define InterfaceBase_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
const Int16 INTERFACE_SIZE_DEVICEID   =  6;
const Int16 INTERFACE_SIZE_RXBLOCK    = 256;
const Int16 INTERFACE_SIZE_TXBLOCK    = 256;
//
const Int16 INTERFACE_SIZE_CONVERSION = 256;
//
const Boolean INTERFACE_INIT_RXECHO   = false;
//
//
//----------------------------------------------------
//  CInterfaceBase
//----------------------------------------------------
class CInterfaceBase
{
  protected:
  char FDeviceID[INTERFACE_SIZE_DEVICEID];
  char FRXBlock[INTERFACE_SIZE_RXBLOCK];
  Boolean FRXEcho;
  int  FRXIndex;
  char FTXBlock[INTERFACE_SIZE_TXBLOCK];
  char FCVBuffer[INTERFACE_SIZE_CONVERSION];
  //
  public:
  //
  // Constructor
  CInterfaceBase(const char* deviceid);
  //
  // Property
  const char* GetDevideID(void)
  {
    return FDeviceID;
  }
  char* GetRXBlock(void)
  {
    return FRXBlock;
  }
  char* GetTXBlock(void)
  {
    return FTXBlock;
  }
  void SetRXEcho(Boolean rxecho)
  {
    FRXEcho = rxecho;
  }
  Boolean GetRXEcho(void)
  {
    return FRXEcho;    
  }
  //
  // Helper
  char* ConvertPChar(const char* mask, char* value);
  char* ConvertString(const char* mask, String value);
  char* ConvertByte(const char* mask, Byte value);
  char* ConvertDual(const char* mask, UInt16 value);
  char* ConvertQuad(const char* mask, UInt32 value);
  char* ConvertInt16(const char* mask, Int16 value);
  char* ConvertUInt16(const char* mask, UInt16 value);
  char* ConvertInt32(const char* mask, Int32 value);
  char* ConvertUInt32(const char* mask, UInt32 value);
  char* ConvertFloat32(const char* mask, Float32 value);
  char* ConvertDouble64(const char* mask, Double64 value);
  // Handler
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  // Read
  virtual UInt8 GetRXCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
  // Write
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
};
//
#endif // InterfaceBase_h
//
//