//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#include "LaserScanner.h"
#include "InterfaceUart.h"
//
//-------------------------------------------------------------------------------
//  LaserScanner - Constructor
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(const char* pid, 
                             //!!!!!! CTriggerIn* ptriggerin, 
                             CTriggerOut* ptriggerout, 
                             CDacMcp4725* pdacx, CDacMcp4725* pdacy,
                             DOnEventLaserScanner oneventlaserscanner,
                             DOnEventTimeSpan oneventtimespan)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  //!!!!!!  FPTriggerIn = ptriggerin;
  FPTriggerOut = ptriggerout;
  FPDacX = pdacx;
  FPDacY = pdacy;
  FOnEvent = oneventlaserscanner;
  FState = slsUndefined;
  FPTimeSpan = new CTimeSpan("LSTS", oneventtimespan);
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Property
//-------------------------------------------------------------------------------
const char* CLaserScanner::StateText(EStateLaserScanner state)
{
  if (slsIdle == state) return LASERSCANNER_EVENT_IDLE;
  if (slsPLPMove == state) return LASERSCANNER_EVENT_PLPMOVE;
  if (slsPLPWait == state) return LASERSCANNER_EVENT_PLPWAIT;
  if (slsPLPPulse == state) return LASERSCANNER_EVENT_PLPPULSE;
  return LASERSCANNER_EVENT_UNDEFINED;
}
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}
void CLaserScanner::SetState(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s", FID, StateText(FState));
      FOnEvent(this, FBuffer);
    }
  }
}  
//
//-------------------------------------------------------------------------------
//  LaserScanner - Method
//-------------------------------------------------------------------------------
Boolean CLaserScanner::Open()
{
  FPTimeSpan->Open();
  // debug FPTimeSpan->StartPeriod(3000.0f);
  SetState(slsIdle);
  return true;
}
//
Boolean CLaserScanner::Close()
{
  SetState(slsUndefined);
  return true;
}
//
void CLaserScanner::Move(void)
{
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
}
void CLaserScanner::Wait(void)
{
  FPTimeSpan->StartPeriod(FDelay);
}
void CLaserScanner::Pulse(void)
{
//!!!!!!!!!!  FPTriggerOut->PulseActive(FPulsePeriod / 2);
}
//
Boolean CLaserScanner::PulseLaserPosition(UInt32 positionx, UInt32 positiony,                                           
                                          UInt32 pulseperiod, UInt32 pulsecount)
{
  FPositionX = positionx;
  FPositionY = positiony;
  FDelay = 3000;
  FPulsePeriod = pulseperiod;
  FPulseCount = pulsecount;
  SetState(slsPLPMove);
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Execute
//-------------------------------------------------------------------------------
void CLaserScanner::Execute(void)
{
  //Boolean TSResult = 
  FPTimeSpan->Execute();
  switch (FState)
  {
    case slsUndefined:
      return;
    case slsIdle:
      return;
    case slsPLPMove:
      Move();
      Wait();
      SetState(slsPLPWait);
      return;
    case slsPLPWait:
      if (FPTimeSpan->IsIdle())
      {
        SetState(slsPLPPulse);
      }
      return;
    case slsPLPPulse:
      SetState(slsIdle);
      return;    
  }
}
//
#endif // DISPATCHER_LASERSCANNER
//
