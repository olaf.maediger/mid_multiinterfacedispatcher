//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERPOSITION)
//
#include "DispatcherLaserPosition.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
extern CError Error;
extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
// extern CLed LedSystem;
//
//#########################################################
//  Dispatcher - LaserPosition - Constructor
//#########################################################
CDispatcherLaserPosition::CDispatcherLaserPosition(void)
{
}
//
//#########################################################
//  Dispatcher - LaserPosition - Execution
//#########################################################
bool CDispatcherLaserPosition::ExecutePulseLaserPosition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x> <y> <p> <c> 
  if (parametercount < 4)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PX = atoi(parameters[0]);
  int PY = atoi(parameters[1]);
  int PP = atoi(parameters[2]);
  int PW = atoi(parameters[3]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i %i", command, PX, PY, PP, PW);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserPosition::ExecuteAbortLaserposition(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserPosition::ExecutePulselaserRow(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <p> <c>
  if (parametercount < 2)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PP = atoi(parameters[0]);
  int PC = atoi(parameters[1]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i", command, PP, PC);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserPosition::ExecuteAbortLaserRow(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserPosition::ExecutePulseLaserColumn(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <p> <c>
  if (parametercount < 2)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PP = atoi(parameters[0]);
  int PC = atoi(parameters[1]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i", command, PP, PC);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLaserPosition::ExecuteAbortLaserColumn(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  //LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - LaserPosition - Handler
//#########################################################
bool CDispatcherLaserPosition::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_PLP, command))
  {
    return ExecutePulseLaserPosition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALP, command))
  {
    return ExecuteAbortLaserposition(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PLR, command))
  {
    return ExecutePulselaserRow(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALR, command))
  {
    return ExecuteAbortLaserRow(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PLC, command))
  {
    return ExecutePulseLaserColumn(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_ALC, command))
  {
    return ExecuteAbortLaserColumn(command, parametercount, parameters);
  }
  return false;
}
//
bool CDispatcherLaserPosition::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_LASERPOSITION
//
//####################################################################
//####################################################################
//####################################################################
//
