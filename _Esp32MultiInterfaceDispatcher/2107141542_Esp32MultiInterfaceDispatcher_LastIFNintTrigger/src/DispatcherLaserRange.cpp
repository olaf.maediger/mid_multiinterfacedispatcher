//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERRANGE)
//
#include "Error.h"
#include "DispatcherLaserRange.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
extern CError Error;
extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
// extern CLed LedSystem;
//
//#########################################################
//  Dispatcher - LaserRange - Constructor
//#########################################################
CDispatcherLaserRange::CDispatcherLaserRange(void)
{
}
//
//#########################################################
//  Dispatcher - LaserRange - Execution
//#########################################################
bool CDispatcherLaserRange::ExecuteGetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int PX = 0;
  sprintf(Command.GetBuffer(), "%s %i", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PX = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteGetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int PY = 0;
  sprintf(Command.GetBuffer(), "%s %i", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <py>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PY = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteGetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int XL = 0;
  int XH = 0;
  int DX = 0;
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <xl> <xh> <dx>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int XL = atoi(parameters[0]);
  int XH = atoi(parameters[1]);
  int DX = atoi(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteGetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int YL = 0;
  int YH = 0;
  int DY = 0;
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, YL, YH, DY);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <yl> <yh> <dy>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int YL = atoi(parameters[0]);
  int YH = atoi(parameters[1]);
  int DY = atoi(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, YL, YH, DY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteGetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int DM = 0;
  sprintf(Command.GetBuffer(), "%s %i", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <dm>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int DM = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteGetPulseWidth(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int PW = 0;
  sprintf(Command.GetBuffer(), "%s %i", command, PW);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLaserRange::ExecuteSetPulseWidth(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <pw>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PW = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, PW);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - LaserRange - Handler
//#########################################################
bool CDispatcherLaserRange::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_GPX, command))
  {
    return ExecuteGetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPX, command))
  {
    return ExecuteSetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GPY, command))
  {
    return ExecuteGetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPY, command))
  {
    return ExecuteSetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRX, command))
  {
    return ExecuteGetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRX, command))
  {
    return ExecuteSetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRY, command))
  {
    return ExecuteGetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRY, command))
  {
    return ExecuteSetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GDM, command))
  {
    return ExecuteGetDelayMotion(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SDM, command))
  {
    return ExecuteSetDelayMotion(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GPW, command))
  {
    return ExecuteGetPulseWidth(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPW, command))
  {
    return ExecuteSetPulseWidth(command, parametercount, parameters);
  } 
  return false;
}
//
bool CDispatcherLaserRange::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_LASER
//
//####################################################################
//####################################################################
//####################################################################
//
