//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "Trigger.h"

#include "InterfaceUart.h"

extern CUart UartCommand;
//
CTrigger::CTrigger(const char* pid, int pin, 
                   ETriggerDirection triggerdirection)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = false;
  FLevel = tlUndefined;
}

CTrigger::CTrigger(const char* pid, int pin, 
                   ETriggerDirection triggerdirection, 
                   bool inverted)
{
  FState = stUndefined;
  strcpy(FID, pid);
  FOnEvent = 0;
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = inverted;
  FLevel = tlUndefined;
}

void CTrigger::SetOnEvent(DOnEventTrigger onevent)
{
  FOnEvent = onevent;
}
//
void CTrigger::SetState(EStateTrigger state)
{
  // ??? if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {       
      sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, STATE_TRIGGER[FState]);
      FOnEvent(this, FBuffer);
      UartCommand.WriteLine(FBuffer);
    }
  }
}
//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

Boolean CTrigger::Open()
{
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  FLevel = tlPassive;  
  SetState(stPassive);
  return true;
}

Boolean CTrigger::Close()
{
  SetPassive();
  pinMode(FPin, INPUT);
  FLevel = tlUndefined;
  SetState(stPassive);
  return true;
}

void CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
}
//
void CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
}
//
bool CTrigger::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = timeout + millis();
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
bool CTrigger::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = timeout + millis();
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}
bool CTrigger::WaitForTime(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {  
    FTimeOut = (long unsigned)(timeout + millis());
    SetState(stWaitForTime);
    return true;
  }
  return false;
}
//
void CTrigger::Execute(void)
{
  switch (FState)
  {
    case stPassive:
      break;
    case stActive:
      break;
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            if (FOnEvent) 
            {
              sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, TRIGGER_EVENT_TIMEOUT);
              FOnEvent(this, FBuffer);
            }
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            if (FOnEvent) 
            {
              sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, TRIGGER_EVENT_TIMEOUT);
              FOnEvent(this, FBuffer);
            }
            SetState(stPassive);
          }
        }
      break;
    case stWaitForTime:
      sprintf(FBuffer, "%lu %lu", FTimeOut, millis());
      FOnEvent(this, FBuffer);
      delay(100);
      sprintf(FBuffer, "%lu %lu", FTimeOut, millis());
      FOnEvent(this, FBuffer);
      delay(100);
      sprintf(FBuffer, "%lu %lu", FTimeOut, millis());
      FOnEvent(this, FBuffer);
      delay(100);
      
      if (FTimeOut <= millis())// GetTime())
      {
        if (FOnEvent) 
        {
          sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, TRIGGER_EVENT_TIMEOUT);
          FOnEvent(this, FBuffer);
        }
        if (tlActive == GetLevel())
        {
          SetState(stActive);
        }
        else
        {
          SetState(stPassive);
        }
      }
      break;
    default:
      FState = stUndefined;
  }
}
//
#endif // DISPATCHER_TRIGGER
//
