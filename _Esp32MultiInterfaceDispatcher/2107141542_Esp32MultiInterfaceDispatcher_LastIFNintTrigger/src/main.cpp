//
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//
#include "DefinitionSystem.h"
#include "Error.h"
#include "Automation.h"
#include "Command.h"
#include "DefinitionPin.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#include "Dispatcher.h"
#if defined(DISPATCHER_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(DISPATCHER_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(DISPATCHER_UART)
#include "DispatcherUart.h"
#endif
#if defined(DISPATCHER_BT)
#include "DispatcherBt.h"
#endif
#if defined(DISPATCHER_WLAN)
#include "DispatcherWlan.h"
#endif
#if defined(DISPATCHER_LAN)
#include "DispatcherLan.h"
#endif
#if defined(DISPATCHER_SDCARD)
#include "DispatcherSDCard.h"
#endif
#if defined(DISPATCHER_MQTT)
#include "DispatcherMqtt.h"
#endif
#if defined(DISPATCHER_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
// #if defined(DISPATCHER_RTC)
// #include "DispatcherRtc.h"
// #endif
#if defined(DISPATCHER_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(DISPATCHER_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(DISPATCHER_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#if defined(DISPATCHER_TRIGGER)
#include "Trigger.h"
#include "DispatcherTrigger.h"
#endif
#if defined(DISPATCHER_LASERRANGE)
#include "DispatcherLaserRange.h"
#endif
#if defined(DISPATCHER_LASERPOSITION)
#if defined(DACX_INTERNAL) | defined(DACY_INTERNAL)
  #include "DacInternal.h"
#endif
#if defined(DACX_MCP4725) | defined(DACY_MCP4725)
#include "DacMcp4725.h"
#endif
#include "DispatcherLaserPosition.h"
#endif
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
//-----------------------------------------------------------
// Segment - Global Variables - Interface
//-----------------------------------------------------------
#if defined(INTERFACE_UART)
CUart UartCommand(&Serial);
// CUart UartProgram(&Serial1, PIN_UART0_RXD, PIN_UART0_TXD);
#endif
#if defined(INTERFACE_BT)
CBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
CLan LanCommand;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Protocol
//-----------------------------------------------------------
#if defined(PROTOCOL_SDCARD)
CSDCard SDCardProtocol;
// CSDCard SDCardProtocol(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
#if defined(PROTOCOL_MQTT)
CMqtt MqttProtocol;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Dispatcher
//-----------------------------------------------------------
CError        Error;
CAutomation   Automation;
CCommand      Command;
//
#if defined(DISPATCHER_COMMON)
CDispatcherCommon DispatcherCommon;
#endif
#if defined(DISPATCHER_SYSTEM)
CTimeRelative TimeRelativeSystem(TIMERELATIVE_ID, TIMERELATIVE_MESSAGE_ON);
CDispatcherSystem DispatcherSystem;
#endif
#if defined(DISPATCHER_UART)
CDispatcherUart DispatcherUart;
#endif
#if defined(DISPATCHER_BT)
CDispatcherBT DispatcherBT;
#endif
#if defined(DISPATCHER_WLAN)
CDispatcherWlan DispatcherWlan;
#endif
#if defined(DISPATCHER_LAN)
CDispatcherLan DispatcherLan;
#endif
#if defined(DISPATCHER_SDCARD)
CDispatcherSDCard DispatcherSDCard;
#endif
#if defined(DISPATCHER_MQTT)
CDispatcherMqtt DispatcherMqtt;
#endif
#if defined(DISPATCHER_LEDSYSTEM)
CLed LedSystem("LEDS", PIN_LEDSYSTEM, LEDSYSTEM_INVERTED); 
CDispatcherLedSystem DispatcherLedSystem;
#endif
#if defined(DISPATCHER_RTC)
CRTCInternal RTCInternal; // not here
CDispatcherRtc DispatcherRtc;
#endif
#if defined(DISPATCHER_NTP)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
CDispatcherNtp DispatcherNtp;
#endif
#if defined(DISPATCHER_WATCHDOG)
CDispatcherWatchDog DispatcherWatchDog;
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
CDispatcherI2CDisplay DispatcherI2CDisplay;
C2CDisplay I2CDisplay;
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
CMenu MenuSystem("MS");                       
#endif
#if defined(DISPATCHER_TRIGGER)
CLed LedLaser("LL", PIN_LEDLASER);//, LEDLASER_INVERTED);                 
CTrigger TriggerIn("TIS", PIN_TRIGGERIN_SYNCHRONIZE, tdInput, NOT_INVERTED);
// CTrigger TriggerOut("TOL", PIN_TRIGGEROUT_LASER, tdOutput, NOT_INVERTED);
CDispatcherTrigger DispatcherTrigger;
#endif
#if defined(DISPATCHER_LASERRANGE)
CDispatcherLaserRange DispatcherLaserRange;
#endif
#if defined(DISPATCHER_LASERPOSITION)
  #if (defined(DACX_INTERNAL) || defined(DACY_INTERNAL)) 
  CDacInternal DacInternal_XY(true, true);
  #endif
  #ifdef DACX_MCP4725
  //CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
  CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725A0_I2CADDRESS_DEFAULT);
  #endif
  #ifdef DACY_MCP4725
  //CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
  CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725A0_I2CADDRESS_DEFAULT);
  #endif
CDispatcherLaserPosition DispatcherLaserPosition;
#endif
//
//#################################################################
//  Global - Event
//#################################################################
void OnWriteHelp(void)
{
#if defined(INTERFACE_UART)
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine();
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_COMMON);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_H, SHORT_H); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPH, SHORT_GPH); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GSV, SHORT_GSV); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GHV, SHORT_GHV); 
  #endif
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_APE, SHORT_APE); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_RSS, SHORT_RSS); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WTR, SHORT_WTR); 
  #endif
  #if defined(DISPATCHER_UART)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_UART);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCU, SHORT_WCU);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_RCU, SHORT_RCU);
  #endif
  #if defined(DISPATCHER_BT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_BT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_WLAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WLAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_LAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_SDCARD)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SDCARD);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_MQTT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_MQTT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_LEDSYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LEDSYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GLS, SHORT_GLS);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSO, SHORT_LSO);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSF, SHORT_LSF);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSB, SHORT_LSB);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSA, SHORT_LSA);
  #endif
  #if defined(DISPATCHER_RTC)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_RTC);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_NTP)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_NTP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_WATCHDOG)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WATCHDOG);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_I2CDISPLAY)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_I2CDISPLAY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_TRIGGER)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_TRIGGER);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_STA, SHORT_STA);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_STP, SHORT_STP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GTL, SHORT_GTL);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WTA, SHORT_WTA);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WTP, SHORT_WTP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WFT, SHORT_WFT);
  #endif
  #if defined(DISPATCHER_LASERRANGE)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LASERRANGE);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPX, SHORT_GPX);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SPX, SHORT_SPX);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPY, SHORT_GPY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SPY, SHORT_SPY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GRX, SHORT_GRX);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SRX, SHORT_SRX);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GRY, SHORT_GRY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SRY, SHORT_SRY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GDM, SHORT_GDM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SDM, SHORT_SDM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPW, SHORT_GPW);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_SPW, SHORT_SPW);
  #endif
  #if defined(DISPATCHER_LASERPOSITION)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LASERPOSITION);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_PLP, SHORT_PLP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_ALP, SHORT_ALP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_PLR, SHORT_PLR);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_ALR, SHORT_ALR);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_PLC, SHORT_PLC);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_ALC, SHORT_ALC);
  #endif
  UartCommand.WriteLine(MASK_ENDLINE);
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN) 
#endif
#if defined(INTERFACE_LAN) 
#endif
#if defined(PROTOCOL_MQTT)
#endif
#if defined(PROTOCOL_SDCARD)
#endif
}
//
#if defined(DISPATCHER_TRIGGER)
void OnEventTrigger(CTrigger* trigger, const char* event)
{
  #if defined(INTERFACE_UART)
  UartCommand.WriteEvent(event);
  #endif
}
#endif
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
//
//
//#################################################################
//  Global - Setup - Main
//#################################################################
void setup() 
{ 
//####################################################
  Error.Open();
//----------------------------------------------------
// Interface - Uart
//----------------------------------------------------
#if defined(INTERFACE_UART)
  UartCommand.Open(115200);
  delay(300);
  UartCommand.SetRXEcho(UART_INIT_RXECHO);
  UartCommand.WriteText("\r\n# Uart-Connection[");
  if (UartCommand.GetRXEcho())
  {
    UartCommand.WriteText("RXEcho=true");  
  }
  else
  {
    UartCommand.WriteText("RXEcho=false");  
  }
  UartCommand.WriteLine("] established.");  
#endif
//
//----------------------------------------------------
// Interface - Bt
//----------------------------------------------------
#if defined(INTERFACE_BT)
#endif
//
//----------------------------------------------------
// Interface - Wlan
//----------------------------------------------------
#if defined(INTERFACE_WLAN)
#endif
//
//----------------------------------------------------
// Interface - Lan
//----------------------------------------------------
#if defined(INTERFACE_LAN)
#endif
//####################################################
//----------------------------------------------------
// Protocol - SDCard
//----------------------------------------------------
#if defined(PROTOCOL_SDCARD)
  SerialProgram.WriteLine("# Initializing SDCARD ...");    
  while (SDCard.Open(&SD) < 1)
  {
    SerialProgram.WriteLine(": Initializing SDCARD failed - Insert correct SDCARD[FAT32, initfile]!");
    delay(5000);
  }
  SerialProgram.WriteLine("# Initializing SD card done.");
  //
  Boolean Result = XmlFile.Open(&SD);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Initfile Not Found!");
    return;
  }
  //  
  SerialProgram.Write("# Reading Initfile: ");
  SerialProgram.WriteLine(XML_INITFILE);
  Result = XmlFile.ReadFile(XML_INITFILE);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Reading File!");
    return;
  }
  // SerialProgram.WriteLine("# Content XmlFile:");
  // Serial.println(XmlFile.GetText());
  //
  SerialProgram.WriteLine("# Parsing Initfile");
  Result = XmlFile.Parse();
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Parsing Xml!");
    return;
  }
  //
  SerialProgram.WriteLine("# Result Xml Query:");
  XMLNode* XmlRoot = XmlFile.GetRoot();
  if (!XmlRoot) return;
  //----------------------------------------------------------------------
  XMLElement* XmlWifi = XmlFile.GetElement(XmlRoot, "wifi");
  if (!XmlWifi) return;
  //
  XMLElement* XmlSSID = XmlFile.GetElement(XmlWifi, "ssid");
  if (!XmlSSID) return;
  SerialProgram.Write("# WifiSSID[");
  SerialProgram.Write(XmlSSID->GetText());
  SerialProgram.WriteLine("]");
  WifiSSID = XmlSSID->GetText();
  //
  XMLElement* XmlPASSWORD = XmlFile.GetElement(XmlWifi, "password");
  if (!XmlPASSWORD) return;
  SerialProgram.Write("# WifiPASSWORD[");
  SerialProgram.Write(XmlPASSWORD->GetText());
  SerialProgram.WriteLine("]");
  WifiPASSWORD = XmlPASSWORD->GetText();
  //----------------------------------------------------------------------
  XMLElement* XmlMQTTBroker = XmlFile.GetElement(XmlRoot, "mqttbroker");
  if (!XmlMQTTBroker) return;
  //
  XMLElement* XmlIPAddress = XmlFile.GetElement(XmlMQTTBroker, "ipaddress");
  if (!XmlIPAddress) return;
  SerialProgram.Write("# BrokerIPAddress[");
  SerialProgram.Write(XmlIPAddress->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPAddress = XmlIPAddress->GetText();
  //
  XMLElement* XmlIPPort = XmlFile.GetElement(XmlMQTTBroker, "ipport");
  if (!XmlIPPort) return;
  SerialProgram.Write("# BrokerIPPort[");
  SerialProgram.Write(XmlIPPort->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPPort = atoi(XmlIPPort->GetText());
  //----------------------------------------------------------------------
  // !!! Free Ram !!!
  XmlFile.Close();
  //----------------------------------------------------------------------
  // !!! !!! !!! !!! !!! !!!
  DispatcherFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!DispatcherFile.ParseFile(INIT_COMMANDFILE))
  {
    SerialProgram.WriteLine(": Error: Cannot Read Dispatcherfile!");
    return;
  }
  SerialProgram.Write("# DispatcherFile[");
  SerialProgram.Write(INIT_COMMANDFILE);
  SerialProgram.WriteLine("]:");
  bool DispatcherLoop = true;
  while (DispatcherLoop)
  {
    String Dispatcher = DispatcherFile.GetDispatcher();
    DispatcherLoop = (0 < Dispatcher.length());
    if (DispatcherLoop)
    {
      SerialProgram.Write("# Dispatcher[");
      SerialProgram.Write(Dispatcher.c_str());
      SerialProgram.WriteLine("]");
    }
  }
  DispatcherFile.Close();  
#endif
//
//----------------------------------------------------
// Protocol - Mqtt
//----------------------------------------------------
#if defined(PROTOCOL_MQTT)
  MQTTClient.Initialise(WifiSSID.c_str(), WifiPASSWORD.c_str());
  MQTTClient.Open(MQTTBrokerIPAddress.c_str(), MQTTBrokerIPPort); 
#endif
//####################################################
//----------------------------------------------------
// Dispatcher - Common
//----------------------------------------------------
  Command.Open();
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.SetOnWriteHelp(OnWriteHelp);
  DispatcherCommon.Open();
#endif
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Open();
  DispatcherSystem.Open();
#endif
#if defined(DISPATCHER_UART)
#endif
#if defined(DISPATCHER_BT)
#endif
#if defined(DISPATCHER_WLAN)
#endif
#if defined(DISPATCHER_LAN)
#endif
#if defined(DISPATCHER_SDCARD)
#endif
#if defined(DISPATCHER_MQTT)
#endif
#if defined(DISPATCHER_LEDSYSTEM)
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(50);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff(); 
#endif
#if defined(DISPATCHER_RTC)
  RTCInternal.Open();
#endif
#if defined(DISPATCHER_NTP)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  TimeAbsoluteSystem.Open();  
#endif
#if defined(DISPATCHER_WATCHDOG)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  MenuSystem.Open();
#endif
#if defined(DISPATCHER_TRIGGER)
  TriggerIn.SetOnEvent(OnEventTrigger);
  TriggerIn.Open(); // !!!!!!!!
  // !!!!! TriggerOut.SetOnEvent(OnEventTrigger);
  // TriggerOut.Open(); // !!!!!!!!
  DispatcherTrigger.Open();  
#endif
#if defined(DISPATCHER_LASERRANGE)
  LedLaser.Open();
  LedLaser.SetOff();
  DispatcherLaserRange.Open();
#endif
#if defined(DISPATCHER_LASERPOSITION)
  #if (defined(DACX_INTERNAL) || defined(DACY_INTERNAL)) 
  DacInternal_XY.Open();
  #endif  
  #ifdef DACX_MCP4725
  DacMcp4725_X.Open();
  #endif  
  #ifdef DACY_MCP4725
  DacMcp4725_Y.Open();
  #endif  
  DispatcherLaserPosition.Open();
#endif
//
//##############################################################
// Init Application...
//##############################################################
  Automation.Open();
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.WriteProgramHeader();
  DispatcherCommon.WriteHelp();
#endif
#if defined(DISPATCHER_UART)
  // Preparating first input:
  // ???? UartCommand.WritePrompt();
#endif
#if defined(DISPATCHER_BT)
#endif
#if defined(DISPATCHER_WLAN)
#endif
#if defined(DISPATCHER_LAN)
#endif
#if defined(DISPATCHER_SDCARD)
#endif
#if defined(DISPATCHER_MQTT)
#endif
}  
//
//#################################################################
//  Global - Loop - Main
//#################################################################
void loop()
{
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Wait_Execute();
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
  LedSystem.Blink_Execute(Command.GetBuffer());
#endif
#if defined(DISPATCHER_TRIGGER)
  DispatcherTrigger.Execute();
#endif
#if defined(DISPATCHER_LASERRANGE)
  DispatcherLaserRange.Execute();
#endif
#if defined(DISPATCHER_LASERPOSITION)
  DispatcherLaserPosition.Execute();
#endif
//
  if (Command.AnalyseInterfaceBlock())
  { 
    Boolean R = false;
    char* CC = Command.GetCommand();
    int CPC = Command.GetParameterCount();
    char** CPS = Command.GetParameters();
#if defined(DISPATCHER_COMMON)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherCommon.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_SYSTEM)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherSystem.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_UART)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherUart.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_BT)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherBt.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_WLAN)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherWlan.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_LAN)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherLan.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_SDCARD)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherSDCard.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_MQTT)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherMqtt.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherLedSystem.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_RTC)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherRtc.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_NTP)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherNtp.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_WATCHDOG)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherWatchdog.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_I2CDISPLAY)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherI2CDisplay.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_TRIGGER)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherTrigger.HandleInterface(CC, CPC, CPS);
    }
    TriggerIn.Execute();
    // TriggerOut.Execute();
#endif
#if defined(DISPATCHER_LASERRANGE)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherLaserRange.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_LASERPOSITION)
    if ((!R) && (!Error.IsActive()))
    {
      R = DispatcherLaserPosition.HandleInterface(CC, CPC, CPS);
    }
#endif
    if (!R)
    {
      Error.SetCode(ecUnknownCommand);
    }
    Error.Handle();
    Command.SetState(scIdle);
#if defined(INTERFACE_UART)
    // ???? UartCommand.WriteLinePrompt();
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
 } 
}
//
//######################################################################
//######################################################################
//######################################################################

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/* --> Automation:

  SerialProgram, MQTTClient);
// #if defined(COMMAND_SDCARD)
//   CommandH.andle(SerialProgram, SDCard);
// #endif // SDCARD_ISPLUGGED
//   Command.Handle(SerialProgram);
//   
// #endif // COMMAND_UART
//   // Preemptive Multitasking:
//   //------------------------------------------------------------------------
//   // NC TimeRelativeSystemus.Wait_Execute();
// #if defined(COMMAND_NTP)  
//   TimeRelativeSystem.Wait_Execute();
//   TimeAbsoluteSystem.Wait_Execute();
// #endif
// #if defined(COMMAND_I2CDISPLAY)
//   MenuSystem.Display_Execute();
// #endif
//   //
//   // ???? LedSystem.Blink_Execute(GlobalBuffer);  
*/


