 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include <Arduino.h>
//
//##############################################################
//  Definition - Project
//##############################################################
//
#define ARGUMENT_PROJECT    "MID - Esp32MultiInterfaceDispatcher"
#define ARGUMENT_SOFTWARE   "00V09lasbeta"
#define ARGUMENT_DATE       "210713"
#define ARGUMENT_TIME       "1055"
#define ARGUMENT_AUTHOR     "OMDevelop"
//
//##############################################################
//  Definition - Command
//##############################################################
//--------------------------------
//#undef INTERFACE_UART
#define INTERFACE_UART
#if defined(INTERFACE_UART)
#define ARGUMENT_UART_PORT       "Uart(Serial-USB)"
#define ARGUMENT_UART_PARAMETER  "115200, N, 8, 1"
#endif
//
#undef INTERFACE_BT
// #define INTERFACE_BT
#if defined(INTERFACE_BT)
#define ARGUMENT_BT_PORT         "Bt()"
#define ARGUMENT_BT_PARAMETER    "Bt-Parameter"
#endif
//
#undef INTERFACE_WLAN
// #define INTERFACE_WLAN
#if defined(INTERFACE_WLAN)
#define ARGUMENT_WLAN_PORT       "Wlan()"
#define ARGUMENT_WLAN_PARAMETER  "Wlan-Parameter"
#endif
//
#undef INTERFACE_LAN
// #define INTERFACE_LAN
#if defined(INTERFACE_LAN)
#define ARGUMENT_LAN_PORT        "Lan()"
#define ARGUMENT_LAN_PARAMETER   "Lan-Parameter"
#endif
// //
// //-------------------------------
#undef PROTOCOL_SDCARD
// // !!!# define PROTOCOL_SDCARD
// // 
#undef PROTOCOL_MQTT
// // #define PROTOCOL_MQTT
// //
// //-------------------------------
// #undef DISPATCHER_COMMON
#define DISPATCHER_COMMON
// //
// #undef DISPATCHER_SYSTEM
#define DISPATCHER_SYSTEM
// 
// #undef DISPATCHER_UART
#define DISPATCHER_UART
//
// #undef DISPATCHER_BT
// #define DISPATCHER_BT
// //
// #undef DISPATCHER_WLAN
// #define DISPATCHER_WLAN
// //
// #undef DISPATCHER_LAN
// #define DISPATCHER_LAN
// //
// #undef DISPATCHER_SDCARD
// #define DISPATCHER_SDCARD
// //
// #undef DISPATCHER_MQTT
// #define DISPATCHER_MQTT
//
// #undef DISPATCHER_RTC
// // !!! #define DISPATCHER_RTC
// //
// #undef DISPATCHER_NTP
// // !!! #define DISPATCHER_NTP
// //
// #undef DISPATCHER_WATCHDOG
// // !!! #define DISPATCHER_WATCHDOG
// //
// #undef DISPATCHER_I2CDISPLAY
// // !!! #define DISPATCHER_I2CDISPLAY
//
// #undef DISPATCHER_LEDSYSTEM
#define DISPATCHER_LEDSYSTEM
//
// #undef DISPATCHER_TRIGGER
#define DISPATCHER_TRIGGER
//
// #undef DISPATCHER_LASERRANGE
#define DISPATCHER_LASERRANGE
//
// #undef DISPATCHER_LASERPOSITION
#define DISPATCHER_LASERPOSITION
//
// Defines : Hardware specific 
#if defined(DISPATCHER_TRIGGER)
    #define TRIGGEROUT_LASER
//  #undef  TRIGGEROUT_LASER
    // 
    #define TRIGGERIN_SYNCHRONIZE
//  #undef  TRIGGERIN_SYNCHRONIZE
#endif
//
#if defined(DISPATCHER_LASERPOSITION)
    // #define DACX_INTERNAL // JIHLEMANN
    #undef  DACX_INTERNAL // JIHLEMANN
    //
    // #define DACY_INTERNAL // JIHLEMANN
    #undef  DACY_INTERNAL // JIHLEMANN
    // PSIMON
    #define DACX_MCP4725 // PSIMON
    // #undef DACX_MCP4725 // PSIMON
    //
    // #define DACY_MCP4725 // PSIMON
    #undef  DACY_MCP4725 // PSIMON
#endif

// //-------------------------------
//
//##############################################################
//  Definition - Processor
//##############################################################
//----------------------------------
// ARDUINO NANOR3 :
// #define PROCESSOR_NANOR3
//----------------------------------
//----------------------------------
// ARDUINO UNOR3 :
// #define PROCESSOR_UNOR3
//----------------------------------
// Arduino Mega2560
// #define PROCESSOR_MEGA2560
//----------------------------------
//----------------------------------
// Arduino DueM3
// #define PROCESSOR_DUEM3
//----------------------------------
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//----------------------------------
//----------------------------------
// STM32F407VG 
// #define PROCESSOR_STM32F407VG
//----------------------------------
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//----------------------------------
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//----------------------------------
//----------------------------------
// Esp8266
// #define PROCESSOR_ESP8266
//----------------------------------
//----------------------------------
// Esp32
// 
#define PROCESSOR_ESP32
//----------------------------------
//
//##############################################################
//  Definition - Output
//##############################################################
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_STM32F407VG
#define ARGUMENT_HARDWARE "Stm32F407VG"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
#ifdef PROCESSOR_ESP8266
#define ARGUMENT_HARDWARE "Esp8266"
#endif
#ifdef PROCESSOR_ESP32
#define ARGUMENT_HARDWARE "Esp32"
#endif
//
//##############################################################
//  Definition - Global
//##############################################################
//
#define TRUE                    1
#define FALSE                   0
#define INIT_ERRORCODE          ecNone
#define INIT_RXDECHO            false
#define NOT_INVERTED            false
#define INVERTED                true
//
#define SEPARATOR_ZERO          0x00
#define SEPARATOR_CR            0x0D
#define SEPARATOR_LF            0x0A
//
#define TERMINAL_NEWLINE        "\r\n"
#define TERMINAL_SPACE          " "
// ???????? #define TERMINAL_PROMPT         ">"
// ->
#define TERMINAL_COMMAND        "@"
// <-
#define TERMINAL_RESPONSE       "&"
#define TERMINAL_EVENT          "!"
#define TERMINAL_COMMENT        "#"
#define TERMINAL_DEBUG          "|"
#define TERMINAL_ERROR          "?"
#define TERMINAL_WARNING        "%"
#define TERMINAL_TEXT           "\""
//
// 
// //##########################################
// //  PROCESSOR_NANOR3
// //##########################################
// #if defined(PROCESSOR_NANOR3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_NANOR3
// //
// //##########################################
// //  PROCESSOR_UNOR3
// //##########################################
// #if defined(PROCESSOR_UNOR3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_UNOR3
// //
// //##########################################
// //  PROCESSOR_MEGA2560
// //##########################################
// #if defined(PROCESSOR_MEGA2560)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_MEGA2560
// //
// //##########################################
// //  PROCESSOR_DUEM3
// //##########################################
// #if defined(PROCESSOR_DUEM3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_DUEM3
// //
// //##########################################
// //  PROCESSOR_STM32F103C8
// //##########################################
// #if defined(PROCESSOR_STM32F103C8)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// // Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
// //
// // Forward-Declaration IRQ SlitPX
// // example: void ISREncoderSlitPXPulseA(void);
// // example: void ISREncoderSlitPXPulseB(void);
// //
// #define SERIALCOMMAND    Serial1   // CommunicationPC
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALPRINTER    Serial3   // Serial-Printer
// //
// #endif // PROCESSOR_STM32F103C8
// //
// //##########################################
// //  PROCESSOR_STM32F407VG
// //##########################################
// #if defined(PROCESSOR_STM32F407VG)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float32 float
// #define Double64 double
// //
// // Type - IRQ-Handler
// typedef void (*TInterruptFunction)(void);
// //
// // Definition - Serial 
// #define SERIALCOMMAND    Serial1   // CommunicationPC
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALPRINTER    Serial3   // Serial-Printer
// #define SERIALDEBUG4     Serial4   // Serial-Printer
// #define SERIALDEBUG5     Serial5   // Serial-Printer
// #define SERIALDEBUG6     Serial5   // Serial-Printer
// //
// #endif // PROCESSOR_STM32F103C8
// //
// //
// //##########################################
// //  PROCESSOR_TEENSY32
// //##########################################
// #if defined(PROCESSOR_TEENSY32)
// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// // Choose 72MHz and not 80MHz(Overclocked) !
// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 signed int 
// #define UInt16 unsigned int
// #define Int32 long signed int
// #define UInt32 long unsigned int
// #define Int64 long long signed int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_TEENSY32
// //
// ////##########################################
// ////  PROCESSOR_TEENSY36
// ////##########################################
// #if defined(PROCESSOR_TEENSY36)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_TEENSY36
// //
// //##########################################
// //  PROCESSOR_ESP8266
// //##########################################
// #if defined(PROCESSOR_ESP8266)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// // Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
// // Forward-Declaration IRQ Function
// // example: void ISREncoderSlitPXPulseA(void);
// //
// #define SERIALUSBPROGRAM SerialUSB // Serial-USBProgramming
// #define SERIALPRINTER    Serial1   // Serial-Printer
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALCOMMAND    Serial3   // CommunicationPC
// //
// #endif 
// // PROCESSOR_ESP8266
//
//##########################################
//  PROCESSOR_ESP32
//##########################################
#if defined(PROCESSOR_ESP32)
//------------------------------------------------------
// Segment - Global Type
//------------------------------------------------------
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int16_t
#define UInt16 uint16_t
#define Int32 int32_t
#define UInt32 uint32_t
#define Int64 int64_t
#define UInt64 uint64_t
#define Float32 float
#define Double64 double
//-------------------------------------------------
// // Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
// // Forward-Declaration IRQ Function
// void ISRMotorEncoderLM393Left(void);
// void ISRMotorEncoderLM393Right(void);
// //------------------------------------------------------
// // Segment - Global Constant
// //------------------------------------------------------
// // CommunicationPC == Serial
// #define SERIAL_PC  0
// // Serial-AxisXY
// #define SERIAL_XY  1
// // Serial-AxisZW
// #define SERIAL_ZW  2
// //-------------------------------------------------
// // SA7
// //#define WIFI_SSID                         "FritzBoxSA7"
// //#define WIFI_SSID                         "TPLink03"
// //#define MQTTBROKER_IPADDRESS              "192.168.178.76" // OMSA7MqttBroker

// #define WIFI_SSID                         "TPLink03"
// #define WIFI_PASSWORD                     "01234567890123456789"
// #define MQTTBROKER_IPADDRESS              "192.168.178.123" // RPiMqttBroker
// #define MQTTBROKER_PORT                   1883
// //
// //// LLG
// //#define WIFI_SSID                         "LLG-Guest"
// //#define WIFI_PASSWORD                     "llg-2017"
// //#define MQTTBROKER_IPADDRESS              "192.168.15.59"
// //#define MQTTBROKER_PORT                   1883
// //
// // !!!!!!!!! DONT FORGET THE PRESET SLASH : "/" !!!!!!!!!!!!!!!!!
// #define XML_INITFILE                     "/remoterobotcar.ini"
// //
// #define INIT_COMMANDFILE                 "/sdc.cmd"
// // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// //
// //-------------------------------------------------
// #if defined(NTP_ISPLUGGED)
// //
// #ifndef INCLUDE_NTP
// #define INCLUDE_NTP
// //
// #define IPADDRESS_NTPSERVER               "de.pool.ntp.org"
// //
// #endif // INCLUDE_NTPCLIENT
// //
// #endif // NTPCLIENT_ISPLUGGED
// //-------------------------------------------------
// #if defined(MQTTCLIENT_ISPLUGGED)
// //
// // MQTT - Data
// #define TOPIC_COMMAND                     "/Command"
// #define TOPIC_TEXT                        "/Text"
// #define TOPIC_EVENT                       "/Event"
// #define TOPIC_RESPONSE                    "/Response"
// #define TOPIC_COMMENT                     "/Comment"
// #define TOPIC_DEBUG                       "/Debug"
// //
// #define EVENT_BROKERCONNECTED             "Broker connected"
// #define EVENT_BROKERDISCONNECTED          "Broker disconnected"
// //
// #endif // MQTTCLIENT_ISPLUGGED
// //
// //-------------------------------------------------
// #if defined(MOTORVNH2SP30_ISPLUGGED)
// // Motor - Left
// const Byte   PWMCHANNEL_MOTORLEFT           =     0;  // [0, 1, 15]
// const UInt32 PWMFREQUENCY_MOTORLEFT         = 10000;  // [Hz]
// const Byte   PWMRESOLUTION_MOTORLEFT        =     8;  // [bit]
// const UInt16 PWMHIGH_MOTORLEFT              =   250;  // [1]
// const UInt16 PWMLOW_MOTORLEFT               =    60;  // [1]
// //
// // Motor - Right
// const Byte   PWMCHANNEL_MOTORRIGHT          =     1;  // [0, 1, 15]
// const UInt32 PWMFREQUENCY_MOTORRIGHT        = 10000;  // [Hz]
// const Byte   PWMRESOLUTION_MOTORRIGHT       =     8;  // [bit]
// const UInt16 PWMHIGH_MOTORRIGHT             =   250;  // [1]
// const UInt16 PWMLOW_MOTORRIGHT              =    60;  // [1]
// //
// #endif // MOTORVNH2SP30_ISPLUGGED
// //
// //-------------------------------------------------
// #if defined(REMOTEWIRELESSSWITCH_ISPLUGGED)
// //const int 
// #endif // REMOTEWIRELESSSWITCH_ISPLUGGED
// //
// //-------------------------------------------------
// #if defined(DACMCP4725_ISPLUGGED)
// const int I2CADDRESS_POSITIONX = 0x62;
// const int I2CADDRESS_POSITIONY = 0x63;
// #endif 
//
//
//
#endif // PROCESSOR_ESP32
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
#endif // Defines_h
