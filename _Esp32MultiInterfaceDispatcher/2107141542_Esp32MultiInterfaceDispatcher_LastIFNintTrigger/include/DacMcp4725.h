//
//--------------------------------
//  Library Dac
//--------------------------------
//
#ifndef DacMcp4725_h
#define DacMcp4725_h
//
#include <Wire.h>
#include "DefinitionSystem.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
class CDacMcp4725
{
  public:
// PSimon 1808301555_LASController_07V03_PS :
// const static Byte MCP4725_I2CADDRESS_DEFAULT   = 0x62;
// NC  const static Byte MCP4725_I2CADDRESS_HIGH      = 0x63;
// 1904091508_LASController_07V05_PSimon_LASBox :
  const static Byte MCP4725A0_I2CADDRESS_DEFAULT = 0x60;
// NC  const static Byte MCP4725A0_I2CADDRESS_HIGH    = 0x61;
// NC  const static Byte MCP4725A2_I2CADDRESS_DEFAULT = 0x64;
// NC  const static Byte MCP4725A2_I2CADDRESS_HIGH    = 0x65;
  private:
  const static Byte MCP4725_WRITEDAC             = 0x40;
  const static Byte MCP4725_WRITEDACEEPROM       = 0x20;

  private:
  Byte FI2CAddress;
  UInt32 FValue;
  
  public:
  CDacMcp4725(Byte i2caddress);
  Boolean Open();
  Boolean Close();
  UInt32 GetValue();
  void SetValue(UInt32 value);
};
//
#endif // DacMcp4725_h
