//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef DispatcherTrigger_h
#define DispatcherTrigger_h
//
#include "Error.h"
#include "Dispatcher.h"
#include "Trigger.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - Trigger - SHORT
// //----------------------------------------------------------------
#define SHORT_STA   "STA"
#define SHORT_STP   "STP"
#define SHORT_GTL   "GTL"
#define SHORT_WTA   "WTA"
#define SHORT_WTP   "WTP"
#define SHORT_WFT   "WFT"
//
//----------------------------------------------------------------
// Dispatcher - Trigger - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_TRIGGER " Help (Trigger):"
#define MASK_STA    " %-3s                : Set TriggerOut Active"
#define MASK_STP    " %-3s                : Set TriggerOut Passive"
#define MASK_GTL    " %-3s                : Get TriggerLevel"
#define MASK_WTA    " %-3s <t>            : Wait <t>ime{ms} Trigger Active"
#define MASK_WTP    " %-3s <t>            : Wait <t>ime{ms} Trigger Passive"
#define MASK_WFT    " %-3s <t>            : Wait for <t>ime{ms}"
//
//----------------------------------------------------------------
// Dispatcher - Trigger
//----------------------------------------------------------------
class CDispatcherTrigger : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherTrigger(void);
  //
  bool ExecuteSetTriggerActive(char* command, int parametercount, char** parameters);
  bool ExecuteSetTriggerPassive(char* command, int parametercount, char** parameters);
  bool ExecuteGetTriggerLevel(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerActive(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerPassive(char* command, int parametercount, char** parameters);
  bool ExecuteWaitForTime(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool virtual Execute();
};
//
#endif // DispatcherLedSystem_h
//
#endif // DISPATCHER_TRIGGER
//
//###################################################################################
//###################################################################################
//###################################################################################
