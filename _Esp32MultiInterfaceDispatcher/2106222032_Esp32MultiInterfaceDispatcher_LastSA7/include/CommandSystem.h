//
#include "Defines.h"
//
#if defined(COMMAND_SYSTEM)
//
#ifndef CommandSystem_h
#define CommandSystem_h
// 
#include "Command.h"
//
class CCommandSystem : public CCommand
{

};
//
#endif
//
#endif // COMMAND_SYSTEM
//
//
// //
// #include "Defines.h"
// //
// #if defined(COMMAND_SYSTEM)
// //
// #ifndef CommandSystem_h
// #define CommandSystem_h
// //
// #include "Command.h"
// //
// //----------------------------------------------------------------
// // Help - Command - SHORT - Definition
// //----------------------------------------------------------------
// #define SHORT_GPH   "GPH"                   // GPH - Get Program Header
// #define SHORT_GSV   "GSV"                   // GSV - Get Software Version
// #define SHORT_GHV   "GHV"                   // GHV - Get Hardware Version
// #define SHORT_APE   "APE"                   // APE - Abort Process Execution
// #define SHORT_RSS   "RSS"                   // RSS - Reset System
// #define SHORT_WTR   "WTR"                   // WTR T        - WaitTimeRelative - overload from NTPClient/RTCInternal
// #define SHORT_WTA   "WTA"                   // WTA HH MM SS - WaitTimeAbsolute - overload from NTPClient/RTCInternal
// //
// //----------------------------------------------------------------
// // Help - Command - MASK - Definition
// //----------------------------------------------------------------
// #define HELP_COMMAND_SYSTEM       " Help (System):"
// #define MASK_GPH                  " %-3s                  : Get Program Header"
// #define MASK_GSV                  " %-3s                  : Get Software Version"
// #define MASK_GHV                  " %-3s                  : Get Hardware Version"
// #define MASK_APE                  " %-3s                  : <a>bort <p>rocess <e>xecution"
// #define MASK_RSS                  " %-3s                  : <r>e<s>et <s>ystem"
// #define MASK_WTR                  " %-3s <t>              : Wait Time Relative <t>ime{ms}"
// #define MASK_WTA                  " %-3s <hh> <mm> <ss>   : Wait Time Absolute <hh> <mm> <ss>"
// //

// {
//   private:
//   public:
//   CCommandSystem(void);
//   //
//   bool ExecuteGetProgramHeader(void);
//   bool ExecuteGetSoftwareVersion(void);
//   bool ExecuteGetHardwareVersion(void);
//   bool ExecuteAbortProcessExecution(void);
//   bool ExecuteResetSystem(void);
//   bool ExecuteWaitTimeRelative(void);
// #if defined(COMMAND_RTC) || defined(COMMAND_NTPCLIENT)
//   bool ExecuteWaitTimeAbsolute(void);
// #endif  
//   //
//   bool Execute(char* pcommand);
// };
// //
// #endif // Command_h
// //
// #endif // COMMAND_SYSTEM
// //  