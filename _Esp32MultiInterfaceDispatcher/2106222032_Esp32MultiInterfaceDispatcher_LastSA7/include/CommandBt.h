//
#include "Defines.h"
//
#if defined(COMMAND_BT)
//
#ifndef CommandBt_h
#define CommandBt_h
// 
#include "Command.h"
//
class CCommandBt : public CCommand
{

};
//
#endif
//
#endif // COMMAND_BT
//
//


// //
// #include "Defines.h"
// //
// #if defined(COMMAND_BT)
// //
// #ifndef CommandBt_h
// #define CommandBt_h
// //
// #include "Command.h"
// //
// //----------------------------------------------------------------
// // Help - Command - SHORT - Definition
// //----------------------------------------------------------------
// #define SHORT_WCB   "WCB"                   // WCB - Write <c>ommand Bt
// #define SHORT_RCB   "RCB"                   // RCB - Read Command Bt
// // 
// //----------------------------------------------------------------
// // Help - Command - MASK - Definition
// //----------------------------------------------------------------
// #define HELP_COMMAND_BT          " Help (Bt):"
// #define MASK_WCB                  " %-3s <c>              : Write <c>ommand Bt"
// #define MASK_RCB                  " %-3s                  : Read Command Bt"
// //
// class CCommandBt : public CCommand
// {

// };
// //
// #endif // CommandBt_h
// //
// #endif // COMMAND_BT
// //