//
#include "Defines.h"
//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Automation.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "CommandFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
#include "CommandCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "CommandSystem.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "CommandLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "CommandRtc.h"
#endif
#if defined(COMMAND_NTP)
#include "CommandNtp.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "CommandWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "CommandI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#include "Command.h"
//-------------------------------
//
//
//
//
//--------------------------------
#if defined(INTERFACE_UART)
extern CUart UartPC;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan Wlan;
#endif
#if defined(INTERFACE_LAN)
extern CLan Lan;
#endif
#if defined(INTERFACE_BT)
extern CBt Bt;
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
extern CSDCard SDCard;
#endif
#if defined(PROTOCOL_MQTT)
extern CMqtt Mqtt;
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
extern CCommand Command;
#endif
#if defined(COMMAND_SYSTEM)
extern CCommand Command;
#endif
#if defined(COMMAND_LEDSYSTEM)
extern CLed LedSystem;
#endif
#if defined(COMMAND_RTC)
extern CRtc RtcLocal;
#endif
#if defined(COMMAND_NTP)
extern CNtp NtpClient;
#endif
#if defined(COMMAND_WATCHDOG)
extern CWatchDog WatchDog;
#endif
#if defined(COMMAND_I2CDISPLAY)
extern 
#endif
//-------------------------------
//
//----------------------------------------------------------------
// Segment - Automation
//----------------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimePreset = 0L;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}
//
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  FTimePreset = 0L;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(void)
{ 
  delay(100);  
}

void CAutomation::WriteEvent(String text)
{
#if defined(INTERFACE_UART)
  UartPC.WriteText(text.c_str());
  UartPC.WriteLine();
  UartPC.WritePrompt();
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
}

void CAutomation::WriteEvent(String mask, int value)
{
#if defined(INTERFACE_UART)
  UartPC.WriteInt32(mask.c_str(), value);
  UartPC.WriteLine();
  UartPC.WritePrompt();
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
}

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(void)
{  //----------------------------
#if defined(INTERFACE_UART)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#endif
#if defined(PROTOCOL_MQTT)
  MQTTClient.RefreshConnection();
  MQTTClient.Execute();  
#endif
//-------------------------------
#if defined(COMMAND_COMMON)
#endif
#if defined(COMMAND_SYSTEM)
#endif
#if defined(COMMAND_LEDSYSTEM)
#endif
#if defined(COMMAND_RTC)
#endif
#if defined(COMMAND_NTP)
#endif
#if defined(COMMAND_WATCHDOG)
#endif
#if defined(COMMAND_I2CDISPLAY)
#endif
//-------------------------------
}
//
void CAutomation::HandleReset(void)
{
  delay(100);
  SetState(saIdle);
}
//
void CAutomation::Handle(void)
{
#if defined(COMMAND_WATCHDOG)
  WatchDog.Trigger();    
#endif
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle();
      break;
    case saReset:
      HandleReset();
      break;
    default: // saUndefined
      HandleUndefined();
      break;
  }  
}
