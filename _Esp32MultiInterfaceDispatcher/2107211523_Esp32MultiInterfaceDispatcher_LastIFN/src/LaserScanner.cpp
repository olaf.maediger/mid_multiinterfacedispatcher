//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#include "LaserScanner.h"
#include "InterfaceUart.h"
//
//-------------------------------------------------------------------------------
//  LaserScanner - Constructor
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(const char* pid, CTrigger* ptrigger, 
                             CDacMcp4725* pdacx, CDacMcp4725* pdacy,
                             DOnEventLaserScanner oneventlaserscanner,
                             DOnEventTimeSpan oneventtimespan)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPTrigger = ptrigger;
  FPDacX = pdacx;
  FPDacY = pdacy;
  FOnEvent = oneventlaserscanner;
  FState = slsUndefined;
  FPTimeSpan = new CTimeSpan("LSTS", oneventtimespan);
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Property
//-------------------------------------------------------------------------------
const char* CLaserScanner::StateText(EStateLaserScanner state)
{
  if (slsIdle == state) return "Idle";
  if (slsPLPMove == state) return "PLPMove";
  if (slsPLPPulse == state) return "PLPPulse";
  return "Undefined";
}
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}
void CLaserScanner::SetState(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s", FID, StateText(FState));
      FOnEvent(this, FBuffer);
    }
  }
}  
//
//-------------------------------------------------------------------------------
//  LaserScanner - Method
//-------------------------------------------------------------------------------
Boolean CLaserScanner::Open()
{
  FPTimeSpan->Open();
  // debug FPTimeSpan->StartPeriod(3000.0f);
  SetState(slsIdle);
  return true;
}
//
Boolean CLaserScanner::Close()
{
  SetState(slsUndefined);
  return true;
}
//
Boolean CLaserScanner::PulseLaserPosition(UInt16 positionx, UInt16 positiony, 
                                          UInt32 pulseperiod, UInt32 pulsecount)
{
  FPositionX = positionx;
  FPositionY = positiony;
  FPulsePeriod = pulseperiod;
  FPulseCount = pulsecount;
  SetState(slsPLPMove);
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Execute
//-------------------------------------------------------------------------------
void CLaserScanner::Execute(void)
{
  //Boolean TSResult = 
  FPTimeSpan->Execute();
  switch (FState)
  {
    case slsUndefined:
    case slsEnd:
      return;
    case slsIdle:
      return;
    case slsPLPMove:
      SetState(slsPLPPulse);
      return;
    case slsPLPPulse:
      SetState(slsIdle);
      return;    
  }
}
//
#endif // DISPATCHER_LASERSCANNER
//
