//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "Trigger.h"

#include "InterfaceUart.h"

extern CUart UartCommand;
//
CTrigger::CTrigger(const char* pid, int pin, 
                   ETriggerDirection triggerdirection)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = false;
  FLevel = tlUndefined;
}

CTrigger::CTrigger(const char* pid, int pin, 
                   ETriggerDirection triggerdirection, 
                   bool inverted)
{
  FState = stUndefined;
  strcpy(FID, pid);
  FOnEvent = 0;
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = inverted;
  FLevel = tlUndefined;
}

void CTrigger::SetOnEvent(DOnEventTrigger onevent)
{
  FOnEvent = onevent;
}
//
void CTrigger::SetState(EStateTrigger state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, STATE_TRIGGER[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

Boolean CTrigger::Open()
{
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  SetLevel(tlPassive);
  SetState(stPassive);
  return true;
}

Boolean CTrigger::Close()
{
  SetPassive();
  SetLevel(tlPassive);
  pinMode(FPin, INPUT);
  SetState(stPassive);
  return true;
}
//
void CTrigger::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
bool CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
  return true;
}
//
bool CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
  return true;
}
//
bool CTrigger::PulseActive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlPassive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulseActive);
      return true;
    default:
      return false;
  }
}
//
bool CTrigger::PulsePassive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlActive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulsePassive);
      return true;
    default:
      return false;
  }
  return false;
}
//
bool CTrigger::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
//
bool CTrigger::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}
//
void CTrigger::Execute(void)
{  
  switch (FState)
  {
    case stActive:
      if ((tdInput == FDirection) && (tlPassive == GetLevel()))
      {
        SetState(stPassive);
      }
      break;
    case stPassive:
      if ((tdInput == FDirection) && (tlActive == GetLevel()))
      {
        SetState(stActive);
      }
      break;
    case stPulseActive:
      if (FTimeOut <= millis())
      {
        SetPassive();
      }
      break;
    case stPulsePassive:
      if (FTimeOut <= millis())
      {
        SetActive();
      }
      break;
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    default:
      SetState(stUndefined);
  }
}
//
#endif // DISPATCHER_TRIGGER
//
