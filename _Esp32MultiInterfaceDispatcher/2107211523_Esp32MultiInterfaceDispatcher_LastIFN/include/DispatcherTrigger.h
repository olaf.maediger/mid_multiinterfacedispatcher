//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef DispatcherTrigger_h
#define DispatcherTrigger_h
//
#include "Error.h"
#include "Dispatcher.h"
#include "Trigger.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - Trigger - SHORT
// //----------------------------------------------------------------
#define SHORT_STA   "STA"
#define SHORT_STP   "STP"
#define SHORT_PTA   "PTA"
#define SHORT_PTP   "PTP"
#define SHORT_GTL   "GTL"
#define SHORT_WTA   "WTA"
#define SHORT_WTP   "WTP"
//
//----------------------------------------------------------------
// Dispatcher - Trigger - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_TRIGGER " Help (Trigger):"
#define MASK_STA    " %-3s                 : Set TriggerOut Active"
#define MASK_STP    " %-3s                 : Set TriggerOut Passive"
#define MASK_PTA    " %-3s <t>             : Pulse TriggerOut Active <t>ime{ms}"
#define MASK_PTP    " %-3s <t>             : Pulse TriggerOut Passive <t>ime{ms}"
#define MASK_GTL    " %-3s                 : Get TriggerIn Level"
#define MASK_WTA    " %-3s <t>             : Wait TriggerIn Active <t>ime{ms}"
#define MASK_WTP    " %-3s <t>             : Wait TriggerIn Passive <t>ime{ms}"
//
//----------------------------------------------------------------
// Dispatcher - Trigger
//----------------------------------------------------------------
class CDispatcherTrigger : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherTrigger(void);
  //
  bool ExecuteSetTriggerOutActive(char* command, int parametercount, char** parameters);
  bool ExecuteSetTriggerOutPassive(char* command, int parametercount, char** parameters);
  bool ExecutePulseTriggerOutActive(char* command, int parametercount, char** parameters);
  bool ExecutePulseTriggerOutPassive(char* command, int parametercount, char** parameters);  
  bool ExecuteGetTriggerInLevel(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerInActive(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerInPassive(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
};
//
#endif // DispatcherLedSystem_h
//
#endif // DISPATCHER_TRIGGER
//
//###################################################################################
//###################################################################################
//###################################################################################
