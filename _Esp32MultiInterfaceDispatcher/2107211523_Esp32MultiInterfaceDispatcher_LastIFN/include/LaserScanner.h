//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Trigger.h"
#include "DacMcp4725.h"
#include "TimeSpan.h"
//
//--------------------------------
//  LaserScanner - Constant
//--------------------------------
const int LASERSCANNER_SIZE_ID                   = 6;
const int LASERSCANNER_SIZE_BUFFER               = 64;
//
const char* const LASERSCANNER_EVENT_MASK        = "LaserScanner[%s]=%s";
//
// const char* const TRIGGER_EVENT_UNDEFINED       = "Undefined";
// const char* const TRIGGER_EVENT_ACTIVE          = "Active";
// const char* const TRIGGER_EVENT_PASSIVE         = "Passive";
// const char* const TRIGGER_EVENT_PULSEACTIVE     = "PulseActive";
// const char* const TRIGGER_EVENT_PULSEPASSIVE    = "PulsePassive";
// const char* const TRIGGER_EVENT_WAITFORACTIVE   = "WaitForActive";
// const char* const TRIGGER_EVENT_WAITFORPASSIVE  = "WaitForPassive";
// const char* const TRIGGER_EVENT_TIMEOUT         = "TimeOut";
// //
// const char* const STATE_TRIGGER[] = {TRIGGER_EVENT_UNDEFINED, 
//                                      TRIGGER_EVENT_ACTIVE, TRIGGER_EVENT_PASSIVE,
//                                      TRIGGER_EVENT_PULSEACTIVE, TRIGGER_EVENT_PULSEPASSIVE,
//                                      TRIGGER_EVENT_WAITFORACTIVE, TRIGGER_EVENT_WAITFORPASSIVE};
//
//--------------------------------
//  LaserScanner - Type
//--------------------------------
class CLaserScanner;
typedef void (*DOnEventLaserScanner)(CLaserScanner* plaserscanner, const char* pevent);
//
enum EStateLaserScanner
{
  slsUndefined = 0,
  slsIdle = 1,
  slsPLPMove = 2,
  slsPLPPulse = 3,
  slsEnd = 4
};
//
class CLaserScanner
{
  private:
  EStateLaserScanner FState;
  char FID[LASERSCANNER_SIZE_ID];
  char FBuffer[LASERSCANNER_SIZE_BUFFER];
  DOnEventLaserScanner FOnEvent;
  CTrigger* FPTrigger;
  CDacMcp4725* FPDacX;
  CDacMcp4725* FPDacY;
  CTimeSpan* FPTimeSpan;
  UInt16 FPositionX, FPositionY;
  UInt32 FPulsePeriod, FPulseCount;
  //  
  public:
  // Constructor
  CLaserScanner(const char* pid, CTrigger* ptrigger, 
                CDacMcp4725* pdacx, CDacMcp4725* pdacy,
                DOnEventLaserScanner oneventlaserscanner,
                DOnEventTimeSpan oneventtimespan);
  // Property
  const char* StateText(EStateLaserScanner state);
  EStateLaserScanner GetState(void);
  void SetState(EStateLaserScanner state);
  // Method
  Boolean Open(void);
  Boolean Close(void);
  Boolean PulseLaserPosition(UInt16 positionx, UInt16 positiony, UInt32 pulseperiod, UInt32 pulsecount);
  //
  void Execute(void);
};
//
#endif // LaserScanner_h
//
#endif // DISPATCHER_LASERSCANNER
//
