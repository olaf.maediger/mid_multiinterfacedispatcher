//
#include "Defines.h"
//
#ifndef Command_h
#define Command_h
//
#include <string.h>
//
#include "Automation.h"
#include "Utilities.h"
#include "Serial.h"
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"// 
#endif
//
#define ARGUMENT_PROJECT    "MID - Esp32MultiInterfaceDispatcher"
#define ARGUMENT_SOFTWARE   "00V00"
#define ARGUMENT_DATE       "210510"
#define ARGUMENT_TIME       "0757"
#define ARGUMENT_AUTHOR     "OMDevelop"
#define ARGUMENT_PORT       "SerialProgram (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_STM32F407VG
#define ARGUMENT_HARDWARE "Stm32F407VG"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
#ifdef PROCESSOR_ESP8266
#define ARGUMENT_HARDWARE "Esp8266"
#endif
#ifdef PROCESSOR_ESP32
#define ARGUMENT_HARDWARE "Esp32"
#endif
// 
//----------------------------------------------------------------
// Help - Command - SHORT - Definition
//----------------------------------------------------------------
#if defined(COMMAND_COMMON)
#define SHORT_H     "H"                     // H - (this) Help
#endif
//
#if defined(COMMAND_SYSTEM)
#define SHORT_GPH   "GPH"                   // GPH - Get Program Header
#define SHORT_GSV   "GSV"                   // GSV - Get Software Version
#define SHORT_GHV   "GHV"                   // GHV - Get Hardware Version
#define SHORT_APE   "APE"                   // APE - Abort Process Execution
#define SHORT_RSS   "RSS"                   // RSS - Reset System
#define SHORT_WTR   "WTR"                   // WTR T        - WaitTimeRelative - overload from NTPClient/RTCInternal
#define SHORT_WTA   "WTA"                   // WTA HH MM SS - WaitTimeAbsolute - overload from NTPClient/RTCInternal
#endif
//
// HELP_COMMAND_UART
#if defined(COMMAND_UART)
#define SHORT_WCU   "WCU"                   // WCU - Write <c>ommand Uart 
#define SHORT_RCU   "RCU"                   // RCU - Read Command Uart 
#endif
//
// HELP_COMMAND_WLAN
#if defined(COMMAND_WLAN)
#define SHORT_WCW   "WCW"                   // WCW - Write <c>ommand Wlan
#define SHORT_RCW   "RCW"                   // RCW - Read Command Wlan
#endif
//
// HELP_COMMAND_LAN
#if defined(COMMAND_LAN)
#define SHORT_WCL   "WCL"                   // WCL - Write <c>ommand Lan
#define SHORT_RCL   "RCL"                   // RCL - Read Command Lan
#endif
//
// HELP_COMMAND_BT
#if defined(COMMAND_BT)
#define SHORT_WCB   "WCB"                   // WCB - Write <c>ommand Bt
#define SHORT_RCB   "RCB"                   // RCB - Read Command Bt
#endif
//
// HELP_COMMAND_SDCard
#if defined(COMMAND_SDCARD)
#define SHORT_OCF   "OCF"                   // OCF - Open Command<f>ile for writing"
#define SHORT_WCF   "WCF"                   // WCF - Write <c>ommand with <p>arameter(s) to File"
#define SHORT_CCF   "CCF"                   // CCF - Close Command<f>ile end writing"
#define SHORT_ECF   "ECF"                   // ECF - Execute Command<f>ile"
#define SHORT_ACF   "ACF"                   // ACF - Abort Execution Commandfile"
#endif
//
// HELP_COMMAND_MQTT
#if defined(COMMAND_MQTT)
#define SHORT_WCM   "WCM"                   // WCM - Write <c>ommand Mqtt
#define SHORT_RCM   "RCM"                   // RCM - Read Command Mqtt
#endif
//
// HELP_LEDSYSTEM
#if defined(COMMAND_LEDSYSTEM)
#define SHORT_GLS   "GLS"                   // GLS - Get LedSystem
#define SHORT_LSH   "LSH"                   // LSH - Set LedSystem High
#define SHORT_LSL   "LSL"                   // LSL - Set LedSystem Low
#define SHORT_BLS   "BLS"                   // BLS - Blink LedSystem <p>eriod{ms} <c>ount{1}
#endif
//
#if defined(COMMAND_RTC)
#define SHORT_SRD   "SRD"                   // SRD - Set <r>tc-Date <yy><mm><dd>
#define SHORT_GRD   "GRD"                   // GRD - Get <r>tc-Date <yy><mm><dd>
#define SHORT_SRT   "SRT"                   // SRT - Set <r>tc-Time <hh><mm><ss>
#define SHORT_GRT   "GRT"                   // GRT - Get <r>tc-Time <hh><mm><ss>
#endif
//
#if defined(COMMAND_NTPCLIENT)
#define SHORT_GND   "GND"                   // GND - Get Ntp-Date
#define SHORT_GNT   "GNT"                   // GNT - Get Ntp-Time
#endif
//
#if defined(COMMAND_WATCHDOG)
#define SHORT_PWD   "PWD"                   // PWD - Pulse WatchDog
#endif
//
// HELP_IC2DISPLAY
#if defined(COMMAND_I2CDISPLAY)
#define SHORT_CLD   "CLD"                   // CLD - Clear Display
#define SHORT_STD   "STD"                   // STD - Set Text Display <r>ow <c>olumn <t>ext
#endif
//
// 
//----------------------------------------------------------------
// Help - Command - MASK - Definition
//----------------------------------------------------------------
#define TITLE_LINE                "--------------------------------------------------"
#define MASK_PROJECT              "- Project:   %-35s -"
#define MASK_SOFTWARE             "- Version:   %-35s -"
#define MASK_HARDWARE             "- Hardware:  %-35s -"
#define MASK_DATE                 "- Date:      %-35s -"
#define MASK_TIME                 "- Time:      %-35s -"
#define MASK_AUTHOR               "- Author:    %-35s -"
#define MASK_PORT                 "- Port:      %-35s -"
#define MASK_PARAMETER            "- Parameter: %-35s -"
//
//-----------------------------------------------------------------------------------------
//
#if defined(COMMAND_COMMON)
#define HELP_COMMAND_COMMON       " Help (Common):"
#define MASK_H                    " %-3s                  : This Help"
#endif
//

#if defined(COMMAND_SYSTEM)
#define HELP_COMMAND_SYSTEM       " Help (System):"
#define MASK_GPH                  " %-3s                  : Get Program Header"
#define MASK_GSV                  " %-3s                  : Get Software Version"
#define MASK_GHV                  " %-3s                  : Get Hardware Version"
#define MASK_APE                  " %-3s                  : <a>bort <p>rocess <e>xecution"
#define MASK_RSS                  " %-3s                  : <r>e<s>et <s>ystem"
#define MASK_WTR                  " %-3s <t>              : Wait Time Relative <t>ime{ms}"
#define MASK_WTA                  " %-3s <hh> <mm> <ss>   : Wait Time Absolute <hh> <mm> <ss>"
#endif
//
#if defined(COMMAND_UART)
#define HELP_COMMAND_UART         " Help (Uart):"
#define MASK_WCU                  " %-3s <c>              : Write <c>ommand Uart"
#define MASK_RCU                  " %-3s                  : Read Command Uart"
#endif
//
#if defined(COMMAND_WLAN)
#define HELP_COMMAND_WLAN         " Help (Wlan):"
#define MASK_WCW                  " %-3s <c>              : Write <c>ommand Wlan"
#define MASK_RCW                  " %-3s                  : Read Command Wlan"
#endif
//
#if defined(COMMAND_LAN)
#define HELP_COMMAND_LAN          " Help (Lan):"
#define MASK_WCL                  " %-3s <c>              : Write <c>ommand Lan"
#define MASK_RCL                  " %-3s                  : Read Command Lan"
#endif
//
#if defined(COMMAND_BT)
#define HELP_COMMAND_BT          " Help (Bt):"
#define MASK_WCB                  " %-3s <c>              : Write <c>ommand Bt"
#define MASK_RCB                  " %-3s                  : Read Command Bt"
#endif
//
#if defined(COMMAND_SDCARD)
#define HELP_COMMAND_SDCARD       " Help (SDCard):"
#define MASK_OCF                  " %-3s <f>              : Open Command<f>ile for writing"
#define MASK_WCF                  " %-3s <c> <p> ..       : Write <c>ommand with <p>arameter(s) to File"
#define MASK_CCF                  " %-3s                  : Close Command<f>ile end writing"
#define MASK_ECF                  " %-3s <f>              : Execute Command<f>ile"
#define MASK_ACF                  " %-3s                  : Abort Execution Commandfile"
#endif
//
#if defined(COMMAND_MQTT)
#define HELP_COMMAND_MQTT         " Help (Command Mqtt):"
#define MASK_WCM                  " %-3s <l>              : Write Command Mqtt <l>ine"
#define MASK_RCM                  " %-3s                  : Read Command Mqtt"
#endif
//
#if defined(COMMAND_LEDSYSTEM)
#define HELP_COMMAND_LEDSYSTEM    " Help (LedSystem):"
#define MASK_GLS                  " %-3s                  : Get State LedSystem"
#define MASK_LSH                  " %-3s                  : Switch LedSystem On"
#define MASK_LSL                  " %-3s                  : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <n> <p> <w>      : Blink LedSystem <n>times <p>eriod{ms}/<w>ith{ms}"
#endif
//
#if defined(COMMAND_RTC)
#define HELP_COMMAND_RTC          " Help (Rtc):"
#define MASK_SRD                  " %-3s <yy> <mm> <dd>   : Set <r>tc-Date <yy><mm><dd>"
#define MASK_GRD                  " %-3s                  : Get <r>tc-Date"
#define MASK_SRT                  " %-3s <hh> <mm> <ss>   : Set <r>tc-Time <hh><mm><ss>"
#define MASK_GRT                  " %-3s                  : Get <r>tc-Date"
#endif
//
#if defined(COMMAND_NTPCLIENT)
#define HELP_COMMAND_NTPCLIENT    " Help (NtpClient):"
#define MASK_GND                  " %-3s                  : Get NTPClient Date {yy.mm.dd}"
#define MASK_GNT                  " %-3s                  : Get NTPClient Time {hh:mm:ss}"
#endif
//
#if defined(COMMAND_WATCHDOG)
#define HELP_COMMAND_WATCHDOG     " Help (WatchDog):"
#define MASK_PWD                  " %-3s                  : Pulse WatchDog"
#endif
//
#if defined(COMMAND_I2CDISPLAY)
#define HELP_COMMAND_I2CDISPLAY   " Help (I2CDisplay):"
#define MASK_CLD                  " %-3s                  : <cl>ear <d>isplay"
#define MASK_STD                  " %-3s <r> <c> <t>      : Show <t>ext at <r>ow <c>olumn"
#endif
//
//
#define MASK_ENDLINE " ###"
//
//-----------------------------------------------------------------------------------------
#if defined(COMMAND_UART)
//
// Command-Parameter
#define SIZE_TXDBUFFER 32
#define SIZE_RXDBUFFER 32
#define SIZE_RESPONSEBUFFER 32
#define COUNT_TEXTPARAMETERS 5
#define SIZE_TEXTPARAMETER   8
//
#define MASK_STATEPROCESS         "STP %i %i"
//
//-----------------------------------------------------------------------------------------
const int COUNT_SOFTWAREVERSION = 1;
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      "IRQ-Version %s"
#define MASK_HARDWAREVERSION      "Hardware-Version %s"
//
#endif
//
//-----------------------------------------------------------------------------------------
enum EStateCommand
{
  scError     = -2,
  scUndefined = -1,
  scInit      = 0,
  scIdle      = 1,
  scBusy      = 2
};
//
class CCommand
{
  private:
  EStateCommand FState;
#if defined(COMMAND_UART)
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Int16 FRxdBufferIndex = 0;
  Character FCommandText[SIZE_RXDBUFFER];
  PCharacter FPCommand;
  PCharacter FPParameters[COUNT_TEXTPARAMETERS];
  Int16 FParameterCount = 0;
#endif
  //    
  public:
  CCommand(void);
  ~CCommand(void);
  //
  const char* StateText(EStateCommand state);
  //
  EStateCommand GetState(void);
  void SetState(EStateCommand state);
  //
  bool Open(void);
  bool Close(void);
  //
#if defined(COMMAND_UART)
  inline PCharacter GetRxdBuffer(void)
  {
    return FRxdBuffer;
  }
  inline PCharacter GetTxdBuffer(void)
  {
    return FTxdBuffer;
  }
  //
  inline PCharacter GetPCommand(void)
  {
    return FPCommand;
  }
  inline Byte GetParameterCount(void)
  {
    return FParameterCount;
  }
  inline PCharacter GetPParameters(UInt8 index)
  {
    return FPParameters[index];
  } 
  //
  void ZeroRxdBuffer(void);
  void ZeroTxdBuffer(void);
  void ZeroCommandText(void);
#endif
  //
  void AbortProcessExecution(void);
  //
#if defined(COMMAND_UART)
  //  Segment - Execution - Helper
  void WriteHelp(CSerial &serial);
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
#endif
  //
#if defined(COMMAND_UART) && defined(COMMAND_COMMON)
  //  Segment - Execution - Common
  bool ExecuteGetHelp(CSerial &serial);
#endif
  //
#if defined(COMMAND_UART) && defined(COMMAND_SYSTEM)  
  //  Segment - Execution - System
  bool ExecuteGetProgramHeader(CSerial &serial);
  bool ExecuteGetSoftwareVersion(CSerial &serial);
  bool ExecuteGetHardwareVersion(CSerial &serial);
  //
  bool ExecuteAbortProcessExecution(CSerial &serial);
  bool ExecuteResetSystem(CSerial &serial); 
  bool ExecuteWaitTimeRelative(CSerial &serial);
  bool ExecuteWaitTimeAbsolute(CSerial &serial);
#endif
  //
  //  Segment - Execution - Uart
#if defined(COMMAND_UART)
  bool ExecuteWriteCommandUart(CSerial &serial);
  bool ExecuteReadCommandUart(CSerial &serial);
#endif
  //
#if defined(COMMAND_UART) && defined(COMMAND_WLAN)
  bool ExecuteWriteCommandWlan(CSerial &serial);
  bool ExecuteReadCommandWlan(CSerial &serial);
#endif
  //
#if defined(COMMAND_UART) && defined(COMMAND_LAN)
  bool ExecuteWriteCommandLan(CSerial &serial);
  bool ExecuteReadCommandLan(CSerial &serial);
#endif
  //
#if defined(COMMAND_UART) && defined(COMMAND_BT)
  bool ExecuteWriteCommandBt(CSerial &serial);
  bool ExecuteReadCommandBt(CSerial &serial);
#endif
  //
  //  Segment - Execution - SDCard
#if defined(COMMAND_UART) && defined(COMMAND_SDCARD)
  bool ExecuteOpenCommandFile(CSerial &serial);
  bool ExecuteWriteCommandFile(CSerial &serial);
  bool ExecuteCloseCommandFile(CSerial &serial);
  bool ExecuteExecuteCommandFile(CSerial &serial);
  bool ExecuteAbortCommandFile(CSerial &serial);
#endif
  //
  //  Segment - Execution - Mqtt
#if defined(COMMAND_UART) && defined(COMMAND_MQTT)
#endif
  //
  //  Segment - Execution - LedSystem
#if defined(COMMAND_UART) && defined(COMMAND_LEDSYSTEM)
  bool ExecuteGetLedSystem(CSerial &serial);
  bool ExecuteLedSystemOn(CSerial &serial);
  bool ExecuteLedSystemOff(CSerial &serial);
  bool ExecuteBlinkLedSystem(CSerial &serial);
#endif
  //
  //  Segment - Execution - Rtc
#if defined(COMMAND_UART) && defined(COMMAND_RTC)
  bool ExecuteSetRtcDate(CSerial &serial);
  bool ExecuteGetRtcDate(CSerial &serial);
  bool ExecuteSetRtcTime(CSerial &serial);
  bool ExecuteGetRtcTime(CSerial &serial);
#endif
  //
  //  Segment - Execution - NTPClient
#if defined(COMMAND_UART) && defined(COMMAND_NTPCLIENT)
  bool ExecuteGetNTPClientDate(CSerial &serial);
  bool ExecuteGetNTPClientTime(CSerial &serial);
#endif
  //
  //  Segment - Execution - Watchdog
#if defined(COMMAND_UART) && defined(COMMAND_WATCHDOG)
  bool ExecutePulseWatchDog(CSerial &serial); 
#endif
  //
  //  Segment - Execution - I2CDisplay
#if defined(COMMAND_UART) && defined(COMMAND_I2CDISPLAY)
  bool ExecuteClearI2CDisplay(CSerial &serial); 
  bool ExecuteShowTextI2CDisplay(CSerial &serial);
#endif
  //
  //------------------------------------------------------
  // Main
  //------------------------------------------------------
  void Init();
  //
#if defined(COMMAND_UART)  
  bool AnalyseCommandText(CSerial &serial);
  //void WritePrompt(void);
  void WriteEvent(String line);
  void WriteResponse(String line);
  void WriteComment(String line);
  void WriteComment(String mask, String line);
  void DistributeCommand(void);
  bool DetectRxdLine(CSerial &serial);
#endif  
  //  
#if defined(COMMAND_SDCARD) 
  bool DetectSDCardLine(CSDCard &sdcard);
#endif  
#if defined(COMMAND_MQTTCLIENT) 
  bool DetectMQTTClientLine(CMQTTClient &mqttclient);
#endif  
  //
#if defined(COMMAND_UART) 
  bool Handle(CSerial &serial);
  bool Execute(CSerial &serial);
#endif 
#if defined(COMMAND_SDCARD)
  bool Handle(CSerial &serial, CSDCard &sdcard);
#endif  
#if defined(COMMAND_MQTT)
  bool Handle(CSerial &serial, CMQTTClient &mqttclient);
#endif 
};
//
#endif // Command_h
