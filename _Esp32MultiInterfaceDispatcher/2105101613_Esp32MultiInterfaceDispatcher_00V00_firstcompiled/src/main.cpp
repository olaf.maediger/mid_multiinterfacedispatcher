//
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//
#include "Defines.h"
#include "Error.h"
#include "Automation.h"
#include "Utilities.h"
#include "TimeRelative.h"
#include "Serial.h"
//
//--------------------------------
#if defined(COMMAND_COMMON)
#include "CommandCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "CommandSystem.h"
#endif
#if defined(COMMAND_UART)
#include "CommandUART.h"
#endif
#if defined(COMMAND_WLAN)
#include "CommandWLAN.h"
#endif
#if defined(COMMAND_LAN)
#include "CommandLAN.h"
#endif
#if defined(COMMAND_BT)
#include "CommandBT.h"
#endif
#if defined(COMMAND_SDCARD)
#include "SDCard.h"
#include "XmlFile.h"
#include "CommandFile.h"
#endif
#if defined(COMMAND_MQTT)
#include "CommandMQTT.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "CommandLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "CommandRTC.h"
#endif
#if defined(COMMAND_NTP)
#include "CommandNTP.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "CommandWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "CommandI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
//
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
CError        Error;
// ??? CCommand      Command;
#if defined(SDCARD_ISPLUGGED)
CCommandFile  CommandFile;
#endif
CAutomation   Automation;
//
// debug: char GlobalBuffer[128];
// 
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - LedSystem
//-----------------------------------------------------------
#if defined(COMMAND_LEDSYSTEM)
CLed LedSystem(PIN_LEDSYSTEM, "LEDS", LEDSYSTEM_INVERTED); 
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Uart
//-----------------------------------------------------------
#if defined(COMMAND_UART)
CSerial SerialProgram(&Serial1, PIN_UART0_RXD, PIN_UART0_TXD);
// CSerial SerialProgram(&Serial1, PIN_UART1_RXD, PIN_UART1_TXD);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - WatchDog
//-----------------------------------------------------------
#if defined(COMMAND_WATCHDOG)
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - RTC
//-----------------------------------------------------------
#if defined(COMMAND_RTC)
CRTCInternal RTCInternal;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Time / NTPClient
//-----------------------------------------------------------
#if defined(COMMAND_SYSTEM)
CTimeRelative TimeRelativeSystem("TRSM", INIT_MESSAGE_ON);
#endif
//
#if defined(COMMAND_NTP)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - I2CDisplay
//-----------------------------------------------------------
#if defined(COMMAND_I2CDISPLAY)
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
CMenu MenuSystem("MS");                       
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - SDCARD
//-----------------------------------------------------------
//
#if defined(COMMAND_SDCARD)
CSDCard SDCard(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - MQTT
//-----------------------------------------------------------
#if defined(COMMAND_MQTT)
CMQTTClient MQTTClient;
#endif
//
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
//
//
//#################################################################
//  Global - Setup - Main
//#################################################################
void setup() 
{ //----------------------------------------------------
  // Device - IOPin
  //----------------------------------------------------
  // #if defined(WATCHDOG_ISPLUGGED)
  //   WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
  // #endif
  //----------------------------------------------------
  // Device - Led - LedSystem
  //----------------------------------------------------
#if defined(COMMAND_LEDSYSTEM)  
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(100);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff();
#endif  
//  
//----------------------------------------------------
// Device - Uart
//----------------------------------------------------
#if defined(COMMAND_UART)
  SerialProgram.Open(115200);
  SerialProgram.SetRxdEcho(RXDECHO_ON);
  delay(300);
  SerialProgram.WriteLine("\r\n# Serial-Connection: [...]");
#endif
  //
  //----------------------------------------------------
  // Device - RTC
  //----------------------------------------------------}
#if defined(COMMAND_RTC)
  RTCInternal.Open();
#endif
  //
  //---------------------------------------------------------
  // Device - NTPClient - TimeRelative/Absolute
  //---------------------------------------------------------
#if defined(COMMAND_NTP)
  TimeRelativeSystem.Open();
  //
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  TimeAbsoluteSystem.Open();  
#endif
  //
  //---------------------------------------------------------
  // Device - MQTT
  //---------------------------------------------------------
#if defined(COMMAND_MQTT)
  MQTTClient.Initialise(WifiSSID.c_str(), WifiPASSWORD.c_str());
  MQTTClient.Open(MQTTBrokerIPAddress.c_str(), MQTTBrokerIPPort); 
#endif 
  //
  //----------------------------------------------------
  // Device - SDCARD
  //----------------------------------------------------
#if defined(COMMAND_SDCARD)
  SerialProgram.WriteLine("# Initializing SDCARD ...");    
  while (SDCard.Open(&SD) < 1)
  {
    SerialProgram.WriteLine(": Initializing SDCARD failed - Insert correct SDCARD[FAT32, initfile]!");
    delay(5000);
  }
  SerialProgram.WriteLine("# Initializing SD card done.");
  //
  Boolean Result = XmlFile.Open(&SD);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Initfile Not Found!");
    return;
  }
  //  
  SerialProgram.Write("# Reading Initfile: ");
  SerialProgram.WriteLine(XML_INITFILE);
  Result = XmlFile.ReadFile(XML_INITFILE);
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Reading File!");
    return;
  }
  // SerialProgram.WriteLine("# Content XmlFile:");
  // Serial.println(XmlFile.GetText());
  //
  SerialProgram.WriteLine("# Parsing Initfile");
  Result = XmlFile.Parse();
  if (!Result) 
  {
    SerialProgram.WriteLine(": Error: Parsing Xml!");
    return;
  }
  //
  SerialProgram.WriteLine("# Result Xml Query:");
  XMLNode* XmlRoot = XmlFile.GetRoot();
  if (!XmlRoot) return;
  //----------------------------------------------------------------------
  XMLElement* XmlWifi = XmlFile.GetElement(XmlRoot, "wifi");
  if (!XmlWifi) return;
  //
  XMLElement* XmlSSID = XmlFile.GetElement(XmlWifi, "ssid");
  if (!XmlSSID) return;
  SerialProgram.Write("# WifiSSID[");
  SerialProgram.Write(XmlSSID->GetText());
  SerialProgram.WriteLine("]");
  WifiSSID = XmlSSID->GetText();
  //
  XMLElement* XmlPASSWORD = XmlFile.GetElement(XmlWifi, "password");
  if (!XmlPASSWORD) return;
  SerialProgram.Write("# WifiPASSWORD[");
  SerialProgram.Write(XmlPASSWORD->GetText());
  SerialProgram.WriteLine("]");
  WifiPASSWORD = XmlPASSWORD->GetText();
  //----------------------------------------------------------------------
  XMLElement* XmlMQTTBroker = XmlFile.GetElement(XmlRoot, "mqttbroker");
  if (!XmlMQTTBroker) return;
  //
  XMLElement* XmlIPAddress = XmlFile.GetElement(XmlMQTTBroker, "ipaddress");
  if (!XmlIPAddress) return;
  SerialProgram.Write("# BrokerIPAddress[");
  SerialProgram.Write(XmlIPAddress->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPAddress = XmlIPAddress->GetText();
  //
  XMLElement* XmlIPPort = XmlFile.GetElement(XmlMQTTBroker, "ipport");
  if (!XmlIPPort) return;
  SerialProgram.Write("# BrokerIPPort[");
  SerialProgram.Write(XmlIPPort->GetText());
  SerialProgram.WriteLine("]");
  MQTTBrokerIPPort = atoi(XmlIPPort->GetText());
  //----------------------------------------------------------------------
  // !!! Free Ram !!!
  XmlFile.Close();
  //----------------------------------------------------------------------
  // !!! !!! !!! !!! !!! !!!
  CommandFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!CommandFile.ParseFile(INIT_COMMANDFILE))
  {
    SerialProgram.WriteLine(": Error: Cannot Read Commandfile!");
    return;
  }
  SerialProgram.Write("# CommandFile[");
  SerialProgram.Write(INIT_COMMANDFILE);
  SerialProgram.WriteLine("]:");
  bool CommandLoop = true;
  while (CommandLoop)
  {
    String Command = CommandFile.GetCommand();
    CommandLoop = (0 < Command.length());
    if (CommandLoop)
    {
      SerialProgram.Write("# Command[");
      SerialProgram.Write(Command.c_str());
      SerialProgram.WriteLine("]");
    }
  }
  CommandFile.Close();  
#endif  
  //
  //----------------------------------------------------
  // Device - I2CDisplay
  //----------------------------------------------------
#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  //
  MenuSystem.Open();
#endif
  //
  //##############################################################
  //----------------------------------------------------
  // Device - Command
  //----------------------------------------------------
#if defined(COMMAND_UART)  
  Command.Open();
  Command.WriteProgramHeader(SerialProgram);
  Command.WriteHelp(SerialProgram);
  SerialProgram.WritePrompt();
#endif  
  //----------------------------------------------------
  // Device - Automation
  //----------------------------------------------------
  Automation.Open();
}  
//
//#################################################################
//  Global - Loop - Main
//#################################################################
void loop()
{
#if defined(COMMAND_UART)
  if (Error.Handle(SerialProgram))
  {
    Command.SetState(scIdle);
    //SerialProgram.WritePrompt();
  }
#if defined(COMMAND_MQTT)
  Command.Handle(SerialProgram, MQTTClient);
#endif // MQTTCLIENT_ISPLUGGED
#if defined(COMMAND_SDCARD)
  Command.Handle(SerialProgram, SDCard);
#endif // SDCARD_ISPLUGGED
  Command.Handle(SerialProgram);
  Automation.Handle(SerialProgram); 
#endif // COMMAND_UART
  // Preemptive Multitasking:
  //------------------------------------------------------------------------
  // NC TimeRelativeSystemus.Wait_Execute();
#if defined(COMMAND_NTP)  
  TimeRelativeSystem.Wait_Execute();
  TimeAbsoluteSystem.Wait_Execute();
#endif
#if defined(COMMAND_I2CDISPLAY)
  MenuSystem.Display_Execute();
#endif
  //
  // ???? LedSystem.Blink_Execute(GlobalBuffer);  
}





