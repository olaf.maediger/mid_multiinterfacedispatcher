//
#include "Defines.h"
//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Led.h"
#include "Automation.h"
#include "Command.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#include "CommandFile.h"
#endif
#if defined(MOTORL298N_ISPLUGGED)
#include "MotorL298N.h"
#endif
#if defined(ENCODERIRQ_ISPLUGGED)
#include "EncoderIRQ.h"
#endif
#if defined(MOTORENCODER_ISPLUGGED)
#include "MotorEncoder.h"
#endif
//
//----------------------------------------------------------------
// Segment - Constant
//----------------------------------------------------------------
//
const int TIME_OUTPUT_MS = 1000; // [ms]
//
const char* FORMAT_EVENT_MOTORENCODERLM393 = "GEPB %li %li %i %i";
//
// ----------------------------------------------------------------
// Segment - Field
// ----------------------------------------------------------------
//
extern CCommand Command;
//
extern CLed LedSystem;
//
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
//
#if defined(MQTTCLIENT_ISPLUGGED)
extern CMQTTClient MQTTClient;
#endif
//
#if defined(RFIDCLIENT_ISPLUGGED)
extern CRFIDClient RFIDClient;
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
extern CMotorVNH2SP30 MotorLeft;
extern CMotorVNH2SP30 MotorRight;
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
extern CMotorEncoderLM393 MotorEncoderLM393Left;
extern CMotorEncoderLM393 MotorEncoderLM393Right;
#endif
//
#if defined(SDCARD_ISPLUGGED)
extern CSDCard      SDCard;
extern CCommandFile CommandFile;
#endif
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
extern CPinInput       SignalA0;//("LL", PIN_GROUPA_A0, LEVEL_INVERTED);
extern CPinInput       SignalA1;//("SP", PIN_GROUPA_A1, LEVEL_INVERTED);
extern CPinInput       SignalA2;//("IE", PIN_GROUPA_A2, LEVEL_INVERTED);
extern CPinInput       SignalB0;//("SC", PIN_GROUPB_B0);
extern CPinInput       SignalB1;//("SE", PIN_GROUPB_B1);
extern CPinInput       SignalB2;//("SO", PIN_GROUPB_B2);
extern CRF433MHzClient RF433MHzTransmit;//("WT", PIN_RF433MHZ_TRANSMIT);
extern CRemoteSwitch   RemoteSwitch;//("RS");  
#endif
//
//----------------------------------------------------------------
// Segment - Automation
//----------------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimePreset = 0L;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}
//
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  FTimePreset = 0L;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(CSerial &serial)
{ 
  delay(100);  
}

void CAutomation::WriteEvent(CSerial &serial, String text)
{
  serial.WriteLine(text.c_str());
  serial.WritePrompt();
}

void CAutomation::WriteEvent(CSerial &serial, String mask, int value)
{
  serial.Write(mask.c_str(), value);
  serial.Write("\r\n");
  serial.WritePrompt();
}

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(CSerial &serial)
{ 
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.RefreshConnection();
  MQTTClient.Execute();  
#endif
//
#if defined(RFIDCLIENT_ISPLUGGED)
  RFIDClient.Execute();
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
  if (TIME_OUTPUT_MS <= millis() - FTimePreset)
  { // debug LedSystem.Toggle();
    Int32 PL, DPL = 0L;
    Int32 PR, DPR = 0L;
    Boolean Changed = MotorEncoderLM393Left.GetPositionChanged(PL, DPL);
    Changed |= MotorEncoderLM393Right.GetPositionChanged(PR, DPR);
    if (Changed)
    {
      sprintf(SBuffer, FORMAT_EVENT_MOTORENCODERLM393, PL, PR, DPL, DPR);
      Command.WriteEvent(SBuffer);
    }   
    FTimePreset = millis();
  }
#endif // MOTORENCODERLM393_ISPLUGGED
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
  EPinInputLevel ILA0 = SignalA0.GetLevel();
  EPinInputLevel ILA1 = SignalA1.GetLevel();
  EPinInputLevel ILA2 = SignalA2.GetLevel();
  EPinInputLevel ILB0 = SignalB0.GetLevel();
  EPinInputLevel ILB1 = SignalB1.GetLevel();
  EPinInputLevel ILB2 = SignalB2.GetLevel();
  Boolean SO = RemoteSwitch.GetSwitchOn();
  sprintf(SBuffer, "SA[%i.%i.%i] SB[%i.%i.%i] SO[%i]", (int)ILA2, (int)ILA1, (int)ILA0, (int)ILB2, (int)ILB1, (int)ILB0, (int)SO);
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                Command.WriteEvent(SBuffer);
#endif
}

void CAutomation::HandleReset(CSerial &serial)
{
  delay(100);
  SetState(saIdle);
}


void CAutomation::Handle(CSerial &serial)
{
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Trigger();    
#endif 
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle(serial);
      break;
    case saReset:
      HandleReset(serial);
      break;
    default: // saUndefined
      HandleUndefined(serial);
      break;
  }  
}
