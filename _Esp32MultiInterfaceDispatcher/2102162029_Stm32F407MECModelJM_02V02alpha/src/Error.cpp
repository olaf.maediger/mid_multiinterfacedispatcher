#include "Defines.h"
//
#include "Error.h"
//
#ifdef SERIALCOMMAND_ISPLUGGED
#include "Serial.h"
#endif
#ifdef MQTTCLIENT_ISPLUGGED
#include "MQTTClient.h"
#endif
//
#ifdef SERIALCOMMAND_ISPLUGGED
extern CSerial SerialCommand;
#endif
#ifdef MQTTCLIENT_ISPLUGGED
extern CMQTTClient MQTTClient;
#endif
//
CError::CError(void)
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode(void)
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open(void)
{
  FErrorCode = ecNone;
  return true; 
}
  
Boolean CError::Close(void)
{
  FErrorCode = ecNone;
  return true; 
}

const char* CError::ErrorCodeText(EErrorCode errorcode)
{
  switch (errorcode)
  {
    case ecNone:
      return (const char*)"No Error";
    case ecInvalidCommand:
      return (const char*)"Invalid Command";
    case ecCommandTimingFailure:
      return (const char*)"Command Timing Failure";
    case ecCommandInvalidParameter:
      return (const char*)"Command invalid Parameter";
    case ecToManyParameters:
      return (const char*)"To Many Parameters";
    case ecNotEnoughParameters:
      return (const char*)"Not Enough Parameters";
    case ecMissingTargetParameter:
      return (const char*)"Missing Target Parameter";
    case ecFailMountard:
      return (const char*)"Fail Mount ard";    
    case ecFailOpenCommandFile:
      return (const char*)"Fail Open Command File";
    case ecFailParseCommandFile:
      return (const char*)"Fail Parse Command File";
    case ecFailWriteCommandFile:
      return (const char*)"Fail Write Command File";
    case ecFailCloseCommandFile:
      return (const char*)"Fail Close Command File";
    case ecFailUnmountard:
      return (const char*)"Fail Unmount ard";
    case ecCommandFileNotOpened:
      return (const char*)"Command File Not Opened";
    case ecInvalidAxis:
      return (const char*)"Invalid Axis";
    default: // ecUnknown:
      return "Unknown Error"; 
  }
}

Boolean CError::Handle(CSerial &serial)
{
  if (ecNone != FErrorCode)
  {  
    char Buffer[48];
    sprintf(Buffer, "%s Error[%i]: %s!", TERMINAL_ERROR, FErrorCode, ErrorCodeText(FErrorCode));
#ifdef SERIALCOMMAND_ISPLUGGED
    SerialCommand.Write(Buffer);
    SerialCommand.WriteNewLine();
    SerialCommand.WritePrompt();
#endif
//
#ifdef MQTTCLIENT_ISPLUGGED
    MQTTClient.Publish("/Error", Buffer);
#endif
    FErrorCode = ecNone;
    return true;
  }
  return false;
}
