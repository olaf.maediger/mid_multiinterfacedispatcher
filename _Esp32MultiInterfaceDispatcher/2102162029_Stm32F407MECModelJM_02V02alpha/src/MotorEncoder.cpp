//
#include "Defines.h"
//
#if defined(MOTORENCODER_ISPLUGGED)
//
#include "MotorEncoder.h"
//
#include "Serial.h"
//
extern CSerial SerialCommand;
//
CMotorEncoder::CMotorEncoder(String id, 
                             CMotorBase* pmotor, CEncoderBase* pencoder,
                             CLimitswitch* plimitswitchlow, CLimitswitch* plimitswitchhigh,
                             CLed* pledlimitlow, CLed* pledlimithigh,
                             UInt32 positionramp, UInt32 positionresolution)
{
  FID = id;
  FMotorActivity = INIT_MOTOR_ACTIVITY;
  FPMotor = pmotor;
  FPEncoder = pencoder;
  FPLimitswitchLow = plimitswitchlow;
  FPLimitswitchHigh = plimitswitchhigh;
  FPLedLimitLow = pledlimitlow;
  FPLedLimitHigh = pledlimithigh;
  FPositionRamp = positionramp;
  FPositionResolution = positionresolution;
  FStateMotorPreset = FPMotor->GetState();
  FPositionPreset = FPEncoder->GetPosition();
  FPWMActualPreset = INIT_PWM_MINIMUM;
}
//
Boolean CMotorEncoder::Open(void)
{
  FPMotor->Open();
  FPEncoder->Open();
  FPLimitswitchLow->Open();
  FPLimitswitchHigh->Open();
  FPLedLimitLow->Open();
  FPLedLimitHigh->Open();
  // not here FDeltaPositionResolution = INIT_DELTAPOSITION_RESOLUTION;
  // not here FDeltaPositionRamp = INIT_DELTAPOSITION_RAMP;
  FPositionTarget = 0;
  FPEncoder->SetPosition(0);
  FStateMotorPreset = FPMotor->GetState();
  FPositionPreset = FPEncoder->GetPosition();
  FTickPreset = millis();
  return true;
}
Boolean CMotorEncoder::Close(void)
{
  FPMotor->Close();
  FPEncoder->Close();
  FPLimitswitchLow->Close();
  FPLimitswitchHigh->Close();
  FPLedLimitLow->Close();
  FPLedLimitHigh->Close();
  // not here FPositionResolution = INIT_DELTAPOSITION_RESOLUTION;
  // not here FPositionRamp = INIT_DELTAPOSITION_RAMP;
  FPositionTarget = 0;
  FPEncoder->SetPosition(0);
  FStateMotorPreset = FPMotor->GetState();
  FPositionPreset = FPEncoder->GetPosition();
  return true;
}
//
EMotorActivity CMotorEncoder::GetMotorActivity(void)
{
  return FMotorActivity;
}
void CMotorEncoder::SetMotorActivity(EMotorActivity value)
{
  FMotorActivity = value;
}
//
Int32 CMotorEncoder::GetPositionActual(void)
{
  return FPEncoder->GetPosition();
}

void CMotorEncoder::SetPositionActual(Int32 position)
{
  FPEncoder->SetPosition(position);
}
//
Int32 CMotorEncoder::GetPositionTarget(void)
{
  return FPositionTarget;
}
void CMotorEncoder::SetPositionTarget(Int32 value)
{
  FPositionTarget = value;
}
//
Float32 CMotorEncoder::CalculatePwmActual(void)
{
  Float32 PA = FPEncoder->GetPosition();
  Float32 PT = FPositionTarget;
  Float32 DPTA = abs(PT - PA);
  Float32 PR = FPositionRamp;
  Float32 PWML = FPMotor->GetPwmLow();
  //Float32 PWMH = FPMotor->GetPwmHigh();
  Float32 PWMT = FPMotor->GetPwmTarget();
  //char SBuffer[64];
  //
  if (PR < DPTA)
  { // exterior : abs(PositionTarget - PositionActual) > FDeltaPositionRamp
    // exterior : DPTA > DPR ( > 0)
    return PWMT;
  }
  // interior : 0 <= FDeltaPositionRamp <= abs(PositionTarget - PositionActual)
  //            0 <= DPR <= DPT 
  Float32 PWMA = PWML + DPTA * (PWMT - PWML) / PR;
  // debug 
  // sprintf(SBuffer, "~~~CPA[%s]", FID.c_str());
  // SerialCommand.Write(SBuffer);
  // sprintf(SBuffer, " PWMT[%s]", FloatString(PWMT, 3));
  // SerialCommand.Write(SBuffer);
  // sprintf(SBuffer, " PWML[%s]", FloatString(PWML, 3));
  // SerialCommand.Write(SBuffer);
  // sprintf(SBuffer, " PWMH[%s]", FloatString(PWMH, 3));
  // SerialCommand.Write(SBuffer);
  //sprintf(SBuffer, " PWMA[%s]", FloatString(PWMA, 3));
  // SerialCommand.WriteLine("");
  // !!! task switch is necessary!!!
  delay(1);
  // !!! !!! !!! !!! !!! !!! !!! !!! 
  return PWMA;
}
//
//-----------------------------------------------------------------
// FreeRun / Controlled
//-----------------------------------------------------------------
void CMotorEncoder::StopMotion(void)
{
  FPMotor->StopMotion();
}
//
//-----------------------------------------------------------------
// Move FreeRun
//-----------------------------------------------------------------
void CMotorEncoder::MoveMotorPositive(Float32 pwm)
{
  FPMotor->SetPwmTarget(pwm);
  FPMotor->MovePositive(FPMotor->GetPwmTarget());
  SetMotorActivity(maFreeRun);
}

void CMotorEncoder::MoveMotorNegative(Float32 pwm)
{
  FPMotor->SetPwmTarget(pwm);
  FPMotor->MoveNegative(FPMotor->GetPwmTarget());
  SetMotorActivity(maFreeRun);
}
//
//-----------------------------------------------------------------
// Move Controlled
//-----------------------------------------------------------------
void CMotorEncoder::MovePositionAbsolute(Int32 position, Float32 pwm)
{
  FPMotor->SetPwmTarget(pwm);
  FPMotor->SetPwmActual(pwm);
  FPositionTarget = position;
  SetMotorActivity(maControlled);
  FPMotor->ForceStateBusy();
  //
  char SBuffer[64];
  sprintf(SBuffer, "=== MPA[%s]", FID.c_str());
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMT[%s]", FloatString(FPMotor->GetPwmTarget(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWML[%s]", FloatString(FPMotor->GetPwmLow(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMH[%s]", FloatString(FPMotor->GetPwmHigh(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMA[%s]", FloatString(FPMotor->GetPwmActual(), 3));
  SerialCommand.WriteLine(SBuffer);
}

void CMotorEncoder::MovePositionRelative(Int32 position, Float32 pwm)
{
  FPositionTarget += position;
  FPMotor->SetPwmTarget(pwm);
  FPMotor->SetPwmActual(pwm);
  SetMotorActivity(maControlled);
  FPMotor->ForceStateBusy();
  //
  char SBuffer[64];
  sprintf(SBuffer, "=== MPR[%s]", FID.c_str());
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMT[%s]", FloatString(FPMotor->GetPwmTarget(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWML[%s]", FloatString(FPMotor->GetPwmLow(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMH[%s]", FloatString(FPMotor->GetPwmHigh(), 3));
  SerialCommand.Write(SBuffer);
  sprintf(SBuffer, " PWMA[%s]", FloatString(FPMotor->GetPwmActual(), 3));
  SerialCommand.WriteLine(SBuffer);
}
//
//-----------------------------------------------------------------
// Execute Asynchrone
//-----------------------------------------------------------------
void CMotorEncoder::Execute(void)
{
  if (FPLimitswitchLow->IsActive())  
  {
    FPLedLimitLow->SetOn();
    delay(100);
  }
  else
  {
    FPLedLimitLow->SetOff();
  }
  FPLedLimitHigh->SetOff();
  if (FPLimitswitchHigh->IsActive())  
  {
    FPLedLimitHigh->SetOn();
    delay(100);
  }
  else
  {
    FPLedLimitHigh->SetOff();
  }
  //
  int MA = GetMotorActivity();
  if (maFreeRun == MA)
  {
  }
  else
  if (maControlled == MA)
  {
    if (FPLimitswitchLow->IsActive())  
    {
      FPLedLimitLow->SetOn();
      SerialCommand.WriteLine("Limitswitch[LOW] Active : StopMotion");
      FPMotor->StopMotion();
      delay(100);
    }
    if (FPLimitswitchHigh->IsActive())  
    {
      FPLedLimitHigh->SetOn();
      SerialCommand.WriteLine("Limitswitch[HIGH] Active : StopMotion");
      FPMotor->StopMotion();
      delay(100);
    }
    //
    Int32 PA = FPEncoder->GetPosition();
    Int32 PT = FPositionTarget;
    UInt32 DPTA = abs(PT - PA);
    EStateMotor SM = FPMotor->GetState();
    // !!! EMotorActivity MA = GetMotorActivity();
    UInt32 PR = FPositionResolution;
    //--------------------------------------
    // Controlling - Idle :
    //--------------------------------------
    if (smBusy == SM)
    { 
      if (PR < DPTA) 
      { 
        // debug char SBuffer[64];
        // debug Float32 PWML = FPMotor->GetPwmLow();
        // debug Float32 PWMH = FPMotor->GetPwmHigh();
        // debug Float32 PWMT = FPMotor->GetPwmTarget();
        Float32 PWMA = CalculatePwmActual();
        if (PA < PT)
        { // Idle -> Motion Positive
          if (FPWMActualPreset != PWMA)
          {
            FPWMActualPreset = PWMA;
            FPMotor->MovePositive(PWMA);
            // debug
            // sprintf(SBuffer, "! >>> MPWM %s +++ PWMA[%s]", FID.c_str(), FloatString(PWMA, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWMT[%s]", FloatString(PWMT, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWML[%s]", FloatString(PWML, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWMH[%s]", FloatString(PWMH, 3));
            // SerialCommand.WriteLine(SBuffer);
          }
        } else
        { // Idle -> Motion Negative
          if (FPWMActualPreset != PWMA)
          {
            FPWMActualPreset = PWMA;
            FPMotor->MoveNegative(PWMA);
            // debug
            // sprintf(SBuffer, "! <<< MPWM %s --- PWMA[%s]", FID.c_str(), FloatString(PWMA, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWMT[%s]", FloatString(PWMT, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWML[%s]", FloatString(PWML, 3));
            // SerialCommand.Write(SBuffer);
            // sprintf(SBuffer, " PWMH[%s]", FloatString(PWMH, 3));
            // SerialCommand.WriteLine(SBuffer);
          }
        }
      }
    }
    //--------------------------------------
    // Controlling - Busy :
    //--------------------------------------
    if (smBusy == SM)
    { 
      if (DPTA < PR) 
      { // Position reached : Busy -> Idle
        SerialCommand.WriteLine("StopMotion");
        FPMotor->StopMotion();
        delay(100);
      } else
      { 
        UInt16 PWMA = CalculatePwmActual();
        if (PWMA != FPWMActualPreset)
        {
          FPWMActualPreset = PWMA;
          if (PA < PT)
          { 
            FPMotor->MovePositive(FPWMActualPreset);
          } else
          if (PT < PA)
          {
            FPMotor->MoveNegative(FPWMActualPreset);
          }
        }
      }
    }
  }
  // 
  //--------------------------------------
  // Output
  //--------------------------------------
  if (250 < (millis() - FTickPreset))
  {
    FTickPreset = millis();
    Int32 PA = FPEncoder->GetPosition();
    if (PA != FPositionPreset)
    {
      FPositionPreset = PA;
      Int32 DPTA = FPositionTarget - PA;
      Float32 PWMA = FPMotor->GetPwmActual();
      // Float32 PWMT = FPMotor->GetPwmTarget();
      // Float32 PWML = FPMotor->GetPwmLow();
      // Float32 PWMH = FPMotor->GetPwmHigh();
      int SM = FPMotor->GetState();
      char SBuffer[64];
      sprintf(SBuffer, "! MPOS[%s] SM[%i] PA[%i] DP[%i]", FID.c_str(), SM, PA, DPTA);
      SerialCommand.Write(SBuffer);
      sprintf(SBuffer, " PWMA[%s]", FloatString(PWMA, 3));
      // SerialCommand.Write(SBuffer);
      // sprintf(SBuffer, " PWML[%s]", FloatString(PWML, 3));
      // SerialCommand.Write(SBuffer);
      // sprintf(SBuffer, " PWMT[%s]", FloatString(PWMT, 3));
      // SerialCommand.Write(SBuffer);
      // sprintf(SBuffer, " PWMH[%s]", FloatString(PWMH, 3));
      SerialCommand.WriteLine(SBuffer);
    }
  }
}
//
//-----------------------------------------------------------------
// Message Asynchrone
//-----------------------------------------------------------------
void CMotorEncoder::Message(CSerial serial)
{
  // !!! Int32 PA = FPEncoder->GetPosition();
  // !!! Int32 PT = FPositionTarget;
  // !!! UInt32 DPTA = abs(PT - PA);
  // !!! EStateMotor SM = FPMotor->GetState();
  // !!! EMotorActivity MA = GetMotorActivity();
  // !!! UInt32 DPR = FDeltaPositionResolution;
  //
  //FPEncoder->Message()
}
//
#endif // MOTORENCODER_ISPLUGGED
//
