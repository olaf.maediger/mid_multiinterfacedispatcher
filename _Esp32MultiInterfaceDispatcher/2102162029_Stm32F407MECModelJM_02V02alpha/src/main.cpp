//
#include <Arduino.h>
#include "Defines.h"
#include "Pinout.h"
#include "Error.h"
#include "Command.h"
#include "Automation.h"
#include "Utilities.h"
//
#include "IOPin.h"
#include "Serial.h"
#include "Led.h"
//
#if defined(PININPUT_ISPLUGGED)
#include "PinInput.h"
#endif
#if defined(PINOUTPUT_ISPLUGGED)
#include "PinOutput.h"
#endif
#if defined(DACINTERNAL_ISPLUGGED)
#include "DacInternal.h"
#endif
#if defined(DACMCP4725_ISPLUGGED)
#include "DacMCP4725.h"
#endif
#if defined(DACMCP4725_ISPLUGGED)
#include "DacMCP4725.h"
#endif
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
#include "RTCInternal.h"
#endif
#include "TimeRelative.h"
#if defined(NTPCLIENT_ISPLUGGED)
#include "NTPClient.h"
#include "TimeAbsolute.h"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#include "TriggerInput.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#include "XmlFile.h"
#include "CommandFile.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#include "Menu.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
#if defined(MOTORL298N_ISPLUGGED)
#include "MotorL298N.h"
#endif
#if defined(ENCODERIRQ_ISPLUGGED)
#include "EncoderIRQ.h"
#endif
#if defined(TRIGGER_ISPLUGGED)
#include "TriggerIRQ.h"
#endif
#if defined(LIMITSWITCH_ISPLUGGED)
#include "LimitSwitch.h"
#endif
#if defined(MOTORENCODER_ISPLUGGED)
#include "MotorEncoder.h"
#endif
//
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
CError        Error;
CCommand      Command;
#if defined(SDCARD_ISPLUGGED)
CCommandFile  CommandFile;
#endif
CAutomation   Automation;
//
// debug:
char GlobalBuffer[128];
// 
#if defined(NTPCLIENT_ISPLUGGED) || defined(MQTTCLIENT_ISPLUGGED)
String WifiSSID             = WIFI_SSID;
String WifiPASSWORD         = WIFI_PASSWORD;
String MQTTBrokerIPAddress  = MQTTBROKER_IPADDRESS;
int    MQTTBrokerIPPort     = MQTTBROKER_PORT;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Led
//-----------------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, "LEDS", LEDSYSTEM_INVERTED); 
//
#if defined(MULTICHANNELLED_ISPLUGGED)
CLed LedChannel1(PIN_LED_CHANNEL1, '1');
CLed LedChannel2(PIN_LED_CHANNEL2, '2');
CLed LedChannel3(PIN_LED_CHANNEL3, '3');
CLed LedChannel4(PIN_LED_CHANNEL4, '4');
CLed LedChannel5(PIN_LED_CHANNEL5, '5');
CLed LedChannel6(PIN_LED_CHANNEL6, '6');
CLed LedChannel7(PIN_LED_CHANNEL7, '7');
CLed LedChannel8(PIN_LED_CHANNEL8, '8');
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//-----------------------------------------------------------
//
#if defined(SERIALCOMMAND_ISPLUGGED)
CSerial SerialCommand(&Serial6);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - IOPin
//-----------------------------------------------------------
//
#if defined(WATCHDOG_ISPLUGGED)
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - RTCInternal
//-----------------------------------------------------------
//
#if defined(RTCINTERNAL_ISPLUGGED)
CRTCInternal RTCInternal;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Time / NTPClient
//-----------------------------------------------------------
#if defined(NTPCLIENT_ISPLUGGED)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
#endif
//
CTimeRelative TimeRelativeSystem("TRSM", INIT_MESSAGE_ON);
#if defined(NTPCLIENT_ISPLUGGED)
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - TriggerInput
//-----------------------------------------------------------
#if defined(TRIGGERINPUT_ISPLUGGED)
CTriggerInput TriggerInputSystem("S", PIN_TRIGGERINPUT_SIGNAL);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - I2CDisplay
//-----------------------------------------------------------
//
#if defined(I2CDISPLAY_ISPLUGGED)
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
//
CMenu MenuSystem("MS");                       
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - SDCARD
//-----------------------------------------------------------
//
#if defined(SDCARD_ISPLUGGED)
CSDCard SDCard(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - MQTTClient
//-----------------------------------------------------------
#if defined(MQTTCLIENT_ISPLUGGED)
CMQTTClient MQTTClient;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - MotorL298N
//-----------------------------------------------------------
#if defined(MOTORL298N_ISPLUGGED)
CMotorL298N MotorX("MX", PIN_MOTORX_PWM, 
                   PIN_MOTORX_ENA, PIN_MOTORX_ENB,
                   INIT_PWMLOW_MOTORX, INIT_PWMHIGH_MOTORX);
CMotorL298N MotorY("MY", PIN_MOTORY_PWM, 
                   PIN_MOTORY_ENA, PIN_MOTORY_ENB,
                   INIT_PWMLOW_MOTORY, INIT_PWMHIGH_MOTORY);
CMotorL298N MotorU("MU", PIN_MOTORU_PWM, 
                   PIN_MOTORU_ENA, PIN_MOTORU_ENB,
                   INIT_PWMLOW_MOTORU, INIT_PWMHIGH_MOTORU);
CMotorL298N MotorV("MV", PIN_MOTORV_PWM, 
                   PIN_MOTORV_ENA, PIN_MOTORV_ENB,
                   INIT_PWMLOW_MOTORV, INIT_PWMHIGH_MOTORV);
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - EncoderIRQ
//-------------------------------------------------------------
#if defined(ENCODERIRQ_ISPLUGGED)
//
void HandleInterruptXA(void);
void HandleInterruptXB(void);
//
void HandleInterruptYA(void);
void HandleInterruptYB(void);
//
void HandleInterruptUA(void);
void HandleInterruptUB(void);
//
void HandleInterruptVA(void);
void HandleInterruptVB(void);
//
CEncoderIRQ EncoderX("EX", PIN_ENCODERX_ENA, PIN_ENCODERX_ENB, 
                     HandleInterruptXA, HandleInterruptXB);
CEncoderIRQ EncoderY("EY", PIN_ENCODERY_ENA, PIN_ENCODERY_ENB, 
                     HandleInterruptYA, HandleInterruptYB);
CEncoderIRQ EncoderU("EU", PIN_ENCODERU_ENA, PIN_ENCODERU_ENB, 
                     HandleInterruptUA, HandleInterruptUB);
CEncoderIRQ EncoderV("EV", PIN_ENCODERV_ENA, PIN_ENCODERV_ENB, 
                     HandleInterruptVA, HandleInterruptVB);                     
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - TriggerIRQ
//-------------------------------------------------------------
#if defined(TRIGGER_ISPLUGGED)
CTriggerIRQ TriggerX("TX", PIN_TRIGGERX);
CTriggerIRQ TriggerY("TY", PIN_TRIGGERY);
CTriggerIRQ TriggerU("TU", PIN_TRIGGERU);
CTriggerIRQ TriggerV("TV", PIN_TRIGGERV);
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - LimitSwitch
//-------------------------------------------------------------
#if defined(LIMITSWITCH_ISPLUGGED)
CLimitswitch LimitswitchXLow(PIN_LIMITSWITCHX_LOW, true);
CLimitswitch LimitswitchXHigh(PIN_LIMITSWITCHX_HIGH, true);
CLimitswitch LimitswitchYLow(PIN_LIMITSWITCHY_LOW, true);
CLimitswitch LimitswitchYHigh(PIN_LIMITSWITCHY_HIGH, true);
CLimitswitch LimitswitchULow(PIN_LIMITSWITCHU_LOW, true);
CLimitswitch LimitswitchUHigh(PIN_LIMITSWITCHU_HIGH, true);
CLimitswitch LimitswitchVLow(PIN_LIMITSWITCHV_LOW, true);
CLimitswitch LimitswitchVHigh(PIN_LIMITSWITCHV_HIGH, true);
//
CLed LedSwitchXLow(PIN_LED_SWITCHX_LOW, "LEDSXL", false);
CLed LedSwitchXHigh(PIN_LED_SWITCHX_HIGH, "LEDSXH", false);
CLed LedSwitchYLow(PIN_LED_SWITCHY_LOW, "LEDSYL", false);
CLed LedSwitchYHigh(PIN_LED_SWITCHY_HIGH, "LEDSYH", false);
CLed LedSwitchULow(PIN_LED_SWITCHU_LOW, "LEDSUL", false);
CLed LedSwitchUHigh(PIN_LED_SWITCHU_HIGH, "LEDSUH", false);
CLed LedSwitchVLow(PIN_LED_SWITCHV_LOW, "LEDSVL", false);
CLed LedSwitchVHigh(PIN_LED_SWITCHV_HIGH, "LEDSVH", false);
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - MotorEncoder
//-------------------------------------------------------------
#if defined(MOTORENCODER_ISPLUGGED)
CMotorEncoder MotorEncoderX("MEX", 
                            &MotorX, &EncoderX, 
                            &LimitswitchXLow, &LimitswitchXHigh,
                            &LedSwitchXLow, &LedSwitchXHigh,
                            INIT_POSITIONRAMP_X, INIT_POSITIONRESOLUTION_X);
CMotorEncoder MotorEncoderY("MEY", 
                            &MotorY, &EncoderY, 
                            &LimitswitchYLow, &LimitswitchYHigh,
                            &LedSwitchYLow, &LedSwitchYHigh,
                            INIT_POSITIONRAMP_Y, INIT_POSITIONRESOLUTION_Y);
CMotorEncoder MotorEncoderU("MEU", 
                            &MotorU, &EncoderU, 
                            &LimitswitchULow, &LimitswitchUHigh,
                            &LedSwitchULow, &LedSwitchUHigh,
                            INIT_POSITIONRAMP_U, INIT_POSITIONRESOLUTION_U);
CMotorEncoder MotorEncoderV("MEV", 
                            &MotorV, &EncoderV, 
                            &LimitswitchVLow, &LimitswitchVHigh,
                            &LedSwitchVLow, &LedSwitchVHigh,
                            INIT_POSITIONRAMP_V, INIT_POSITIONRESOLUTION_V);
#endif
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
//
#if defined(ENCODERIRQ_ISPLUGGED)
//
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// InterruptHandler - EncoderX
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void HandleInterruptXA(void) 
{
  if (smBusy == MotorX.GetState())
  {
    EncoderX.HandleIrqA();
#if defined(TRIGGER_ISPLUGGED)
    TriggerX.HandleIrqA(EncoderX.GetPosition());
#endif
  }
}
void HandleInterruptXB(void) 
{
  if (smBusy == MotorX.GetState())
  {
    EncoderX.HandleIrqB();
#if defined(TRIGGER_ISPLUGGED)
    TriggerX.HandleIrqB(EncoderX.GetPosition());
#endif
  }
}
//
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// InterruptHandler - EncoderY
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void HandleInterruptYA(void) 
{
  if (smBusy == MotorY.GetState())
  {
    EncoderY.HandleIrqA();
#if defined(TRIGGER_ISPLUGGED)
    TriggerY.HandleIrqA(EncoderY.GetPosition());
#endif
  }
}
void HandleInterruptYB(void) 
{
  if (smBusy == MotorY.GetState())
  {
    EncoderY.HandleIrqB();
#if defined(TRIGGER_ISPLUGGED)
    TriggerY.HandleIrqB(EncoderY.GetPosition());
#endif
  }
}
//
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// InterruptHandler - EncoderU
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void HandleInterruptUA(void) 
{
  if (smBusy == MotorU.GetState())
  {
    EncoderU.HandleIrqA();
#if defined(TRIGGER_ISPLUGGED)
    TriggerU.HandleIrqA(EncoderU.GetPosition());
#endif
  }
}
void HandleInterruptUB(void) 
{
  if (smBusy == MotorU.GetState())
  {
    EncoderU.HandleIrqB();
#if defined(TRIGGER_ISPLUGGED)
    TriggerU.HandleIrqB(EncoderU.GetPosition());
#endif
  }
}
//
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// InterruptHandler - EncoderV
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void HandleInterruptVA(void) 
{
  if (smBusy == MotorV.GetState())
  {
    EncoderV.HandleIrqA();
#if defined(TRIGGER_ISPLUGGED)
    TriggerV.HandleIrqA(EncoderV.GetPosition());
#endif
  }
}
void HandleInterruptVB(void) 
{
  if (smBusy == MotorV.GetState())
  {
    EncoderV.HandleIrqB();
#if defined(TRIGGER_ISPLUGGED)
    TriggerV.HandleIrqB(EncoderV.GetPosition());
#endif
  }
}
#endif // ENCODERIRQ_ISPLUGGED
//
//#################################################################
//  Global - Setup - Main
//#################################################################
void setup() 
{ //
  //----------------------------------------------------
  // Device - IOPin
  //----------------------------------------------------
// #if defined(WATCHDOG_ISPLUGGED)
//   WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
// #endif
  //----------------------------------------------------
  // Device - Led - LedSystem
  //----------------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(100);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff();
  //
#if defined(MULTICHANNELLED_ISPLUGGED)
  LedChannel1.Open();
  LedChannel2.Open();
  LedChannel3.Open();
  LedChannel4.Open();
  LedChannel5.Open();
  LedChannel6.Open();
  LedChannel7.Open();
  LedChannel8.Open();  
  for (int BI = 0; BI < 10; BI++)
  {
    LedChannel1.SetOff();
    LedChannel2.SetOn();
    LedChannel3.SetOff();
    LedChannel4.SetOn();
    LedChannel5.SetOff();
    LedChannel6.SetOn();
    LedChannel7.SetOff();
    LedChannel8.SetOn();
    delay(100);
    LedChannel1.SetOn();
    LedChannel2.SetOff();
    LedChannel3.SetOn();
    LedChannel4.SetOff();
    LedChannel5.SetOn();
    LedChannel6.SetOff();
    LedChannel7.SetOn();
    LedChannel8.SetOff();
    delay(30);
  }
  LedChannel1.SetOff();
  LedChannel2.SetOff();
  LedChannel3.SetOff();
  LedChannel4.SetOff();
  LedChannel5.SetOff();
  LedChannel6.SetOff();
  LedChannel7.SetOff();
  LedChannel8.SetOff();
#endif  
//  
//----------------------------------------------------
// Device - Serial
//----------------------------------------------------
#if defined(SERIALCOMMAND_ISPLUGGED)
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_ON);
  delay(300);
  SerialCommand.WriteLine("\r\n# Serial-Connection: [...]");
#endif
  //
  //----------------------------------------------------
  // Device - RTCInternal
  //----------------------------------------------------}
#if defined(RTCINTERNAL_ISPLUGGED)
  RTCInternal.Open();
#endif
  //
  //---------------------------------------------------------
  // Device - MQTTClient / NTPClient - TimeRelative/Absolute
  //---------------------------------------------------------
#if defined(NTPCLIENT_ISPLUGGED)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  //
  TimeAbsoluteSystem.Open();  
#endif
  TimeRelativeSystem.Open();
  //
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.Initialise(WifiSSID.c_str(), WifiPASSWORD.c_str());
  MQTTClient.Open(MQTTBrokerIPAddress.c_str(), MQTTBrokerIPPort); 
#endif 
  //
  //---------------------------------------------------------
  // Device - TriggerInput
  //---------------------------------------------------------
#if defined(TRIGGERINPUT_ISPLUGGED)
  TriggerInputSystem.Open();
#endif
  // 
  //----------------------------------------------------
  // Device - SDCARD
  //----------------------------------------------------
#if defined(SDCARD_ISPLUGGED)
  SerialCommand.WriteLine("# Initializing SDCARD ...");    
  while (SDCard.Open(&SD) < 1)
  {
    SerialCommand.WriteLine(": Initializing SDCARD failed - Insert correct SDCARD[FAT32, initfile]!");
    delay(5000);
  }
  SerialCommand.WriteLine("# Initializing SD card done.");
  //
  Boolean Result = XmlFile.Open(&SD);
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Initfile Not Found!");
    return;
  }
  //  
  SerialCommand.Write("# Reading Initfile: ");
  SerialCommand.WriteLine(XML_INITFILE);
  Result = XmlFile.ReadFile(XML_INITFILE);
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Reading File!");
    return;
  }
  // SerialCommand.WriteLine("# Content XmlFile:");
  // Serial.println(XmlFile.GetText());
  //
  SerialCommand.WriteLine("# Parsing Initfile");
  Result = XmlFile.Parse();
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Parsing Xml!");
    return;
  }
  //
  SerialCommand.WriteLine("# Result Xml Query:");
  XMLNode* XmlRoot = XmlFile.GetRoot();
  if (!XmlRoot) return;
  //----------------------------------------------------------------------
  XMLElement* XmlWifi = XmlFile.GetElement(XmlRoot, "wifi");
  if (!XmlWifi) return;
  //
  XMLElement* XmlSSID = XmlFile.GetElement(XmlWifi, "ssid");
  if (!XmlSSID) return;
  SerialCommand.Write("# WifiSSID[");
  SerialCommand.Write(XmlSSID->GetText());
  SerialCommand.WriteLine("]");
  WifiSSID = XmlSSID->GetText();
  //
  XMLElement* XmlPASSWORD = XmlFile.GetElement(XmlWifi, "password");
  if (!XmlPASSWORD) return;
  SerialCommand.Write("# WifiPASSWORD[");
  SerialCommand.Write(XmlPASSWORD->GetText());
  SerialCommand.WriteLine("]");
  WifiPASSWORD = XmlPASSWORD->GetText();
  //----------------------------------------------------------------------
  XMLElement* XmlMQTTBroker = XmlFile.GetElement(XmlRoot, "mqttbroker");
  if (!XmlMQTTBroker) return;
  //
  XMLElement* XmlIPAddress = XmlFile.GetElement(XmlMQTTBroker, "ipaddress");
  if (!XmlIPAddress) return;
  SerialCommand.Write("# BrokerIPAddress[");
  SerialCommand.Write(XmlIPAddress->GetText());
  SerialCommand.WriteLine("]");
  MQTTBrokerIPAddress = XmlIPAddress->GetText();
  //
  XMLElement* XmlIPPort = XmlFile.GetElement(XmlMQTTBroker, "ipport");
  if (!XmlIPPort) return;
  SerialCommand.Write("# BrokerIPPort[");
  SerialCommand.Write(XmlIPPort->GetText());
  SerialCommand.WriteLine("]");
  MQTTBrokerIPPort = atoi(XmlIPPort->GetText());
  //----------------------------------------------------------------------
  // !!! Free Ram !!!
  XmlFile.Close();
  //----------------------------------------------------------------------
  // !!! !!! !!! !!! !!! !!!
  CommandFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!CommandFile.ParseFile(INIT_COMMANDFILE))
  {
    SerialCommand.WriteLine(": Error: Cannot Read Commandfile!");
    return;
  }
  SerialCommand.Write("# CommandFile[");
  SerialCommand.Write(INIT_COMMANDFILE);
  SerialCommand.WriteLine("]:");
  bool CommandLoop = true;
  while (CommandLoop)
  {
    String Command = CommandFile.GetCommand();
    CommandLoop = (0 < Command.length());
    if (CommandLoop)
    {
      SerialCommand.Write("# Command[");
      SerialCommand.Write(Command.c_str());
      SerialCommand.WriteLine("]");
    }
  }
  CommandFile.Close();  
#endif  
  //
  //----------------------------------------------------
  // Device - I2CDisplay
  //----------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  //
  MenuSystem.Open();
#endif
  //
  //----------------------------------------------------
  // Device - MotorL298N
  //----------------------------------------------------
#if defined(MOTORL298N_ISPLUGGED)
  MotorX.Open();
  MotorY.Open();
  MotorU.Open();
  MotorV.Open();
#endif
  //
  //----------------------------------------------------
  // Device - EncoderIRQ
  //----------------------------------------------------
#if defined(ENCODERIRQ_ISPLUGGED)
  EncoderX.Open();
  EncoderY.Open();
  EncoderU.Open();
  EncoderV.Open();
#endif
  //
  //----------------------------------------------------
  // Device - LimitSwitch
  //----------------------------------------------------
#if defined(LIMITSWITCH_ISPLUGGED)
  LimitswitchXLow.Open();
  LimitswitchXHigh.Open();
  LimitswitchYLow.Open();
  LimitswitchYHigh.Open();
  LimitswitchULow.Open();
  LimitswitchUHigh.Open();
  LimitswitchVLow.Open();
  LimitswitchVHigh.Open();
  //
  LedSwitchXLow.Open();
  LedSwitchXHigh.Open();
  LedSwitchYLow.Open();
  LedSwitchYHigh.Open();
  LedSwitchULow.Open();
  LedSwitchUHigh.Open();
  LedSwitchVLow.Open();
  LedSwitchVHigh.Open();
  for (int IL = 0; IL < 3; IL++)
  {
    LedSwitchXLow.SetOff();
    LedSwitchXHigh.SetOn();
    LedSwitchYLow.SetOff();
    LedSwitchYHigh.SetOn();
    LedSwitchULow.SetOff();
    LedSwitchUHigh.SetOn();
    LedSwitchVLow.SetOff();
    LedSwitchVHigh.SetOn();
    delay(100);
    LedSwitchXLow.SetOn();
    LedSwitchXHigh.SetOff();
    LedSwitchYLow.SetOn();
    LedSwitchYHigh.SetOff();
    LedSwitchULow.SetOn();
    LedSwitchUHigh.SetOff();
    LedSwitchVLow.SetOn();
    LedSwitchVHigh.SetOff();
    delay(100);
    LedSwitchXLow.SetOn();
    LedSwitchXHigh.SetOn();
    LedSwitchYLow.SetOn();
    LedSwitchYHigh.SetOn();
    LedSwitchULow.SetOn();
    LedSwitchUHigh.SetOn();
    LedSwitchVLow.SetOn();
    LedSwitchVHigh.SetOn();
    delay(100);
    LedSwitchXLow.SetOff();
    LedSwitchXHigh.SetOff();
    LedSwitchYLow.SetOff();
    LedSwitchYHigh.SetOff();
    LedSwitchULow.SetOff();
    LedSwitchUHigh.SetOff();
    LedSwitchVLow.SetOff();
    LedSwitchVHigh.SetOff();
    delay(100);
  }
#endif
  //
  //----------------------------------------------------
  // Device - Trigger
  //----------------------------------------------------
#if defined(TRIGGER_ISPLUGGED)
  TriggerX.Open();
  TriggerY.Open();
  TriggerU.Open();
  TriggerV.Open();
#endif
  //
  //----------------------------------------------------
  // Device - MotorEncoder
  //----------------------------------------------------
#if defined(MOTORENCODER_ISPLUGGED)
  MotorEncoderX.Open();
  MotorEncoderY.Open();
  MotorEncoderU.Open();
  MotorEncoderV.Open();
#endif
  //
  //----------------------------------------------------
  // Device - Command
  //----------------------------------------------------
  Command.Open();
  Command.WriteProgramHeader(SerialCommand);
  Command.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //----------------------------------------------------
  // Device - Automation
  //----------------------------------------------------
  Automation.Open();
}
//
//#################################################################
//  Global - Loop - Main
//#################################################################
//
void loop() 
{
#if defined(SERIALCOMMAND_ISPLUGGED)
  if (Error.Handle(SerialCommand))
  {
    Command.SetState(scIdle);
    //SerialCommand.WritePrompt();
  }
#if defined(MQTTCLIENT_ISPLUGGED)
  Command.Handle(SerialCommand, MQTTClient);
#endif // MQTTCLIENT_ISPLUGGED
#if defined(SDCARD_ISPLUGGED)
  Command.Handle(SerialCommand, SDCard);
#endif // SDCARD_ISPLUGGED
  Command.Handle(SerialCommand);
  Automation.Handle(SerialCommand); 
#endif // SERIALCOMMAND_ISPLUGGED
  // Preemptives Multitasking:
  //------------------------------------------------------------------------
  // NC TimeRelativeSystemus.Wait_Execute();
  TimeRelativeSystem.Wait_Execute();
#if defined(NTP_ISPLUGGED)  
  TimeAbsoluteSystem.Wait_Execute();
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
  MotorPositionReached.Wait_Execute();
#endif  
#if defined(TRIGGERINPUT_ISPLUGGED)  
  TriggerInputSystem.Wait_Execute();
#endif  
//------------------------------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  MenuSystem.Display_Execute();
#endif
  //
  LedSystem.Blink_Execute();  
#if defined(MULTICHANNELLED_ISPLUGGED)  
  LedChannel1.Blink_Execute();
  LedChannel2.Blink_Execute();
  LedChannel3.Blink_Execute();
  LedChannel4.Blink_Execute();
  LedChannel5.Blink_Execute();
  LedChannel6.Blink_Execute();
  LedChannel7.Blink_Execute();
  LedChannel8.Blink_Execute();
#endif  
//------------------------------------------------------------------------
#if defined(MOTORL298N_ISPLUGGED)
  MotorX.Execute();
  MotorY.Execute();
  MotorU.Execute();
  MotorV.Execute();
  //------------------------------
  MotorX.Message(SerialCommand);
  MotorY.Message(SerialCommand);
  MotorU.Message(SerialCommand);
  MotorV.Message(SerialCommand);
#endif
#if defined(ENCODERIRQ_ISPLUGGED)
  EncoderX.Execute();
  EncoderY.Execute();
  EncoderU.Execute();
  EncoderV.Execute();
  //------------------------------
  EncoderX.Message(SerialCommand);
  EncoderY.Message(SerialCommand);
  EncoderU.Message(SerialCommand);
  EncoderV.Message(SerialCommand);
#endif
#if defined(MOTORENCODER_ISPLUGGED)
  MotorEncoderX.Execute();
  MotorEncoderY.Execute();
  MotorEncoderU.Execute();
  MotorEncoderV.Execute();
  //------------------------------
  MotorEncoderX.Message(SerialCommand);
  MotorEncoderY.Message(SerialCommand);
  MotorEncoderU.Message(SerialCommand);
  MotorEncoderV.Message(SerialCommand);
#endif
}

