//
//--------------------------------
//  Library IOPin
//--------------------------------
//
#include "IOPin.h"
//
CIOPin::CIOPin(int pin, EIOPinDirection pindirection)
{
  FPin = pin;
  FDirection = pindirection;
  FInverted = false;
  FLevel = ioplUndefined;
}

CIOPin::CIOPin(int pin, EIOPinDirection pindirection, bool inverted)
{
  FPin = pin;
  FDirection = pindirection;
  FInverted = inverted;
  FLevel = ioplUndefined;
}

Boolean CIOPin::Open()
{
  switch (FDirection)
  {
    case iopdOutput:
      SetOutput();
      break;
    default: // iopdInput
      SetInput();
      break;
  }
  SetOff();
  return true;
}

Boolean CIOPin::Close()
{
  SetOff();
  SetInput();
  FLevel = ioplUndefined;
  return true;
}

void CIOPin::SetInput()
{
  pinMode(FPin, INPUT);
  FDirection = iopdInput;
}

void CIOPin::SetOutput()
{
  pinMode(FPin, OUTPUT);
  FDirection = iopdOutput;
}

void CIOPin::SetOn()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = ioplOn;
}

void CIOPin::SetOff()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = ioplOff;
}

EIOPinLevel CIOPin::GetLevel()
{
  Byte BValue = digitalRead(FPin);
  if (FInverted)
  {
    if (0 < BValue)
    {
      FLevel = ioplOff;
    }
    else
    {
      FLevel = ioplOn;
    }
  }
  else
  { // Not Inverted
    if (0 < digitalRead(FPin))
    {
      FLevel = ioplOn;
    }
    else
    {
      FLevel = ioplOff;
    }      
  }
  return FLevel;
}



