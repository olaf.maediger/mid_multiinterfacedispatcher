//
//--------------------------------
//  Library PinOutput
//--------------------------------
//
#include "Defines.h"
//
#include "PinOutput.h"
//
CPinOutput::CPinOutput(String id, UInt8 pin, EPinOutputLevel level)
{
  FID = id;
  FPin = pin;
  FLevel = polLow;
}

EPinOutputLevel CPinOutput::GetLevel(void)
{
  return FLevel;
}  

void CPinOutput::WriteLevel(EPinOutputLevel level)
{
  FLevel = level;
  switch (FLevel)
  {
    case polLow:
      digitalWrite(FPin, LOW);
      break;
    case polHigh:
      digitalWrite(FPin, HIGH);
      break;
  }
}  

Boolean CPinOutput::Open()
{
  pinMode(FPin, OUTPUT);
  WriteLevel(polLow);
  return true;
}

Boolean CPinOutput::Close()
{
  pinMode(FPin, INPUT_PULLUP);
  WriteLevel(polLow);
  return true;
}
//
//-------------------------------------------------------------------------------
//  PinOutput - Execute
//-------------------------------------------------------------------------------
EPinOutputLevel CPinOutput::Execute(void)
{
  return FLevel;
}
//
