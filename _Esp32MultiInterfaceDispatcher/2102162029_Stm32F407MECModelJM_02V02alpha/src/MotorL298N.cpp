//
#include "Defines.h"
//
#if defined(MOTORL298N_ISPLUGGED)
//
#include "MotorL298N.h"
//
CMotorL298N::CMotorL298N(String name, UInt8 pinpwm, 
                         UInt8 pinena, UInt8 pinenb,
                         Float32 pwmlow, Float32 pwmhigh)
: CMotorBase(name, pinpwm, pinena, pinenb, pwmlow, pwmhigh)
{

}

Boolean CMotorL298N::Open(void)
{
  return CMotorBase::Open();
}
Boolean CMotorL298N::Close(void)
{
  return CMotorBase::Close();
}
//
#endif // MOTORL298N_ISPLUGGED)
//