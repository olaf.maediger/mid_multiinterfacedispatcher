//
#include "Defines.h"
//
#if defined(TRIGGER_ISPLUGGED)
//
#include "TriggerBase.h"
//
CTriggerBase::CTriggerBase(String name,
                           UInt8 pintrigger)
{
  FName = name;
  //
  FPinTrigger = pintrigger;
  //
  Init();
}
void CTriggerBase::Init()
{
  FState = INIT_TRIGGER_STATE;
  FTriggerReduction = INIT_TRIGGER_REDUCTION;
  FTriggerPeriod = INIT_TRIGGER_PERIOD;
  FTriggerCount = INIT_TRIGGER_COUNT;
  FCountOffset = INIT_TRIGGER_COUNTOFFSET;
  FCountPreset = INIT_TRIGGER_COUNTPRESET;
  FCountActual = INIT_TRIGGER_COUNTACTUAL;
  FPositionStart = INIT_TRIGGER_POSITIONSTART;
  FPositionEnd = INIT_TRIGGER_POSITIONEND;
}
//
Boolean CTriggerBase::Open(void)
{
  if (0 < FPinTrigger)
  {
    pinMode(FPinTrigger, OUTPUT); 
  }
  return true;
}
Boolean CTriggerBase::Close(void)
{
  if (0 < FPinTrigger)
  {
    pinMode(FPinTrigger, INPUT);
  }
  return true;
}
//
EStateTrigger CTriggerBase::GetState(void)
{
  return FState;
}
void CTriggerBase::SetState(EStateTrigger state)
{
  FState = state;
}
//
UInt32 CTriggerBase::GetTriggerReduction(void)
{
  return FTriggerReduction;
}
void CTriggerBase::SetTriggerReduction(UInt32 reduction)
{
  FTriggerReduction = reduction;
}
UInt32 CTriggerBase::GetTriggerTotal(void)
{
  return FTriggerTotal;
}
void CTriggerBase::SetTriggerTotal(UInt32 trigger)
{
  FTriggerTotal= trigger;
}
//
UInt32 CTriggerBase::GetTriggerPeriod(void)
{
  return FTriggerPeriod;
}
void CTriggerBase::SetTriggerPeriod(UInt32 value)
{
  FTriggerPeriod = value;
}
UInt32 CTriggerBase::GetTriggerCount(void)
{
  return FTriggerCount;
}
void CTriggerBase::SetTriggerCount(UInt32 value)
{
  FTriggerCount = value;
}
//
// FreeMoving
//
UInt32 CTriggerBase::GetCountOffset(void)
{
  return FCountOffset;
}
UInt32 CTriggerBase::GetCountPreset(void)
{
  return FCountPreset;
}
UInt32 CTriggerBase::GetCountActual(void)
{
  return FCountActual;
}
//  
void CTriggerBase::SetCountOffset(UInt32 count)
{
  FCountOffset = count;
}
void CTriggerBase::SetCountPreset(UInt32 count)
{
  FCountPreset = count;
}
void CTriggerBase::SetCountActual(UInt32 count)
{
  FCountActual = count;
}
//
// PositionControlled
//
Int32 CTriggerBase::GetPositionStart(void)
{
  return FPositionStart;
}
void CTriggerBase::SetPositionStart(Int32 value)
{
  FPositionStart = value;
}
Int32 CTriggerBase::GetPositionEnd(void)
{
  return FPositionEnd;
}
void CTriggerBase::SetPositionEnd(Int32 value)
{
  FPositionEnd = value;
}
//
//------------------------------------------------------
//
void CTriggerBase::IncrementCountActual(Int32 position)
{
  if (FPinTrigger <= 0)
  {
    SetState(stDisabled);
  }
  else
  {
    switch (FState)
    {
      case stDisabled:
        FCountActual = 0;
        FTriggerTotal = 0;
        return;
      case stPeriodCount:
      // !!!!!!!!!!
        return;
      case stFreeMoving:
        if (0 < FTriggerReduction)
        { 
          FCountActual++;
          if (FTriggerReduction <= FCountActual)
          {
            FCountActual = 0;
            FTriggerTotal++;
            if ((FCountOffset <= FTriggerTotal) && (FTriggerTotal <= FCountPreset))
            {
              digitalWrite(FPinTrigger, LOW);
              digitalWrite(FPinTrigger, HIGH);
              digitalWrite(FPinTrigger, LOW);
            }
          }
        }
        return;
      case stPositionControlled:
        if (0 < FTriggerReduction)
        { 
          FCountActual++;
          if (FTriggerReduction <= FCountActual)
          {
            FCountActual = 0;
            if ((FPositionStart <= position) && (position <= FPositionEnd))
            { 
                digitalWrite(FPinTrigger, LOW);
                digitalWrite(FPinTrigger, HIGH);
                FCountActual = 0;
                digitalWrite(FPinTrigger, LOW);
            }
          }
        }
        return;
    }
  }
}

void CTriggerBase::HandleIrqA(Int32 position)
{
  IncrementCountActual(position);
}

void CTriggerBase::HandleIrqB(Int32 position)
{
  IncrementCountActual(position);
}
//
#endif // TRIGGER_ISPLUGGED
//