//
#include "Defines.h"
//
#if defined(ENCODERIRQ_ISPLUGGED)
//
#include "EncoderBase.h"
//
extern char GlobalBuffer[];
//
CEncoderBase::CEncoderBase(String name,
                           UInt8 pinena, UInt8 pinenb,
                           TInterruptFunction pirqhandlerena, 
                           TInterruptFunction pirqhandlerenb)
{
  FName = name;
  FMessagePosition = false;
  //
  FPinENA = pinena;
  FPinENB = pinenb;
  FPosition = 0;
  //
  FPIrqHandlerENA = pirqhandlerena;
  FPIrqHandlerENB = pirqhandlerenb;
}
//
Int32 CEncoderBase::GetPosition(void)
{
  return FPosition;
}
  
void CEncoderBase::SetPosition(Int32 position)
{
  FPosition = position;
  FMessagePosition = true;
}

void CEncoderBase::IncrementPosition(void)
{
  FPosition++;
  FMessagePosition = true;
}

void CEncoderBase::DecrementPosition(void)
{
  FPosition--;
  FMessagePosition = true;
}

void CEncoderBase::HandleIrqA(void)
{
  if (0 < digitalRead(FPinENA))
  { // HIGH == ENA
    if (0 < digitalRead(FPinENB))
    { // HIGH == ENB
      FPosition--;
    }
    else
    { // LOW == ENB
      FPosition++;
    }    
  }
  else
  { // LOW == ENA
    if (0 < digitalRead(FPinENB))
    { // HIGH == ENB
      FPosition++;
    }
    else
    { // LOW == ENB
      FPosition--;
    }        
  }  
  FMessagePosition = true;
}

void CEncoderBase::HandleIrqB(void)
{
  if (0 < digitalRead(FPinENB))
  { // HIGH == ENB
    if (0 < digitalRead(FPinENA))
    { // HIGH == ENA
      FPosition++;
    }
    else
    { // LOW == ENA
      FPosition--;
    }    
  }
  else
  { // LOW == ENB
    if (0 < digitalRead(FPinENA))
    { // HIGH == ENA
      FPosition--;
    }
    else
    { // LOW == ENA
      FPosition++;
    }        
  }  
  FMessagePosition = true;
}
//
//
void CEncoderBase::Execute(void)
{
  // nothing
}
//
void CEncoderBase::Message(CSerial serial)
{
  if (FMessagePosition)
  {
    FMessagePosition = false;
  //!!!sprintf(GlobalBuffer, "EPA %s %i", FName.c_str(), FPosition);
  //!!!serial.WriteEvent(GlobalBuffer);
  }
}

//
#endif // ENCODERIRQ_ISPLUGGED
//