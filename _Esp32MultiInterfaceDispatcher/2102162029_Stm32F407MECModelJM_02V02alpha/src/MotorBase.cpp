//
#include "Defines.h"
//
#if defined(MOTORL298N_ISPLUGGED)
//
#include "MotorBase.h"
//
CMotorBase::CMotorBase(String name, UInt8 pinpwm, 
                       UInt8 pinena, UInt8 pinenb,
                       Float32 pwmlow, Float32 pwmhigh)
{
  FName = name;
  FRefreshedPwmLow = false;
  FRefreshedPwmHigh = false;
  FRefreshedPwmTarget = false;
  FRefreshedPwmActual = false;
  FRefreshedState = false;
  FRefreshedDirection = false;
  //
  FPinPWM = pinpwm;
  FPinENA = pinena;
  FPinENB = pinenb;
  //
  FPInstance = (TIM_TypeDef *)pinmap_peripheral(digitalPinToPinName(FPinPWM), PinMap_PWM);
  FPwmChannel = STM_PIN_CHANNEL(pinmap_function(digitalPinToPinName(FPinPWM), PinMap_PWM));
  FPTimer = new HardwareTimer(FPInstance);
  //
  FPwmLow = pwmlow;
  FPwmHigh = pwmhigh;
  FPwmTarget = PWM_ZERO;
  FPwmActual = PWM_ZERO;
  SetState(smIdle);
  FRefreshedPwmLow = true;
  FRefreshedPwmHigh = true;
  FRefreshedPwmTarget = true;
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
}

EStateMotor CMotorBase::GetState(void)
{
  // if ((HIGH == digitalRead(FPinENA)) && (LOW == digitalRead(FPinENB)))
  //   return smBusy;
  // if ((LOW == digitalRead(FPinENA)) && (HIGH == digitalRead(FPinENB)))
  //   return smBusy;
  // // if ((LOW == digitalRead(FPinENA)) && (LOW == digitalRead(FPinENB)))
  // // if ((HIGH == digitalRead(FPinENA)) && (HIGH == digitalRead(FPinENB)))
  // return smIdle;
  return FState;
}
void CMotorBase::SetState(EStateMotor value)
{
  FState = value;
  FRefreshedState = true;
}

void CMotorBase::ForceStateBusy(void)
{
  FState = smBusy;
  FRefreshedState = true;
}

EMotorDirection CMotorBase::GetDirection(void)
{
  if ((HIGH == digitalRead(FPinENA)) && (LOW == digitalRead(FPinENB)))
  {
    return mdPositive;    
  }
  if ((LOW == digitalRead(FPinENA)) && (HIGH == digitalRead(FPinENB)))
  {
    return mdNegative;
  }
  // H == FPinENA && H == FPinENB
  // L == FPinENA && L == FPinENB
  return mdZero;
}

Float32 CMotorBase::GetPwmLow(void)
{
  return FPwmLow;
}
void CMotorBase::SetPwmLow(Float32 value)
{  
  if ((INIT_PWM_MINIMUM <= value) && (value <= INIT_PWM_MAXIMUM))
  {
    FPwmLow = value;
    FRefreshedPwmLow = true;
  }
}

Float32 CMotorBase::GetPwmHigh(void)
{
  return FPwmHigh;
}
void CMotorBase::SetPwmHigh(Float32 value)
{
  if ((INIT_PWM_MINIMUM <= value) && (value <= INIT_PWM_MAXIMUM))
  {
    FPwmHigh = value;
    FRefreshedPwmHigh = true;
  }
}

Float32 CMotorBase::GetPwmTarget(void)
{
  return FPwmTarget;
}
void CMotorBase::SetPwmTarget(Float32 value)
{
  if ((FPwmLow <= value) && (value <= FPwmHigh))
  {
    FPwmTarget = value;
    FRefreshedPwmTarget = true;
  }
  else
  {
    if (value < FPwmLow)
    {
      FPwmTarget = FPwmLow;
      FRefreshedPwmTarget = true;
    }
    else
      if (FPwmHigh < value)
      {
        FPwmTarget = FPwmHigh;
        FRefreshedPwmTarget = true;        
      }    
  }
  
}

Float32 CMotorBase::GetPwmActual(void)
{
  return FPwmActual;
}
void CMotorBase::SetPwmActual(Float32 value)
{
  if ((FPwmLow <= value) && (value <= FPwmHigh))
  {
    FPwmActual = value;
    FPTimer->setPWM(FPwmChannel, FPinPWM, 20000, value);
    FRefreshedPwmActual = true;
  }
}

Boolean CMotorBase::Open(void)
{
  pinMode(FPinENA, OUTPUT);
  digitalWrite(FPinENA, LOW);
  pinMode(FPinENB, OUTPUT);
  digitalWrite(FPinENB, LOW);
  pinMode(FPinPWM, OUTPUT);
  //
  SetPwmLow(FPwmLow);
  SetPwmHigh(FPwmHigh);
  SetPwmTarget(INIT_PWM_LOW);
  SetPwmActual(PWM_ZERO);
  SetState(smIdle);
  return true;
}

Boolean CMotorBase::Close(void)
{
  digitalWrite(FPinENA, LOW);
  digitalWrite(FPinENB, LOW);
  pinMode(FPinENA, INPUT);
  pinMode(FPinENB, INPUT);
  pinMode(FPinPWM, INPUT);
  //
  FPwmTarget = PWM_ZERO;
  FPwmActual = PWM_ZERO;
  FPTimer->setPWM(FPwmChannel, FPinPWM, 10, 0);
  SetState(smIdle);
  FRefreshedPwmTarget = true;
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
  return true;
}

void CMotorBase::MovePositive(Float32 pwm) 
{
  digitalWrite(FPinENA, HIGH);
  digitalWrite(FPinENB, LOW);
  SetPwmActual(pwm);
  SetState(smBusy);
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
}

void CMotorBase::MoveNegative(Float32 pwm)
{
  digitalWrite(FPinENA, LOW);
  digitalWrite(FPinENB, HIGH);
  SetPwmActual(pwm);
  SetState(smBusy);
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
}

void CMotorBase::StopMotion(void)
{
  digitalWrite(FPinENA, LOW);
  digitalWrite(FPinENB, LOW);
  // ??? SetPwmTarget(PWM_ZERO);
  SetPwmActual(PWM_ZERO);
  SetState(smIdle);
  FRefreshedPwmTarget = true;
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
}
void CMotorBase::AbortMotion(void)
{
  digitalWrite(FPinENA, LOW);
  digitalWrite(FPinENB, LOW);
  // ??? SetPwmTarget(PWM_ZERO);
  SetPwmActual(PWM_ZERO);
  SetState(smIdle);
  FRefreshedPwmTarget = true;
  FRefreshedPwmActual = true;
  FRefreshedState = true;
  FRefreshedDirection = true;
}
//
//------------------------------------------------------------
//
void CMotorBase::Execute(void)
{
  // nothing
}

void CMotorBase::Message(CSerial serial)
{
  // !!!! char Buffer[64];
  if (FRefreshedPwmLow)
  {
    FRefreshedPwmLow = false;
//!!!    sprintf(Buffer, "PWMLOW[%s] %s", FName.c_str(), FloatString(GetPwmLow(), 3));
//!!!    serial.WriteEvent(Buffer);
  }
  if (FRefreshedPwmHigh)
  {
    FRefreshedPwmHigh = false;
//!!!    sprintf(Buffer, "PWMHIG[%s] %s", FName.c_str(), FloatString(GetPwmHigh(), 3));
//!!!    serial.WriteEvent(Buffer);
  }
  if (FRefreshedPwmTarget)
  {
    FRefreshedPwmTarget = false;
//!!!    sprintf(Buffer, "PWMTAR[%s] %s", FName.c_str(), FloatString(GetPwmTarget(), 3));
//!!!    serial.WriteEvent(Buffer);
  }
  if (FRefreshedPwmActual)
  {
    FRefreshedPwmActual = false;
//!!!    sprintf(Buffer, "PWMACT[%s] %s", FName.c_str(), FloatString(GetPwmActual(), 3));
//!!!    serial.WriteEvent(Buffer);
  }
  if (FRefreshedState)
  {
    FRefreshedState = false;
//!!!    sprintf(Buffer, "STA[%s] %i", FName.c_str(), GetState());
//!!!    serial.WriteEvent(Buffer);
  }
  if (FRefreshedDirection)
  {
    FRefreshedDirection = false;
//!!!    sprintf(Buffer, "DIR[%s] %i", FName.c_str(), GetDirection());
//!!!    serial.WriteEvent(Buffer);
  }  
}
//
#endif // MOTORL298N_ISPLUGGED
//