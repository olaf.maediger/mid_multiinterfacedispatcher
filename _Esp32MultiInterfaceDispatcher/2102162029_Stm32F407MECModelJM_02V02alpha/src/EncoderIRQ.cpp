//
#include "Defines.h"
//
#if defined(ENCODERIRQ_ISPLUGGED)
//
#include "EncoderIRQ.h"
//
CEncoderIRQ::CEncoderIRQ(String name,
                         UInt8 pinena, UInt8 pinenb,
                         TInterruptFunction pirqhandlerena, 
                         TInterruptFunction pirqhandlerenb)
  : CEncoderBase(name, pinena, pinenb, pirqhandlerena, pirqhandlerenb)
{
}
  
//
Boolean CEncoderIRQ::Open(void)
{
  pinMode(FPinENA, INPUT_PULLDOWN); // -> no VoltageOffset!!!
  attachInterrupt(digitalPinToInterrupt(FPinENA), FPIrqHandlerENA, CHANGE);
  pinMode(FPinENB, INPUT_PULLDOWN); // -> no VoltageOffset!!!
  attachInterrupt(digitalPinToInterrupt(FPinENB), FPIrqHandlerENB, CHANGE);
  return true;
}
Boolean CEncoderIRQ::Close(void)
{
  detachInterrupt(digitalPinToInterrupt(FPinENA));
  detachInterrupt(digitalPinToInterrupt(FPinENB));
  return true;
}
//
#endif // ENCODERIRQ_ISPLUGGED
//