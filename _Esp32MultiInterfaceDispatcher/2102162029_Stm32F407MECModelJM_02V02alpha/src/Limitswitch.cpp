//
#include "Defines.h"
//
#if defined(LIMITSWITCH_ISPLUGGED)
//
#include "Limitswitch.h"
//
//--------------------------------
//  Library Limitswitch
//--------------------------------
//
CLimitswitch::CLimitswitch(int pin)
{
  FPin = pin;
  FInverted = false;
}

CLimitswitch::CLimitswitch(int pin, bool inverted)
{
  FPin = pin;
  FInverted = inverted;
}

Boolean CLimitswitch::Open()
{
  pinMode(FPin, INPUT);
  return true;
}

Boolean CLimitswitch::Close()
{
  pinMode(FPin, INPUT);
  return true;
}
//
#endif
