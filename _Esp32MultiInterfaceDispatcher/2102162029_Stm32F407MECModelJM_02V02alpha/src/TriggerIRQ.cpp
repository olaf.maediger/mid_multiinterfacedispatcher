//
#include "Defines.h"
//
#if defined(TRIGGER_ISPLUGGED)
//
#include "TriggerIRQ.h"
//
CTriggerIRQ::CTriggerIRQ(String name,
                         UInt8 pintrigger)
  : CTriggerBase(name, pintrigger)
{
  Init();
}
//
Boolean CTriggerIRQ::Open(void)
{
  return CTriggerBase::Open();
}
Boolean CTriggerIRQ::Close(void)
{
  return CTriggerBase::Close();
}
//
#endif // TRIGGER_ISPLUGGED
//