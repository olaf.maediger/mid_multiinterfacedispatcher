//
#include "Defines.h"
//
#include "Error.h"
#include "Automation.h"
#include "Command.h"
#include "Led.h"
#include "TimeRelative.h"
#if defined(PININPUT_ISPLUGGED)
#include "PinInput.h"
#endif
#if defined(PINOUTPUT_ISPLUGGED)
#include "PinOutput.h"
#endif
#if defined(DACINTERNAL_ISPLUGGED)
#include "DacInternal.h"
#endif
#if defined(SERIALCOMMAND_ISPLUGGED)
#include "Serial.h"
#endif
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#include "TimeRelative.h"
#if defined(NTPCLIENT_ISPLUGGED)
#include "TimeAbsolute.h"
#include "NTPClient.h"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#include "TriggerInput.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#include "CommandFile.h"
#endif
#if defined(RFIDCLIENT_ISPLUGGED)
#include "RFIDClient.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
#include "RTCInternal.h"
#endif
#include "IOPin.h"
//
#if defined(MOTORL298N_ISPLUGGED)
#include "MotorBase.h"
#include "MotorL298N.h"
#endif
#if defined(ENCODERIRQ_ISPLUGGED)
#include "EncoderBase.h"
#include "EncoderIRQ.h"
#endif
#if defined(MOTORENCODER_ISPLUGGED)
#include "MotorEncoder.h"
#endif
#if defined(TRIGGER_ISPLUGGED)
#include "TriggerIRQ.h"
#endif
//
extern CSerial SerialCommand;
//
//-------------------------------------------
//  External Global Variables
//-------------------------------------------
//
extern CError               Error;
extern CCommand             Command;
#if defined(SDCARD_ISPLUGGED)
extern CCommandFile         CommandFile;
#endif
// extern CAutomation          Automation;
extern CTimeRelative        TimeRelativeSystem;
#if defined(NTPCLIENT_ISPLUGGED)
extern CTimeAbsolute        TimeAbsoluteSystem;
#endif
extern CLed                 LedSystem;
#if defined(MULTICHANNELLED_ISPLUGGED)
extern CLed                 LedChannel1;
extern CLed                 LedChannel2;
extern CLed                 LedChannel3;
extern CLed                 LedChannel4;
extern CLed                 LedChannel5;
extern CLed                 LedChannel6;
extern CLed                 LedChannel7;
extern CLed                 LedChannel8;
#endif
//
#if defined(SERIALCOMMAND_ISPLUGGED)
 extern CSerial              SerialCommand;
#endif
// 
#if defined(I2CDISPLAY_ISPLUGGED)
extern CI2CDisplay          I2CDisplay;
#endif
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog            WatchDog;
#endif
#if defined(NTPCLIENT_ISPLUGGED)
extern CNTPClient           NTPClient;
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
extern CTriggerInput        TriggerInputSystem;
#endif
#if defined(SDCARD_ISPLUGGED)
extern SDCard               SDCard;
#endif
#if defined(RFIDCLIENT_ISPLUGGED)
extern CRFIDClient          RFIDClient;
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
extern CRTCInternal         RTCInternal;
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
extern CMQTTClient          MQTTClient;
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
extern CMotorL298N  MotorX;
extern CMotorL298N  MotorY;
extern CMotorL298N  MotorU;
extern CMotorL298N  MotorV;
#endif
#if defined(ENCODERIRQ_ISPLUGGED)
extern CEncoderIRQ  EncoderX;
extern CEncoderIRQ  EncoderY;
extern CEncoderIRQ  EncoderU;
extern CEncoderIRQ  EncoderV;
#endif
#if defined(MOTORENCODER_ISPLUGGED)
extern CMotorEncoder MotorEncoderX;
extern CMotorEncoder MotorEncoderY;
extern CMotorEncoder MotorEncoderU;
extern CMotorEncoder MotorEncoderV;
#endif
#if defined(TRIGGER_ISPLUGGED)
extern CTriggerIRQ TriggerX;
extern CTriggerIRQ TriggerY;
extern CTriggerIRQ TriggerU;
extern CTriggerIRQ TriggerV;
#endif

//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand(void)
{
  FState = scUndefined;
}

CCommand::~CCommand(void)
{
  FState = scUndefined;
}
//
//#########################################################
//  Segment - Property
//#########################################################
//
EStateCommand CCommand::GetState(void)
{
  return FState;
}

void CCommand::SetState(EStateCommand state)
{
  if (state != FState)
  {
    FState = state;
    sprintf(GetTxdBuffer(), "SC %s", StateText(FState));
#ifdef SERIALCOMMAND_ISPLUGGED
    SerialCommand.WriteEvent(GetTxdBuffer());
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
    MQTTClient.WriteResponse(GetTxdBuffer());
#endif
  }
}
//
//#########################################################
//  Segment - Property
//#########################################################
//
bool CCommand::Open(void)
{
  SetState(scInit);
  Init();
  SetState(scIdle);
  return true;
}
bool CCommand::Close(void)
{
  SetState(scUndefined);
  return true;
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
const char* CCommand::StateText(EStateCommand state)
{
  switch (state)
  {
    case scError:
      return (const char*)"Error";
    case scUndefined:
      return (const char*)"Undefined";
    case scInit:
      return (const char*)"Init";
    case scIdle:
      return (const char*)"Idle";
    case scBusy:
      return (const char*)"Busy";
    default: // not in list -> Undefined
      return (const char*)"Unknown";
  }
}
// Defines Reset-Function (identically HW-Reset):
void (*PResetSystem)(void) = 0;
//
void CCommand::ZeroRxdBuffer(void)
{
  int CI;
  for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}
void CCommand::ZeroTxdBuffer(void)
{
  int CI;
  for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}
//
void CCommand::ZeroCommandText(void)
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FCommandText[CI] = 0x00;
  }
}
//
void CCommand::AbortProcessExecution(void)
{
#if defined(SDCARD_ISPLUGGED)
  CommandFile.Close();
#endif  
  // Close all
  TimeRelativeSystem.Wait_Abort();
#if defined(NTPCLIENT_ISPLUGGED)
  TimeAbsoluteSystem.Wait_Abort();
#endif  
#if defined(TRIGGERINPUT_ISPLUGGED)
  TriggerInputSystem.Wait_Abort();
#endif  
  //
  LedSystem.Blink_Abort();
#if defined(MULTICHANNELLED_ISPLUGGED)
  LedChannel1.Blink_Abort();
  LedChannel2.Blink_Abort();
  LedChannel3.Blink_Abort();
  LedChannel4.Blink_Abort();
  LedChannel5.Blink_Abort();
  LedChannel6.Blink_Abort();
  LedChannel7.Blink_Abort();
  LedChannel8.Blink_Abort();
#endif  
}
//
//#########################################################
//  Segment - Helper - Analyse
//#########################################################
//
void CCommand::Init(void)
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroCommandText();
}

bool CCommand::AnalyseCommandText(CSerial &serial)
{
  char *PTerminal = (char*)" \t\r\n";          
  // debug serial.WriteLine(FCommandText);
  FPCommand = strtok(FCommandText, PTerminal);
  if (FPCommand)
  {
    FParameterCount = 0;
    char *PParameter;
    while (0 != (PParameter = strtok(0, PTerminal)))
    {
      FPParameters[FParameterCount] = PParameter;
      FParameterCount++;
      if (COUNT_TEXTPARAMETERS < FParameterCount)
      {
        Error.SetCode(ecToManyParameters);
        ZeroRxdBuffer();
        serial.WriteNewLine();
        serial.WritePrompt();
        return false;
      }
    }  
    ZeroRxdBuffer();
    serial.WriteNewLine();
    return true;
  }
  ZeroRxdBuffer();
  serial.WriteNewLine();
  serial.WritePrompt();    
  return false;
}

bool CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case TERMINAL_CARRIAGERETURN:
        FRxdBuffer[FRxdBufferIndex] = TERMINAL_ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        // debug serial.WriteLine(FRxdBuffer);
        strcpy(FCommandText, FRxdBuffer);
        return true;
      case TERMINAL_LINEFEED: // ignore
        break;
      default: 
        // debug serial.WriteLine(FRxdBuffer);
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)
bool CCommand::DetectSDCardLine(CSDCard &sdcard)
{
  if (scIdle == GetState())
  { // ready for next Command
    if (CommandFile.IsExecuting())
    { // more Commands exist and no Wait-Command:
      if (TimeRelativeSystem.Wait_Execute()) return false;
#if defined(NTPCLIENT_ISPLUGGED)
      if (TimeAbsoluteSystem.Wait_Execute()) return false;
#endif      
#if defined(TRIGGERINPUT_ISPLUGGED)
      if (TriggerInputSystem.Wait_Execute()) return false;
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
      if (MotorPositionReached.Wait_Execute()) return false;
#endif  
      String Command = CommandFile.GetCommand();
      if (0 < Command.length())
      { // copy CommandFile-Line (next Command) to CommandBuffer:
        strcpy(FCommandText, Command.c_str());
        return true;
      }
    }
  }
  return false;
}
#endif
/*
ecf /sdc.cmd
*/
//
#if defined(MQTTCLIENT_ISPLUGGED)
bool CCommand::DetectMQTTClientLine(CMQTTClient &mqttclient)
{
  if (scIdle == GetState())
  {
    String Line = mqttclient.ReadReceivedText();
    if (0 < Line.length())
    {
      strcpy(FCommandText, Line.c_str());
      return true;
    }
  }
  return false;
}
#endif
//
#if defined(SDCARD_ISPLUGGED)
bool CCommand::Handle(CSerial &serial, CSDCard &sdcard)
{ // Command <- SDCard
  if (DetectSDCardLine(sdcard))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#endif // SDCARD_ISPLUGGED
//
#if defined(MQTTCLIENT_ISPLUGGED)
bool CCommand::Handle(CSerial &serial, CMQTTClient &mqttclient)
{ // Command <- Mqtt
  if (DetectMQTTClientLine(mqttclient))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#endif // MQTTCLIENT_ISPLUGGED
//
bool CCommand::Handle(CSerial &serial)
{ // Command <- (Usb)Serial
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
//
//#########################################################
//  Segment - Helper - Answer
//#########################################################
//
void CCommand::WriteEvent(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteEvent(line);
  SerialCommand.WritePrompt();
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteEvent(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteEvent(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteEvent(line);
//#endif 
}

void CCommand::WriteResponse(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteResponse(line);
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteResponse(line);
//???  MQTTClient.WritePrompt();
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteAnswer(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteAnswer(line);
//#endif
}

void CCommand::WriteComment(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteComment(line);
  SerialCommand.WritePrompt();
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteComment(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteComment(line);
//#endif 
}
void CCommand::WriteComment(String mask, String line)
{
  sprintf(GetTxdBuffer(), mask.c_str(), line.c_str());
  WriteComment(GetTxdBuffer());
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
//  serial.WriteComment();
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteComment();
//  serial.WriteNewLine();
//  serial.WriteComment();
  serial.WriteLine(MASK_ENDLINE);
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(TITLE_LINE);
  MQTTClient.WriteComment(MASK_PROJECT, ARGUMENT_PROJECT);
  MQTTClient.WriteComment(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  MQTTClient.WriteComment(MASK_HARDWARE, ARGUMENT_HARDWARE);
  MQTTClient.WriteComment(MASK_DATE, ARGUMENT_DATE);
  MQTTClient.WriteComment(MASK_TIME, ARGUMENT_TIME);
  MQTTClient.WriteComment(MASK_AUTHOR, ARGUMENT_AUTHOR);
  MQTTClient.WriteComment(MASK_PORT, ARGUMENT_PORT);
  MQTTClient.WriteComment(MASK_PARAMETER, ARGUMENT_PARAMETER);
  MQTTClient.WriteComment(TITLE_LINE);
  MQTTClient.WriteComment(MASK_ENDLINE);
#endif  
}
//
void CCommand::WriteIRQVersion(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED)
  sprintf(GetTxdBuffer(), MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteComment(GetTxdBuffer());
#endif
//######################################################
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
#endif
}
//
void CCommand::WriteHardwareVersion(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
  sprintf(GetTxdBuffer(), MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteComment(GetTxdBuffer());
#endif
//######################################################
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
#endif
}
//
void CCommand::WriteHelp(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
    serial.WriteNewLine();
    serial.WriteComment(); serial.WriteLine(HELP_COMMON);
    serial.WriteComment(); serial.Write(MASK_H, SHORT_H); serial.WriteLine();
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.Write(MASK_GPH, SHORT_GPH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GSV, SHORT_GSV); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GHV, SHORT_GHV); serial.WriteLine();
  #endif
    serial.WriteComment(); serial.Write(MASK_RSS, SHORT_RSS); serial.WriteLine();
  #if defined(WATCHDOG_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_PWD, SHORT_PWD); serial.WriteLine();
  #endif
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.Write(MASK_APE, SHORT_APE); serial.WriteLine();
  #endif
    serial.WriteComment(); serial.Write(MASK_WTR, SHORT_WTR); serial.WriteLine();
  #if defined(NTPCLIENT_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_WTA, SHORT_WTA); serial.WriteLine();
  #endif
  #if defined(TRIGGERINPUT_ISPLUGGED)  
    serial.WriteComment(); serial.Write(MASK_WTI, SHORT_WTI); serial.WriteLine();
  #endif
    //
    // System Commands Enabled
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.WriteLine(HELP_LEDSYSTEM);
    serial.WriteComment(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_LSH, SHORT_LSH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_LSL, SHORT_LSL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BLS, SHORT_BLS); serial.WriteLine();
  #endif
  #if defined(MULTICHANNELLED_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_BL1, SHORT_BL1); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL2, SHORT_BL2); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL3, SHORT_BL3); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL4, SHORT_BL4); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL5, SHORT_BL5); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL6, SHORT_BL6); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL7, SHORT_BL7); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL8, SHORT_BL8); serial.WriteLine();
  #endif
  //
  // Serial
  #if defined(SERIALCOMMAND_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_SERIAL_COMMAND);
    serial.WriteComment(); serial.Write(MASK_WSC, SHORT_WSC); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_RSC, SHORT_RSC); serial.WriteLine();
  #endif
  //
  #if defined(I2CDISPLAY_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_I2CDISPLAY);
    serial.WriteComment(); serial.Write(MASK_CLI, SHORT_CLI); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_STI, SHORT_STI); serial.WriteLine();
  #endif
  //
  #if defined(SDCARD_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_SDCOMMAND);
//!!!    serial.WriteComment(); serial.Write(MASK_OCF, SHORT_OCF); serial.WriteLine();
//!!!    serial.WriteComment(); serial.Write(MASK_WCF, SHORT_WCF); serial.WriteLine();
//!!!    serial.WriteComment(); serial.Write(MASK_CCF, SHORT_CCF); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_ECF, SHORT_ECF); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_ACF, SHORT_ACF); serial.WriteLine();
  #endif
  //
  // NTPClient
  #if defined(NTPCLIENT_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_NTPCLIENT);
    serial.WriteComment(); serial.Write(MASK_GNT, SHORT_GNT); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GND, SHORT_GND); serial.WriteLine();
  #endif
  //
  //
  // MQTTClient
  #if defined(MQTTCLIENT_ISPLUGGED)
  // !!!!!!!!  serial.WriteAnswer(); serial.WriteLine(HELP_MQTTCLIENT);
  #endif
  //
  // MotorL298N
  #if defined(MOTORL298N_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_MOTORL298N);
    serial.WriteComment(); serial.Write(MASK_GWL, SHORT_GWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SWL, SHORT_SWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GWH, SHORT_GWH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SWH, SHORT_SWH); serial.WriteLine();
    //
    serial.WriteComment(); serial.Write(MASK_MMP, SHORT_MMP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MMN, SHORT_MMN); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SM, SHORT_SM);   serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_A, SHORT_A);   serial.WriteLine();
  #endif
    //
    // EncoderIRQ
  #if defined(ENCODERIRQ_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_ENCODERIRQ);
    serial.WriteComment(); serial.Write(MASK_GEP, SHORT_GEP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SEP, SHORT_SEP); serial.WriteLine();
  #endif
    //
    // MotorEncoder
  #if defined(MOTORENCODER_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_MOTORENCODER);
    serial.WriteComment(); serial.Write(MASK_MFP, SHORT_MFP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MFN, SHORT_MFN); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MPA, SHORT_MPA); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MPR, SHORT_MPR); serial.WriteLine();
  #endif
    //
    // Trigger
  #if defined(TRIGGER_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_TRIGGER);
    serial.WriteComment(); serial.WriteLine(HELP_TRIGGERPERIOD);
    serial.WriteComment(); serial.Write(MASK_GTP, SHORT_GTP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_STP, SHORT_STP); serial.WriteLine();
    serial.WriteComment(); serial.WriteLine(HELP_TMOVECOMMON);
    serial.WriteComment(); serial.Write(MASK_GTR, SHORT_GTR); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_STR, SHORT_STR); serial.WriteLine();
    serial.WriteComment(); serial.WriteLine(HELP_TFREEMOVE);
    serial.WriteComment(); serial.Write(MASK_GTC, SHORT_GTC); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_STC, SHORT_STC); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_TMP, SHORT_TMP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_TMN, SHORT_TMN); serial.WriteLine();
    serial.WriteComment(); serial.WriteLine(HELP_TPOSITIONCONTROLLED);
    serial.WriteComment(); serial.Write(MASK_GPR, SHORT_GPR); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SPR, SHORT_SPR); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_TPA, SHORT_TPA); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_TPR, SHORT_TPR); serial.WriteLine();
  #endif
  //
  serial.WriteComment(); serial.WriteLine(MASK_ENDLINE);
#endif
//################################################################
#if defined(MQTTCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_COMMON);
    MQTTClient.WriteComment(MASK_H, SHORT_H);
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(MASK_GPH, SHORT_GPH);
    MQTTClient.WriteComment(MASK_GSV, SHORT_GSV);
    MQTTClient.WriteComment(MASK_GHV, SHORT_GHV);
  #endif
    MQTTClient.WriteComment(MASK_RSS, SHORT_RSS);
  #if defined(WATCHDOG_ISPLUGGED)
    MQTTClient.WriteComment(MASK_PWD, SHORT_PWD);
  #endif
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(MASK_A, SHORT_A);
  #endif
  #if defined(NTPCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(MASK_WTR, SHORT_WTR);
    MQTTClient.WriteComment(MASK_WTA, SHORT_WTA);
  #endif
  #if defined(TRIGGERINPUT_ISPLUGGED)
    MQTTClient.WriteComment(MASK_WTI, SHORT_WTI);
  #endif
    //
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(HELP_LEDSYSTEM);
    MQTTClient.WriteComment(MASK_GLS, SHORT_GLS);
    MQTTClient.WriteComment(MASK_LSH, SHORT_LSH);
    MQTTClient.WriteComment(MASK_LSL, SHORT_LSL);
    MQTTClient.WriteComment(MASK_BLS, SHORT_BLS);
  #endif
  #if defined(MULTICHANNELLED_ISPLUGGED)  
    MQTTClient.WriteComment(MASK_BL1, SHORT_BL1);
    MQTTClient.WriteComment(MASK_BL2, SHORT_BL2);
    MQTTClient.WriteComment(MASK_BL3, SHORT_BL3);
    MQTTClient.WriteComment(MASK_BL4, SHORT_BL4);
    MQTTClient.WriteComment(MASK_BL5, SHORT_BL5);
    MQTTClient.WriteComment(MASK_BL6, SHORT_BL6);
    MQTTClient.WriteComment(MASK_BL7, SHORT_BL7);
    MQTTClient.WriteComment(MASK_BL8, SHORT_BL8);
  #endif
  //
  // Serial
  MQTTClient.WriteComment(HELP_SERIAL_COMMAND);
  MQTTClient.WriteComment(MASK_WLC, SHORT_WLC);
  MQTTClient.WriteComment(MASK_RLC, SHORT_RLC);
  //
  // I2CDisplay
  #if defined(I2CDISPLAY_ISPLUGGED)
    MQTTClient.WriteComment(HELP_I2CDISPLAY);
    MQTTClient.WriteComment(MASK_CLI, SHORT_CLI);
    MQTTClient.WriteComment(MASK_STI, SHORT_STI);
  #endif
  //
  // NTPClient
  #if defined(NTPCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_NTPCLIENT);
    MQTTClient.WriteComment(MASK_GNT, SHORT_GNT);
    MQTTClient.WriteComment(MASK_GND, SHORT_GND);
  #endif
  //
  // SDCard
  #if defined(SDCARD_ISPLUGGED)
    MQTTClient.WriteComment(HELP_SDCOMMAND);
    // !!! MQTTClient.WriteComment(MASK_OCF, SHORT_OCF);
    // !!! MQTTClient.WriteComment(MASK_WCF, SHORT_WCF);
    // !!! MQTTClient.WriteComment(MASK_CCF, SHORT_CCF);
    MQTTClient.WriteComment(MASK_ECF, SHORT_ECF);
    MQTTClient.WriteComment(MASK_ACF, SHORT_ACF);
  #endif
  //
  // RFIDClient
  #if defined(RFIDCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_RFIDCLIENT);
    // ... MQTTClient.WriteComment(MASK_SRL, SHORT_SRL); 
  #endif
    //
  #if defined(MQTTCLIENT_ISPLUGGED)
    // MQTTClient.WriteComment(HELP_MQTTCLIENT);
  #endif
    //
  // MotorVNH2SP30
  #if defined(MOTORVNH2SP30_ISPLUGGED)
    MQTTClient.WriteComment(HELP_MOTORVNH2SP30);
    MQTTClient.WriteComment(MASK_GLWL, SHORT_GLWL);
    MQTTClient.WriteComment(MASK_SLWL, SHORT_SLWL);
    MQTTClient.WriteComment(MASK_GLWH, SHORT_GLWH);
    MQTTClient.WriteComment(MASK_SLWH, SHORT_SLWH);
    //
    MQTTClient.WriteComment(MASK_GRWL, SHORT_GRWL);
    MQTTClient.WriteComment(MASK_SRWL, SHORT_SRWL);
    MQTTClient.WriteComment(MASK_GRWH, SHORT_GRWH);
    MQTTClient.WriteComment(MASK_SRWH, SHORT_SRWH);
    //
    MQTTClient.WriteComment(MASK_A,   SHORT_A);
    MQTTClient.WriteComment(MASK_SL,  SHORT_SL);
    MQTTClient.WriteComment(MASK_SR,  SHORT_SR);
    MQTTClient.WriteComment(MASK_MLP, SHORT_MLP);
    MQTTClient.WriteComment(MASK_MLN, SHORT_MLN);
    MQTTClient.WriteComment(MASK_MRP, SHORT_MRP);
    MQTTClient.WriteComment(MASK_MRN, SHORT_MRN);
    MQTTClient.WriteComment(MASK_MBP, SHORT_MBP);
    MQTTClient.WriteComment(MASK_MBN, SHORT_MBN);
    MQTTClient.WriteComment(MASK_RBP, SHORT_RBP);
    MQTTClient.WriteComment(MASK_RBN, SHORT_RBN);
  #endif
    //
    // MotorEncoderLM393
  #if defined(MOTORENCODERLM393_ISPLUGGED)
    MQTTClient.WriteComment(HELP_MOTORENCODERLM393);
    MQTTClient.WriteComment(MASK_GEPB, SHORT_GEPB);
    MQTTClient.WriteComment(MASK_SEPL, SHORT_SEPL);
    MQTTClient.WriteComment(MASK_SEPR, SHORT_SEPR);
    MQTTClient.WriteComment(MASK_WPR, SHORT_WPR);
  #endif
    //
    // RF433MHzClient / RemoteWirelessSwitch
  #if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
    MQTTClient.WriteComment(HELP_REMOTEWIRELESSSWITCH);
    MQTTClient.WriteComment(MASK_RSO, SHORT_RSO);
    MQTTClient.WriteComment(MASK_RSF, SHORT_RSF);
  #endif  
    //
    // LaserScanner
  #if defined(LASERSCANNER_JI)
    MQTTClient.WriteComment(HELP_LASERSCANNER);
    MQTTClient.WriteComment(MASK_SPP, SHORT_SPP);
    MQTTClient.WriteComment(MASK_GPP, SHORT_GPP);
    MQTTClient.WriteComment(MASK_PLA, SHORT_PLA);
    MQTTClient.WriteComment(MASK_PLC, SHORT_PLC);
    MQTTClient.WriteComment(MASK_MPX, SHORT_MPX);
    MQTTClient.WriteComment(MASK_MPY, SHORT_MPY);
    MQTTClient.WriteComment(MASK_MPP, SHORT_MPP);
  #endif
  #if defined(LASERSCANNER_PS)
    MQTTClient.WriteComment(HELP_LASERSCANNER);
    MQTTClient.WriteComment(MASK_, SHORT_);
    //
  #endif
    //
    MQTTClient.WriteComment(MASK_ENDLINE);   
#endif
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
bool CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteHelp(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
  return true;
}
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteProgramHeader(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
  return true;
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteGetIRQVersion(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), COUNT_SOFTWAREVERSION);  
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteIRQVersion(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), COUNT_HARDWAREVERSION);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteHardwareVersion(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(WATCHDOG_ISPLUGGED)
bool CCommand::ExecutePulseWatchDog(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( PWD - ):
  // Execute:
  WatchDog.ForceTrigger();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
bool CCommand::ExecuteResetSystem(CSerial &serial)
{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return false;
//  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RSS - ):
  // Execute:
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  delay(2000);
  //
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // Reset System !!!
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if (defined(PROCESSOR_NANOR3)||defined(PROCESSOR_UNOR3)||defined(PROCESSOR_MEGA2560))
  PResetSystem();
#elif defined(PROCESSOR_DUEM3)
#elif defined(PROCESSOR_STM32F103C8)
#elif (defined(PROCESSOR_TEENSY32)||defined(PROCESSOR_TEENSY36))
  // problem because of reprogramming !!! _reboot_Teensyduino_();
  // undefined _restart_Teensyduino_();
  // undefined init_pins();
  // the only possibility for this time:
  // OK (no reset!) _init_Teensyduino_internal_();
  // TOP:!!!
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
  CPU_RESTART
  //
#endif
  return true;
}
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteAbortProcessExecution(CSerial &serial)
{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return false;
//  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( A - ):
  // Execute:
  AbortProcessExecution();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
bool CCommand::ExecuteWaitTimeRelative(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: { WTR <time> }
  UInt32 TR = atol(GetPParameters(0));
  // Execute:
  TimeRelativeSystem.Wait_Start(TR);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), (long int)TR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
//
#if defined(NTPCLIENT_ISPLUGGED)
bool CCommand::ExecuteWaitTimeAbsolute(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( WTA hh mm ss ):
  Byte HH = atoi(GetPParameters(0));
  Byte MM = atoi(GetPParameters(1));
  Byte SS = atoi(GetPParameters(2));
  // Execute:
  TimeAbsoluteSystem.Wait_Start(HH, MM, SS);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u %u %u", GetPCommand(), HH, MM, SS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGERINPUT_ISPLUGGED)
bool CCommand::ExecuteWaitTriggerInput(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( WTI re ):    
  bool RE  = (0 < atoi(GetPParameters(0)));
  // Execute:
  TriggerInputSystem.Wait_Start(RE);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), (int)RE);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - NTPClient
//#########################################################
//
#if defined(NTPCLIENT_ISPLUGGED)
bool CCommand::ExecuteGetNTPClientTime(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GNT - ):
  // Execute:
  String HH = "??";
  String MM = "??";
  String SS = "??";
  NTPClient.GetTime(HH, MM, SS);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %s %s %s", GetPCommand(), HH, MM, SS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(NTPCLIENT_ISPLUGGED)
bool CCommand::ExecuteGetNTPClientDate(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GND - ):
  // Execute:
  String YY = "??";
  String MM = "??";
  String DD = "??";
  NTPClient.GetDate(YY, MM, DD);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %s %s %s", GetPCommand(), YY, MM, DD);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteGetLedSystem(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  int SLS = LedSystem.GetState();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), SLS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
  return true;
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteLedSystemOn(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  LedSystem.SetOn();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
  return true;
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteLedSystemOff(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  LedSystem.SetOff();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
  return true;
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
bool CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ // <BLS> <c> <p> <w>
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedSystem.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u %u %u", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel1(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel1.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel2(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel2.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel3(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel3.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel4(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel4.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel5(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel5.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel6(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel6.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel7(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel7.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
bool CCommand::ExecuteBlinkLedChannel8(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel8.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - SDCommand
//#########################################################
//
#if defined(SDCARD_ISPLUGGED)
bool CCommand::ExecuteOpenCommandFile(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( OCF <f> ):
  String FN = GetPParameters(0);
 // ???? Automation.SetCommandFileName(FN);
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", GetPCommand(), GetPParameters(0));
  WriteResponse(GetTxdBuffer());
  return true;
}
#endif

#if defined(SDCARD_ISPLUGGED)
bool CCommand::ExecuteWriteCommandFile(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( WCF <c> <p> ):
  // Execution: -
  // Response:
  serial.Write(TERMINAL_RESPONSE);
  serial.Write(' ');
  serial.Write(GetPCommand());
  serial.Write(' ');
  int PC = GetParameterCount();
  for (int II = 0; II < PC; II++)
  {
    serial.Write(GetPParameters(II));
    serial.Write(' ');
  }
  serial.WriteLine("");
  serial.WritePrompt();
  return true;
}
#endif

#if defined(SDCARD_ISPLUGGED)
bool CCommand::ExecuteCloseCommandFile(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  // Analyse parameters ( CCF - ):
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  return true;
}
#endif

#if defined(SDCARD_ISPLUGGED)
bool CCommand::ExecuteExecuteCommandFile(CSerial &serial)
{ // ecf /sdc.cmd
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( ECF <f> ):
  String CF = GetPParameters(0);
  // Execution: 
  // !!! !!! !!! !!! !!! !!!
  CommandFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!CommandFile.ParseFile(CF.c_str()))
  {
    Error.SetCode(ecFailParseCommandFile);
    return false;
  }
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", GetPCommand(), CF.c_str());
  SerialCommand.WritePrompt();
  WriteResponse(GetTxdBuffer());
  // Response:
  SerialCommand.Write("# CommandFile[");
  SerialCommand.Write(CF);
  SerialCommand.WriteLine("]:");
  bool CommandLoop = true;
  while (CommandLoop)
  {
    String Command = CommandFile.GetCommand();
    CommandLoop = (0 < Command.length());
    if (CommandLoop)
    {
      SerialCommand.Write("# Command[");
      SerialCommand.Write(Command.c_str());
      SerialCommand.WriteLine("]");
    }
  }
  // Preparation for executing Commands from CommandFile:
  CommandFile.Open(&SD);
  if (!CommandFile.ParseFile(CF.c_str()))
  {
    Error.SetCode(ecFailParseCommandFile);
    return false;
  }
  // !!! !!! !!! !!! !!! !!!
  CommandFile.ResetExecution();
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();  
  // only after Command-Completition: CommandFile.Close();
  return true;
}
#endif
/*
ecf /sdc.cmd 
 */
//
#if defined(SDCARD_ISPLUGGED)
bool CCommand::ExecuteAbortCommandFile(CSerial &serial)
{
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( ACF - ):
  // Execution:
  AbortAll();
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s 1", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - Serial - Command
//#########################################################
bool CCommand::ExecuteWriteLineSerialCommand(CSerial &serial)
{ // must be corrected!!!
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }  
  // Analyse parameters ( WLC <l> ):
  String Line = GetPParameters(0);
  // Execute:
  SerialCommand.WriteLine(Line);
  // Response:
  sprintf(Command.GetTxdBuffer(), "%s %s", GetPCommand(), Line.c_str());
  WriteResponse(Command.GetTxdBuffer());
  return true;
}

bool CCommand::ExecuteReadLineSerialCommand(CSerial &serial)
{ // must be corrected!!!
  // Analyse parameters ( RLC - ):
  // Execute:
  String Line = SerialCommand.ReadLine();
  // Response:
  sprintf(Command.GetTxdBuffer(), "%s %s", GetPCommand(), Line.c_str());
  WriteResponse(Command.GetTxdBuffer());
  return true;
}
//
//#########################################################
//  Segment - Execution - I2CDisplay
//#########################################################
//
#if defined(I2CDISPLAY_ISPLUGGED)
bool CCommand::ExecuteClearScreenI2CDisplay(CSerial &serial)
{
  // Analyse parameters ( CLI - ):
  // Execute:
  I2CDisplay.ClearDisplay();
  // Response:
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif

#if defined(I2CDISPLAY_ISPLUGGED)
bool CCommand::ExecuteShowTextI2CDisplay(CSerial &serial)
{
  //!!!!if (GetParameterCount() < 3)
  // Analyse parameters ( STI <c> <r> <t> ):
  Byte R = atol(GetPParameters(0));
  Byte C = atol(GetPParameters(1));
  String T = GetPParameters(2);
  // Execute:
  I2CDisplay.SetCursorPosition(R, C);
  I2CDisplay.WriteText(T);
  // Response:
  sprintf(GetTxdBuffer(), "%s %i %i %s", GetPCommand(), R, C, T.c_str());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - MotorL298N
//#########################################################
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteGetPwmLow(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GWL <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 P = 0;
  switch (A)
  {
    case 'X':
      P = MotorX.GetPwmLow();
      break;
    case 'Y':
      P = MotorY.GetPwmLow();
      break;
    case 'U':
      P = MotorU.GetPwmLow();
      break;
    case 'V':
      P = MotorV.GetPwmLow();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteSetPwmLow(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SWL <a> <p>):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 P = atoi(GetPParameters(1));
  switch (A)
  {
    case 'X':
      MotorX.SetPwmLow(P);
      break;
    case 'Y':
      MotorY.SetPwmLow(P);
      break;
    case 'U':
      MotorU.SetPwmLow(P);
      break;
    case 'V':
      MotorV.SetPwmLow(P);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteGetPwmHigh(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GWH <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 P = 0;
  switch (A)
  {
    case 'X':
      P = MotorX.GetPwmHigh();
      break;
    case 'Y':
      P = MotorY.GetPwmHigh();
      break;
    case 'U':
      P = MotorU.GetPwmHigh();
      break;
    case 'V':
      P = MotorV.GetPwmHigh();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteSetPwmHigh(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SWH <a> <p>):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 P = atoi(GetPParameters(1));
  switch (A)
  {
    case 'X':
      MotorX.SetPwmHigh(P);
      break;
    case 'Y':
      MotorY.SetPwmHigh(P);
      break;
    case 'U':
      MotorU.SetPwmHigh(P);
      break;
    case 'V':
      MotorV.SetPwmHigh(P);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteMoveMotorPositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  if (smBusy == MotorX.GetState())
  {
    MotorX.AbortMotion();
  }
  if (smBusy == MotorY.GetState())
  {
    MotorY.AbortMotion();
  }
  if (smBusy == MotorU.GetState())
  {
    MotorU.AbortMotion();
  }
  if (smBusy == MotorV.GetState())
  {
    MotorV.AbortMotion();
  }
  //
  SetState(scBusy);
  // Analyse parameters ( MMP <a> <v>):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 V = atoi(GetPParameters(1));
  switch (A)
  {
    case 'X':
      MotorEncoderX.SetMotorActivity(maFreeRun);
      MotorX.MovePositive(V);
      break;
    case 'Y':
      MotorEncoderY.SetMotorActivity(maFreeRun);
      MotorY.MovePositive(V);
      break;
    case 'U':
      MotorEncoderU.SetMotorActivity(maFreeRun);
      MotorU.MovePositive(V);
      break;
    case 'V':
      MotorEncoderV.SetMotorActivity(maFreeRun);
      MotorV.MovePositive(V);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, V);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteMoveMotorNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  if (smBusy == MotorX.GetState())
  {
    MotorX.AbortMotion();
  }
  if (smBusy == MotorY.GetState())
  {
    MotorY.AbortMotion();
  }
  if (smBusy == MotorU.GetState())
  {
    MotorU.AbortMotion();
  }
  if (smBusy == MotorV.GetState())
  {
    MotorV.AbortMotion();
  }
  //
  SetState(scBusy);
  // Analyse parameters ( MMP <a> <v>):
  char A = GetPParameters(0)[0];
  // Execute:
  UInt16 V = atoi(GetPParameters(1));
  switch (A)
  {
    case 'X':
      MotorEncoderX.SetMotorActivity(maFreeRun);
      MotorX.MoveNegative(V);
      break;
    case 'Y':
      MotorEncoderY.SetMotorActivity(maFreeRun);
      MotorY.MoveNegative(V);
      break;
    case 'U':
      MotorEncoderU.SetMotorActivity(maFreeRun);
      MotorU.MoveNegative(V);
      break;
    case 'V':
      MotorEncoderV.SetMotorActivity(maFreeRun);
      MotorV.MoveNegative(V);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %u", GetPCommand(), A, V);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
// HELPER!!!!!!!!!!!!!!!!
#if defined(MOTORL298N_ISPLUGGED)
void CCommand::StopAllAxes(void)
{
  TriggerX.SetState(stDisabled);
  TriggerY.SetState(stDisabled);
  TriggerU.SetState(stDisabled);
  TriggerV.SetState(stDisabled);
  //
  if (smBusy == MotorX.GetState())
  {
    MotorX.AbortMotion();
  }
  if (smBusy == MotorY.GetState())
  {
    MotorY.AbortMotion();
  }
  if (smBusy == MotorU.GetState())
  {
    MotorU.AbortMotion();
  }
  if (smBusy == MotorV.GetState())
  {
    MotorV.AbortMotion();
  }
}
#endif
//
// HELPER!!!!!!!!!!!!!!!!
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteStopMotor(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SM <m> ):
  char M = GetPParameters(0)[0];
  // Execute:
  switch (M)
  {
    case 'X':
      MotorX.StopMotion();
      break;
    case 'Y':
      MotorY.StopMotion();
      break;
    case 'U':
      MotorU.StopMotion();
      break;
    case 'V':
      MotorV.StopMotion();
      break;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c", GetPCommand(), M);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
bool CCommand::ExecuteAbortAllMotors(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( A - ):
  // Execute:
  MotorX.AbortMotion();
  MotorY.AbortMotion();
  MotorU.AbortMotion();
  MotorV.AbortMotion();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - EncoderIRQ
//#########################################################
#if defined(ENCODERIRQ_ISPLUGGED)
bool CCommand::ExecuteGetEncoderPosition(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GEP <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 P = 0;
  switch (A)
  {
    case 'X':
      P = EncoderX.GetPosition();
      break;
    case 'Y':
      P = EncoderY.GetPosition();
      break;
    case 'U':
      P = EncoderU.GetPosition();
      break;
    case 'V':
      P = EncoderV.GetPosition();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(ENCODERIRQ_ISPLUGGED)
bool CCommand::ExecuteSetEncoderPosition(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( SEP <a> <p>):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 P = (Int32)atol(GetPParameters(1));
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      EncoderX.SetPosition(P);
      break;
    case 'y': case 'Y':
      A = 'Y';
      EncoderY.SetPosition(P);
      break;
    case 'u': case 'U':
      A = 'U';
      EncoderU.SetPosition(P);
      break;
    case 'v': case 'V':
      A = 'V';
      EncoderV.SetPosition(P);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Execution - MotorEncoder
//#########################################################
#if defined(MOTORENCODER_ISPLUGGED)
bool CCommand::ExecuteMoveFreePositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( MFP <a> <v> ):
  char A = GetPParameters(0)[0];
  UInt8 V = atoi(GetPParameters(1));
  // Execute:
  switch (A)
  {
    case 'X':
      MotorEncoderX.MoveMotorPositive(V);
      break;
    case 'Y':
      MotorEncoderY.MoveMotorPositive(V);
      break;
    case 'U':
      MotorEncoderU.MoveMotorPositive(V);
      break;
    case 'V':
      MotorEncoderV.MoveMotorPositive(V);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, V);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORENCODER_ISPLUGGED)
bool CCommand::ExecuteMoveFreeNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( MFN <a> <v> ):
  char A = GetPParameters(0)[0];
  UInt8 V = atoi(GetPParameters(1));
  // Execute:
  switch (A)
  {
    case 'X':
      MotorEncoderX.MoveMotorNegative(V);
      break;
    case 'Y':
      MotorEncoderY.MoveMotorNegative(V);
      break;
    case 'U':
      MotorEncoderU.MoveMotorNegative(V);
      break;
    case 'V':
      MotorEncoderV.MoveMotorNegative(V);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, V);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORENCODER_ISPLUGGED)
bool CCommand::ExecuteMovePositionAbsolute(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( MPA <a> <p> <v> ):
  char A = GetPParameters(0)[0];
  Int32 PT = atoi(GetPParameters(1));
  UInt8 VT = atoi(GetPParameters(2));
  // Execute:
  switch (A)
  {
    case 'X':
      MotorEncoderX.MovePositionAbsolute(PT, VT);
      break;
    case 'Y':
      MotorEncoderY.MovePositionAbsolute(PT, VT);
      break;
    case 'U':
      MotorEncoderU.MovePositionAbsolute(PT, VT);
      break;
    case 'V':
      MotorEncoderV.MovePositionAbsolute(PT, VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PT, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(MOTORENCODER_ISPLUGGED)
bool CCommand::ExecuteMovePositionRelative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( MPR <a> <p> <v> ):
  char A = GetPParameters(0)[0];
  Int32 PT = atoi(GetPParameters(1));
  UInt8 VT = atoi(GetPParameters(2));
  // Execute:
  switch (A)
  {
    case 'X':
      MotorEncoderX.MovePositionRelative(PT, VT);
      break;
    case 'Y':
      MotorEncoderY.MovePositionRelative(PT, VT);
      break;
    case 'U':
      MotorEncoderU.MovePositionRelative(PT, VT);
      break;
    case 'V':
      MotorEncoderV.MovePositionRelative(PT, VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PT, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif





//
//#########################################################
//  Segment - Trigger - TriggerPeriod
//#########################################################
//
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteSetTriggerPeriodCount(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( STP <a> <p> <c>):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 TP = (Int32)atol(GetPParameters(1));
  Int32 TC = (Int32)atol(GetPParameters(2));
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TriggerX.SetTriggerPeriod(TP);
      TriggerX.SetTriggerCount(TC);
      break;
    case 'y': case 'Y':
      A = 'Y';
      TriggerY.SetTriggerPeriod(TP);
      TriggerY.SetTriggerCount(TC);
      break;
    case 'u': case 'U':
      A = 'U';
      TriggerU.SetTriggerPeriod(TP);
      TriggerU.SetTriggerCount(TC);
      break;
    case 'v': case 'V':
      A = 'V';
      TriggerV.SetTriggerPeriod(TP);
      TriggerV.SetTriggerCount(TC);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, TP, TC);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteGetTriggerPeriodCount(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( GTC <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 TP = 0;
  Int32 TC = 0;
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TP = TriggerX.GetTriggerPeriod();
      TC = TriggerX.GetTriggerCount();
      break;
    case 'y': case 'Y':
      A = 'Y';
      TP = TriggerY.GetTriggerPeriod();
      TC = TriggerY.GetTriggerCount();
      break;
    case 'u': case 'U':
      A = 'U';
      TP = TriggerU.GetTriggerPeriod();
      TC = TriggerU.GetTriggerCount();
      break;
    case 'v': case 'V':
      A = 'V';
      TP = TriggerV.GetTriggerPeriod();
      TC = TriggerV.GetTriggerCount();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, TP, TC);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Trigger - MOveCommon
//#########################################################
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteSetTriggerReduction(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( STR <a> <r> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 TR = (Int32)atol(GetPParameters(1));
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TriggerX.SetTriggerReduction(TR);
      break;
    case 'y': case 'Y':
      A = 'Y';
      TriggerY.SetTriggerReduction(TR);
      break;
    case 'u': case 'U':
      A = 'U';
      TriggerU.SetTriggerReduction(TR);
      break;
    case 'v': case 'V':
      A = 'V';
      TriggerV.SetTriggerReduction(TR);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, TR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteGetTriggerReduction(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( GTC <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 TR = 0;
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TR = TriggerX.GetTriggerReduction();
      break;
    case 'y': case 'Y':
      A = 'Y';
      TR = TriggerY.GetTriggerReduction();
      break;
    case 'u': case 'U':
      A = 'U';
      TR = TriggerU.GetTriggerReduction();
      break;
    case 'v': case 'V':
      A = 'V';
      TR = TriggerV.GetTriggerReduction();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, TR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Trigger - FreeMoving
//#########################################################
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteSetTriggerCountOffsetPreset(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( STC <a> <o> <p>):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 CO = (Int32)atol(GetPParameters(1));
  Int32 CP = (Int32)atol(GetPParameters(2));
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TriggerX.SetCountOffset(CO);
      TriggerX.SetCountOffset(CP);
      break;
    case 'y': case 'Y':
      A = 'Y';
      TriggerY.SetCountOffset(CO);
      TriggerY.SetCountOffset(CP);
      break;
    case 'u': case 'U':
      A = 'U';
      TriggerU.SetCountOffset(CO);
      TriggerU.SetCountOffset(CP);
      break;
    case 'v': case 'V':
      A = 'V';
      TriggerV.SetCountOffset(CO);
      TriggerV.SetCountOffset(CP);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, CO, CP);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteGetTriggerCountOffsetPreset(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( GTC <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 CO = 0;
  Int32 CP = 0;
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      CO = TriggerX.GetCountOffset();
      CP = TriggerX.GetCountPreset();
      break;
    case 'y': case 'Y':
      A = 'Y';
      CO = TriggerY.GetCountOffset();
      CP = TriggerY.GetCountPreset();
      break;
    case 'u': case 'U':
      A = 'U';
      CO = TriggerU.GetCountOffset();
      CP = TriggerU.GetCountPreset();
      break;
    case 'v': case 'V':
      A = 'V';
      CO = TriggerV.GetCountOffset();
      CP = TriggerV.GetCountPreset();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, CO, CP);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteTriggerMovePositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( TMP <m> <w> ):
  char A = GetPParameters(0)[0];
  UInt8 VT = atoi(GetPParameters(1));
  // Execute:
  switch (A)
  {
    case 'X':
      TriggerX.SetState(stFreeMoving);
      MotorEncoderX.MoveMotorPositive(VT);
      break;
    case 'Y':
      TriggerY.SetState(stFreeMoving);
      MotorEncoderY.MoveMotorPositive(VT);
      break;
    case 'U':
      TriggerU.SetState(stFreeMoving);
      MotorEncoderU.MoveMotorPositive(VT);
      break;
    case 'V':
      TriggerV.SetState(stFreeMoving);
      MotorEncoderV.MoveMotorPositive(VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteTriggerMoveNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( TMN <m> <w> ):
  char A = GetPParameters(0)[0];
  UInt8 VT = atoi(GetPParameters(1));
  // Execute:
  switch (A)
  {
    case 'X':
      TriggerX.SetState(stFreeMoving);
      MotorEncoderX.MoveMotorNegative(VT);
      break;
    case 'Y':
      TriggerY.SetState(stFreeMoving);
      MotorEncoderY.MoveMotorNegative(VT);
      break;
    case 'U':
      TriggerU.SetState(stFreeMoving);
      MotorEncoderU.MoveMotorNegative(VT);
      break;
    case 'V':
      TriggerV.SetState(stFreeMoving);
      MotorEncoderV.MoveMotorNegative(VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i", GetPCommand(), A, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//#########################################################
//  Segment - Trigger - PositionControlled
//#########################################################
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteSetTriggerPositionRange(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( SPR <a> <s> <e>):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 PS = (Int32)atol(GetPParameters(1));
  Int32 PE = (Int32)atol(GetPParameters(2));
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      TriggerX.SetPositionStart(PS);
      TriggerX.SetPositionEnd(PE);
      break;
    case 'y': case 'Y':
      A = 'Y';
      TriggerY.SetPositionStart(PS);
      TriggerY.SetPositionEnd(PE);
      break;
    case 'u': case 'U':
      A = 'U';
      TriggerU.SetPositionStart(PS);
      TriggerU.SetPositionEnd(PE);
      break;
    case 'v': case 'V':
      A = 'V';
      TriggerV.SetPositionStart(PS);
      TriggerV.SetPositionEnd(PE);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PS, PE);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteGetTriggerPositionRange(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  // Analyse parameters ( GPR <a> ):
  char A = GetPParameters(0)[0];
  // Execute:
  Int32 PS = 0;
  Int32 PE = 0;
  switch (A)
  {
    case 'x': case 'X':
      A = 'X';
      PS = TriggerX.GetPositionStart();
      PE = TriggerX.GetPositionEnd();
      break;
    case 'y': case 'Y':
      A = 'Y';
      PS = TriggerY.GetPositionStart();
      PE = TriggerY.GetPositionEnd();
      break;
    case 'u': case 'U':
      A = 'U';
      PS = TriggerU.GetPositionStart();
      PE = TriggerU.GetPositionEnd();
      break;
    case 'v': case 'V':
      A = 'V';
      PS = TriggerV.GetPositionStart();
      PE = TriggerV.GetPositionEnd();
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PS, PE);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteTriggerPositionAbsolute(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( TPA <m> <p> <v> ):
  char A = GetPParameters(0)[0];
  Int32 PT = atoi(GetPParameters(1));
  UInt8 VT = atoi(GetPParameters(2));
  // Execute:
  switch (A)
  {
    case 'X':
      TriggerX.SetState(stPositionControlled);
      MotorEncoderX.MovePositionAbsolute(PT, VT);
      break;
    case 'Y':
      TriggerY.SetState(stPositionControlled);
      MotorEncoderY.MovePositionAbsolute(PT, VT);
      break;
    case 'U':
      TriggerU.SetState(stPositionControlled);
      MotorEncoderU.MovePositionAbsolute(PT, VT);
      break;
    case 'V':
      TriggerV.SetState(stPositionControlled);
      MotorEncoderV.MovePositionAbsolute(PT, VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PT, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
#if defined(TRIGGER_ISPLUGGED)
bool CCommand::ExecuteTriggerPositionRelative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return false;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return false;
  }
  SerialCommand.WritePrompt();
  //
  StopAllAxes();
  //
  SetState(scBusy);
  // Analyse parameters ( TPR <m> <p> <v> ):
  char A = GetPParameters(0)[0];
  Int32 PT = atoi(GetPParameters(1));
  UInt8 VT = atoi(GetPParameters(2));
  // Execute:
  switch (A)
  {
    case 'X':
      TriggerX.SetState(stPositionControlled);
      MotorEncoderX.MovePositionRelative(PT, VT);
      break;
    case 'Y':
      TriggerY.SetState(stPositionControlled);
      MotorEncoderY.MovePositionRelative(PT, VT);
      break;
    case 'U':
      TriggerU.SetState(stPositionControlled);
      MotorEncoderU.MovePositionRelative(PT, VT);
      break;
    case 'V':
      TriggerV.SetState(stPositionControlled);
      MotorEncoderV.MovePositionRelative(PT, VT);
      break;
    default:
      Error.SetCode(ecInvalidAxis);
      return false;
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %c %i %i", GetPCommand(), A, PT, VT);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  return true;
}
#endif
//
//
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
bool CCommand::Execute(CSerial &serial)
{
  // debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
  // debug Serial.WriteLine(TxdBuffer);
  //  Common
  if (!strcmp(SHORT_H, GetPCommand()))
  {
    return ExecuteGetHelp(serial);
  } else
#if defined(COMMAND_SYSTEMENABLED)
  if (!strcmp(SHORT_GPH, GetPCommand()))
  {
    return ExecuteGetProgramHeader(serial);
  } else 
  if (!strcmp(SHORT_GSV, GetPCommand()))
  {
    return ExecuteGetIRQVersion(serial);
  } else 
  if (!strcmp(SHORT_GHV, GetPCommand()))
  {
    return ExecuteGetHardwareVersion(serial);
  } else 
#endif
#if defined(WATCHDOG_ISPLUGGED)
  if (!strcmp(SHORT_PWD, GetPCommand()))
  {
    return ExecutePulseWatchDog(serial);
  } else
#endif
#if defined(COMMAND_SYSTEMENABLED)
  if (!strcmp(SHORT_RSS, GetPCommand()))
  {
    return ExecuteResetSystem(serial);
  } else
  if (!strcmp(SHORT_APE, GetPCommand()))
  {
    return ExecuteAbortProcessExecution(serial);
  } else
#endif
  if (!strcmp(SHORT_WTR, GetPCommand()))
  {
    return ExecuteWaitTimeRelative(serial);
  } else   
#if defined(NTPCLIENT_ISPLUGGED)
  if (!strcmp(SHORT_WTA, GetPCommand()))
  {
    return ExecuteWaitTimeAbsolute(serial);
  } else   
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
  if (!strcmp(SHORT_WTI, GetPCommand()))
  {
    return ExecuteWaitTriggerInput(serial);
  } else
#endif  
  //
#if defined(NTPCLIENT_ISPLUGGED)
  // ----------------------------------
  // NTPClient
  // ----------------------------------
  if (!strcmp(SHORT_GNT, GetPCommand()))
  {
    return ExecuteGetNTPClientTime(serial);
  } else
  if (!strcmp(SHORT_GND, GetPCommand()))
  {
    return ExecuteGetNTPClientDate(serial);
  } else
#endif 
//
#if defined(SDCARD_ISPLUGGED)
  // ----------------------------------
  // SDCard
  // ----------------------------------
  if (!strcmp(SHORT_OCF, GetPCommand()))
  {
    return ExecuteOpenCommandFile(serial);
  } else 
  if (!strcmp(SHORT_WCF, GetPCommand()))
  {
    return ExecuteWriteCommandFile(serial);
  } else 
  if (!strcmp(SHORT_CCF, GetPCommand()))
  {
    return ExecuteCloseCommandFile(serial);
  } else 
  if (!strcmp(SHORT_ECF, GetPCommand()))
  {
    return ExecuteExecuteCommandFile(serial);
  } else 
  if (!strcmp(SHORT_ACF, GetPCommand()))
  {
    return ExecuteAbortCommandFile(serial);
  } else
#endif
  
  // ----------------------------------
  // I2CDisplay
  // ----------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  if (!strcmp(SHORT_CLI, GetPCommand()))
  {
    return ExecuteClearScreenI2CDisplay(serial);
  } else if (!strcmp(SHORT_STI, GetPCommand()))
  {
    return ExecuteShowTextI2CDisplay(serial);
  } else
#endif
  // ----------------------------------
  // RFIDClient
  // ----------------------------------
#if defined(RFIDCLIENT_ISPLUGGED)
  if (!strcmp(SHORT_GID, GetPCommand()))
  {
    return ExecuteGetRFID(serial);
  } else
#endif
  //
#if defined(COMMAND_SYSTEMENABLED)
  // ----------------------------------
  // LedSystem
  // ----------------------------------
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    return ExecuteGetLedSystem(serial);
  } else if (!strcmp(SHORT_LSH, GetPCommand()))
  {
    return ExecuteLedSystemOn(serial);
  } else if (!strcmp(SHORT_LSL, GetPCommand()))
  {
    return ExecuteLedSystemOff(serial);
  } else if (!strcmp(SHORT_BLS, GetPCommand()))
  {
    return ExecuteBlinkLedSystem(serial);
  } else
#endif  
#if defined(MULTICHANNELLED_ISPLUGGED)
  if (!strcmp(SHORT_BL1, GetPCommand()))
  {
    return ExecuteBlinkLedChannel1(serial);
  } else
  if (!strcmp(SHORT_BL2, GetPCommand()))
  {
    return ExecuteBlinkLedChannel2(serial);
  } else
  if (!strcmp(SHORT_BL3, GetPCommand()))
  {
    return ExecuteBlinkLedChannel3(serial);
  } else
  if (!strcmp(SHORT_BL4, GetPCommand()))
  {
    return ExecuteBlinkLedChannel4(serial);
  } else
  if (!strcmp(SHORT_BL5, GetPCommand()))
  {
    return ExecuteBlinkLedChannel5(serial);
  } else
  if (!strcmp(SHORT_BL6, GetPCommand()))
  {
    return ExecuteBlinkLedChannel6(serial);
  } else
  if (!strcmp(SHORT_BL7, GetPCommand()))
  {
    return ExecuteBlinkLedChannel7(serial);
  } else
  if (!strcmp(SHORT_BL8, GetPCommand()))
  {
    return ExecuteBlinkLedChannel8(serial);
  } else
#endif
  // ----------------------------------
  // MotorL298N
  // ----------------------------------
#if defined(MOTORL298N_ISPLUGGED)
  if (!strcmp(SHORT_GWL, GetPCommand()))
  {
    return ExecuteGetPwmLow(serial);
  } else
  if (!strcmp(SHORT_SWL, GetPCommand()))
  {
    return ExecuteSetPwmLow(serial);
  } else
  if (!strcmp(SHORT_GWH, GetPCommand()))
  {
    return ExecuteGetPwmHigh(serial);
  } else
  if (!strcmp(SHORT_SWH, GetPCommand()))
  {
    return ExecuteSetPwmHigh(serial);
  } else
  //
  if (!strcmp(SHORT_MMP, GetPCommand()))
  {
    return ExecuteMoveMotorPositive(serial);
  } else
  if (!strcmp(SHORT_MMN, GetPCommand()))
  {
    return ExecuteMoveMotorNegative(serial);
  } else
  //
  if (!strcmp(SHORT_SM, GetPCommand()))
  {
    return ExecuteStopMotor(serial);
  } else
  if (!strcmp(SHORT_A, GetPCommand()))
  {
    return ExecuteAbortAllMotors(serial);
  } else
#endif
  // ----------------------------------
  // EncoderIRQ
  // ----------------------------------
#if defined(ENCODERIRQ_ISPLUGGED)
  if (!strcmp(SHORT_GEP, GetPCommand()))
  {
    return ExecuteGetEncoderPosition(serial);
  } else
  if (!strcmp(SHORT_SEP, GetPCommand()))
  {
    return ExecuteSetEncoderPosition(serial);
  } else
#endif
  // ----------------------------------
  // MotorEncoder
  // ----------------------------------
#if defined(MOTORENCODER_ISPLUGGED)
  if (!strcmp(SHORT_MFP, GetPCommand()))
  {
    return ExecuteMoveFreePositive(serial);
  } else
  if (!strcmp(SHORT_MFN, GetPCommand()))
  {
    return ExecuteMoveFreeNegative(serial);
  } else
  if (!strcmp(SHORT_MPA, GetPCommand()))
  {
    return ExecuteMovePositionAbsolute(serial);
  } else
  if (!strcmp(SHORT_MPR, GetPCommand()))
  {
    return ExecuteMovePositionRelative(serial);
  } else
#endif
  // ----------------------------------
  // Trigger
  // ----------------------------------
#if defined(TRIGGER_ISPLUGGED)
  // TriggerPeriod
  if (!strcmp(SHORT_GTP, GetPCommand()))
  {
    return ExecuteGetTriggerPeriodCount(serial);
  } else
  if (!strcmp(SHORT_STP, GetPCommand()))
  {
    return ExecuteSetTriggerPeriodCount(serial);
  } else
  // TriggerMoveCommon  
  if (!strcmp(SHORT_GTR, GetPCommand()))
  {
    return ExecuteGetTriggerReduction(serial);
  } else
  if (!strcmp(SHORT_STR, GetPCommand()))
  {
    return ExecuteSetTriggerReduction(serial);
  } else
  // TriggerFreeMove
  if (!strcmp(SHORT_GTC, GetPCommand()))
  {
    return ExecuteGetTriggerCountOffsetPreset(serial);
  } else
  if (!strcmp(SHORT_STC, GetPCommand()))
  {
    return ExecuteSetTriggerCountOffsetPreset(serial);
  } else
  if (!strcmp(SHORT_TMP, GetPCommand()))
  {
    return ExecuteTriggerMovePositive(serial);
  } else
  if (!strcmp(SHORT_TMN, GetPCommand()))
  {
    return ExecuteTriggerMoveNegative(serial);
  } else
  // TriggerPositionControlled
  if (!strcmp(SHORT_GPR, GetPCommand()))
  {
    return ExecuteGetTriggerPositionRange(serial);
  } else
  if (!strcmp(SHORT_SPR, GetPCommand()))
  {
    return ExecuteSetTriggerPositionRange(serial);
  } else
  if (!strcmp(SHORT_TPA, GetPCommand()))
  {
    return ExecuteTriggerPositionAbsolute(serial);
  } else
  if (!strcmp(SHORT_TPR, GetPCommand()))
  {
    return ExecuteTriggerPositionRelative(serial);
  } else
#endif
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    Error.SetCode(ecInvalidCommand);
  }
  return false;
}
