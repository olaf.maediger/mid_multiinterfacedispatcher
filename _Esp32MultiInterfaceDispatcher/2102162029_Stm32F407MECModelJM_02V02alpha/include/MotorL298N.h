//
#include "Defines.h"
//
#if defined(MOTORL298N_ISPLUGGED)
//
#include "MotorBase.h"
//
class CMotorL298N : public CMotorBase
{
private:
  //
public:
  CMotorL298N(String name, UInt8 pinpwm, 
              UInt8 pinenablea, UInt8 pinenableb,
              Float32 pwmlow, Float32 pwmhigh);
  //
  Boolean Open(void) override;
  Boolean Close(void) override;
};
//
#endif // MOTORL298N_ISPLUGGED
//