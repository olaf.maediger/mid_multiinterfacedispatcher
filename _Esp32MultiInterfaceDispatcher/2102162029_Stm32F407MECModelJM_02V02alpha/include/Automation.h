//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Defines.h"
//
#ifndef Automation_h
#define Automation_h
//
#include "Pinout.h"
#include "Serial.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  saReset = 1
};
//
class CAutomation
{
  private:
  // Field
  EStateAutomation FState;
  long unsigned FTimePreset;
  //
  void WriteEvent(CSerial &serial, String text);
  void WriteEvent(CSerial &serial, String mask, int value);
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  //
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // State
  void HandleUndefined(CSerial &serial);  
  void HandleIdle(CSerial &serial);  
  void HandleReset(CSerial &serial); 
  // 
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif
