//
#include "Defines.h"
//
#ifndef Command_h
#define Command_h
//
#include <string.h>
//
#include "Automation.h"
#include "Utilities.h"
#include "Serial.h"
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"// 
#endif
//
#define ARGUMENT_PROJECT    "MEC - MotorEncoderController STM32F407VGT6"
#define ARGUMENT_SOFTWARE   "02V02alpha"
#define ARGUMENT_DATE       "210216"
#define ARGUMENT_TIME       "0730"
#define ARGUMENT_AUTHOR     "OMDevelop"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_STM32F407VG
#define ARGUMENT_HARDWARE "Stm32F407VG"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
#ifdef PROCESSOR_ESP8266
#define ARGUMENT_HARDWARE "Esp8266"
#endif
#ifdef PROCESSOR_ESP32
#define ARGUMENT_HARDWARE "Esp32"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#if defined(WATCHDOG_ISPLUGGED)
#define SHORT_PWD   "PWD"
#endif
#define SHORT_RSS   "RSS"
#define SHORT_APE   "APE"
#endif
#define SHORT_WTR   "WTR"     // WTR T        - WaitTimeRelative
#if defined(NTPCLIENT_ISPLUGGED)
#define SHORT_WTA   "WTA"     // WTA HH MM SS - WaitTimeAbsolute
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#define SHORT_WTI   "WTI"     // WTI RE       - WaitInputTriggered
#endif
//
// HELP_SERIAL_COMMAND
#if defined(SERIALCOMMAND_ISPLUGGED)
#define SHORT_WSC   "WSC"
#define SHORT_RSC   "RSC"
#endif
//
// HELP_IC2DISPLAY
#if defined(I2CDISPLAY_ISPLUGGED)
#define SHORT_CLI   "CLI"
#define SHORT_STI   "STI"
#endif
// 
// HELP_NTPCLIENT
#if defined(NTPCLIENT_ISPLUGGED)
#define SHORT_GNT   "GNT"
#define SHORT_GND   "GND"
#endif
//
// HELP_SDCOMMAND
#if defined(SDCARD_ISPLUGGED)
#define SHORT_OCF   "OCF"
#define SHORT_WCF   "WCF"
#define SHORT_CCF   "CCF"
#define SHORT_ECF   "ECF"
#define SHORT_ACF   "ACF"
#endif
//
// HELP_RFIDCLIENT
#if defined(RFIDCLIENT_ISPLUGGED)
// #define SHORT_SRL   "SRL"   // SRL I RGB C
#endif
//
// HELP_LEDSYSTEM
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
#endif
#if defined(MULTICHANNELLED_ISPLUGGED)
#define SHORT_BL1   "BL1"
#define SHORT_BL2   "BL2"
#define SHORT_BL3   "BL3"
#define SHORT_BL4   "BL4"
#define SHORT_BL5   "BL5"
#define SHORT_BL6   "BL6"
#define SHORT_BL7   "BL7"
#define SHORT_BL8   "BL8"
#endif
//
// HELP_MOTORL298N
#if defined(MOTORL298N_ISPLUGGED)
#define SHORT_GWL   "GWL"     // GPL M       - GetPwmLow
#define SHORT_SWL   "SWL"     // SPL M PL    - SetPwmLow
#define SHORT_GWH   "GWH"     // GPH M       - GetPwmHigh
#define SHORT_SWH   "SWH"     // SPH M PH    - SetPwmHigh
#define SHORT_MMP   "MMP"     // MMP M VT    - MoveMotorPositive
#define SHORT_MMN   "MMN"     // MRN M VT    - MoveMotorNegative
#define SHORT_SM    "SM"      // SM M        - StopMotor
#define SHORT_A     "A"       // A           - AbortAllMotors
#endif
//
// HELP_ENCODERIRQ
#if defined(ENCODERIRQ_ISPLUGGED)
#define SHORT_GEP   "GEP"    // GEP M        - GetEncoderPosition
#define SHORT_SEP   "SEP"    // SEP M EP     - SetEncoderPosition
#endif
//
// HELP_MOTORENCODER
#if defined(MOTORENCODER_ISPLUGGED)
#define SHORT_MFP   "MFP"     // MFP M VT - MoveFreePositive
#define SHORT_MFN   "MFN"     // MFN M VT - MoveFreeNegative
#define SHORT_MPA   "MPA"     // MPA M PA VT - MovePositionAbsolute
#define SHORT_MPR   "MPR"     // MPR M PR VT - MovePositionRelative
#endif
//
// HELP_TRIGGER
#if defined(TRIGGER_ISPLUGGED)
// TriggerPeriod
#define SHORT_GTP   "GTP"     //  GTP - Get Trigger Period Period Count
#define SHORT_STP   "STP"     //  STP P C - Set Trigger Period Count
// TriggerMoveCommon
#define SHORT_GTR   "GTR"     //  GTR A - Get Trigger Reduction 
#define SHORT_STR   "STR"     //  STR A R - Set Trigger Reduction 
// TriggerFreeMove
#define SHORT_GTC   "GTC"     //  GTC A - Get Trigger CountOffset CountPreset
#define SHORT_STC   "STC"     //  STC A O P - Set Trigger CountOffset CountPreset
#define SHORT_TMP   "TMP"     //  TMP A W - Trigger Move Positive Axis PWM
#define SHORT_TMN   "TMN"     //  TMN A W - Trigger Move Negative Axis PWM
// TriggerPositionControlled
#define SHORT_GPR   "GPR"     //  GPR A - Get Trigger Position Range : PS, PE
#define SHORT_SPR   "SPR"     //  SPR A PS PE - Get Trigger Position Range PS .. PE
#define SHORT_TPA   "TPA"     //  TPA A PT PWM - Trigger Position Absolute PT(PS, PE), PWM
#define SHORT_TPR   "TPR"     //  TPR A PD PWM - Trigger Position Relative PD(PS, PE), PWM
#endif
//
#define MASK_ENDLINE " ###"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
#define TITLE_LINE            "--------------------------------------------------"
#define MASK_PROJECT          "- Project:   %-35s -"
#define MASK_SOFTWARE         "- Version:   %-35s -"
#define MASK_HARDWARE         "- Hardware:  %-35s -"
#define MASK_DATE             "- Date:      %-35s -"
#define MASK_TIME             "- Time:      %-35s -"
#define MASK_AUTHOR           "- Author:    %-35s -"
#define MASK_PORT             "- Port:      %-35s -"
#define MASK_PARAMETER        "- Parameter: %-35s -"
//
//-----------------------------------------------------------------------------------------
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s                : This Help"
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_GPH                  " %-3s                : Get Program Header"
#define MASK_GSV                  " %-3s                : Get IRQ-Version"
#define MASK_GHV                  " %-3s                : Get Hardware-Version"
#endif
#define MASK_RSS                  " %-3s                : Reset System"
#if defined(WATCHDOG_ISPLUGGED)
#define MASK_PWD                  " %-3s                : Pulse WatchDog"
#endif
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_APE                  " %-3s                : <a>bort <p>rocess <e>xecution"
#endif
#define MASK_WTR                  " %-3s <t>            : Wait Time Relative T{ms}"
#if defined(NTPCLIENT_ISPLUGGED)
#define MASK_WTA                  " %-3s <hh> <mm> <ss> : Wait Time Absolute {HH:MM:SS}"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#define MASK_WTI                  " %-3s <re>           : Wait Trigger Input <risingedge>{1}"
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s                : Get State LedSystem"
#define MASK_LSH                  " %-3s                : Switch LedSystem On"
#define MASK_LSL                  " %-3s                : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <n> <p> <w>    : Blink LedSystem <n>times <p>eriod{ms}/<w>ith{ms}"
#endif
#if defined(MULTICHANNELLED_ISPLUGGED)
#define MASK_BL1                  " %-3s <n> <p> <w>     : Blink LedChannel1 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL2                  " %-3s <n> <p> <w>     : Blink LedChannel2 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL3                  " %-3s <n> <p> <w>     : Blink LedChannel3 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL4                  " %-3s <n> <p> <w>     : Blink LedChannel4 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL5                  " %-3s <n> <p> <w>     : Blink LedChannel5 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL6                  " %-3s <n> <p> <w>     : Blink LedChannel6 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL7                  " %-3s <n> <p> <w>     : Blink LedChannel7 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL8                  " %-3s <n> <p> <w>     : Blink LedChannel8 <n>times <p>eriod{ms}/<w>ith{ms}"
#endif
//
#if defined(SERIALCOMMAND_ISPLUGGED)
#define HELP_SERIAL_COMMAND       " Help (SerialCommand):"
#define MASK_WSC                  " %-3s <t>            : WriteLine SerialCommand <t>ext"
#define MASK_RSC                  " %-3s                : ReadLine SerialCommand"
#endif
//
#if defined(I2CDISPLAY_ISPLUGGED)
#define HELP_I2CDISPLAY           " Help (I2CDisplay):"
#define MASK_CLI                  " %-3s                 : Clear Display"
#define MASK_STI                  " %-3s <r> <c> <t>     : Show <t>ext at <r>ow <c>ol"
#endif
//
#if defined(NTPCLIENT_ISPLUGGED)
#define HELP_NTPCLIENT            " Help (NTPClient):"
#define MASK_GNT                  " %-3s                 : Get NTPClient Time {hh:mm:ss}"
#define MASK_GND                  " %-3s                 : Get NTPClient Date {yy.mm.dd}"
#endif
//
#if defined(SDCARD_ISPLUGGED)
#define HELP_SDCOMMAND            " Help (SDCommand):"
#define MASK_OCF                  " %-3s <f>            : Open Command<f>ile for writing"
#define MASK_WCF                  " %-3s <c> <p> ..     : Write <c>ommand with <p>arameter(s) to File"
#define MASK_CCF                  " %-3s                : Close Command<f>ile for writing"
#define MASK_ECF                  " %-3s <f>            : Execute Command<f>ile"
#define MASK_ACF                  " %-3s                : Abort Execution Commandfile"
#endif
//
#if defined(MOTORL298N_ISPLUGGED)
#define HELP_MOTORL298N           " Help (MotorL298N):"
#define MASK_GWL                  " %-3s <m>            : Get <m>otor PwmLow[%%]"
#define MASK_SWL                  " %-3s <m> <p>        : Set <m>otor <p>wmLow[%%]"
#define MASK_GWH                  " %-3s <m>            : Get <m>otor PwmHigh[%%]"
#define MASK_SWH                  " %-3s <m> <p>        : Set <m>otor <p>wmHigh[%%]"
//
#define MASK_MMP                  " %-3s <m> <v>        : Move <m>otor Positive <v>elocity[%%]"
#define MASK_MMN                  " %-3s <m> <v>        : Move <m>otor Negative <v>elocity[%%]"
#define MASK_SM                   " %-3s <m>            : Stop <m>otor"
#define MASK_A                    " %-3s                : Abort all Motors"
#endif
//
#if defined(ENCODERIRQ_ISPLUGGED)
#define HELP_ENCODERIRQ           " Help (EncoderIRQ):"
#define MASK_GEP                  " %-3s <m>            : Get <m>otor Encoder Position[stp]"
#define MASK_SEP                  " %-3s <m> <p>        : Set <m>otor Encoder <p>osition[stp]"
#endif
 //
#if defined(MOTORENCODER_ISPLUGGED)
#define HELP_MOTORENCODER         " Help (MotorEncoder):"
#define MASK_MFP                  " %-3s <m> <vt>       : Move Free Positive <m>otor VT[%%]"
#define MASK_MFN                  " %-3s <m> <vt>       : Move Free Negative <m>otor VT[%%]"
#define MASK_MPA                  " %-3s <m> <pa> <vt>  : Move <p>osition <a>bsolute[stp] VT[%%]"
#define MASK_MPR                  " %-3s <m> <pr> <vt>  : Move <p>osition <r>elative{stp} VT[%%]"
#endif
//
// HELP_TRIGGER
#if defined(TRIGGER_ISPLUGGED)
#define HELP_TRIGGER              " Help (Trigger):"
#define HELP_TRIGGERPERIOD        " Help (-Period):"
#define MASK_GTP                  " %-3s                : Get Trigger-Period[%%]-count[1]"
#define MASK_STP                  " %-3s <p> <c>        : Set Trigger-Period[%%]-count[1]"
#define HELP_TMOVECOMMON          " Help (-MoveCommon):"
#define MASK_GTR                  " %-3s <m>            : Get Trigger Reduction <m>[xyuv]"
#define MASK_STR                  " %-3s <m> <r>        : Set Trigger Reduction <m>[xyuv] <r>[0..R]"
#define HELP_TFREEMOVE            " Help (-FreeMove):"
#define MASK_GTC                  " %-3s <m>            : Get Trigger <m>otor[xyuv] Count-Offset -Preset"
#define MASK_STC                  " %-3s <m> <o> <p>    : Set Trigger <m>otor[xyuv] Count<o>ffset[1] -<p>reset[1]"
#define MASK_TMP                  " %-3s <m> <w>        : Trigger Move Positive <m>otor[xyuv] P<w>M[%%]"
#define MASK_TMN                  " %-3s <m> <w>        : Trigger Move Negative <m>otor[xyuv] P<w>M[%%]"
#define HELP_TPOSITIONCONTROLLED  " Help (-PositionControlled):"
#define MASK_GPR                  " %-3s <m>            : Get Trigger <m>otor Position Range"
#define MASK_SPR                  " %-3s <m> <s> <e>    : Set Trigger <m>otor Position-Range <s>tart[1], <e>nd[1]"
#define MASK_TPA                  " %-3s <m> <t> <w>    : Trigger <m>otor Position<t>arget[1] P<w>M[%%]"
#define MASK_TPR                  " %-3s <m> <d> <w>    : Trigger <m>otor Position<d>elta[1] P<w>M[%%]"
// 
#endif
//
#define MASK_STATEPROCESS         "STP %i %i"

//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      "IRQ-Version %s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      "Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define SIZE_SERIALBUFFER 32
#define SIZE_RESPONSEBUFFER 32
#define SIZE_COMMANDBUFFER 64
#define COUNT_TEXTPARAMETERS 5
#define SIZE_TEXTPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
enum EStateCommand
{
  scError     = -2,
  scUndefined = -1,
  scInit      = 0,
  scIdle      = 1,
  scBusy      = 2
};
//
class CCommand
{
  private:
  EStateCommand FState;
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Int16 FRxdBufferIndex = 0;
  Character FCommandText[SIZE_RXDBUFFER];
  PCharacter FPCommand;
  PCharacter FPParameters[COUNT_TEXTPARAMETERS];
  Int16 FParameterCount = 0;
  //    
  public:
  CCommand(void);
  ~CCommand(void);
  //
  const char* StateText(EStateCommand state);
  //
  EStateCommand GetState(void);
  void SetState(EStateCommand state);
  //
  bool Open(void);
  bool Close(void);
  //
  inline PCharacter GetRxdBuffer(void)
  {
    return FRxdBuffer;
  }
  inline PCharacter GetTxdBuffer(void)
  {
    return FTxdBuffer;
  }
  //
  inline PCharacter GetPCommand(void)
  {
    return FPCommand;
  }
  inline Byte GetParameterCount(void)
  {
    return FParameterCount;
  }
  inline PCharacter GetPParameters(UInt8 index)
  {
    return FPParameters[index];
  } 
  //
  void ZeroRxdBuffer(void);
  void ZeroTxdBuffer(void);
  void ZeroCommandText(void);
  //
  void AbortProcessExecution(void);
  //
  //  Segment - Execution - Common
  void WriteProgramHeader(CSerial &serial);
  void WriteIRQVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  bool ExecuteGetHelp(CSerial &serial);
  bool ExecuteGetProgramHeader(CSerial &serial);
  bool ExecuteGetIRQVersion(CSerial &serial);
  bool ExecuteGetHardwareVersion(CSerial &serial);
  //
#if defined(WATCHDOG_ISPLUGGED)
  bool ExecutePulseWatchDog(CSerial &serial); 
#endif
  bool ExecuteResetSystem(CSerial &serial); 
  bool ExecuteAbortProcessExecution(CSerial &serial);
  bool ExecuteWaitTimeRelative(CSerial &serial);
#if defined(NTPCLIENT_ISPLUGGED)
  bool ExecuteWaitTimeAbsolute(CSerial &serial);
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
  bool ExecuteWaitTriggerInput(CSerial &serial);
#endif  
  //
  //  Segment - Execution - LedSystem
  bool ExecuteGetLedSystem(CSerial &serial);
  bool ExecuteLedSystemOn(CSerial &serial);
  bool ExecuteLedSystemOff(CSerial &serial);
  bool ExecuteBlinkLedSystem(CSerial &serial);
#if defined(MULTICHANNELLED_ISPLUGGED)
  bool ExecuteBlinkLedChannel1(CSerial &serial);
  bool ExecuteBlinkLedChannel2(CSerial &serial);
  bool ExecuteBlinkLedChannel3(CSerial &serial);
  bool ExecuteBlinkLedChannel4(CSerial &serial);
  bool ExecuteBlinkLedChannel5(CSerial &serial);
  bool ExecuteBlinkLedChannel6(CSerial &serial);
  bool ExecuteBlinkLedChannel7(CSerial &serial);
  bool ExecuteBlinkLedChannel8(CSerial &serial);
#endif  
  //
  //  Segment - Execution - Serial
  bool ExecuteWriteLineSerialCommand(CSerial &serial);
  bool ExecuteReadLineSerialCommand(CSerial &serial);
  //
  //  Segment - Execution - I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
  bool ExecuteClearScreenI2CDisplay(CSerial &serial); 
  bool ExecuteShowTextI2CDisplay(CSerial &serial);
#endif
  //
  //  Segment - Execution - NTPClient
#if defined(NTPCLIENT_ISPLUGGED)
  bool ExecuteGetNTPClientTime(CSerial &serial);
  bool ExecuteGetNTPClientDate(CSerial &serial);
#endif 
  //
  //  Segment - Execution - SDCard
#if defined(SDCARD_ISPLUGGED)
  bool ExecuteOpenCommandFile(CSerial &serial);
  bool ExecuteWriteCommandFile(CSerial &serial);
  bool ExecuteCloseCommandFile(CSerial &serial);
  bool ExecuteExecuteCommandFile(CSerial &serial);
  bool ExecuteAbortCommandFile(CSerial &serial);
#endif
  //  Segment - Execution - MotorL298N
#if defined(MOTORL298N_ISPLUGGED)
  void StopAllAxes(void); // Helper!
  bool ExecuteGetPwmLow(CSerial &serial);
  bool ExecuteSetPwmLow(CSerial &serial);
  bool ExecuteGetPwmHigh(CSerial &serial);
  bool ExecuteSetPwmHigh(CSerial &serial);
  bool ExecuteMoveMotorPositive(CSerial &serial);
  bool ExecuteMoveMotorNegative(CSerial &serial);
  bool ExecuteStopMotor(CSerial &serial);
  bool ExecuteAbortAllMotors(CSerial &serial);
#endif
  //  Segment - Execution - EncoderIRQ
#if defined(ENCODERIRQ_ISPLUGGED)
  bool ExecuteGetEncoderPosition(CSerial &serial);
  bool ExecuteSetEncoderPosition(CSerial &serial);
#endif
  //  Segment - Execution - MotorEncoder
#if defined(MOTORENCODER_ISPLUGGED)
  bool ExecuteMoveFreePositive(CSerial &serial);
  bool ExecuteMoveFreeNegative(CSerial &serial);
  bool ExecuteMovePositionAbsolute(CSerial &serial);
  bool ExecuteMovePositionRelative(CSerial &serial);
#endif
  //  Segment - Execution - Trigger
#if defined(TRIGGER_ISPLUGGED)
  // TriggerPeriod
  bool ExecuteGetTriggerPeriodCount(CSerial &serial);
  bool ExecuteSetTriggerPeriodCount(CSerial &serial);
  // TriggerMoveCommon  
  bool ExecuteGetTriggerReduction(CSerial &serial);
  bool ExecuteSetTriggerReduction(CSerial &serial);
  // TriggerFreeMove
  bool ExecuteGetTriggerCountOffsetPreset(CSerial &serial);
  bool ExecuteSetTriggerCountOffsetPreset(CSerial &serial);
  bool ExecuteTriggerMovePositive(CSerial &serial);
  bool ExecuteTriggerMoveNegative(CSerial &serial);
  // TriggerPositionControlled
  bool ExecuteGetTriggerPositionRange(CSerial &serial);
  bool ExecuteSetTriggerPositionRange(CSerial &serial);
  bool ExecuteTriggerPositionAbsolute(CSerial &serial);
  bool ExecuteTriggerPositionRelative(CSerial &serial);
#endif
  //
  // Main
  //
  bool AnalyseCommandText(CSerial &serial);
  //void WritePrompt(void);
  void WriteEvent(String line);
  void WriteResponse(String line);
  void WriteComment(String line);
  void WriteComment(String mask, String line);
  //  
  void DistributeCommand(void);
  //
  void Init();
#if defined(SERIALCOMMAND_ISPLUGGED) 
  bool DetectRxdLine(CSerial &serial);
#endif  
#if defined(SDCARD_ISPLUGGED) 
  bool DetectSDCardLine(CSDCard &sdcard);
#endif  
#if defined(MQTTCLIENT_ISPLUGGED) 
  bool DetectMQTTClientLine(CMQTTClient &mqttclient);
#endif  
  //
#if defined(SERIALCOMMAND_ISPLUGGED) 
  bool Handle(CSerial &serial);
  bool Execute(CSerial &serial);
#endif 
#if defined(SDCARD_ISPLUGGED)
  bool Handle(CSerial &serial, CSDCard &sdcard);
#endif  
#if defined(MQTTCLIENT_ISPLUGGED)
  bool Handle(CSerial &serial, CMQTTClient &mqttclient);
#endif 
};
//
#endif // Command_h
