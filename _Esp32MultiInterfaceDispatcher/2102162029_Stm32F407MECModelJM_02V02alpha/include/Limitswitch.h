//
#include "Defines.h"
//
#if defined(LIMITSWITCH_ISPLUGGED)
//
//--------------------------------
//  Library Limitswitch
//--------------------------------
//
#ifndef Limitswitch_h
#define Limitswitch_h
//
#include "Arduino.h"
#include "Pinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
class CLimitswitch
{
  private:
  int FPin;
  Boolean FInverted;
  //  
  public:
  CLimitswitch(int pin);
  CLimitswitch(int pin, bool inverted);
  //
  inline Boolean IsInverted()
  {
    return FInverted;
  }
  inline Boolean IsActive(void)
  {
    Boolean Level = (HIGH == digitalRead(FPin));
    if (FInverted)
    {
      return !Level;
    }
    return Level;
  }  
  //
  Boolean Open();
  Boolean Close();
};
//
#endif // Limitswitch_h
//
#endif // LIMITSWITCH_ISPLUGGED
