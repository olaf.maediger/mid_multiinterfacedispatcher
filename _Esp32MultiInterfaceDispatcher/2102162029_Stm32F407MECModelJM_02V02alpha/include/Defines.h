 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include <Arduino.h>
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//
#define INIT_MESSAGE_ON  true
#define INIT_MESSAGE_OFF false
//
// !!! #undef COMMAND_SYSTEMENABLED
// !!! 
#define COMMAND_SYSTEMENABLED
//
// !!! #undef SERIALCOMMAND_ISPLUGGED
// 
#define SERIALCOMMAND_ISPLUGGED
//
#undef WATCHDOG_ISPLUGGED
// !!! #define WATCHDOG_ISPLUGGED
//
#undef PININPUT_ISPLUGGED
// !!! #define PININPUT_ISPLUGGED
//
// !!! 
#undef PINOUTPUT_ISPLUGGED
// #define PINOUTPUT_ISPLUGGED
//
#undef DACINTERNAL_ISPLUGGED
// !!! #define DACINTERNAL_ISPLUGGED
//
// !!! 
#undef DACMCP4725_ISPLUGGED
// #define DACMCP4725_ISPLUGGED
//
#undef TRIGGERINPUT_ISPLUGGED
// !!! #define TRIGGERINPUT_ISPLUGGED
//
#undef NTPCLIENT_ISPLUGGED
// !!! #define NTPCLIENT_ISPLUGGED
#if defined(NTPCLIENT_ISPLUGGED)
// #define NTPCLIENT_TIMEOFFSET 3600 // WinterTime [s]
#define NTPCLIENT_TIMEOFFSET 7200 // SummerTime [s]
#endif
//
#undef SDCARD_ISPLUGGED
// !!!# define SDCARD_ISPLUGGED
// 
#undef I2CDISPLAY_ISPLUGGED
// !!! #define I2CDISPLAY_ISPLUGGED
//
#undef MQTTCLIENT_ISPLUGGED
// !!! #define MQTTCLIENT_ISPLUGGED
//
#undef RTCINTERNAL_ISPLUGGED
// !!! #define RTCINTERNAL_ISPLUGGED
//
#undef MULTICHANNELLED_ISPLUGGED
// !!! #define MULTICHANNELLED_ISPLUGGED
//
//!!! #undef MOTORL298N_ISPLUGGED
//!!! 
#define MOTORL298N_ISPLUGGED
//
//!!! #undef ENCODERIRQ_ISPLUGGED
// !!! 
#define ENCODERIRQ_ISPLUGGED        
//
//!!! #undef TRIGGER_ISPLUGGED
// !!! 
#define TRIGGER_ISPLUGGED        
//
//!!! #undef MOTORENCODER_ISPLUGGED
//!!! 
#define MOTORENCODER_ISPLUGGED
//
//!!! #undef LIMITSWITCH_ISPLUGGED
//!!! 
#define LIMITSWITCH_ISPLUGGED
//
//
//----------------------------------
// ARDUINO NANOR3 :
// #define PROCESSOR_NANOR3
//----------------------------------
//----------------------------------
// ARDUINO UNOR3 :
// #define PROCESSOR_UNOR3
//----------------------------------
// Arduino Mega2560
// #define PROCESSOR_MEGA2560
//----------------------------------
//----------------------------------
// Arduino DueM3
// #define PROCESSOR_DUEM3
//----------------------------------
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//----------------------------------
//----------------------------------
// STM32F407VG 
// 
#define PROCESSOR_STM32F407VG
//----------------------------------
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//----------------------------------
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//----------------------------------
//----------------------------------
// Esp8266
// #define PROCESSOR_ESP8266
//----------------------------------
//----------------------------------
// Esp32
// #define PROCESSOR_ESP32
//----------------------------------
//
//
//##########################################
//  CONSTANT - GLOBAL ALL
//##########################################
// 
#define TRUE                    1
#define FALSE                   0
#define INIT_ERRORCODE          ecNone
#define INIT_RXDECHO            false
#define NOT_INVERTED            false
#define INVERTED                true
//
#define TERMINAL_ZERO           0x00
#define TERMINAL_CARRIAGERETURN 0x0D
#define TERMINAL_LINEFEED       0x0A
#define TERMINAL_NEWLINE        "\r\n"
#define TERMINAL_SPACE          " "
#define TERMINAL_INPUT          ">"
// ->
#define TERMINAL_COMMAND        "@"
// <-
#define TERMINAL_TEXT           "%"
#define TERMINAL_EVENT          "!"
#define TERMINAL_RESPONSE       "&"
#define TERMINAL_COMMENT        "#"
#define TERMINAL_DEBUG          "|"
#define TERMINAL_ERROR          ":"
#define TERMINAL_WARNING        "."
//
//
//##########################################
//  PROCESSOR_NANOR3
//##########################################
#if defined(PROCESSOR_NANOR3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_NANOR3
//
//##########################################
//  PROCESSOR_UNOR3
//##########################################
#if defined(PROCESSOR_UNOR3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_UNOR3
//
//##########################################
//  PROCESSOR_MEGA2560
//##########################################
#if defined(PROCESSOR_MEGA2560)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_MEGA2560
//
//##########################################
//  PROCESSOR_DUEM3
//##########################################
#if defined(PROCESSOR_DUEM3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_DUEM3
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Type IRQ Function
typedef void (*TInterruptFunction)(void); 
//
// Forward-Declaration IRQ SlitPX
// example: void ISREncoderSlitPXPulseA(void);
// example: void ISREncoderSlitPXPulseB(void);
//
#define SERIALCOMMAND    Serial1   // CommunicationPC
#define SERIALNETWORK    Serial2   // Serial-LAN
#define SERIALPRINTER    Serial3   // Serial-Printer
//
#endif // PROCESSOR_STM32F103C8
//
//##########################################
//  PROCESSOR_STM32F407VG
//##########################################
#if defined(PROCESSOR_STM32F407VG)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float32 float
#define Double64 double
//
// Type - IRQ-Handler
typedef void (*TInterruptFunction)(void);
//
// Definition - Serial 
#define SERIALCOMMAND    Serial1   // CommunicationPC
#define SERIALNETWORK    Serial2   // Serial-LAN
#define SERIALPRINTER    Serial3   // Serial-Printer
#define SERIALDEBUG4     Serial4   // Serial-Printer
#define SERIALDEBUG5     Serial5   // Serial-Printer
#define SERIALDEBUG6     Serial5   // Serial-Printer
//
#endif // PROCESSOR_STM32F103C8
//
//
//##########################################
//  PROCESSOR_TEENSY32
//##########################################
#if defined(PROCESSOR_TEENSY32)
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Choose 72MHz and not 80MHz(Overclocked) !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 signed int 
#define UInt16 unsigned int
#define Int32 long signed int
#define UInt32 long unsigned int
#define Int64 long long signed int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
#if defined(PROCESSOR_TEENSY36)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_TEENSY36
//
//##########################################
//  PROCESSOR_ESP8266
//##########################################
#if defined(PROCESSOR_ESP8266)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Type IRQ Function
typedef void (*TInterruptFunction)(void); 
// Forward-Declaration IRQ Function
// example: void ISREncoderSlitPXPulseA(void);
//
#define SERIALUSBPROGRAM SerialUSB // Serial-USBProgramming
#define SERIALPRINTER    Serial1   // Serial-Printer
#define SERIALNETWORK    Serial2   // Serial-LAN
#define SERIALCOMMAND    Serial3   // CommunicationPC
//
#endif 
// PROCESSOR_ESP8266
//
//##########################################
//  PROCESSOR_ESP32
//##########################################
#if defined(PROCESSOR_ESP32)
//------------------------------------------------------
// Segment - Global Type
//------------------------------------------------------
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float32 float
#define Double64 double
//-------------------------------------------------
// Type IRQ Function
typedef void (*TInterruptFunction)(void); 
// Forward-Declaration IRQ Function
void ISRMotorEncoderLM393Left(void);
void ISRMotorEncoderLM393Right(void);
//------------------------------------------------------
// Segment - Global Constant
//------------------------------------------------------
// CommunicationPC == Serial
#define SERIAL_PC  0
// Serial-AxisXY
#define SERIAL_XY  1
// Serial-AxisZW
#define SERIAL_ZW  2
//-------------------------------------------------
// SA7
//#define WIFI_SSID                         "FritzBoxSA7"
//#define WIFI_SSID                         "TPLink03"
//#define MQTTBROKER_IPADDRESS              "192.168.178.76" // OMSA7MqttBroker

#define WIFI_SSID                         "TPLink03"
#define WIFI_PASSWORD                     "01234567890123456789"
#define MQTTBROKER_IPADDRESS              "192.168.178.123" // RPiMqttBroker
#define MQTTBROKER_PORT                   1883
//
//// LLG
//#define WIFI_SSID                         "LLG-Guest"
//#define WIFI_PASSWORD                     "llg-2017"
//#define MQTTBROKER_IPADDRESS              "192.168.15.59"
//#define MQTTBROKER_PORT                   1883
//
// !!!!!!!!! DONT FORGET THE PRESET SLASH : "/" !!!!!!!!!!!!!!!!!
#define XML_INITFILE                     "/remoterobotcar.ini"
//
#define INIT_COMMANDFILE                 "/sdc.cmd"
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//-------------------------------------------------
#if defined(NTPCLIENT_ISPLUGGED)
//
#ifndef INCLUDE_NTPCLIENT
#define INCLUDE_NTPCLIENT
//
#define IPADDRESS_NTPSERVER               "de.pool.ntp.org"
//
#endif // INCLUDE_NTPCLIENT
//
#endif // NTPCLIENT_ISPLUGGED
//-------------------------------------------------
#if defined(MQTTCLIENT_ISPLUGGED)
//
// MQTT - Data
#define TOPIC_COMMAND                     "/Command"
#define TOPIC_TEXT                        "/Text"
#define TOPIC_EVENT                       "/Event"
#define TOPIC_RESPONSE                    "/Response"
#define TOPIC_COMMENT                     "/Comment"
#define TOPIC_DEBUG                       "/Debug"
//
#define EVENT_BROKERCONNECTED             "Broker connected"
#define EVENT_BROKERDISCONNECTED          "Broker disconnected"
//
#endif // MQTTCLIENT_ISPLUGGED
//
//-------------------------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED)
// Motor - Left
const Byte   PWMCHANNEL_MOTORLEFT           =     0;  // [0, 1, 15]
const UInt32 PWMFREQUENCY_MOTORLEFT         = 10000;  // [Hz]
const Byte   PWMRESOLUTION_MOTORLEFT        =     8;  // [bit]
const UInt16 PWMHIGH_MOTORLEFT              =   250;  // [1]
const UInt16 PWMLOW_MOTORLEFT               =    60;  // [1]
//
// Motor - Right
const Byte   PWMCHANNEL_MOTORRIGHT          =     1;  // [0, 1, 15]
const UInt32 PWMFREQUENCY_MOTORRIGHT        = 10000;  // [Hz]
const Byte   PWMRESOLUTION_MOTORRIGHT       =     8;  // [bit]
const UInt16 PWMHIGH_MOTORRIGHT             =   250;  // [1]
const UInt16 PWMLOW_MOTORRIGHT              =    60;  // [1]
//
#endif // MOTORVNH2SP30_ISPLUGGED
//
//-------------------------------------------------
#if defined(REMOTEWIRELESSSWITCH_ISPLUGGED)
//const int 
#endif // REMOTEWIRELESSSWITCH_ISPLUGGED
//
//-------------------------------------------------
#if defined(DACMCP4725_ISPLUGGED)
const int I2CADDRESS_POSITIONX = 0x62;
const int I2CADDRESS_POSITIONY = 0x63;
#endif 
//
#endif // PROCESSOR_ESP32
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
#endif // Defines_h
