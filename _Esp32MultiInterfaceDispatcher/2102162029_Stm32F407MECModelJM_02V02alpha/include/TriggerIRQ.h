//
#include "Defines.h"
//
#if defined(TRIGGER_ISPLUGGED)
//
#include "TriggerBase.h"
//
class CTriggerIRQ : public CTriggerBase
{
public:
  CTriggerIRQ(String name,
			        UInt8 pintrigger);
  //
  Boolean Open(void) override;
  Boolean Close(void) override;
};
//
#endif // TRIGGER_ISPLUGGED
//