//
//--------------------------------
//  Library PinOutput
//--------------------------------
//
#include "Defines.h"
//
#ifndef PinOutput_h
#define PinOutput_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//const Boolean INIT_RISINGEDGE = true;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EPinOutputLevel
{
  polLow   = 0,
  polHigh  = 1
};
//
class CPinOutput
{
  private:
  String FID;
  UInt16 FPin;
  EPinOutputLevel FLevel;
  //
  public:
  CPinOutput(String id, UInt8 pin, EPinOutputLevel level);
  //
  EPinOutputLevel GetLevel(void);
  void WriteLevel(EPinOutputLevel level);
  //
  Boolean Open();
  Boolean Close();
  // 
  EPinOutputLevel Execute(void);
};
//
#endif // PinOutput_h
//
