//
#include "Defines.h"
//
#if defined(TRIGGER_ISPLUGGED)
//
#ifndef TriggerBase_h
#define TriggerBase_h
//
#include "Serial.h"
//
enum EStateTrigger 
{
  stDisabled = 0,
  stPeriodCount = 1,
  stFreeMoving = 2,
  stPositionControlled = 3
};
//
const EStateTrigger INIT_TRIGGER_STATE = stDisabled;
const UInt32 INIT_TRIGGER_REDUCTION = 1;
const UInt32 INIT_TRIGGER_PERIOD = 500;
const UInt32 INIT_TRIGGER_COUNT = 3;
const UInt32 INIT_TRIGGER_COUNTOFFSET = 1;
const UInt32 INIT_TRIGGER_COUNTPRESET = 100000;
const UInt32 INIT_TRIGGER_COUNTACTUAL = 0;
const Int32 INIT_TRIGGER_POSITIONSTART = -100000;
const Int32 INIT_TRIGGER_POSITIONEND = +100000;
//
//
class CTriggerBase
{
protected:
  String FName;
  UInt8 FPinTrigger;
  EStateTrigger FState;
  UInt32 FTriggerReduction;
  UInt32 FTriggerTotal;
  UInt32 FTriggerPeriod;
  UInt32 FTriggerCount;
  UInt32 FCountOffset;
  UInt32 FCountPreset;
  UInt32 FCountActual;
  Int32 FPositionStart;
  Int32 FPositionEnd;
  //
  void Init(void);
  void IncrementCountActual(Int32 position);
  //  
public:
  CTriggerBase(String name,
               UInt8 pintrigger);
  //
  virtual Boolean Open(void);
  virtual Boolean Close(void);
  //
  EStateTrigger GetState(void);
  void SetState(EStateTrigger state);
  //
  UInt32 GetTriggerReduction(void);
  void SetTriggerReduction(UInt32 reduction);
  UInt32 GetTriggerTotal(void);
  void SetTriggerTotal(UInt32 trigger);
  //
  UInt32 GetTriggerPeriod(void);
  UInt32 GetTriggerCount(void);
  void SetTriggerPeriod(UInt32 period);
  void SetTriggerCount(UInt32 count);

  // FreeMoving
  UInt32 GetCountOffset(void);
  UInt32 GetCountPreset(void);
  UInt32 GetCountActual(void);
  void SetCountOffset(UInt32 count);
  void SetCountPreset(UInt32 count);
  void SetCountActual(UInt32 count);
  //
  Int32 GetPositionStart(void);
  Int32 GetPositionEnd(void);
  void SetPositionStart(Int32 position);
  void SetPositionEnd(Int32 position);
  //
  void HandleIrqA(Int32 position);
  void HandleIrqB(Int32 position);
};
//
#endif // TriggerBase_h
//
#endif // TRIGGER_ISPLUGGED)
//