//
#include "Defines.h"
//
#if defined(ENCODERIRQ_ISPLUGGED)
//
#include "EncoderBase.h"
//
class CEncoderIRQ : public CEncoderBase
{
public:
  CEncoderIRQ(String name, UInt8 pinena, UInt8 pinenb,
              TInterruptFunction pirqhandlerena, 
              TInterruptFunction pirqhandlerenb);
  //
  Boolean Open(void) override;
  Boolean Close(void) override;
};
//
#endif // ENCODERIRQ_ISPLUGGED
//