//
#include "Defines.h"
//
#if defined(MOTORL298N_ISPLUGGED)
//
#ifndef MotorBase_h
#define MotorBase_h
//
#include "Arduino.h"
//
#include "Utilities.h"
#include "Serial.h"
//
enum EStateMotor
{
  smIdle = 0,
  smBusy = 1
};
//
enum EMotorDirection
{
  mdNegative = -1,
  mdZero = 0,
  mdPositive = 1
};
//
const Float32 PWM_ZERO         = 0; // [%]
const Float32 INIT_PWM_MAXIMUM = 100; // [%]
const Float32 INIT_PWM_MINIMUM = PWM_ZERO; // [%]
const Float32 INIT_PWM_HIGH    = INIT_PWM_MAXIMUM; // [%]
const Float32 INIT_PWM_LOW     = INIT_PWM_MINIMUM; // [%]
//
class CMotorBase
{
private: 
  Boolean FRefreshedPwmLow, FRefreshedPwmHigh;
  Boolean FRefreshedPwmTarget, FRefreshedPwmActual;
  Boolean FRefreshedState, FRefreshedDirection;
  //
protected:
  String FName;
  EStateMotor FState;
  UInt8 FPinPWM, FPinENA, FPinENB;
  TIM_TypeDef* FPInstance;
  UInt32 FPwmChannel;
  HardwareTimer* FPTimer;
  Float32 FPwmLow; // [%]
  Float32 FPwmHigh; // [%]
  Float32 FPwmTarget; // [%]
  Float32 FPwmActual; // [%]
  //
  void SetState(EStateMotor value);
  //
public:
  CMotorBase(String name, UInt8 pinpwm, 
             UInt8 pinena, UInt8 pinenb,
             Float32 pwmlow, Float32 pwmhigh);
  //
  EStateMotor GetState(void);
  EMotorDirection GetDirection(void);
  void ForceStateBusy(void);
  //
  Float32 GetPwmLow(void);
  void SetPwmLow(Float32 value);
  Float32 GetPwmHigh(void);
  void SetPwmHigh(Float32 value);
  Float32 GetPwmTarget(void);
  void SetPwmTarget(Float32 value);
  Float32 GetPwmActual(void);
  void SetPwmActual(Float32 value);
  //
  virtual Boolean Open(void);// = 0;
  virtual Boolean Close(void);// = 0;
  //
  virtual void MovePositive(Float32 pwm);
  virtual void MoveNegative(Float32 pwm);
  virtual void StopMotion(void);
  virtual void AbortMotion(void);
  //
  void Execute(void);
  void Message(CSerial serial);
};
//
#endif // MotorBase_h
//
#endif // MOTORL298N_ISPLUGGED
//