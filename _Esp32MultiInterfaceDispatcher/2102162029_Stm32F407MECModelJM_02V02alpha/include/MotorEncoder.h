//
#include "Defines.h"
//
#if defined(MOTORENCODER_ISPLUGGED)
//
#include <math.h>
//
#include "Utilities.h"
#include "MotorBase.h"
#include "EncoderBase.h"
#include "Led.h"
#include "Limitswitch.h"
//
#include "Serial.h"
//
#ifndef MotorEncoder_h
#define MotorEncoder_h
//
enum EMotorActivity
{
  maFreeRun = 0,
  maControlled = 1
};
//
const EMotorActivity INIT_MOTOR_ACTIVITY = maFreeRun;
//
class CMotorEncoder
{
protected:
  String FID;
  EMotorActivity FMotorActivity;
  CMotorBase* FPMotor;
  CEncoderBase* FPEncoder;
  CLimitswitch* FPLimitswitchLow;
  CLimitswitch* FPLimitswitchHigh;
  CLed* FPLedLimitLow;
  CLed* FPLedLimitHigh;
  //
  Int32 FPositionTarget;
  //
  UInt32 FTickPreset;
  Int32 FPositionPreset;
  UInt32 FPositionRamp;
  UInt32 FPositionResolution;
  //
  Float32 CalculatePwmActual(void);
  //
  Float32 FPWMActualPreset;
  EStateMotor FStateMotorPreset;
  //
public:
  CMotorEncoder(String id, 
                CMotorBase* pmotor, CEncoderBase* pencoder,
                CLimitswitch* plimitswitchlow, CLimitswitch* plimitswitchhigh,
                CLed* pledlimitlow, CLed* pledlimithigh,
                UInt32 positionramp, UInt32 positionresolution);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  EMotorActivity GetMotorActivity(void);
  void SetMotorActivity(EMotorActivity value);
  //
  Int32 GetPositionActual(void);
  void SetPositionActual(Int32 position);
  //
  Int32 GetPositionTarget(void);
  void SetPositionTarget(Int32 position);
  //
  // FreeRun / Controlled:
  void StopMotion(void);
  //
  // Move FreeRun:
  void MoveMotorPositive(Float32 pwm);
  void MoveMotorNegative(Float32 pwm);
  //
  // Move Controlled:
  void MovePositionAbsolute(Int32 position, Float32 pwm);
  void MovePositionRelative(Int32 position, Float32 pwm);
  //
  void Execute(void);
  void Message(CSerial serial);
};
//
#endif // MotorEncoder_h
//
#endif // MOTORENCODER_ISPLUGGED
//