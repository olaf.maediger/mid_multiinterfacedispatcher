//
#include "Defines.h"
//
#if defined(ENCODERIRQ_ISPLUGGED)
//
#ifndef EncoderBase_h
#define EncoderBase_h
//
#include "Serial.h"
//
class CEncoderBase
{
private:
  Boolean FMessagePosition;
  //
protected:
  String FName;
  UInt8 FPinENA, FPinENB;
  Int32 FPosition;
  TInterruptFunction FPIrqHandlerENA;
  TInterruptFunction FPIrqHandlerENB;
  //  
public:
  CEncoderBase(String name,
               UInt8 pinena, UInt8 pinenb,
               TInterruptFunction pirqhandlerena, 
               TInterruptFunction pirqhandlerenb);
  //
  virtual Boolean Open(void) = 0;
  virtual Boolean Close(void) = 0;
  //
  Int32 GetPosition(void);
  void SetPosition(Int32 position);
  //
  void IncrementPosition(void);
  void DecrementPosition(void);
  //
  void HandleIrqA(void);
  void HandleIrqB(void);
  //
  void Execute(void);
  void Message(CSerial serial);
};
//
#endif // EncoderBase_h
//
#endif // ENCODERIRQ_ISPLUGGED)
//