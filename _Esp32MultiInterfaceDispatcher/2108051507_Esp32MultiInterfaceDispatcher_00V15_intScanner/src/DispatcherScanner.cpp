//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_SCANNER)
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
#include "Error.h"
#include "DispatcherScanner.h"
#if (defined(DACX_INTERNAL) | defined(DACY_INTERNAL))
#include "DacInternal.h"
#endif
#if (defined(DACX_MCP4725) | defined(DACY_MCP4725))
#include "DacMCP4725.h"
#endif
#if defined(DISPATCHER_TRIGGER)
#include "TimeSpan.h"
#endif
//
extern CError Error;
extern CCommand Command;
// 
#if defined(DISPATCHER_TRIGGER)
  #if (defined(DACX_INTERNAL) || defined(DACY_INTERNAL)) 
  extern CDacInternal ScannerInternalXY;
  #endif
  #ifdef DACX_MCP4725
  extern CDacMcp4725 ScannerMcp4725X;
  #endif
  #ifdef DACY_MCP4725
  extern CDacMcp4725 ScannerMcp4725Y;
  #endif
  extern CTimeSpan ScannerDelayMotion;
#endif
//
//#########################################################
//  Dispatcher - Scanner - Constructor
//#########################################################
CDispatcherScanner::CDispatcherScanner(void)
{
}
//
//#########################################################
//  Dispatcher - Scanner - Execution
//#########################################################
bool CDispatcherScanner::ExecuteGetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int PX = 0;
  #if defined(DACX_INTERNAL)
  PX = DacInternal_XY.GetValueX();
  #endif
  #if defined(DACX_MCP4725)
  PX = ScannerMcp4725X.GetValue();
  #endif
  sprintf(Command.GetBuffer(), "%s %i", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetPositionX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <x>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PX = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, PX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  #if defined(DACX_INTERNAL)
  ScannerInternal_XY.SetValueX(PX);
  #endif
  #if defined(DACX_MCP4725)
  ScannerMcp4725X.SetValue(PX);
  #endif
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int PY = 0;
  #if defined(DACY_INTERNAL)
  PY = DacInternal_XY.GetValueX();
  #endif
  #if defined(DACY_MCP4725)
  PY = ScannerMcp4725Y.GetValue();
  #endif
  sprintf(Command.GetBuffer(), "%s %i", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetPositionY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <py>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PY = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, PY);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  #if defined(DACY_INTERNAL)
  ScannerInternal_XY.SetValueY(PY);
  #endif
  #if defined(DACY_MCP4725)
  ScannerMcp4725Y.SetValue(PY);
  #endif
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int XL = 0;
  int XH = 0;
  int DX = 0;
  #if defined(DACX_INTERNAL)
  XL = DacInternal_XY.GetRangeXL();
  XH = DacInternal_XY.GetRangeXH();
  DX = DacInternal_XY.GetRangeDX();
  #endif
  #if defined(DACX_MCP4725)
  XL = ScannerMcp4725X.GetRangeLow();
  XH = ScannerMcp4725X.GetRangeHigh();
  DX = ScannerMcp4725X.GetRangeDelta();
  #endif
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetRangeX(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <xl> <xh> <dx>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int XL = atoi(parameters[0]);
  int XH = atoi(parameters[1]);
  int DX = atoi(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerMcp4725X.SetRangeLow(XL);
  ScannerMcp4725X.SetRangeHigh(XH);
  ScannerMcp4725X.SetRangeDelta(DX);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int XL = 0;
  int XH = 0;
  int DX = 0;
  #if defined(DACX_INTERNAL)
  XL = DacInternal_XY.GetRangeXL();
  XH = DacInternal_XY.GetRangeXH();
  DX = DacInternal_XY.GetRangeDX();
  #endif
  #if defined(DACX_MCP4725)
  XL = ScannerMcp4725Y.GetRangeLow();
  XH = ScannerMcp4725Y.GetRangeHigh();
  DX = ScannerMcp4725Y.GetRangeDelta();
  #endif
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetRangeY(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <xl> <xh> <dx>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int XL = atoi(parameters[0]);
  int XH = atoi(parameters[1]);
  int DX = atoi(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, XL, XH, DX);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerMcp4725Y.SetRangeLow(XL);
  ScannerMcp4725Y.SetRangeHigh(XH);
  ScannerMcp4725Y.SetRangeDelta(DX);
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteGetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int DM = ScannerDelayMotion.GetPeriod();
  sprintf(Command.GetBuffer(), "%s %i", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherScanner::ExecuteSetDelayMotion(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <dm>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int DM = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, DM);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  ScannerDelayMotion.SetPeriod(DM);
  ExecuteEnd();
  return true;
}

// bool CDispatcherScanner::ExecuteGetPulseWidth(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: -
//   // Response:
//   int DM = ScannerDelayMotion.GetValue();
//   sprintf(Command.GetBuffer(), "%s %i", command, DM);
//   ExecuteResponse(Command.GetBuffer());
//   // Action: -
//   ExecuteEnd();
//   return true;
// }
//
// bool CDispatcherScanner::ExecuteSetPulseWidth(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: <pw>
//   if (parametercount < 1)
//   {
//     Error.SetCode(ecMissingParameter);
//     return false;
//   }
//   int PW = atoi(parameters[0]);
//   // Response:
//   sprintf(Command.GetBuffer(), "%s %i", command, PW);
//   ExecuteResponse(Command.GetBuffer());
//   // Action:
//   // LedSystem.Blink_Start(PC, PP, PW);
//   ExecuteEnd();
//   return true;
// }
//
//#########################################################
//  Dispatcher - Scanner - Handler
//#########################################################
bool CDispatcherScanner::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_GPX, command))
  {
    return ExecuteGetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPX, command))
  {
    return ExecuteSetPositionX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GPY, command))
  {
    return ExecuteGetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SPY, command))
  {
    return ExecuteSetPositionY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRX, command))
  {
    return ExecuteGetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRX, command))
  {
    return ExecuteSetRangeX(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GRY, command))
  {
    return ExecuteGetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SRY, command))
  {
    return ExecuteSetRangeY(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GDM, command))
  {
    return ExecuteGetDelayMotion(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_SDM, command))
  {
    return ExecuteSetDelayMotion(command, parametercount, parameters);
  }
  // if (!strcmp(SHORT_GPW, command))
  // {
  //   return ExecuteGetPulseWidth(command, parametercount, parameters);
  // }
  // if (!strcmp(SHORT_SPW, command))
  // {
  //   return ExecuteSetPulseWidth(command, parametercount, parameters);
  // } 
  return false;
}
//
#endif // DISPATCHER_LASER
//
//####################################################################
//####################################################################
//####################################################################
//
