//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_UART)
//
#include "Error.h"
#include "DispatcherUart.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//
extern CError Error;
extern CCommand Command;
// 
#if defined(INTERFACE_UART)
extern CUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanCommand;
#endif
//
//#########################################################
//  Dispatcher - Uart - Constructor
//#########################################################
CDispatcherUart::CDispatcherUart(void)
{
}
//
//#########################################################
//  Dispatcher - Uart - Execution
//#########################################################
bool CDispatcherUart::ExecuteWriteCommandUart(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: p0, p1, p2, p3
  if (4 <= parametercount)
  {
    sprintf(Command.GetBuffer(), "%s %s %s %s", parameters[0], parameters[1], parameters[2], parameters[3]);
  }
  else
  if (3 <= parametercount)
  {
    sprintf(Command.GetBuffer(), "%s %s %s", parameters[0], parameters[1], parameters[2]);
  }
  else
  if (2 == parametercount)
  {
    sprintf(Command.GetBuffer(), "%s %s", parameters[0], parameters[1]);
  }
  else
  if (1 == parametercount)
  {
    sprintf(Command.GetBuffer(), "%s", parameters[0]);
  }
  else
  {
    sprintf(Command.GetBuffer(), "No command!");
  }
  // Response:
  ExecuteResponse(Command.GetBuffer());
  // Action:
  UartCommand.WriteLine(Command.GetBuffer());
  ExecuteEnd();
  return true;
}
bool CDispatcherUart::ExecuteReadCommandUart(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  int RxCount = 0;
  if (UartCommand.ReadLine(Command.GetBuffer(), RxCount))
  {
    sprintf(Command.GetBuffer(), "%s UartLine[%s]", TERMINAL_RESPONSE, Command.GetBuffer());
  }
  else
  {
    sprintf(Command.GetBuffer(), "%s No UartLine detected", TERMINAL_RESPONSE);
  }
  UartCommand.WriteLine(Command.GetBuffer());
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Uart - Handler
//#########################################################
bool CDispatcherUart::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_WCU, command))
  {
    return ExecuteWriteCommandUart(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_RCU, command))
  {
    return ExecuteReadCommandUart(command, parametercount, parameters);
  }
  return false;
}
//
bool CDispatcherUart::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_UART
//
//####################################################################
//####################################################################
//####################################################################
//
