//
//--------------------------------
//  Library TimeSpan
//--------------------------------
//
#include "DefinitionSystem.h"
//
// no directive 
//
#include "TimeSpan.h"
//
//-------------------------------------------------------------------------------
//  TimeSpan - Constructor
//-------------------------------------------------------------------------------
CTimeSpan::CTimeSpan(const char* id)
{
  strcpy(FID, id);
  FTimePeriod = INIT_TIMESPAN_MS;
  FTimeStart = INIT_TIMESPAN_MS;
  FOnEvent = 0;
  FState = stsUnknown;
}

CTimeSpan::CTimeSpan(const char* id, DOnEventTimeSpan oneventtimespan)
{
  strcpy(FID, id);
  FTimePeriod = INIT_TIMESPAN_MS;
  FTimeStart = INIT_TIMESPAN_MS;
  FOnEvent = oneventtimespan;
  FState = stsUnknown;
}
//
//-------------------------------------------------------------------------------
//  TimeSpan - Property
//-------------------------------------------------------------------------------
const char* CTimeSpan::StateText(EStateTimeSpan state)
{
  if (stsIdle == state) return "Idle";
  if (stsBusy == state) return "Busy";
  return "Unknown";
}

EStateTimeSpan CTimeSpan::GetState(void)
{
  return FState;
}
void CTimeSpan::SetState(EStateTimeSpan state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "TimeSpan[%s]: %s", FID, StateText(FState));
      FOnEvent(this, FBuffer);
    }
  }
} 
Boolean CTimeSpan::IsIdle(void)
{
  return (stsIdle == FState);
}
Boolean CTimeSpan::IsBusy(void)
{
  return (stsBusy == FState);
}
//
//-------------------------------------------------------------------------------
//  TimeSpan - Method
//-------------------------------------------------------------------------------
Boolean CTimeSpan::Open()
{
  SetState(stsIdle);
  return true;
}
Boolean CTimeSpan::Close()
{
  SetState(stsIdle);
  return true;
}

Float32 CTimeSpan::GetPeriod(void)
{
  return FTimePeriod;
}
void CTimeSpan::SetPeriod(Float32 period)
{
  FTimeStart = 0;
  FTimePeriod = period;
}
void CTimeSpan::StartPeriod(void) // [mm.uu]
{
  FTimeStart = micros() / 1000.0f;
  FTimePeriod += FTimeStart;
  SetState(stsBusy);
}
void CTimeSpan::StartPeriod(Float32 timeperiod) // [mm.uu]
{
  FTimeStart = micros() / 1000.0f;
  FTimePeriod = timeperiod + FTimeStart;
  SetState(stsBusy);
}
  
void CTimeSpan::Abort(void)
{
  SetState(stsIdle);
}
//
//-------------------------------------------------------------------------------
//  TimeSpan - Execute
//-------------------------------------------------------------------------------
Boolean CTimeSpan::Execute(void)
{
  switch (FState)
  {
    case stsIdle:
      return false;
    case stsBusy:
      if (FTimePeriod <= (((Float32)micros() / 1000.0f) - FTimeStart))
      {
        SetState(stsIdle);
        return true;
      }
      return false;
    default:
      return false;
  }
}
//
