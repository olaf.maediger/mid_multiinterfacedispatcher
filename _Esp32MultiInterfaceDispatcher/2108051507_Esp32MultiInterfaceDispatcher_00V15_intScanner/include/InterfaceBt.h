#include "Defines.h"
//
#ifdef INTERFACE_BT
//
#ifndef InterfaceBt_h
#define InterfaceBt_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const byte BT_SIZE_RXBLOCK = 32;
const byte BT_SIZE_TXBLOCK = 32;
const Boolean BT_INIT_RXECHO    = true;
//
//----------------------------------------------------
//  CBtBase
//----------------------------------------------------
class CBtBase : public CInterfaceBase
{
  protected:
  char FRXBlock[BT_SIZE_RXBLOCK];
  char FTXBlock[BT_SIZE_TXBLOCK];
  Boolean FRXEcho;
  //
  public:
  CBtBase(void);
  //
  char* GetRXBlock(void)
  {
    return FRXBlock;
  }
  char* GetTXBlock(void)
  {
    return FTXBlock;
  }
  void SetRXEcho(Boolean rxecho);
  Boolean GetRXEcho(void);
  //
  virtual Boolean Open(int baudrate) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual Boolean WriteCharacter(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  virtual UInt8 GetRxCount(void) = 0;
  virtual Boolean ReadCharacter(char &rxcharacter) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};
//
class CBt
{
  private:
  CBtBase* FPBtBase;
  //
  public:
  // Constructor
  CBt(HardwareSerial* pserial);
  // Property
  char* GetRXBlock(void)
  {
    return FPUartBase->GetPRXBlock();
  }
  char* GetTXBlock(void)
  {
    return FPUartBase->GetPTXBlock();
  }
  void SetRXEcho(Boolean rxecho);
  Boolean GetRXEcho(void);
  //
  // Handler
  Boolean Open(int baudrate);
  Boolean Close(void);
  //
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  Boolean WritePrompt(void);
  Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  // Read
  UInt8 GetRxCount(void);
  Boolean ReadCharacter(char &rxcharacter);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  //
  // ??? Boolean Execute(void);
};
//
#endif // InterfaceBt_h
//
#endif // INTERFACE_BT
//
//
