//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_SCANNER)
//
#ifndef DispatcherScanner_h
#define DispatcherScanner_h
//
#include "Command.h"
#include "Dispatcher.h"
#include "Led.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - Scanner - SHORT
// //----------------------------------------------------------------
#define SHORT_GPX   "GPX"
#define SHORT_SPX   "SPX"
#define SHORT_GPY   "GPY"
#define SHORT_SPY   "SPY"
#define SHORT_GRX   "GRX"
#define SHORT_SRX   "SRX"
#define SHORT_GRY   "GRY"
#define SHORT_SRY   "SRY"
#define SHORT_GDM   "GDM"
#define SHORT_SDM   "SDM"
// #define SHORT_GPW   "GPW"
// #define SHORT_SPW   "SPW"
//
//----------------------------------------------------------------
// Dispatcher - Scanner - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_SCANNER    " Help (Scanner):"
#define MASK_GPX                " %-3s                 : Get Position X{0..4095}"
#define MASK_SPX                " %-3s <x>             : Set Position <x>{0..4095}"
#define MASK_GPY                " %-3s                 : Get Position Y{0..4095}"
#define MASK_SPY                " %-3s <y>             : Set Position <y>{0..4095}"
#define MASK_GRX                " %-3s                 : Get Range X{0..4095}"
#define MASK_SRX                " %-3s <xl> <xh> <dx>  : Set Range X <xl>..<xh> <dx>{0..4095}"
#define MASK_GRY                " %-3s                 : Get Range Y{0..4095}"
#define MASK_SRY                " %-3s <yl> <yh> <dy>  : Set Range Y <yl>..<yh> <dy>{0..4095}"
#define MASK_GDM                " %-3s                 : Get Delay Motion{us}"
#define MASK_SDM                " %-3s <d>             : Set <d>>elay{us} Motion"
// #define MASK_GPW                " %-3s                 : Get PulseWidth{us}"
// #define MASK_SPW                " %-3s <w>             : Set Pulse<w>idth{us}"
//
//----------------------------------------------------------------
// Dispatcher - LedSystem
//----------------------------------------------------------------
class CDispatcherScanner : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherScanner(void);
  //
  bool ExecuteGetPositionX(char* command, int parametercount, char** parameters);
  bool ExecuteSetPositionX(char* command, int parametercount, char** parameters);
  bool ExecuteGetPositionY(char* command, int parametercount, char** parameters);
  bool ExecuteSetPositionY(char* command, int parametercount, char** parameters);
  bool ExecuteGetRangeX(char* command, int parametercount, char** parameters);
  bool ExecuteSetRangeX(char* command, int parametercount, char** parameters);
  bool ExecuteGetRangeY(char* command, int parametercount, char** parameters);
  bool ExecuteSetRangeY(char* command, int parametercount, char** parameters);
  bool ExecuteGetDelayMotion(char* command, int parametercount, char** parameters);
  bool ExecuteSetDelayMotion(char* command, int parametercount, char** parameters);
  // bool ExecuteGetPulseWidth(char* command, int parametercount, char** parameters);
  // bool ExecuteSetPulseWidth(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherScanner_h
//
#endif // DISPATCHER_SCANNER
//
//###################################################################################
//###################################################################################
//###################################################################################
