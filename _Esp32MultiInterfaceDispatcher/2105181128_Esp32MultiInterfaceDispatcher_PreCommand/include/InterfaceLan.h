#include "Defines.h"
//
#ifdef INTERFACE_LAN
//
#ifndef InterfaceLan_h
#define InterfaceLan_h
//
#include <Arduino.h>
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
const int LAN_SIZE_RXBUFFER   = 64;
const int LAN_SIZE_TXBUFFER   = 64;
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//

class CLan
{
  private:
  char FRXBuffer[LAN_SIZE_RXBUFFER];
  char FTXBuffer[LAN_SIZE_TXBUFFER];
  //
  public:
  CLan(void);//HardwareSerial* pserial);
  //
  bool Open(void);
  bool Close(void);
  //
  char* GetPRXBuffer(void);
  char* GetPTXBuffer(void);
  //
  void WritePChar(char* mask, char* value);
  void WriteString(char* mask, String value);
  void WriteByte(char* mask, Byte value);
  void WriteDual(char* mask, UInt16 value);
  void WriteQuad(char* mask, UInt32 value);
  void WriteInt16(char* mask, Int16 value);
  void WriteUInt16(char* mask, UInt16 value);
  void WriteInt32(char* mask, Int32 value);
  void WriteUInt32(char* mask, UInt32 value);
  void WriteFloat32(char* mask, Float32 value);
  void WriteDouble64(char* mask, Double64 value);
};
//
#endif // InterfaceLan_h
//
#endif // INTERFACE_LAN
//
