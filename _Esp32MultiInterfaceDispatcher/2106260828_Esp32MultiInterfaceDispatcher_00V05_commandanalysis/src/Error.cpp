#include "Defines.h"
//
#include "Error.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(COMMAND_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#include "Dispatcher.h"
//-------------------------------
//
//
//--------------------------------
#if defined(INTERFACE_UART)
extern CUart UartCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanDispatcher;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanDispatcher;
#endif
#if defined(INTERFACE_BT)
extern CBt BtDispatcher;
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
extern CSDCard SDCard;
#endif
#if defined(PROTOCOL_MQTT)
extern CMqtt Mqtt;
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
extern CDispatcherCommon DispatcherCommon;
#endif
#if defined(COMMAND_SYSTEM)
extern CDispatcher Dispatcher;
#endif
#if defined(COMMAND_LEDSYSTEM)
extern CLed LedSystem;
#endif
#if defined(COMMAND_RTC)
extern CRtc RtcLocal;
#endif
#if defined(COMMAND_NTP)
extern CNtp NtpClient;
#endif
#if defined(COMMAND_WATCHDOG)
extern CWatchDog WatchDog;
#endif
#if defined(COMMAND_I2CDISPLAY)
extern 
#endif
//-------------------------------
//
CError::CError(void)
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode(void)
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open(void)
{
  FErrorCode = ecNone;
  return true; 
}
  
Boolean CError::Close(void)
{
  FErrorCode = ecNone;
  return true; 
}

const char* CError::ErrorCodeText(EErrorCode errorcode)
{
  switch (errorcode)
  {
    case ecNone:
      return ERROR_NONE;
    case ecInvalidCommand:
      return ERROR_INVALIDCOMMAND;
    case ecTimingFailure:
      return ERROR_TIMINGFAILURE;
    case ecInvalidParameter:
      return ERROR_INVALIDPARAMETER;
    case ecToManyParameters:
      return ERROR_TOMANYPARAMETERS;
    case ecNotEnoughParameters:
      return ERROR_NOTENOUGHPARAMETERS;
    case ecMissingParameter:
      return ERROR_MISSINGPARAMETER;
    case ecMountSDCard:
      return ERROR_MOUNTSDCARD;
    case ecOpenCommandFile:
      return ERROR_OPENCOMMANDFILE;
    case ecParseCommandFile:
      return ERROR_PARSECOMMANDFILE;
    case ecWriteCommandFile:
      return ERROR_WRITECOMMANDFILE;
    case ecCloseCommandFile:
      return ERROR_CLOSECOMMANDFILE;
    case ecUnmountSDCard:
      return ERROR_UNMOUNTSDCARD;
    case ecFileNotOpened:
      return ERROR_FILENOTOPENED;
    default: // ecUnknown:
      return ERROR_UNKNOWN;
  }
}

Boolean CError::Handle(void)
{
//   if (ecNone != FErrorCode)
//   {  
//     sprintf(FBuffer, "%s Error[%i]: %s!", TERMINAL_ERROR, FErrorCode, ErrorCodeText(FErrorCode));
// #ifdef INTERFACE_UART
//     UartCommand.WriteText(FBuffer);
//     UartCommand.WriteLine();
//     UartCommand.WritePrompt();
// #endif
// //
// #ifdef MQTTCLIENT_ISPLUGGED
//     MQTTClient.Publish("/Error", Buffer);
// #endif
//     FErrorCode = ecNone;
//     return true;
//   }
  return false;
}
