// //
#include "Defines.h"
//
#include "Error.h"
#include "Dispatcher.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
extern CError     Error;
//
#if defined(INTERFACE_UART)
extern CUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanCommand;
#endif
//
//#########################################################
//  Dispatcher - Constructor
//#########################################################
CDispatcher::CDispatcher(void)
{
  FState = scUndefined;
}
CDispatcher::~CDispatcher(void)
{
  FState = scUndefined;
}
//
//#########################################################
//  Dispatcher - Property
//#########################################################
EStateDispatcher CDispatcher::GetState(void)
{
  return FState;
}
//
void CDispatcher::SetState(EStateDispatcher state)
{
  if (state != FState)
  {
    FState = state;
    //!!!!!!!!!!!!!!!!!sprintf(GetTxdBuffer(), "SC %s", StateText(FState));
#ifdef COMMAND_UART
    SerialDispatcher.WriteEvent(GetTxdBuffer());
#endif
#if defined(COMMAND_MQTT)
    MQTTClient.WriteEvent(GetTxdBuffer());
#endif
  }
}

//
//#########################################################
//  Dispatcher - Helper
//#########################################################
const char* CDispatcher::StateText(EStateDispatcher state)
{
  switch (state)
  {
    case scError:
      return (const char*)"Error";
    case scUndefined:
      return (const char*)"Undefined";
    case scInit:
      return (const char*)"Init";
    case scIdle:
      return (const char*)"Idle";
    case scBusy:
      return (const char*)"Busy";
    default: // not in list -> Undefined
      return (const char*)"Unknown";
  }
}
//
//#########################################################
//  Dispatcher - Handler
//#########################################################
bool CDispatcher::Open(void)
{
  SetState(scInit);
  // Init();
  SetState(scIdle);
  return true;
}
bool CDispatcher::Close(void)
{
  SetState(scUndefined);
  return true;
}




void CDispatcher::WriteEvent(String line)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteEvent(line);
  UartCommand.WritePrompt();
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
  MQTTClient.WriteEvent(line);
#endif
#if defined(PROTOCOL_SDCARD)
  SDCard.WriteEvent(line);
#endif 
#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.WriteEvent(line);
#endif
}
//
void CDispatcher::WriteResponse(String line)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteResponse(line);
#endif

#if defined(INTERFACE_BT)
#endif

#if defined(INTERFACE_WLAN)
#endif

#if defined(INTERFACE_LAN)
#endif

#if defined(PROTOCOL_MQTT)
  MQTTClient.WriteResponse(line);
#endif

#if defined(PROTOCOL_SDCARD)
  SDCard.WriteAnswer(line);
#endif 

#if defined(COMMAND_I2CDISPLAY)
  I2CDisplay.WriteAnswer(line);
#endif
}
//
void CDispatcher::WriteComment(String line)
{
#ifdef INTERFACE_UART
  UartCommand.WriteComment(line);
  UartCommand.WritePrompt();
#endif
#if defined(PROTOCOL_MQTT)
  MQTTClient.WriteComment(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteComment(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteComment(line);
//#endif 
}
//
void CDispatcher::WriteComment(String mask, String line)
{
  //!!!!!!!!!!!!!!!!!!sprintf(GetTxdBuffer(), mask.c_str(), line.c_str());
  //!!!!!!!!!!!!!!!!!!!!WriteComment(GetTxdBuffer());
}

//
//##################################################################
// Dispatcher - Analysis & Execution
//##################################################################
Boolean CDispatcher::ExecuteBegin(void)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecTimingFailure);
    return false;
  }
#if defined(INTERFACE_UART)
  UartCommand.WritePrompt();
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
  SetState(scBusy);
  return true;
}

Boolean CDispatcher::ExecuteResponse(void)
{
#if defined(INTERFACE_UART)
  UartCommand.WritePrompt();
  // !!!!!!!!!!!!!!! WriteResponse(GetBuffer());
  UartCommand.WritePrompt();
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
  return true;
}

Boolean CDispatcher::ExecuteEnd(void)
{
#if defined(INTERFACE_UART)
  UartCommand.WritePrompt();
  SetState(scIdle);
  UartCommand.WritePrompt();
  return true;
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
}











//####################################################################################
//####################################################################################
//####################################################################################

// 

// //
// //##################################################################
// // Dispatcher - Write - Helper
// //##################################################################
// #if defined(COMMAND_COMMON)
// bool CDispatcher::WriteProgramHeader(void)
// {
// #if defined(INTERFACE_UART)
//   // //  UartCommand.WriteComment();
//   // UartCommand.WriteLine(); UartCommand.WriteComment();
//   // UartCommand.WriteLine(TITLE_LINE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PROJECT, ARGUMENT_PROJECT);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_HARDWARE, ARGUMENT_HARDWARE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_DATE, ARGUMENT_DATE);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_TIME, ARGUMENT_TIME);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_AUTHOR, ARGUMENT_AUTHOR);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PORT, ARGUMENT_PORT);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_PARAMETER, ARGUMENT_PARAMETER);
//   // UartCommand.WriteComment();
//   // UartCommand.WriteLine(TITLE_LINE);
//   // UartCommand.WriteComment();
//   // // UartCommand.WriteNewLine();
//   // // UartCommand.WriteComment();
//   // UartCommand.WriteLine(MASK_ENDLINE);
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
//   MQTTClient.WriteComment(TITLE_LINE);
//   MQTTClient.WriteComment(MASK_PROJECT, ARGUMENT_PROJECT);
//   MQTTClient.WriteComment(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
//   MQTTClient.WriteComment(MASK_HARDWARE, ARGUMENT_HARDWARE);
//   MQTTClient.WriteComment(MASK_DATE, ARGUMENT_DATE);
//   MQTTClient.WriteComment(MASK_TIME, ARGUMENT_TIME);
//   MQTTClient.WriteComment(MASK_AUTHOR, ARGUMENT_AUTHOR);
//   MQTTClient.WriteComment(MASK_PORT, ARGUMENT_PORT);
//   MQTTClient.WriteComment(MASK_PARAMETER, ARGUMENT_PARAMETER);
//   MQTTClient.WriteComment(TITLE_LINE);
//   MQTTClient.WriteComment(MASK_ENDLINE);
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
//   return true;
// }
// #endif // COMMAND_COMMON
// //
// #if defined(COMMAND_COMMON)
// void CDispatcher::WriteHardwareVersion(void)
// {
// #if defined(INTERFACE_UART)
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
// }
// #endif // COMMAND_COMMON
// //   sprintf(GetTxdBuffer(), MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// //   serial.WriteComment(GetTxdBuffer());
// // #endif
// // //######################################################
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteComment(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// // #endif
// //
// #if defined(COMMAND_COMMON)
// void CDispatcher::WriteSoftwareVersion(void)
// {
// #if defined(INTERFACE_UART)
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
// }
// #endif // COMMAND_COMMON
// //   sprintf(GetTxdBuffer(), MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// //   serial.WriteComment(GetTxdBuffer());
// // #endif
// // //######################################################
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteComment(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// // #endif
// // }
// // 
// //
// #if defined(COMMAND_COMMON)
// bool CDispatcher::WriteHelp(void)
// {
// #if defined(INTERFACE_UART) 
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN) 
// #endif
// #if defined(INTERFACE_LAN) 
// #endif
// #if defined(PROTOCOL_MQTT)
// #endif
// #if defined(PROTOCOL_SDCARD)
// #endif
//   return false;
// }
// #endif // COMMAND_COMMON


// bool CDispatcher::Execute(void)
// { 
// #if defined(COMMAND_COMMON)  
//   if (!strcmp(SHORT_GPH, GetPDispatcher()))
//   {
//     return ExecuteGetProgramHeader();
//   }
//   if (!strcmp(SHORT_GHV, GetPDispatcher()))
//   {
//     return ExecuteGetHardwareVersion();
//   }
//   if (!strcmp(SHORT_GSV, GetPDispatcher()))
//   {
//     return ExecuteGetSoftwareVersion();
//   }
//   if (!strcmp(SHORT_H, GetPDispatcher()))
//   {
//     return ExecuteGetHelp();
//   }
// #endif  
//   return false;
// }

// bool CDispatcher::HandleInterface(void)
// {
//   if (AnalyseInterfaceBlock())
//   {
//     return Execute();
//   }
//   return false;
// }











// #if defined(SDCARD_ISPLUGGED)
// bool CDispatcher::DetectSDCardLine(CSDCard &sdcard)
// {
//   if (scIdle == GetState())
//   { // ready for next Dispatcher
//     if (DispatcherFile.IsExecuting())
//     { // more Dispatchers exist and no Wait-Dispatcher:
//       if (TimeRelativeSystem.Wait_Execute()) return false;
// #if defined(NTPCLIENT_ISPLUGGED)
//       if (TimeAbsoluteSystem.Wait_Execute()) return false;
// #endif      
// #if defined(TRIGGERINPUT_ISPLUGGED)
//       if (TriggerInputSystem.Wait_Execute()) return false;
// #endif
// #if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
//       if (MotorPositionReached.Wait_Execute()) return false;
// #endif  
//       String Dispatcher = DispatcherFile.GetDispatcher();
//       if (0 < Dispatcher.length())
//       { // copy DispatcherFile-Line (next Dispatcher) to DispatcherBuffer:
//         strcpy(FDispatcherText, Dispatcher.c_str());
//         return true;
//       }
//     }
//   }
//   return false;
// }
// #endif
/*
ecf /sdc.cmd
*/

//
//#########################################################
//  CDispatcher - Execution 
//#########################################################

// //
// #if defined(PROTOCOL_SDCARD)  
//   if (DetectSDCardLine(sdcard))
//   {
//     if (AnalyseDispatcherText(serial))
//     {
//       return Execute(serial);
//     }
//   }
// #endif // SDCARD_ISPLUGGED
// //
// #if defined(PROTOCOL_MQTT)
//   if (DetectMQTTClientLine(mqttclient))
//   {
//     if (AnalyseDispatcherText(serial))
//     {
//       return Execute(serial);
//     }
//   }
// #endif // MQTTCLIENT_ISPLUGGED
// #if defined(INTERFACE_UART)
//   return false;
// #endif
// #if defined(INTERFACE_BT)
// #endif
// #if defined(INTERFACE_WLAN)
// #endif
// #if defined(INTERFACE_LAN)
// #endif

// #include "Error.h"
// //
// #if defined(COMMAND_UART)
// #include "DeviceUART.h"
// #endif
// //#if defined(COMMAND_WLAN)
// // #include "DeviceWLAN.h"
// // #endif
// #if defined(COMMAND_LAN)
// #include "DeviceLAN.h"
// #endif
// // #if defined(COMMAND_BT)
// // #include "DeviceBT.h"
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #include "DeviceI2CDisplay.h"
// // #endif
// // #if defined(COMMAND_SDCARD)
// // #include "DeviceSDCard.h"
// // #endif
// // #if defined(COMMAND_MQTT)
// // #include "DeviceMQTT.h"
// // #endif
//
//-------------------------------------------
//  External Global Variables
//-------------------------------------------
//extern CError               Error;
// //
// #if defined(COMMAND_UART)
// extern CDeviceUART DeviceUartPC;
// #endif
// // #if defined(COMMAND_WLAN)
// // extern CDeviceWLAN DeviceWlan;
// // #endif
// #if defined(COMMAND_LAN)
// extern CDeviceLAN DeviceLan;
// #endif
// // #if defined(COMMAND_BT)
// // extern CDeviceBT DeviceBt;
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // extern CDeviceI2CDisplay DeviceI2CDisplay;
// // #endif
// // #if defined(COMMAND_SDCARD)
// // extern CDeviceSDCard DeviceSDCard;
// // #endif
// // #if defined(COMMAND_MQTT)
// // extern CDeviceMQTT DeviceMqtt;
// // #endif
// //

// void CDispatcher::TransmitDispatcher(String command)
// { // command := SIGN_COMMAND + "<command> <parameter>|n"
//   sprintf(FTxBuffer, "%c%s\r\n", SIGN_COMMAND, command.c_str());
// #if defined(COMMAND_UART)
//   DeviceUartPC.Write(FTxBuffer);
// #endif
// #if defined(COMMAND_WLAN)
//   DeviceWlan.Write(FTxBuffer);
// #endif
// #if defined(COMMAND_LAN)
//   DeviceLan.Write(FTxBuffer);
// #endif
// #if defined(COMMAND_BT)
//   DeviceBt.Write(FTxBuffer);
// #endif
// #if defined(COMMAND_I2CDISPLAY)
// #endif
// #if defined(COMMAND_SDCARD)
//   DeviceSDCard.WriteProtocol(FTxBuffer);
// #endif
// #if defined(COMMAND_MQTT)
//   DeviceMqtt.Write(FTxBuffer);
// #endif
// }
// // //
// // void CDispatcher::TransmitEvent(String event)
// // { // event := SIGN_EVENT + "<event> <parameter>|n"
// // sprintf(FTxBuffer, "%c%s\r\n", SIGN_EVENT, event.c_str());
// // #if defined(COMMAND_UART)
// // DeviceUartPC.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_WLAN)
// // DeviceWlan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_LAN)
// // DeviceLan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_BT)
// // DeviceBt.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #endif
// // #if defined(COMMAND_SDCARD)
// // DeviceSDCard.WriteProtocol(FTxBuffer);
// // #endif
// // #if defined(COMMAND_MQTT)
// // DeviceMqtt.Write(FTxBuffer);
// // #endif
// // }
// // //
// // void CDispatcher::TransmitComment(String comment)
// // { // comment := SIGN_COMMENT + "<comment> <parameter>|n"
// // sprintf(FTxBuffer, "%c%s\r\n", SIGN_COMMENT, comment.c_str());
// // #if defined(COMMAND_UART)
// // DeviceUartPC.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_WLAN)
// // DeviceWlan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_LAN)
// // DeviceLan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_BT)
// // DeviceBt.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #endif
// // #if defined(COMMAND_SDCARD)
// // DeviceSDCard.WriteProtocol(FTxBuffer);
// // #endif
// // #if defined(COMMAND_MQTT)
// // DeviceMqtt.Write(FTxBuffer);
// // #endif
// // }
// // //
// // void CDispatcher::TransmitWarning(String warning)
// // { // warning := SIGN_WARNING + "<warning> <parameter>|n"
// // sprintf(FTxBuffer, "%c%s\r\n", SIGN_WARNING, warning.c_str());
// // #if defined(COMMAND_UART)
// // DeviceUartPC.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_WLAN)
// // DeviceWlan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_LAN)
// // DeviceLan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_BT)
// // DeviceBt.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #endif
// // #if defined(COMMAND_SDCARD)
// // DeviceSDCard.WriteProtocol(FTxBuffer);
// // #endif
// // #if defined(COMMAND_MQTT)
// // DeviceMqtt.Write(FTxBuffer);
// // #endif
// // }
// // //
// // void CDispatcher::TransmitError(String error)
// // { // error := SIGN_ERROR + "<error> <parameter>|n"
// // sprintf(FTxBuffer, "%c%s\r\n", SIGN_ERROR, error.c_str());
// // #if defined(COMMAND_UART)
// // DeviceUartPC.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_WLAN)
// // DeviceWlan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_LAN)
// // DeviceLan.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_BT)
// // DeviceBt.Write(FTxBuffer);
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #endif
// // #if defined(COMMAND_SDCARD)
// // DeviceSDCard.WriteProtocol(FTxBuffer);
// // #endif
// // #if defined(COMMAND_MQTT)
// // DeviceMqtt.Write(FTxBuffer);
// // #endif
// // }
// // //
// // void CDispatcher::ReceiveAnswer(void)
// // { // answer := SIGN_ANSWER + "<answer> <parameter>|n"
// // //sprintf(FTxBuffer, "%c%s\r\n", SIGN_ERROR, error.c_str());
// // #if defined(COMMAND_UART)
// // DeviceUartPC.Read(FRxBuffer);
// // #endif
// // #if defined(COMMAND_WLAN)
// // DeviceWlan.Read(FRxBuffer);
// // #endif
// // #if defined(COMMAND_LAN)
// // DeviceLan.Read(FRxBuffer);
// // #endif
// // #if defined(COMMAND_BT)
// // DeviceBt.Read(FRxBuffer);
// // #endif
// // #if defined(COMMAND_I2CDISPLAY)
// // #endif
// // #if defined(COMMAND_SDCARD)
// // DeviceSDCard.ReadProtocol(FRxBuffer);
// // #endif
// // #if defined(COMMAND_MQTT)
// // DeviceMqtt.Read(FRxBuffer);
// // #endif
// // }

// // // Defines Reset-Function (identically HW-Reset):
// // void (*PResetSystem)(void) = 0;
// // //
// // // void CDispatcher::ZeroRxdBuffer(void)
// // // {
// // //   int CI;
// // //   for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
// // //   {
// // //     FRxdBuffer[CI] = 0x00;
// // //   }
// // //   FRxdBufferIndex = 0;
// // // }
// // // void CDispatcher::ZeroTxdBuffer(void)
// // // {
// // //   int CI;
// // //   for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
// // //   {
// // //     FTxdBuffer[CI] = 0x00;
// // //   }
// // // }
// // // //
// // // void CDispatcher::ZeroDispatcherText(void)
// // // {
// // //   int CI;
// // //   for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
// // //   {
// // //     FDispatcherText[CI] = 0x00;
// // //   }
// // // }
// // // //
// // // void CDispatcher::AbortProcessExecution(void)
// // // {
// // // #if defined(SDCARD_ISPLUGGED)
// // //   DispatcherFile.Close();
// // // #endif  
// // //   // Close all
// // //   TimeRelativeSystem.Wait_Abort();
// // //   //
// // //   LedSystem.Blink_Abort();

// // // }
// // //
// // //#########################################################
// // //  Segment - Helper - Analyse
// // //#########################################################
// // //
// // void CDispatcher::Init(void)
// // {
// // }

// // // bool CDispatcher::AnalyseDispatcherText(CSerial &serial)
// // // {
// // //   char *PTerminal = (char*)" \t\r\n";
// // //   FPDispatcher = strtok(FDispatcherText, PTerminal);
// // //   if (FPDispatcher)
// // //   {
// // //     FParameterCount = 0;
// // //     char *PParameter;
// // //     while (0 != (PParameter = strtok(0, PTerminal)))
// // //     {
// // //       FPParameters[FParameterCount] = PParameter;
// // //       FParameterCount++;
// // //       if (COUNT_TEXTPARAMETERS < FParameterCount)
// // //       {
// // //         Error.SetCode(ecToManyParameters);
// // //         ZeroRxdBuffer();
// // //         serial.WriteNewLine();
// // //         serial.WritePrompt();
// // //         return false;
// // //       }
// // //     }  
// // //     ZeroRxdBuffer();
// // //     serial.WriteNewLine();
// // //     return true;
// // //   }
// // //   ZeroRxdBuffer();
// // //   serial.WriteNewLine();
// // //   serial.WritePrompt();    
// // //   return false;
// // // }

// // // bool CDispatcher::DetectRxdLine(CSerial &serial)
// // // {
// // //   while (0 < serial.GetRxdByteCount())
// // //   {
// // //     Character C = serial.ReadCharacter();
// // //     switch (C)
// // //     {
// // //       case TERMINAL_CARRIAGERETURN:
// // //         FRxdBuffer[FRxdBufferIndex] = TERMINAL_ZERO;
// // //         FRxdBufferIndex = 0; // restart
// // //         strupr(FRxdBuffer);
// // //         strcpy(FDispatcherText, FRxdBuffer);
// // //         return true;
// // //       case TERMINAL_LINEFEED: // ignore
// // //         break;
// // //       default: 
// // //         FRxdBuffer[FRxdBufferIndex] = C;
// // //         FRxdBufferIndex++;
// // //         break;
// // //     }
// // //   }
// // //   return false;
// // // }
// // // //
// // // #if defined(SDCARD_ISPLUGGED)
// // // bool CDispatcher::DetectSDCardLine(CSDCard &sdcard)
// // // {
// // //   if (scIdle == GetState())
// // //   { // ready for next Dispatcher
// // //     if (DispatcherFile.IsExecuting())
// // //     { // more Dispatchers exist and no Wait-Dispatcher:
// // //       if (TimeRelativeSystem.Wait_Execute()) return false;
// // // #if defined(NTPCLIENT_ISPLUGGED)
// // //       if (TimeAbsoluteSystem.Wait_Execute()) return false;
// // // #endif      
// // // #if defined(TRIGGERINPUT_ISPLUGGED)
// // //       if (TriggerInputSystem.Wait_Execute()) return false;
// // // #endif
// // // #if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
// // //       if (MotorPositionReached.Wait_Execute()) return false;
// // // #endif  
// // //       String Dispatcher = DispatcherFile.GetDispatcher();
// // //       if (0 < Dispatcher.length())
// // //       { // copy DispatcherFile-Line (next Dispatcher) to DispatcherBuffer:
// // //         strcpy(FDispatcherText, Dispatcher.c_str());
// // //         return true;
// // //       }
// // //     }
// // //   }
// // //   return false;
// // // }
// // // #endif
// // // /*
// // // ecf /sdc.cmd
// // // */
// // // //
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // // bool CDispatcher::DetectMQTTClientLine(CMQTTClient &mqttclient)
// // // {
// // //   if (scIdle == GetState())
// // //   {
// // //     String Line = mqttclient.ReadReceivedText();
// // //     if (0 < Line.length())
// // //     {
// // //       strcpy(FDispatcherText, Line.c_str());
// // //       return true;
// // //     }
// // //   }
// // //   return false;
// // // }
// // // #endif
// // // //
// // // #if defined(SDCARD_ISPLUGGED)
// // // bool CDispatcher::Handle(CSerial &serial, CSDCard &sdcard)
// // // { // Dispatcher <- SDCard
// // //   if (DetectSDCardLine(sdcard))
// // //   {
// // //     if (AnalyseDispatcherText(serial))
// // //     {
// // //       return Execute(serial);
// // //     }
// // //   }
// // //   return false;
// // // }
// // // #endif // SDCARD_ISPLUGGED
// // // //
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // // bool CDispatcher::Handle(CSerial &serial, CMQTTClient &mqttclient)
// // // { // Dispatcher <- Mqtt
// // //   if (DetectMQTTClientLine(mqttclient))
// // //   {
// // //     if (AnalyseDispatcherText(serial))
// // //     {
// // //       return Execute(serial);
// // //     }
// // //   }
// // //   return false;
// // // }
// // // #endif // MQTTCLIENT_ISPLUGGED
// // // //
// // // bool CDispatcher::Handle(CSerial &serial)
// // // { // Dispatcher <- (Usb)Serial
// // //   if (DetectRxdLine(serial))
// // //   {
// // //     if (AnalyseDispatcherText(serial))
// // //     {
// // //       return Execute(serial);
// // //     }
// // //   }
// // //   return false;
// // // }
// // //
// // //#########################################################
// // //  Segment - Helper - Answer
// // //#########################################################
// // //
// // //void CDispatcher::WritePrompt()
// // //{
// // //#ifdef SERIALCOMMAND_ISPLUGGED
// // //  SerialDispatcher.WritePrompt();
// // //#endif
// // //}
// // //
// // void CDispatcher::WriteEvent(String line)
// // {
// // #ifdef SERIALCOMMAND_ISPLUGGED
// //   SerialDispatcher.WriteEvent(line);
// //   SerialDispatcher.WritePrompt();
// // #endif
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteEvent(line);
// // #endif
// // //#if defined(I2CDISPLAY_ISPLUGGED)
// // //  I2CDisplay.WriteEvent(line);
// // //#endif
// // //#if defined(SDCARD_ISPLUGGED)
// // //  SDCard.WriteEvent(line);
// // //#endif 
// // }

// // void CDispatcher::WriteResponse(String line)
// // {
// // #ifdef SERIALCOMMAND_ISPLUGGED
// //   SerialDispatcher.WriteResponse(line);
// // #endif
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteResponse(line);
// // //???  MQTTClient.WritePrompt();
// // #endif
// // //#if defined(I2CDISPLAY_ISPLUGGED)
// // //  I2CDisplay.WriteAnswer(line);
// // //#endif
// // //#if defined(SDCARD_ISPLUGGED)
// // //  SDCard.WriteAnswer(line);
// // //#endif
// // }

// // void CDispatcher::WriteComment(String line)
// // {
// // #ifdef SERIALCOMMAND_ISPLUGGED
// //   SerialDispatcher.WriteComment(line);
// //   SerialDispatcher.WritePrompt();
// // #endif
// // #if defined(MQTTCLIENT_ISPLUGGED)
// //   MQTTClient.WriteComment(line);
// // #endif
// // //#if defined(I2CDISPLAY_ISPLUGGED)
// // //  I2CDisplay.WriteComment(line);
// // //#endif
// // //#if defined(SDCARD_ISPLUGGED)
// // //  SDCard.WriteComment(line);
// // //#endif 
// // }
// // void CDispatcher::WriteComment(String mask, String line)
// // {
// //   //!!!!!!!!!!!!!!!!!!sprintf(GetTxdBuffer(), mask.c_str(), line.c_str());
// //   //!!!!!!!!!!!!!!!!!!!!WriteComment(GetTxdBuffer());
// // }
// // //
// // //#########################################################
// // //  Segment - Basic Output
// // //#########################################################
// // //
// // // void CDispatcher::WriteProgramHeader(CSerial &serial)
// // // {
// // // #if defined(SERIALCOMMAND_ISPLUGGED) 
// // // //  serial.WriteComment();
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(TITLE_LINE);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_DATE, ARGUMENT_DATE);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_TIME, ARGUMENT_TIME);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_PORT, ARGUMENT_PORT);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // //   serial.Write(TITLE_LINE);
// // //   serial.WriteNewLine();
// // //   serial.WriteComment();
// // // //  serial.WriteNewLine();
// // // //  serial.WriteComment();
// // //   serial.WriteLine(MASK_ENDLINE);
// // // #endif
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // //   MQTTClient.WriteComment(TITLE_LINE);
// // //   MQTTClient.WriteComment(MASK_PROJECT, ARGUMENT_PROJECT);
// // //   MQTTClient.WriteComment(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
// // //   MQTTClient.WriteComment(MASK_HARDWARE, ARGUMENT_HARDWARE);
// // //   MQTTClient.WriteComment(MASK_DATE, ARGUMENT_DATE);
// // //   MQTTClient.WriteComment(MASK_TIME, ARGUMENT_TIME);
// // //   MQTTClient.WriteComment(MASK_AUTHOR, ARGUMENT_AUTHOR);
// // //   MQTTClient.WriteComment(MASK_PORT, ARGUMENT_PORT);
// // //   MQTTClient.WriteComment(MASK_PARAMETER, ARGUMENT_PARAMETER);
// // //   MQTTClient.WriteComment(TITLE_LINE);
// // //   MQTTClient.WriteComment(MASK_ENDLINE);
// // // #endif  
// // // }
// // //
// // // void CDispatcher::WriteIRQVersion(CSerial &serial)
// // // {
// // // #if defined(SERIALCOMMAND_ISPLUGGED)
// // //   sprintf(GetTxdBuffer(), MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// // //   serial.WriteComment(GetTxdBuffer());
// // // #endif
// // // //######################################################
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // //   MQTTClient.WriteComment(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
// // // #endif
// // // }
// // // //
// // // void CDispatcher::WriteHardwareVersion(CSerial &serial)
// // // {
// // // #if defined(SERIALCOMMAND_ISPLUGGED) 
// // //   sprintf(GetTxdBuffer(), MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// // //   serial.WriteComment(GetTxdBuffer());
// // // #endif
// // // //######################################################
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // //   MQTTClient.WriteComment(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
// // // #endif
// // // }
// // // //
// // // void CDispatcher::WriteHelp(CSerial &serial)
// // // {
// // // #if defined(SERIALCOMMAND_ISPLUGGED) 
// // //     serial.WriteNewLine();
// // //     serial.WriteComment(); serial.WriteLine(HELP_COMMON);
// // //     serial.WriteComment(); serial.Write(MASK_H, SHORT_H); serial.WriteLine();
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     serial.WriteComment(); serial.Write(MASK_GPH, SHORT_GPH); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_GSV, SHORT_GSV); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_GHV, SHORT_GHV); serial.WriteLine();
// // //   #endif
// // //     serial.WriteComment(); serial.Write(MASK_RSS, SHORT_RSS); serial.WriteLine();
// // //   #if defined(WATCHDOG_ISPLUGGED)
// // //     serial.WriteComment(); serial.Write(MASK_PWD, SHORT_PWD); serial.WriteLine();
// // //   #endif
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     serial.WriteComment(); serial.Write(MASK_APE, SHORT_APE); serial.WriteLine();
// // //   #endif
// // //     serial.WriteComment(); serial.Write(MASK_WTR, SHORT_WTR); serial.WriteLine();
// // //   #if defined(NTPCLIENT_ISPLUGGED)
// // //     serial.WriteComment(); serial.Write(MASK_WTA, SHORT_WTA); serial.WriteLine();
// // //   #endif
// // //   #if defined(TRIGGERINPUT_ISPLUGGED)  
// // //     serial.WriteComment(); serial.Write(MASK_WTI, SHORT_WTI); serial.WriteLine();
// // //   #endif
// // //     //
// // //     // System Dispatchers Enabled
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_LEDSYSTEM);
// // //     serial.WriteComment(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_LSH, SHORT_LSH); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_LSL, SHORT_LSL); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BLS, SHORT_BLS); serial.WriteLine();
// // //   #endif
// // //   #if defined(MULTICHANNELLED_ISPLUGGED)
// // //     serial.WriteComment(); serial.Write(MASK_BL1, SHORT_BL1); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL2, SHORT_BL2); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL3, SHORT_BL3); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL4, SHORT_BL4); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL5, SHORT_BL5); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL6, SHORT_BL6); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL7, SHORT_BL7); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_BL8, SHORT_BL8); serial.WriteLine();
// // //   #endif
// // //   //
// // //   // Serial
// // //   #if defined(SERIALCOMMAND_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_SERIAL_COMMAND);
// // //     serial.WriteComment(); serial.Write(MASK_WSC, SHORT_WSC); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_RSC, SHORT_RSC); serial.WriteLine();
// // //   #endif
// // //   //
// // //   #if defined(I2CDISPLAY_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_I2CDISPLAY);
// // //     serial.WriteComment(); serial.Write(MASK_CLI, SHORT_CLI); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_STI, SHORT_STI); serial.WriteLine();
// // //   #endif
// // //   //
// // //   #if defined(SDCARD_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_SDCOMMAND);
// // // //!!!    serial.WriteComment(); serial.Write(MASK_OCF, SHORT_OCF); serial.WriteLine();
// // // //!!!    serial.WriteComment(); serial.Write(MASK_WCF, SHORT_WCF); serial.WriteLine();
// // // //!!!    serial.WriteComment(); serial.Write(MASK_CCF, SHORT_CCF); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_ECF, SHORT_ECF); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_ACF, SHORT_ACF); serial.WriteLine();
// // //   #endif
// // //   //
// // //   // NTPClient
// // //   #if defined(NTPCLIENT_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_NTPCLIENT);
// // //     serial.WriteComment(); serial.Write(MASK_GNT, SHORT_GNT); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_GND, SHORT_GND); serial.WriteLine();
// // //   #endif
// // //   //
// // //   //
// // //   // MQTTClient
// // //   #if defined(MQTTCLIENT_ISPLUGGED)
// // //   // !!!!!!!!  serial.WriteAnswer(); serial.WriteLine(HELP_MQTTCLIENT);
// // //   #endif
// // //   //
// // //   // MotorL298N
// // //   #if defined(MOTORL298N_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_MOTORL298N);
// // //     serial.WriteComment(); serial.Write(MASK_GWL, SHORT_GWL); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_SWL, SHORT_SWL); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_GWH, SHORT_GWH); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_SWH, SHORT_SWH); serial.WriteLine();
// // //     //
// // //     serial.WriteComment(); serial.Write(MASK_MMP, SHORT_MMP); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_MMN, SHORT_MMN); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_SM, SHORT_SM);   serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_A, SHORT_A);   serial.WriteLine();
// // //   #endif
// // //     //
// // //     // EncoderIRQ
// // //   #if defined(ENCODERIRQ_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_ENCODERIRQ);
// // //     serial.WriteComment(); serial.Write(MASK_GEP, SHORT_GEP); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_SEP, SHORT_SEP); serial.WriteLine();
// // //   #endif
// // //     //
// // //     // MotorEncoder
// // //   #if defined(MOTORENCODER_ISPLUGGED)
// // //     serial.WriteComment(); serial.WriteLine(HELP_MOTORENCODER);
// // //     serial.WriteComment(); serial.Write(MASK_MFP, SHORT_MFP); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_MFN, SHORT_MFN); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_MPA, SHORT_MPA); serial.WriteLine();
// // //     serial.WriteComment(); serial.Write(MASK_MPR, SHORT_MPR); serial.WriteLine();
// // //   #endif
// // //   //
// // //   serial.WriteComment(); serial.WriteLine(MASK_ENDLINE);
// // // #endif
// // // //################################################################
// // // #if defined(MQTTCLIENT_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_COMMON);
// // //     MQTTClient.WriteComment(MASK_H, SHORT_H);
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     MQTTClient.WriteComment(MASK_GPH, SHORT_GPH);
// // //     MQTTClient.WriteComment(MASK_GSV, SHORT_GSV);
// // //     MQTTClient.WriteComment(MASK_GHV, SHORT_GHV);
// // //   #endif
// // //     MQTTClient.WriteComment(MASK_RSS, SHORT_RSS);
// // //   #if defined(WATCHDOG_ISPLUGGED)
// // //     MQTTClient.WriteComment(MASK_PWD, SHORT_PWD);
// // //   #endif
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     MQTTClient.WriteComment(MASK_A, SHORT_A);
// // //   #endif
// // //   #if defined(NTPCLIENT_ISPLUGGED)
// // //     MQTTClient.WriteComment(MASK_WTR, SHORT_WTR);
// // //     MQTTClient.WriteComment(MASK_WTA, SHORT_WTA);
// // //   #endif
// // //   #if defined(TRIGGERINPUT_ISPLUGGED)
// // //     MQTTClient.WriteComment(MASK_WTI, SHORT_WTI);
// // //   #endif
// // //     //
// // //   #if defined(COMMAND_SYSTEMENABLED)
// // //     MQTTClient.WriteComment(HELP_LEDSYSTEM);
// // //     MQTTClient.WriteComment(MASK_GLS, SHORT_GLS);
// // //     MQTTClient.WriteComment(MASK_LSH, SHORT_LSH);
// // //     MQTTClient.WriteComment(MASK_LSL, SHORT_LSL);
// // //     MQTTClient.WriteComment(MASK_BLS, SHORT_BLS);
// // //   #endif
// // //   #if defined(MULTICHANNELLED_ISPLUGGED)  
// // //     MQTTClient.WriteComment(MASK_BL1, SHORT_BL1);
// // //     MQTTClient.WriteComment(MASK_BL2, SHORT_BL2);
// // //     MQTTClient.WriteComment(MASK_BL3, SHORT_BL3);
// // //     MQTTClient.WriteComment(MASK_BL4, SHORT_BL4);
// // //     MQTTClient.WriteComment(MASK_BL5, SHORT_BL5);
// // //     MQTTClient.WriteComment(MASK_BL6, SHORT_BL6);
// // //     MQTTClient.WriteComment(MASK_BL7, SHORT_BL7);
// // //     MQTTClient.WriteComment(MASK_BL8, SHORT_BL8);
// // //   #endif
// // //   //
// // //   // Serial
// // //   MQTTClient.WriteComment(HELP_SERIAL_COMMAND);
// // //   MQTTClient.WriteComment(MASK_WLC, SHORT_WLC);
// // //   MQTTClient.WriteComment(MASK_RLC, SHORT_RLC);
// // //   //
// // //   // I2CDisplay
// // //   #if defined(I2CDISPLAY_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_I2CDISPLAY);
// // //     MQTTClient.WriteComment(MASK_CLI, SHORT_CLI);
// // //     MQTTClient.WriteComment(MASK_STI, SHORT_STI);
// // //   #endif
// // //   //
// // //   // NTPClient
// // //   #if defined(NTPCLIENT_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_NTPCLIENT);
// // //     MQTTClient.WriteComment(MASK_GNT, SHORT_GNT);
// // //     MQTTClient.WriteComment(MASK_GND, SHORT_GND);
// // //   #endif
// // //   //
// // //   // SDCard
// // //   #if defined(SDCARD_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_SDCOMMAND);
// // //     // !!! MQTTClient.WriteComment(MASK_OCF, SHORT_OCF);
// // //     // !!! MQTTClient.WriteComment(MASK_WCF, SHORT_WCF);
// // //     // !!! MQTTClient.WriteComment(MASK_CCF, SHORT_CCF);
// // //     MQTTClient.WriteComment(MASK_ECF, SHORT_ECF);
// // //     MQTTClient.WriteComment(MASK_ACF, SHORT_ACF);
// // //   #endif
// // //   //
// // //   // RFIDClient
// // //   #if defined(RFIDCLIENT_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_RFIDCLIENT);
// // //     // ... MQTTClient.WriteComment(MASK_SRL, SHORT_SRL); 
// // //   #endif
// // //     //
// // //   #if defined(MQTTCLIENT_ISPLUGGED)
// // //     // MQTTClient.WriteComment(HELP_MQTTCLIENT);
// // //   #endif
// // //     //
// // //   // MotorVNH2SP30
// // //   #if defined(MOTORVNH2SP30_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_MOTORVNH2SP30);
// // //     MQTTClient.WriteComment(MASK_GLWL, SHORT_GLWL);
// // //     MQTTClient.WriteComment(MASK_SLWL, SHORT_SLWL);
// // //     MQTTClient.WriteComment(MASK_GLWH, SHORT_GLWH);
// // //     MQTTClient.WriteComment(MASK_SLWH, SHORT_SLWH);
// // //     //
// // //     MQTTClient.WriteComment(MASK_GRWL, SHORT_GRWL);
// // //     MQTTClient.WriteComment(MASK_SRWL, SHORT_SRWL);
// // //     MQTTClient.WriteComment(MASK_GRWH, SHORT_GRWH);
// // //     MQTTClient.WriteComment(MASK_SRWH, SHORT_SRWH);
// // //     //
// // //     MQTTClient.WriteComment(MASK_A,   SHORT_A);
// // //     MQTTClient.WriteComment(MASK_SL,  SHORT_SL);
// // //     MQTTClient.WriteComment(MASK_SR,  SHORT_SR);
// // //     MQTTClient.WriteComment(MASK_MLP, SHORT_MLP);
// // //     MQTTClient.WriteComment(MASK_MLN, SHORT_MLN);
// // //     MQTTClient.WriteComment(MASK_MRP, SHORT_MRP);
// // //     MQTTClient.WriteComment(MASK_MRN, SHORT_MRN);
// // //     MQTTClient.WriteComment(MASK_MBP, SHORT_MBP);
// // //     MQTTClient.WriteComment(MASK_MBN, SHORT_MBN);
// // //     MQTTClient.WriteComment(MASK_RBP, SHORT_RBP);
// // //     MQTTClient.WriteComment(MASK_RBN, SHORT_RBN);
// // //   #endif
// // //     //
// // //     // MotorEncoderLM393
// // //   #if defined(MOTORENCODERLM393_ISPLUGGED)
// // //     MQTTClient.WriteComment(HELP_MOTORENCODERLM393);
// // //     MQTTClient.WriteComment(MASK_GEPB, SHORT_GEPB);
// // //     MQTTClient.WriteComment(MASK_SEPL, SHORT_SEPL);
// // //     MQTTClient.WriteComment(MASK_SEPR, SHORT_SEPR);
// // //     MQTTClient.WriteComment(MASK_WPR, SHORT_WPR);
// // //   #endif
// // //     //
// // //     // RF433MHzClient / RemoteWirelessSwitch
// // //   #if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
// // //     MQTTClient.WriteComment(HELP_REMOTEWIRELESSSWITCH);
// // //     MQTTClient.WriteComment(MASK_RSO, SHORT_RSO);
// // //     MQTTClient.WriteComment(MASK_RSF, SHORT_RSF);
// // //   #endif  
// // //     //
// // //     // LaserScanner
// // //   #if defined(LASERSCANNER_JI)
// // //     MQTTClient.WriteComment(HELP_LASERSCANNER);
// // //     MQTTClient.WriteComment(MASK_SPP, SHORT_SPP);
// // //     MQTTClient.WriteComment(MASK_GPP, SHORT_GPP);
// // //     MQTTClient.WriteComment(MASK_PLA, SHORT_PLA);
// // //     MQTTClient.WriteComment(MASK_PLC, SHORT_PLC);
// // //     MQTTClient.WriteComment(MASK_MPX, SHORT_MPX);
// // //     MQTTClient.WriteComment(MASK_MPY, SHORT_MPY);
// // //     MQTTClient.WriteComment(MASK_MPP, SHORT_MPP);
// // //   #endif
// // //   #if defined(LASERSCANNER_PS)
// // //     MQTTClient.WriteComment(HELP_LASERSCANNER);
// // //     MQTTClient.WriteComment(MASK_, SHORT_);
// // //     //
// // //   #endif
// // //     //
// // //     MQTTClient.WriteComment(MASK_ENDLINE);   
// // // #endif
// // // }
// // // //
// // // //#########################################################
// // // //  Segment - Dispatcher - Execution - Common
// // // //#########################################################
// // // //
// // // bool CDispatcher::ExecuteGetHelp(CSerial &serial)
// // // {
// // //   if (scIdle != GetState())
// // //   {
// // //     Error.SetCode(ecDispatcherTimingFailure);
// // //     return false;
// // //   }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters: -
// // //   // Response:
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   WriteHelp(serial);
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt(); 
// // //   return true;
// // // }
// // // //
// // // #if defined(COMMAND_SYSTEMENABLED)
// // // bool CDispatcher::ExecuteGetProgramHeader(CSerial &serial)
// // // {
// // //   if (scIdle != GetState())
// // //   {
// // //     Error.SetCode(ecDispatcherTimingFailure);
// // //     return false;
// // //   }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters: -
// // //   // Response:
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   WriteProgramHeader(serial);
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt(); 
// // //   return true;
// // // }
// // // #endif
// // // //
// // // #if defined(COMMAND_SYSTEMENABLED)
// // // bool CDispatcher::ExecuteGetIRQVersion(CSerial &serial)
// // // {
// // //   if (scIdle != GetState())
// // //   {
// // //     Error.SetCode(ecDispatcherTimingFailure);
// // //     return false;
// // //   }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters: -
// // //   // Response:
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s %i", GetPDispatcher(), COUNT_SOFTWAREVERSION);  
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   WriteIRQVersion(serial);
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt();
// // //   return true;
// // // }
// // // #endif
// // // //
// // // #if defined(COMMAND_SYSTEMENABLED)
// // // bool CDispatcher::ExecuteGetHardwareVersion(CSerial &serial)
// // // {
// // //   if (scIdle != GetState())
// // //   {
// // //     Error.SetCode(ecDispatcherTimingFailure);
// // //     return false;
// // //   }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters: -
// // //   // Response
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s %i", GetPDispatcher(), COUNT_HARDWAREVERSION);
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   WriteHardwareVersion(serial);
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt();
// // //   return true;
// // // }
// // // #endif
// // // //
// // // #if defined(WATCHDOG_ISPLUGGED)
// // // bool CDispatcher::ExecutePulseWatchDog(CSerial &serial)
// // // {
// // //   if (scIdle != GetState())
// // //   {
// // //     Error.SetCode(ecDispatcherTimingFailure);
// // //     return false;
// // //   }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters ( PWD - ):
// // //   // Execute:
// // //   WatchDog.ForceTrigger();
// // //   // Response
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt();
// // //   return true;
// // // }
// // // #endif
// // // //
// // // bool CDispatcher::ExecuteResetSystem(CSerial &serial)
// // // {
// // // //  if (scIdle != GetState())
// // // //  {
// // // //    Error.SetCode(ecDispatcherTimingFailure);
// // // //    return false;
// // // //  }
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scBusy);
// // //   // Analyse parameters ( RSS - ):
// // //   // Execute:
// // //   // Response
// // //   SerialDispatcher.WritePrompt();
// // //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// // //   WriteResponse(GetTxdBuffer());
// // //   SerialDispatcher.WritePrompt();
// // //   SetState(scIdle);
// // //   SerialDispatcher.WritePrompt();
// // //   delay(2000);
// // //   //
// // //   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// // //   // Reset System !!!
// // //   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// // // #if (defined(PROCESSOR_NANOR3)||defined(PROCESSOR_UNOR3)||defined(PROCESSOR_MEGA2560))
// // //   PResetSystem();
// // // #elif defined(PROCESSOR_DUEM3)
// // // #elif defined(PROCESSOR_STM32F103C8)
// // // #elif (defined(PROCESSOR_TEENSY32)||defined(PROCESSOR_TEENSY36))
// // //   // problem because of reprogramming !!! _reboot_Teensyduino_();
// // //   // undefined _restart_Teensyduino_();
// // //   // undefined init_pins();
// // //   // the only possibility for this time:
// // //   // OK (no reset!) _init_Teensyduino_internal_();
// // //   // TOP:!!!
// // // #define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
// // // #define CPU_RESTART_VAL 0x5FA0004
// // // #define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
// // //   CPU_RESTART
// // //   //
// // // #endif
// // //   return true;
// // // }
// // //
// // #if defined(COMMAND_SYSTEMENABLED)
// // bool CDispatcher::ExecuteAbortProcessExecution(CSerial &serial)
// // {
// // //  if (scIdle != GetState())
// // //  {
// // //    Error.SetCode(ecDispatcherTimingFailure);
// // //    return false;
// // //  }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( A - ):
// //   // Execute:
// //   AbortProcessExecution();
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // bool CDispatcher::ExecuteWaitTimeRelative(void)
// // { 
// //   // if (scIdle != GetState())
// //   // {
// //   //   Error.SetCode(ecDispatcherTimingFailure);
// //   //   return false;
// //   // }
// //   // if (GetParameterCount() < 1)
// //   // {
// //   //   Error.SetCode(ecNotEnoughParameters);
// //   //   return false;
// //   // }
// //   // SerialDispatcher.WritePrompt();
// //   // SetState(scBusy);
// //   // // Analyse parameters: { WTR <time> }
// //   // UInt32 TR = atol(GetPParameters(0));
// //   // // Execute:
// //   // TimeRelativeSystem.Wait_Start(TR);
// //   // // Response
// //   // SerialDispatcher.WritePrompt();
// //   // sprintf(GetTxdBuffer(), "%s %lu", GetPDispatcher(), (long int)TR);
// //   // WriteResponse(GetTxdBuffer());
// //   // SerialDispatcher.WritePrompt();
// //   // SetState(scIdle);
// //   // SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // //
// // #if defined(NTPCLIENT_ISPLUGGED)
// // bool CDispatcher::ExecuteWaitTimeAbsolute(CSerial &serial)
// // { 
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   if (GetParameterCount() < 3)
// //   {
// //     Error.SetCode(ecNotEnoughParameters);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( WTA hh mm ss ):
// //   Byte HH = atoi(GetPParameters(0));
// //   Byte MM = atoi(GetPParameters(1));
// //   Byte SS = atoi(GetPParameters(2));
// //   // Execute:
// //   TimeAbsoluteSystem.Wait_Start(HH, MM, SS);
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s %u %u %u", GetPDispatcher(), HH, MM, SS);
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // //
// // //#########################################################
// // //  Segment - Execution - NTPClient
// // //#########################################################
// // //
// // #if defined(NTPCLIENT_ISPLUGGED)
// // bool CDispatcher::ExecuteGetNTPClientTime(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( GNT - ):
// //   // Execute:
// //   String HH = "??";
// //   String MM = "??";
// //   String SS = "??";
// //   NTPClient.GetTime(HH, MM, SS);
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s %s %s %s", GetPDispatcher(), HH, MM, SS);
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // #if defined(NTPCLIENT_ISPLUGGED)
// // bool CDispatcher::ExecuteGetNTPClientDate(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( GND - ):
// //   // Execute:
// //   String YY = "??";
// //   String MM = "??";
// //   String DD = "??";
// //   NTPClient.GetDate(YY, MM, DD);
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s %s %s %s", GetPDispatcher(), YY, MM, DD);
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // //#########################################################
// // //  Segment - Execution - LedSystem
// // //#########################################################
// // //
// // #if defined(COMMAND_SYSTEMENABLED)
// // bool CDispatcher::ExecuteGetLedSystem(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters: -
// //   // Execute:
// //   int SLS = LedSystem.GetState();
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s %i", GetPDispatcher(), SLS);
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt(); 
// //   return true;
// // }
// // #endif
// // //
// // #if defined(COMMAND_SYSTEMENABLED)
// // bool CDispatcher::ExecuteLedSystemOn(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters: -
// //   // Execute:
// //   LedSystem.SetOn();
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt(); 
// //   return true;
// // }
// // #endif
// // //
// // #if defined(COMMAND_SYSTEMENABLED)
// // bool CDispatcher::ExecuteLedSystemOff(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters: -
// //   // Execute:
// //   LedSystem.SetOff();
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt(); 
// //   return true;
// // }
// // #endif
// // //
// // #if defined(COMMAND_SYSTEMENABLED)
// // bool CDispatcher::ExecuteBlinkLedSystem(CSerial &serial)
// // { // <BLS> <c> <p> <w>
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   if (GetParameterCount() < 3)
// //   {
// //     Error.SetCode(ecNotEnoughParameters);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters: 
// //   UInt32 PC = atol(GetPParameters(0));  // [1]
// //   UInt32 PP = atol(GetPParameters(1));  // [ms]
// //   UInt32 PW = atol(GetPParameters(2));  // [ms]
// //   // Execute:
// //   LedSystem.Blink_Start(PC, PP, PW);
// //   // Response
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s %u %u %u", GetPDispatcher(), PC, PP, PW);
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // //
// // //#########################################################
// // //  Segment - Execution - SDDispatcher
// // //#########################################################
// // //
// // #if defined(SDCARD_ISPLUGGED)
// // bool CDispatcher::ExecuteOpenDispatcherFile(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   if (GetParameterCount() < 1)
// //   {
// //     Error.SetCode(ecNotEnoughParameters);
// //     return false;
// //   }
// //   // Analyse parameters ( OCF <f> ):
// //   String FN = GetPParameters(0);
// //  // ???? Automation.SetDispatcherFileName(FN);
// //   // Execution: -
// //   // Response:
// //   sprintf(GetTxdBuffer(), "%s %s", GetPDispatcher(), GetPParameters(0));
// //   WriteResponse(GetTxdBuffer());
// //   return true;
// // }
// // #endif

// // #if defined(SDCARD_ISPLUGGED)
// // bool CDispatcher::ExecuteWriteDispatcherFile(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   if (GetParameterCount() < 2)
// //   {
// //     Error.SetCode(ecNotEnoughParameters);
// //     return false;
// //   }
// //   // Analyse parameters ( WCF <c> <p> ):
// //   // Execution: -
// //   // Response:
// //   serial.Write(TERMINAL_RESPONSE);
// //   serial.Write(' ');
// //   serial.Write(GetPDispatcher());
// //   serial.Write(' ');
// //   int PC = GetParameterCount();
// //   for (int II = 0; II < PC; II++)
// //   {
// //     serial.Write(GetPParameters(II));
// //     serial.Write(' ');
// //   }
// //   serial.WriteLine("");
// //   serial.WritePrompt();
// //   return true;
// // }
// // #endif

// // #if defined(SDCARD_ISPLUGGED)
// // bool CDispatcher::ExecuteCloseDispatcherFile(CSerial &serial)
// // {
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   // Analyse parameters ( CCF - ):
// //   // Execution: -
// //   // Response:
// //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   return true;
// // }
// // #endif

// // #if defined(SDCARD_ISPLUGGED)
// // bool CDispatcher::ExecuteExecuteDispatcherFile(CSerial &serial)
// // { // ecf /sdc.cmd
// //   if (scIdle != GetState())
// //   {
// //     Error.SetCode(ecDispatcherTimingFailure);
// //     return false;
// //   }
// //   if (GetParameterCount() < 1)
// //   {
// //     Error.SetCode(ecNotEnoughParameters);
// //     return false;
// //   }
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( ECF <f> ):
// //   String CF = GetPParameters(0);
// //   // Execution: 
// //   // !!! !!! !!! !!! !!! !!!
// //   DispatcherFile.Open(&SD);
// //   // !!! !!! !!! !!! !!! !!!
// //   if (!DispatcherFile.ParseFile(CF.c_str()))
// //   {
// //     Error.SetCode(ecFailParseDispatcherFile);
// //     return false;
// //   }
// //   // Response:
// //   sprintf(GetTxdBuffer(), "%s %s", GetPDispatcher(), CF.c_str());
// //   SerialDispatcher.WritePrompt();
// //   WriteResponse(GetTxdBuffer());
// //   // Response:
// //   SerialDispatcher.Write("# DispatcherFile[");
// //   SerialDispatcher.Write(CF);
// //   SerialDispatcher.WriteLine("]:");
// //   bool DispatcherLoop = true;
// //   while (DispatcherLoop)
// //   {
// //     String Dispatcher = DispatcherFile.GetDispatcher();
// //     DispatcherLoop = (0 < Dispatcher.length());
// //     if (DispatcherLoop)
// //     {
// //       SerialDispatcher.Write("# Dispatcher[");
// //       SerialDispatcher.Write(Dispatcher.c_str());
// //       SerialDispatcher.WriteLine("]");
// //     }
// //   }
// //   // Preparation for executing Dispatchers from DispatcherFile:
// //   DispatcherFile.Open(&SD);
// //   if (!DispatcherFile.ParseFile(CF.c_str()))
// //   {
// //     Error.SetCode(ecFailParseDispatcherFile);
// //     return false;
// //   }
// //   // !!! !!! !!! !!! !!! !!!
// //   DispatcherFile.ResetExecution();
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();  
// //   // only after Dispatcher-Completition: DispatcherFile.Close();
// //   return true;
// // }
// // #endif
// // /*
// // ecf /sdc.cmd 
// //  */
// // //
// // #if defined(SDCARD_ISPLUGGED)
// // bool CDispatcher::ExecuteAbortDispatcherFile(CSerial &serial)
// // {
// //   SerialDispatcher.WritePrompt();
// //   SetState(scBusy);
// //   // Analyse parameters ( ACF - ):
// //   // Execution:
// //   AbortAll();
// //   // Response:
// //   SerialDispatcher.WritePrompt();
// //   sprintf(GetTxdBuffer(), "%s 1", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //
// // //#########################################################
// // //  Segment - Execution - Serial - Dispatcher
// // //#########################################################
// // // bool CDispatcher::ExecuteWriteLineSerialDispatcher(void)
// // // { // must be corrected!!!
// // //   if (GetParameterCount() < 1)
// // //   {
// // //     Error.SetCode(ecNotEnoughParameters);
// // //     return false;
// // //   }  
// // //   // Analyse parameters ( WLC <l> ):
// // //   String Line = GetPParameters(0);
// // //   // Execute:
// // //   SerialDispatcher.WriteLine(Line);
// // //   // Response:
// // //   sprintf(Dispatcher.GetTxdBuffer(), "%s %s", GetPDispatcher(), Line.c_str());
// // //   WriteResponse(Dispatcher.GetTxdBuffer());
// // //   return true;
// // // }

// // // bool CDispatcher::ExecuteReadLineSerialDispatcher(void)
// // // { // must be corrected!!!
// // //   // Analyse parameters ( RLC - ):
// // //   // Execute:
// // //   String Line = SerialDispatcher.ReadLine();
// // //   // Response:
// // //   sprintf(Dispatcher.GetTxdBuffer(), "%s %s", GetPDispatcher(), Line.c_str());
// // //   WriteResponse(Dispatcher.GetTxdBuffer());
// // //   return true;
// // // }
// // //
// // //#########################################################
// // //  Segment - Execution - I2CDisplay
// // //#########################################################
// // //
// // #if defined(I2CDISPLAY_ISPLUGGED)
// // bool CDispatcher::ExecuteClearScreenI2CDisplay(CSerial &serial)
// // {
// //   // Analyse parameters ( CLI - ):
// //   // Execute:
// //   I2CDisplay.ClearDisplay();
// //   // Response:
// //   sprintf(GetTxdBuffer(), "%s", GetPDispatcher());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif

// // #if defined(I2CDISPLAY_ISPLUGGED)
// // bool CDispatcher::ExecuteShowTextI2CDisplay(CSerial &serial)
// // {
// //   //!!!!if (GetParameterCount() < 3)
// //   // Analyse parameters ( STI <c> <r> <t> ):
// //   Byte R = atol(GetPParameters(0));
// //   Byte C = atol(GetPParameters(1));
// //   String T = GetPParameters(2);
// //   // Execute:
// //   I2CDisplay.SetCursorPosition(R, C);
// //   I2CDisplay.WriteText(T);
// //   // Response:
// //   sprintf(GetTxdBuffer(), "%s %i %i %s", GetPDispatcher(), R, C, T.c_str());
// //   WriteResponse(GetTxdBuffer());
// //   SerialDispatcher.WritePrompt();
// //   SetState(scIdle);
// //   SerialDispatcher.WritePrompt();
// //   return true;
// // }
// // #endif
// // //

// #if defined(COMMAND_SYSTEM)
//   if (!strcmp(SHORT_GPH, GetPDispatcher()))
//   {
//     return ExecuteGetProgramHeader(serial);
//   } else 
//   if (!strcmp(SHORT_GSV, GetPDispatcher()))
//   {
//     return ExecuteGetIRQVersion(serial);
//   } else 
//   if (!strcmp(SHORT_GHV, GetPDispatcher()))
//   {
//     return ExecuteGetHardwareVersion(serial);
//   } else 
// #endif
// // // #if defined(WATCHDOG_ISPLUGGED)
// // //   if (!strcmp(SHORT_PWD, GetPDispatcher()))
// // //   {
// // //     return ExecutePulseWatchDog(serial);
// // //   } else
// // // #endif
// // // #if defined(COMMAND_SYSTEMENABLED)
// // //   if (!strcmp(SHORT_RSS, GetPDispatcher()))
// // //   {
// // //     return ExecuteResetSystem(serial);
// // //   } else
// // //   if (!strcmp(SHORT_APE, GetPDispatcher()))
// // //   {
// // //     return ExecuteAbortProcessExecution(serial);
// // //   } else
// // // #endif
// // //   if (!strcmp(SHORT_WTR, GetPDispatcher()))
// // //   {
// // //     return ExecuteWaitTimeRelative(serial);
// // //   } else   
// // // #if defined(NTPCLIENT_ISPLUGGED)
// // //   if (!strcmp(SHORT_WTA, GetPDispatcher()))
// // //   {
// // //     return ExecuteWaitTimeAbsolute(serial);
// // //   } else   
// // // #endif
// // // #if defined(TRIGGERINPUT_ISPLUGGED)
// // //   if (!strcmp(SHORT_WTI, GetPDispatcher()))
// // //   {
// // //     return ExecuteWaitTriggerInput(serial);
// // //   } else
// // // #endif  
// // //   //
// // // #if defined(NTPCLIENT_ISPLUGGED)
// // //   // ----------------------------------
// // //   // NTPClient
// // //   // ----------------------------------
// // //   if (!strcmp(SHORT_GNT, GetPDispatcher()))
// // //   {
// // //     return ExecuteGetNTPClientTime(serial);
// // //   } else
// // //   if (!strcmp(SHORT_GND, GetPDispatcher()))
// // //   {
// // //     return ExecuteGetNTPClientDate(serial);
// // //   } else
// // // #endif 
// // // //
// // // #if defined(SDCARD_ISPLUGGED)
// // //   // ----------------------------------
// // //   // SDCard
// // //   // ----------------------------------
// // //   if (!strcmp(SHORT_OCF, GetPDispatcher()))
// // //   {
// // //     return ExecuteOpenDispatcherFile(serial);
// // //   } else 
// // //   if (!strcmp(SHORT_WCF, GetPDispatcher()))
// // //   {
// // //     return ExecuteWriteDispatcherFile(serial);
// // //   } else 
// // //   if (!strcmp(SHORT_CCF, GetPDispatcher()))
// // //   {
// // //     return ExecuteCloseDispatcherFile(serial);
// // //   } else 
// // //   if (!strcmp(SHORT_ECF, GetPDispatcher()))
// // //   {
// // //     return ExecuteExecuteDispatcherFile(serial);
// // //   } else 
// // //   if (!strcmp(SHORT_ACF, GetPDispatcher()))
// // //   {
// // //     return ExecuteAbortDispatcherFile(serial);
// // //   } else
// // // #endif
  
// // //   // ----------------------------------
// // //   // I2CDisplay
// // //   // ----------------------------------
// // // #if defined(I2CDISPLAY_ISPLUGGED)
// // //   if (!strcmp(SHORT_CLI, GetPDispatcher()))
// // //   {
// // //     return ExecuteClearScreenI2CDisplay(serial);
// // //   } else if (!strcmp(SHORT_STI, GetPDispatcher()))
// // //   {
// // //     return ExecuteShowTextI2CDisplay(serial);
// // //   } else
// // // #endif

// // //   //
// // // #if defined(COMMAND_SYSTEMENABLED)
// // //   // ----------------------------------
// // //   // LedSystem
// // //   // ----------------------------------
// // //   if (!strcmp(SHORT_GLS, GetPDispatcher()))
// // //   {
// // //     return ExecuteGetLedSystem(serial);
// // //   } else if (!strcmp(SHORT_LSH, GetPDispatcher()))
// // //   {
// // //     return ExecuteLedSystemOn(serial);
// // //   } else if (!strcmp(SHORT_LSL, GetPDispatcher()))
// // //   {
// // //     return ExecuteLedSystemOff(serial);
// // //   } else if (!strcmp(SHORT_BLS, GetPDispatcher()))
// // //   {
// // //     return ExecuteBlinkLedSystem(serial);
// // //   } else
// // // #endif  


// //   // ----------------------------------
// //   // Error-Handler
// //   // ----------------------------------
// //   {
// //     Error.SetCode(ecInvalidDispatcher);
// //   }
// //   return false;
// // }
