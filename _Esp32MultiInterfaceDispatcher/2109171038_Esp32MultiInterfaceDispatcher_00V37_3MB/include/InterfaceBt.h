#include "DefinitionSystem.h"
//
#ifdef INTERFACE_BT
//
#ifndef InterfaceBt_h
#define InterfaceBt_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <BluetoothSerial.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const Boolean BT_INIT_RXECHO = false;
//
//----------------------------------------------------
//  Type
//----------------------------------------------------
class CInterfaceBtBase : public CInterfaceBase
{
  public:
  CInterfaceBtBase(const char* deviceid);
  //
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  virtual UInt8 GetRXCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};

class CInterfaceBtUart : public CInterfaceBtBase
{
  protected:
  BluetoothSerial* FPSerial;
  
  public:
  CInterfaceBtUart(const char* deviceid, BluetoothSerial* pserial);
  // Handler
  virtual Boolean Open(int parameter);
  virtual Boolean Close(void);
  // Read
  virtual UInt8 GetRXCount(void);
  virtual Boolean Read(char &rxcharacter);
  virtual Boolean ReadLine(char* prxline, int &rxsize);
  // Write
  virtual Boolean Write(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
};

class CInterfaceBt
{
  private:
  CInterfaceBtBase* FPBtBase; 
  //
  public:
  CInterfaceBt(const char* devicename, BluetoothSerial* pserial);
  //  
  // // Handler
  Boolean Open(int baudrate);
  Boolean Close(void);
  // Property
  char* GetRXBlock(void)
  {
    return FPBtBase->GetRXBlock();
  }
  char* GetTXBlock(void)
  {
    return FPBtBase->GetTXBlock();
  }
  void SetRXEcho(Boolean rxecho)
  {
    FPBtBase->SetRXEcho(rxecho);
  }
  Boolean GetRXEcho(void)
  {
    return FPBtBase->GetRXEcho();
  }
  //
  UInt8 GetRXCount(void);
  //Boolean LineDetected(void);
  Boolean Read(char &character);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  // Boolean WritePrompt(void);
  // Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  // ??? Boolean Execute(void);
};
//
#endif // InterfaceBttt_h
//
#endif // INTERFACE_BT
//
//







