//
#include "Defines.h"
//
#if defined(DISPATCHER_SYSTEM)
//
#ifndef DispatcherSystem_h
#define DispatcherSystem_h
//
#include "Dispatcher.h"
#include "TimeRelative.h"
//
//----------------------------------------------------------------
// Dispatcher - System - SHORT
//----------------------------------------------------------------
#define SHORT_APE   "APE"                   // APE - Abort Process Execution
#define SHORT_RSS   "RSS"                   // RSS - Reset System
#define SHORT_WTR   "WTR"                   // WTR - Wait Time Relative
//
//----------------------------------------------------------------
// Dispatcher - System - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_SYSTEM       " Help (System):"
#define MASK_APE                  " %-3s                  : <a>bort <p>rocess <e>xecution"
#define MASK_RSS                  " %-3s                  : <r>e<s>et <s>ystem"
#define MASK_WTR                  " %-3s <t>              : Wait Time Relative <t>ime{ms}"
//
//----------------------------------------------------------------
// Dispatcher - System
//----------------------------------------------------------------
class CDispatcherSystem : public CDispatcher
{
  protected:
  //bool WriteProgramHeader(void);
  //
  public:
  CDispatcherSystem(void);
  //
  bool ExecuteAbortProcessExecution(int parametercount, char** pparameters);
  bool ExecuteResetSystem(int parametercount, char** pparameters);
  bool ExecuteWaitTimeRelative(int parametercount, char** pparameters);
  //
  bool virtual HandleInterface(char* pcommand, int parametercount, char** pparameters);
};
//
#endif // DispatcherSystem_h
//
#endif // DISPATCHER_SYSTEM
//
