//
#include "Defines.h"
//
#if defined(DISPATCHER_COMMON)
//
#ifndef DispatcherCommon_h
#define DispatcherCommon_h
//
#include "Command.h"
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Common - SHORT
//----------------------------------------------------------------
#define SHORT_GPH   "GPH"                   // GPH - GetProgramHeader
#define SHORT_GHV   "GHV"                   // GHV - GetHardwareVersion
#define SHORT_GSV   "GSV"                   // GSV - GetSoftwareVersion
#define SHORT_H     "H"                     // H - (this) Help
//
//----------------------------------------------------------------
// Dispatcher - Common - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_COMMON       " Help (Common):"
#define MASK_H                    " %-3s                  : This Help"
#define MASK_GPH                  " %-3s                  : Get Program Header"
#define MASK_GSV                  " %-3s                  : Get Software Version"
#define MASK_GHV                  " %-3s                  : Get Hardware Version"
//
#define TITLE_LINE                "--------------------------------------------------"
#define MASK_PROJECT              "- Project:   %-35s -"
#define MASK_SOFTWARE             "- Version:   %-35s -"
#define MASK_HARDWARE             "- Hardware:  %-35s -"
#define MASK_DATE                 "- Date:      %-35s -"
#define MASK_TIME                 "- Time:      %-35s -"
#define MASK_AUTHOR               "- Author:    %-35s -"
#define MASK_PORT                 "- Port:      %-35s -"
#define MASK_PARAMETER            "- Parameter: %-35s -"
//
const int COUNT_SOFTWAREVERSION   = 1;
const int COUNT_HARDWAREVERSION   = 1;
//
#define MASK_SOFTWAREVERSION      "Software-Version %s"
#define MASK_HARDWAREVERSION      "Hardware-Version %s"


typedef void (*CBOnWriteHelp)(void);
//----------------------------------------------------------------
// Dispatcher - Common
//----------------------------------------------------------------
class CDispatcherCommon : public CDispatcher
{
  private:
  CBOnWriteHelp FOnWriteHelp;
  public:
  bool WriteProgramHeader(void);
  protected:
  void WriteHardwareVersion(void);
  void WriteSoftwareVersion(void);
  public:
  bool WriteHelp(void);
  //
  public:
  CDispatcherCommon(void);
  //
  void SetOnWriteHelp(CBOnWriteHelp onwritehelp)
  {
    FOnWriteHelp = onwritehelp;
  };
  //
  bool ExecuteGetHelp(int parametercount, char** pparameters);
  bool ExecuteGetProgramHeader(int parametercount, char** pparameters);
  bool ExecuteGetHardwareVersion(int parametercount, char** pparameters);
  bool ExecuteGetSoftwareVersion(int parametercount, char** pparameters);
  //
  bool virtual HandleInterface(char* pcommand, int parametercount, char** pparameters);
};
//
#endif // DispatcherCommon_h
//
#endif // DISPATCHER_COMMON
//