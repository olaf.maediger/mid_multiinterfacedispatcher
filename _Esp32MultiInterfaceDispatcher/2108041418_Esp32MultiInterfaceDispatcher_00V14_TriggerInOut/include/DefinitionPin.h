//
//-------------------------------------------
//  PinDefinitions for all Controller types
//-------------------------------------------
//
#include "DefinitionSystem.h"
//
#ifndef PinDefinition_h
#define PinDefinition_h
//
//##########################################
//  PROCESSOR_NANOR3
//##########################################
#if defined(PROCESSOR_NANOR3)
//
// System Led 
const int PIN_LEDSYSTEM               = 13; // used from SPI
const bool LEDSYSTEM_INVERTED         = false;
//
// SPI
const int PIN_SPI_SS                  = 10;
const int PIN_SPI_SCK                 = 13; // used from LedSystem
const int PIN_SPI_MISO                = 12;
const int PIN_SPI_MOSI                = 11;
//
// I2C
const int PIN_I2C_SCL                 = 5;
const int PIN_I2C_SDA                 = 4;
//
#endif // PROCESSOR_NANOR3
//
//##########################################
//  PROCESSOR_UNOR3
//##########################################
#if defined(PROCESSOR_UNOR3)
//
// System Led 
const int PIN_LEDSYSTEM               = 13; // used from SPI
const bool LEDSYSTEM_INVERTED         = false;
//
// SPI
const int PIN_SPI_SS                  = 10;
const int PIN_SPI_SCK                 = 13; // used from LedSystem
const int PIN_SPI_MISO                = 12;
const int PIN_SPI_MOSI                = 11;
//
// I2C
const int PIN_I2C_SCL                 = 5;
const int PIN_I2C_SDA                 = 4;
//
#endif // PROCESSOR_UNOR3
//
//##########################################
//  PROCESSOR_MEGA2560
//##########################################
#if defined(PROCESSOR_MEGA2560)
//
// System Led 
const int PIN_LEDSYSTEM               = 13;
const bool LEDSYSTEM_INVERTED         = false;
//
#endif // PROCESSOR_MEGA2560
////
////##########################################
////  PROCESSOR_DUEM3
////##########################################
#if defined(PROCESSOR_DUEM3)
//
// SPI
#define PIN_SPI_CS_ARD               4
#define PIN_SPI_SCK                     76
#define PIN_SPI_MISO                    74
#define PIN_SPI_MOSI                    75
//
// I2C
const int PIN_I2C_SCL                 = 21;
const int PIN_I2C_SDA                 = 20;
//
// Led System
const int PIN_LEDSYSTEM               = 13;
const bool LEDSYSTEM_INVERTED         = false;
// Led Laser
const int PIN_LEDLASER                = 50;
const bool LEDLASER_INVERTED          = false;
//
// System LedLine
//const int PIN_LEDERROR              = 54;
////                                    55
//const int PIN_LEDBUSY               = 56;
////                                    57
//const int PIN_LEDMOTIONX            = 58;
//const int PIN_LEDMOTIONY            = 59;
//                                      60
//                                  
// Dac
const int PIN_DAC_CHANNEL0            = 66;
const int PIN_DAC_CHANNEL1            = 67;
// Definition for ADCInternal:
//#define DAC_INTERNAL_X
//#define DAC_INTERNAL_Y
// Definition for ADCExternalMCP4725:
#define DAC_MCP4725_X
#define DAC_MCP4725_Y

// Dac
const int PIN_DAC_CHANNEL0            = 66;
const int PIN_DAC_CHANNEL1            = 67;
//
// Trigger
const int PIN_TRIGGER_IN              = 53;
const int PIN_TRIGGER_OUT             = 51;
//
#endif // PROCESSOR_DUEM3
//
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//----------------------------------------------------------------------------------
// STM32F103C8, Pins 2..16                  Pin       // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_PC13_LEDSYSTEM                = PC13;   // 
const int PIN_PC14_OSC32OUT                 = PC14;   // 
const int PIN_PC15_OSC32IN                  = PC15;   // 
const int PIN_PA0_ADC0_UART2CTS             = PA0;    // 
const int PIN_PA1_ADC1_UART2RTS_PWM22       = PA1;    // 
const int PIN_PA2_ADC2_UART2TX_PWM23        = PA2;    // 
const int PIN_PA3_ADC3_UART2RX_PWM24        = PA3;    // 
const int PIN_PA4_ADC4_SPI1NSS_CK2          = PA4;    // 
const int PIN_PA5_ADC5_SPI1SCK              = PA5;    // 
const int PIN_PA6_ADC6_SPI1MISO_PWM31       = PA6;    // 
const int PIN_PA7_ADC7_SPI1MOSI_PWM32       = PA7;    // 
const int PIN_PB0_ADC8_PWM33                = PB0;    // 
const int PIN_PB1_ADC9_PWM34                = PB1;    // 
const int PIN_PB10_UART3TX_I2C2SCL_PWM23    = PB10;   // 
const int PIN_PB11_UART3RX_I2C2SDA_PWM24    = PB11;   // 
//----------------------------------------------------------------------------------
// STM32F103C8, Pins 21..37                 Pin       // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_PB12_SPI2NSS                  = PB12;   // 
const int PIN_PB13_SPI2SCK_PWM11N           = PB13;   // 
const int PIN_PB14_SPI2MISO_PWM12N          = PB14;   // 
const int PIN_PB15_SPI2MOSI_PWM13N          = PB15;   // 
const int PIN_PA8_D7_PWM11                  = PA8;    // 
const int PIN_PA9_D8_UART1TX_PWM12          = PA9;    // 
const int PIN_PA10_D2_UART1RX_PWM13         = PA10;   // 
const int PIN_PA11_USBDM_CAN1RD_PWM14       = PA11;   // 
const int PIN_PA12_USBDP_CAN1TD             = PA12;   // 
const int PIN_PA15_ASPI1NSS_PWM21           = PA15;   // 
const int PIN_PB3_D3_ASPI1SCK_PWM22         = PB3;    // 
const int PIN_PB4_D5_ASPI1MISO_PWM31        = PB4;    // 
const int PIN_PB5_D4_ASPI1MOSI_PWM32        = PB5;    // 
const int PIN_PB6_D10_AUART1TX_I2C1SCL      = PB6;    // 
const int PIN_PB7_AUART1RX_I2C1SDA          = PB7;    // 
const int PIN_PB8_D15_CAN1RD_AI2CSCL        = PB8;    // 
const int PIN_PB9_D14_CAN1TD_AI2C1SDA       = PB9;    // 
//----------------------------------------------------------------------------------
//
// ### Serial - RS232 ###
// Serial - RS232_1 - RS232Command
const int PIN_UART1_RXD                     = PIN_PA10_D2_UART1RX_PWM13;    
const int PIN_UART1_TXD                     = PIN_PA9_D8_UART1TX_PWM12;    
// Serial - RS232_2 - RS232Printer
const int PIN_UART2_RXD                     = PIN_PA3_ADC3_UART2RX_PWM24;
const int PIN_UART2_TXD                     = PIN_PA2_ADC2_UART2TX_PWM23;
// Serial - RS232_3 - RS232Debug
const int PIN_UART3_RXD                     = PIN_PB11_UART3RX_I2C2SDA_PWM24;
const int PIN_UART3_TXD                     = PIN_PB10_UART3TX_I2C2SCL_PWM23;
//
// ### Serial - SPI ###
// Serial - SPI - SPI_1
const int PIN_SPI1_NSS                      = PIN_PA4_ADC4_SPI1NSS_CK2; 
const int PIN_SPI1_SCK                      = PIN_PA5_ADC5_SPI1SCK; 
const int PIN_SPI1_MISO                     = PIN_PA6_ADC6_SPI1MISO_PWM31; 
const int PIN_SPI1_MOSI                     = PIN_PA7_ADC7_SPI1MOSI_PWM32; 
// Serial - SPI - SPI_2
const int PIN_SPI2_NSS                      = PIN_PB12_SPI2NSS;
const int PIN_SPI2_SCK                      = PIN_PB13_SPI2SCK_PWM11N; 
const int PIN_SPI2_MISO                     = PIN_PB14_SPI2MISO_PWM12N; 
const int PIN_SPI2_MOSI                     = PIN_PB15_SPI2MOSI_PWM13N;
//
// ### Serial - I2C ###
// Serial - I2C - I2C1
const int PIN_I2C1_SCL                      = PIN_PB6_D10_AUART1TX_I2C1SCL;
const int PIN_I2C1_SDA                      = PIN_PB7_AUART1RX_I2C1SDA;
// Serial - I2C - I2C2
const int PIN_I2C2_SCL                      = PIN_PB10_UART3TX_I2C2SCL_PWM23;
const int PIN_I2C2_SDA                      = PIN_PB11_UART3RX_I2C2SDA_PWM24;
//
// ### Analog - ADC ###
// Analog - ADC - ADConverter
const int PIN_ADC_ANALOGIN0                 = PIN_PA0_ADC0_UART2CTS;
const int PIN_ADC_ANALOGIN1                 = PIN_PA1_ADC1_UART2RTS_PWM22;
const int PIN_ADC_ANALOGIN2                 = PIN_PA2_ADC2_UART2TX_PWM23;
const int PIN_ADC_ANALOGIN3                 = PIN_PA3_ADC3_UART2RX_PWM24;
const int PIN_ADC_ANALOGIN4                 = PIN_PA4_ADC4_SPI1NSS_CK2;
const int PIN_ADC_ANALOGIN5                 = PIN_PA5_ADC5_SPI1SCK;
const int PIN_ADC_ANALOGIN6                 = PIN_PA6_ADC6_SPI1MISO_PWM31;
const int PIN_ADC_ANALOGIN7                 = PIN_PA7_ADC7_SPI1MOSI_PWM32;
const int PIN_ADC_ANALOGIN8                 = PIN_PB0_ADC8_PWM33;
const int PIN_ADC_ANALOGIN9                 = PIN_PB1_ADC9_PWM34;
//
// ### ard - SPI ###
const int PIN_SPI_CS_ARD                 = PIN_SPI1_NSS;
const int PIN_SPI_SCK_ARD                = PIN_SPI1_SCK;
const int PIN_SPI_MISO_ARD               = PIN_SPI1_MISO;
const int PIN_SPI_MOSI_ARD               = PIN_SPI1_MOSI;
//
// ### DIO ###
// 
// Single Led System (fixed)
const int PIN_LEDSYSTEM                     = PIN_PC13_LEDSYSTEM;  // -> Collision LedSystem & SpiClock
const bool LEDSYSTEM_INVERTED               = true;
//
// ### Peripheral ###
//------------------------------------------------------------------------------
// I2CDisplay 
//------------------------------------------------------------------------------
// I2C - I2CDisplay (IC5) - 0x20 + 0x02 : 0x22 - 0b0[010.0001]
#if defined(I2CDISPLAY_ISPLUGGED)
const Byte ADDRESS_I2C1_I2CDISPLAY          = 0x27;
const int PIN_I2CDISPLAY_SDA                = PIN_PB7_AUART1RX_I2C1SDA;
const int PIN_I2CDISPLAY_SCL                = PIN_PB6_D10_AUART1TX_I2C1SCL; 
#endif
//
#endif // PROCESSOR_STM32F103C8
//
//
//##########################################
//  PROCESSOR_TEENSY32
//##########################################
#if defined(PROCESSOR_TEENSY32)
//----------------------------------------------------------------------------------
// Teensy32, Pins 0..12               Pin     // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_0_UARTRXD1_TOUCH        = 0;    // P2  - 0/RX1/T      - UART1RXD
const int PIN_1_UARTTXD1_TOUCH        = 1;    // P3  - 1/TX1/T      - UART1TXD
const int PIN_2_NONE                  = 2;    // P4  - 2            - ADC16BUSY
const int PIN_3_CANTX_PWM             = 3;    // P5  - 3/CAN-TX-PWM - PS2DATAIN
const int PIN_4_CANRX_PWM             = 4;    // P6  - 4/CAN-RX-PWM - PS2CLOCKIN
const int PIN_5_UARTTX1_PWM           = 5;    // P7  - 5/PWM        - ADC16CLOCK
const int PIN_6_PWM                   = 6;    // P8  - 6/PWM        - ADC16DATA
const int PIN_7_UARTRX3_SPIDOUT       = 7;    // P9  - 7/RX3        - UART3RXD
const int PIN_8_UARTTX3_SPIDIN        = 8;    // P10 - 8/TX3        - UART3TXD
const int PIN_9_UARTRX2_SPICS_PWM     = 9;    // P11 - 9/RX2/PWM    - UART2RXD
const int PIN_10_UARTTX2_SPICS_PWM    = 10;   // P12 - 10/TX2/PWM   - UART2TXD
const int PIN_11_SPIMOSI              = 11;   // P13 - 11/MOSI      - SPIMOSI
const int PIN_12_SPIMISO              = 12;   // P14 - 12/MISO      - SPIMISO
//----------------------------------------------------------------------------------
// Teensy32, Pins 13..23              Pin     // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_13_LED_SPISCK           = 13;   // P15 - 13/SCK/LED   - SPISCK
const int PIN_14_A0_SPISCK            = 14;   // P16 - 14/A0        - PS2CLOCKOUT
const int PIN_15_A1_SPICS_TOUCH       = 15;   // P17 - 15/A1/T      - PS2DATAOUT
const int PIN_16_A2_I2CSCL0_TOUCH     = 16;   // P18 - 16/A2/T      - ADC16CSN
const int PIN_17_A3_I2CSDA0_TOUCH     = 17;   // P19 - 17/A3/T      - ADC16TAG
const int PIN_18_A4_I2CSDA0_TOUCH     = 18;   // P20 - 18/A4/T/SDA0 - I2CDATA
const int PIN_19_A5_I2CSCL0_TOUCH     = 19;   // P21 - 19/A5/T/SCL0 - I2CCLOCK
const int PIN_20_A6_SPICS_PWM         = 20;   // P22 - 20/A6/PWM    - SPISLAVEB
const int PIN_21_A7_UARTRX1_SPICS_PWM = 21;   // P23 - 21/A7/PWM    - SPISLAVEA
const int PIN_22_A8_PWM_TOUCH         = 22;   // P24 - 22/A8/T/PWM  - ADC16READ
const int PIN_23_A9_PWM_TOUCH         = 23;   // P25 - 23/A9/T/PWM  - WDI
//----------------------------------------------------------------------------------
//
// ### RS232 ###
// --- Serial - RS232 ---
// Serial - RS232_1 - RS232Command
const int PIN_UART1_RXD               = PIN_0_UARTRXD1_TOUCH;     // -> RXD010V - UART1RXD
const int PIN_UART1_TXD               = PIN_1_UARTTXD1_TOUCH;     // -> TXD010V - UART1TXD
// Serial - RS232_2 - RS232Printer
const int PIN_UART2_RXD               = PIN_9_UARTRX2_SPICS_PWM;  // -> RXD010V - UART2RXD
const int PIN_UART2_TXD               = PIN_10_UARTTX2_SPICS_PWM; // -> TXD010V - UART2TXD
// Serial - RS232_3 - RS232Debug
const int PIN_UART3_RXD               = PIN_7_UARTRX3_SPIDOUT;    // -> RXD010V - UART3RXD
const int PIN_UART3_TXD               = PIN_8_UARTTX3_SPIDIN;     // -> TXD010V - UART3TXD
//
// ### SPI ###
// SPI - Common
const int PIN_SPIMISO                 = PIN_12_SPIMISO;           // -> RTC/FRAM
const int PIN_SPIMOSI                 = PIN_11_SPIMOSI;           // -> RTC/FRAM
const int PIN_SPISCK                  = PIN_13_LED_SPISCK;        // -> RTC/FRAM
// LedSystem(Pin13)-Conflict with SPI-SCK!!!
//
// ### I2C ###
//
// I2C - Common
const int PIN_I2CCLOCK                = PIN_19_A5_I2CSCL0_TOUCH;  // Common
const int PIN_I2CDATA                 = PIN_18_A4_I2CSDA0_TOUCH;  // Common
//
//-------------------------------------------------------------------------
//
// ### DIO ###
// Single Led System (fixed)
const int PIN_LEDSYSTEM                       = PIN_13_LED_SPISCK;  // -> Collision LedSystem & SpiClock
const bool LEDSYSTEM_INVERTED                 = false;
//
// ### I2C - I2CDisplay ###          I2C Address Bit[654.3210]
// I2C - I2CDisplay debug - 0x27
// I2C - I2CDisplay (IC5) - 0x20 + 0x02 : 0x22 - 0b0[010.0001]
#if defined(I2CDISPLAY_ISPLUGGED)
const Byte ADDRESS_I2C1_I2CDISPLAY            = 0x27;
#endif
//
// ### SPI - ard ###
#if defined(ARD_ISPLUGGED)
const int PIN_SPI_CS_ARD           = PIN_9_UARTRX2_SPICS_PWM;                      // -> ard
#endif
//
// ### Analog - ADC - External ### 
const int PIN_ADC16DATA               = PIN_6_PWM;                // -> ADC16
const int PIN_ADC16BUSY               = PIN_2_NONE;               // -> ADC16
const int PIN_ADC16CLOCK              = PIN_5_UARTTX1_PWM;        // -> ADC16
const int PIN_ADC16READ               = PIN_22_A8_PWM_TOUCH;      // -> ADC16
const int PIN_ADC16TAG                = PIN_17_A3_I2CSDA0_TOUCH;  // -> ADC16
const int PIN_ADC16CSN                = PIN_16_A2_I2CSCL0_TOUCH;  // -> ADC16
//?????????????const int PIN_I2C_SCL_ADCMCP3424      = PIN_6_PWM;                // -> ADC16
//
// ### Analog - ADC - MCP3424 - I2C ### 
// Analog - ADC - MCP3424 -        I2C Address Bit[7654.321]X
// I2C - ADC - MCP3424 - see Header MCP342X !
const Byte I2CADDRESS_MCP3424         = 0x68;
// Analog - ADC - MCP3424 - Channel
const int PIN_ANALOGIN1               = 1;                        // to be adapted
const int PIN_ANALOGIN5               = 2;                        // to be adapted
const int PIN_ANALOGIN7               = 3;                        // to be adapted
const int PIN_ANALOGIN9               = 4;                        // to be adapted
//
// ### Watchdog(RTC) ### 
const int PIN_WDI                     = PIN_23_A9_PWM_TOUCH;      // -> Watchdog(RTC)
const int PIN_SPISLAVEA               = PIN_21_A7_UARTRX1_SPICS_PWM; // -> RTC
//
// ### SPI - FRAM ###
const int PIN_SPISLAVEB               = PIN_20_A6_SPICS_PWM;      // -> FRAM
//
// ### PS2 - Keyboard ###
const int PIN_PS2CLOCKOUT             = PIN_14_A0_SPISCK;         // -> PS2 Keyboard
const int PIN_PS2DATAOUT              = PIN_15_A1_SPICS_TOUCH;    // -> PS2 Keyboard
const int PIN_PS2CLOCKIN              = PIN_4_CANRX_PWM;          // -> PS2 Keyboard
const int PIN_PS2DATAIN               = PIN_3_CANTX_PWM;          // -> PS2 Keyboard
//
// ### PCF8574 - I2C - IC9, IC5, IC6, IC13, IC8 ###
// I2C - DIO - PCF8574            I2C Address Bit[654.3210]
// I2C - PCF8574(IC9)  - 0x20 + 0x01 : 0x21 - 0b0[010.0001]
const Byte ADDRESS_I2C1_PCF8574IC9  = 0x21;
// I2C - PCF8574(IC5)  - 0x20 + 0x02 : 0x22 - 0b0[010.0010]
const Byte ADDRESS_I2C1_PCF8574IC5  = 0x22;
// I2C - PCF8574(IC6)  - 0x20 + 0x03 : 0x23 - 0b0[010.0011]
const Byte ADDRESS_I2C1_PCF8574IC6  = 0x23;
// I2C - PCF8574(IC13) - 0x20 + 0x04 : 0x24 - 0b0[010.0100]
const Byte ADDRESS_I2C1_PCF8574IC13 = 0x24;
// I2C - PCF8574(IC8)  - 0x20 + 0x05 : 0x25 - 0b0[010.0101]
const Byte ADDRESS_I2C1_PCF8574IC8  = 0x25;
//
// I2C - I2C1 - PCF8574 - Indices
// I2C - I2C1 - PCF8574(IC9)  - 0x20 + 0x01 : 0x21 - 0b0[010.0001]
const Byte INIT_DATAIC9                       = 0x00;
const Byte INDEX_PCF8574IC9_0_KBMROW1         = 0x00;
const Byte INDEX_PCF8574IC9_1_KBMROW2         = 0x01;
const Byte INDEX_PCF8574IC9_2_KBMROW3         = 0x02;
const Byte INDEX_PCF8574IC9_3_KBMROW4         = 0x03;
const Byte INDEX_PCF8574IC9_4_LEDRED          = 0x04;
const Byte INDEX_PCF8574IC9_5_LCDREADWRITEN   = 0x05;
const Byte INDEX_PCF8574IC9_6_LCDREGSELECT    = 0x06;
const Byte INDEX_PCF8574IC9_7_LCDENABLE       = 0x07;
// I2C - I2C1 - PCF8574(IC5)  - 0x20 + 0x02 : 0x22 - 0b0[010.0010]
const Byte INIT_DATAIC5                       = 0x00;
const Byte INDEX_PCF8574IC5_0_LCDATA0         = 0x00;
const Byte INDEX_PCF8574IC5_1_LCDATA1         = 0x01;
const Byte INDEX_PCF8574IC5_2_LCDATA2         = 0x02;
const Byte INDEX_PCF8574IC5_3_LCDATA3         = 0x03;
const Byte INDEX_PCF8574IC5_4_LCDATA4         = 0x04;
const Byte INDEX_PCF8574IC5_5_LCDATA5         = 0x05;
const Byte INDEX_PCF8574IC5_6_LCDATA6         = 0x06;
const Byte INDEX_PCF8574IC5_7_LCDATA7         = 0x07;
// I2C - I2C1 - PCF8574(IC6)  - 0x20 + 0x03 : 0x23 - 0b0[010.0011]
const Byte INIT_DATAIC6                       = 0x00;
const Byte INDEX_PCF8574IC6_0_VALVE0          = 0x00;
const Byte INDEX_PCF8574IC6_1_VALVE1          = 0x01;
const Byte INDEX_PCF8574IC6_2_VALVE2          = 0x02;
const Byte INDEX_PCF8574IC6_3_VALVE3          = 0x03;
const Byte INDEX_PCF8574IC6_4_VALVE4          = 0x04;
const Byte INDEX_PCF8574IC6_5_VALVE5          = 0x05;
const Byte INDEX_PCF8574IC6_6_VALVE6          = 0x06;
const Byte INDEX_PCF8574IC6_7_VALVE7          = 0x07;
// I2C - I2C1 - PCF8574(IC13) - 0x20 + 0x04 : 0x24 - 0b0[010.0100]
const Byte INIT_DATAIC13                      = 0x00;
const Byte INDEX_PCF8574IC13_0_VALVE8         = 0x00;
const Byte INDEX_PCF8574IC13_1_SWITCHOFF      = 0x01;
const Byte INDEX_PCF8574IC13_2_LEDYELLOW      = 0x02;
const Byte INDEX_PCF8574IC13_3_LEDGREEN       = 0x03;
const Byte INDEX_PCF8574IC13_4_TESTOKN        = 0x04;
const Byte INDEX_PCF8574IC13_5_TESTNOTOKN     = 0x05;
const Byte INDEX_PCF8574IC13_6_MOTORLEFT      = 0x06;
const Byte INDEX_PCF8574IC13_7_MOTORRIGHT     = 0x07;
// I2C - I2C1 - PCF8574(IC8)  - 0x20 + 0x05 : 0x25 - 0b0[010.0101]
const Byte INIT_DATAIC8                       = 0x00;
const Byte INDEX_PCF8574IC8_0_PROGRAMLOCK     = 0x00;
const Byte INDEX_PCF8574IC8_1_KBMCOLUMN1      = 0x01;
const Byte INDEX_PCF8574IC8_2_KBMCOLUMN2      = 0x02;
const Byte INDEX_PCF8574IC8_3_KBMCOLUMN3      = 0x03;
const Byte INDEX_PCF8574IC8_4_BUTTONSTOP      = 0x04;
const Byte INDEX_PCF8574IC8_5_BUTTONVOLUME    = 0x05;
const Byte INDEX_PCF8574IC8_6_BUTTONSTART     = 0x06;
const Byte INDEX_PCF8574IC8_7_ADC16DATA       = 0x07;
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
#if defined(PROCESSOR_TEENSY36)
#endif // PROCESSOR_TEENSY36
//
//
//##########################################
//  PROCESSOR_ESP32
//##########################################
#if defined(PROCESSOR_ESP32)
//----------------------------------------------------------------------------------
// ESP32                                          Pin
//----------------------------------------------------------------------------------
// NC const int PIN_EN
const int PIN_GPIO36_ADC1CH0_SVP_RTC0           = 36; // INPUT ONLY!!!
const int PIN_GPIO39_ADC1CH3_SVN_RTC3           = 39; // INPUT ONLY!!!
const int PIN_GPIO34_ADC1CH6_RTC4               = 34; // INPUT ONLY!!!
const int PIN_GPIO35_ADC1CH7_RTC5               = 35; // INPUT ONLY!!!
const int PIN_GPIO32_ADC1CH4_T9_RTC9            = 32;
const int PIN_GPIO33_ADC1CH5_T8_RTC8            = 33;
const int PIN_GPIO25_ADC2CH8_DAC1_RTC6          = 25;
const int PIN_GPIO26_ADC2CH9_DAC2_RTC7          = 26;
const int PIN_GPIO27_ADC2CH7_T7_RTC17           = 27;
const int PIN_GPIO14_ADC2CH6_T6_HSPICLK_RTC16   = 14;
const int PIN_GPIO12_ADC2CH5_T5_HSPIMISO_RTC15  = 12;
const int PIN_GPIO13_ADC2CH4_T4_HSPIMOSI_RTC14  = 13;
// NC const int PIN_GPIO9_SHDSD2
// NC const int PIN_GPIO10_SWPSD3
// NC const int PIN_GPIO11_CSCCMD
// NC GND
// NC VIN
//----------------------------------------------------------------------------------
// ESP32                                          Pin
//----------------------------------------------------------------------------------
const int PIN_GPIO23_VSPIMOSI                   = 23;
const int PIN_GPIO22_I2CSCL                     = 22;
const int PIN_GPIO1_UART0TX                     = 1;
const int PIN_GPIO3_UART0RX                     = 3;
const int PIN_GPIO21_I2CSDA                     = 21;
const int PIN_GPIO19_VSPIMISO                   = 19;
const int PIN_GPIO18_VSPICLK                    = 18;
const int PIN_GPIO5_VSPICS0                     = 5;
const int PIN_GPIO17_UART2TX                    = 17;
const int PIN_GPIO16_UART2RX                    = 16;
const int PIN_GPIO4_ADC2CH0_T0_RTC10            = 4;
const int PIN_GPIO2_SYSTEMLED_ADC2CH2_T2_RTC12  = 2;
const int PIN_GPIO15_ADC2CH3_T3_HSPICS0_RTC13   = 15;
const int PIN_GPIO0_ADC2CH1_T1_RTC11            = 0;
// NC const int PIN_GPIO8_SDISD1
// NC const int PIN_GPIO7_SDOSDO
// NC const int PIN_GPIO6_SCKCLK
// 3V3
//----------------------------------------------------------------------------------
//
// ### Serial - RS232 ###
//
// Serial - RS232_0 - USB
const int PIN_UART0_TXD                     = PIN_GPIO1_UART0TX; //fixed from USB!!!
const int PIN_UART0_RXD                     = PIN_GPIO3_UART0RX; //fixed from USB!!!
// Serial - RS232_1 - PinSwitching Necesary!!!
const int PIN_UART1_TXD                     = PIN_GPIO32_ADC1CH4_T9_RTC9;// PIN_GPIO4_ADC2CH0_T0_RTC10; // -> RXD FTDI
const int PIN_UART1_RXD                     = PIN_GPIO33_ADC1CH5_T8_RTC8;//PIN_GPIO0_ADC2CH1_T1_RTC11; // -> TXD FTDI
// Serial - RS232_2
const int PIN_UART2_TXD                     = PIN_GPIO17_UART2TX;         // -> RXD FTDI
const int PIN_UART2_RXD                     = PIN_GPIO16_UART2RX;         // -> TXD FTDI
//
// ### Serial - SPI ###
// Serial - SPI - VSPI
const int PIN_VSPI_NSS                      = PIN_GPIO5_VSPICS0;
const int PIN_VSPI_SCK                      = PIN_GPIO18_VSPICLK;
const int PIN_VSPI_MISO                     = PIN_GPIO19_VSPIMISO;
const int PIN_VSPI_MOSI                     = PIN_GPIO23_VSPIMOSI;
// Serial - SPI - HSPI
const int PIN_HSPI_NSS                      = PIN_GPIO15_ADC2CH3_T3_HSPICS0_RTC13;
const int PIN_HSPI_SCK                      = PIN_GPIO14_ADC2CH6_T6_HSPICLK_RTC16;
const int PIN_HSPI_MISO                     = PIN_GPIO12_ADC2CH5_T5_HSPIMISO_RTC15;
const int PIN_HSPI_MOSI                     = PIN_GPIO13_ADC2CH4_T4_HSPIMOSI_RTC14;
//
// ### Serial - I2C ###
const int PIN_I2C_SCL                       = PIN_GPIO22_I2CSCL;
const int PIN_I2C_SDA                       = PIN_GPIO21_I2CSDA;
//
// ### Analog - ADC ###
// Analog - ADC - ADC1
const int PIN_ADC1_ANALOGIN0                = PIN_GPIO36_ADC1CH0_SVP_RTC0;
// NC const int PIN_ADC1_ANALOGIN1
// NC const int PIN_ADC1_ANALOGIN2
const int PIN_ADC1_ANALOGIN3                = PIN_GPIO39_ADC1CH3_SVN_RTC3;
const int PIN_ADC1_ANALOGIN4                = PIN_GPIO32_ADC1CH4_T9_RTC9;
const int PIN_ADC1_ANALOGIN5                = PIN_GPIO33_ADC1CH5_T8_RTC8;
const int PIN_ADC1_ANALOGIN6                = PIN_GPIO34_ADC1CH6_RTC4;
const int PIN_ADC1_ANALOGIN7                = PIN_GPIO35_ADC1CH7_RTC5;
// Analog - ADC - ADC2
const int PIN_ADC2_ANALOGIN0                = PIN_GPIO4_ADC2CH0_T0_RTC10;
const int PIN_ADC2_ANALOGIN1                = PIN_GPIO0_ADC2CH1_T1_RTC11;
const int PIN_ADC2_ANALOGIN2                = PIN_GPIO2_SYSTEMLED_ADC2CH2_T2_RTC12;
const int PIN_ADC2_ANALOGIN3                = PIN_GPIO15_ADC2CH3_T3_HSPICS0_RTC13;
const int PIN_ADC2_ANALOGIN4                = PIN_GPIO13_ADC2CH4_T4_HSPIMOSI_RTC14;
const int PIN_ADC2_ANALOGIN5                = PIN_GPIO12_ADC2CH5_T5_HSPIMISO_RTC15;
const int PIN_ADC2_ANALOGIN6                = PIN_GPIO14_ADC2CH6_T6_HSPICLK_RTC16;
const int PIN_ADC2_ANALOGIN7                = PIN_GPIO27_ADC2CH7_T7_RTC17;
const int PIN_ADC2_ANALOGIN8                = PIN_GPIO25_ADC2CH8_DAC1_RTC6;
const int PIN_ADC2_ANALOGIN9                = PIN_GPIO26_ADC2CH9_DAC2_RTC7;
//
// ### DIO ###
// 
//------------------------------------------------------------------------------
// Led - Single Led System (fixed)
//------------------------------------------------------------------------------
const int PIN_LEDSYSTEM                     = PIN_GPIO2_SYSTEMLED_ADC2CH2_T2_RTC12;
const bool LEDSYSTEM_INVERTED               = false;
//------------------------------------------------------------------------------
// Led - MultiChannel
//------------------------------------------------------------------------------
#if defined(MULTICHANNELLED_ISPLUGGED)
const int PIN_LED_CHANNEL1                  = 26;
const int PIN_LED_CHANNEL2                  = 25;
const int PIN_LED_CHANNEL3                  = 17;
const int PIN_LED_CHANNEL4                  = 16;
const int PIN_LED_CHANNEL5                  = 27;
const int PIN_LED_CHANNEL6                  = 14;
const int PIN_LED_CHANNEL7                  = 12;
const int PIN_LED_CHANNEL8                  = 13;
#endif
//
//
// ### Peripheral ###
//
//------------------------------------------------------------------------------
// ard
//------------------------------------------------------------------------------
#if defined(SDCARD_ISPLUGGED)
// VSPI
const int PIN_SPIV_CS_SDCARD                = PIN_VSPI_NSS;  //  5 == PIN_GPIO5_VSPICS0;
const int PIN_SPIV_SCK                      = PIN_VSPI_SCK;  // 18 == PIN_GPIO18_VSPICLK;
const int PIN_SPIV_MISO                     = PIN_VSPI_MISO; // 19 == PIN_GPIO19_VSPIMISO;
const int PIN_SPIV_MOSI                     = PIN_VSPI_MOSI; // 23 == PIN_GPIO23_VSPIMOSI;
// HSPI
// const int PIN_SPIH_CS_SDCARD             = PIN_HSPI_NSS;
// const int PIN_SPIH_SCK                   = PIN_HSPI_SCK;
// const int PIN_SPIH_MISO                  = PIN_HSPI_MISO;
// const int PIN_SPIH_MOSI                  = PIN_HSPI_MOSI;
#endif
//
//------------------------------------------------------------------------------
// I2CDisplay 
//------------------------------------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
const Byte I2CADDRESS_I2CLCDISPLAY          = 0x27;
const Byte I2CLCDISPLAY_COLCOUNT            = 20;
const Byte I2CLCDISPLAY_ROWCOUNT            =  4;
const int PIN_I2CDISPLAY_SCL                = PIN_I2C_SCL;
const int PIN_I2CDISPLAY_SDA                = PIN_I2C_SDA;
#endif                      
//
//-------------------------------------------------
#if defined(DISPATCHER_TRIGGER)
    #if defined(TRIGGEROUT_LASER)
    const bool TRIGGEROUT_INVERTED      = false;
    const int  PIN_TRIGGEROUT           = 2; // 50
    #endif
    #if defined(TRIGGERIN_SYNCHRONIZE)
    const bool TRIGGERIN_INVERTED       = false;
    const int PIN_TRIGGERIN_SYNCHRONIZE = 14; // 53;
    #endif
#endif
#if defined(DISPATCHER_LASERRANGE)
#endif
#if defined(DISPATCHER_LASERPOSITION)
    #if defined(DAC_INTERNAL)
    const int PIN_DAC_CHANNEL0        = 66;
    const int PIN_DAC_CHANNEL1        = 67;
    #endif
    #if defined(DAC_MCP4725)
    const int PIN_MCP4725_SCK         = 66;
    const int PIN_MCP4725_DAT         = 66;
    #endif
#endif
//
#endif // PROCESSOR_ESP32
//
//
//##########################################
//  PROCESSOR_STM32F403VG
//##########################################
#if defined(PROCESSOR_STM32F407VG)
// Ports: PA, PB, PC, PD, PE
//----------------------------------------------
// STM32F403VG, Pins 1..44        Pin 
//----------------------------------------------
const int PIN_PD8_USART3_TX     = PD8;
const int PIN_PB15_SPI2_MOSI    = PB15;
const int PIN_PD10_USART3_CK    = PD10;
const int PIN_PD9_USART3_RX     = PD9;
const int PIN_PD11_USART3_CTS   = PD11;
const int PIN_PD12_TIM4_CH1     = PD12;
const int PIN_PD13_TIM4_CH2     = PD13;
const int PIN_PD14_TIM4_CH3     = PD14;
const int PIN_PD15_TIM4_CH4     = PD15;
const int PIN_PC6_USART6_TX     = PC6;
const int PIN_PC7_USART6_RX     = PC7;
const int PIN_PC8_TIM3_CH3      = PC8;
const int PIN_PA10_UART1_RX     = PA10;
const int PIN_PA9_UART1_TX      = PA9;
const int PIN_PA12_USART1_RTS   = PA12;
const int PIN_PA11_TIM1_CH4     = PA11;
const int PIN_PC10_SPI3_SCK     = PC10;
const int PIN_PA15_SPI1_NSS     = PA15;
const int PIN_PC12_UART5_TX     = PC12;
const int PIN_PC11_SPI3_MISO    = PC11;
const int PIN_PD0_CAN1_RX       = PD0;
const int PIN_PD1_CAN1_TX       = PD1;
const int PIN_PD2_UART5_RX      = PD2;
const int PIN_PD3_USART2_CTS    = PD3;
const int PIN_PD4_USART2_RTS    = PD4;
const int PIN_PD5_USART2_TX     = PD5;
const int PIN_PD6_USART2_RX     = PD6;
const int PIN_PD7_USART2_CK     = PD7;
const int PIN_PB5_SPI3_MOSI     = PB5;
const int PIN_PB3_SPI1_SCK      = PB3;
const int PIN_PB7_I2C1_SDA      = PB7;
const int PIN_PB6_I2C1_SCL      = PB6;
const int PIN_PB9_SPI2_NSS      = PB9;
const int PIN_PB8_TIM10_CH1     = PB8;
const int PIN_PE1               = PE1;
const int PIN_PE0_TIM4_ETR      = PE0;
//----------------------------------------------
// STM32F403VG, Pins 45..88       Pin 
//----------------------------------------------
const int PIN_PB14_SPI2_MISO    = PB14;
const int PIN_PB13_SPI2_SCK     = PB13;
const int PIN_PB12_SPI2_NSS     = PB12;
const int PIN_PB11_I2C2_SDA     = PB11;
const int PIN_PB10_I2C2_SCL     = PB10;
const int PIN_PE15_TIM1_BKIN    = PE15;
const int PIN_PE14_TIM1_CH4     = PE14;
const int PIN_PE13_TIM1_CH3     = PE13;
const int PIN_PE12_TIM1_CH3N    = PE12;
const int PIN_PE11_TIM1_CH2     = PE11;
const int PIN_PE10_TIM1_CH2N    = PE10;
const int PIN_PE9_TIM1_CH1      = PE9;
const int PIN_PE8_TIM1_CH1N     = PE8;
const int PIN_PE7_TIM1_ETR      = PE7;
const int PIN_PB1_TIM3_CH4      = PB1;
const int PIN_PB0_TIM3_CH3      = PB0;
const int PIN_PC5               = PC5;
const int PIN_PC4               = PC4;
const int PIN_PA7_SPI1_MOSI     = PA7;
const int PIN_PA6_SPI1_MISO     = PA6;
const int PIN_PA5_SPI1_SCK      = PA5;
const int PIN_PA4_SPI1_NSS      = PA4;
const int PIN_PA3_UART2_RX      = PA3;
const int PIN_PA2_UART2_TX      = PA2;
const int PIN_PA1_UART4_RX      = PA1;
const int PIN_PA0_UART4_TX      = PA0;
const int PIN_PC3_SPI2_MOSI     = PC3;
const int PIN_PC2_SPI2_MISO     = PC2;
const int PIN_PC1               = PC1;
const int PIN_PC0               = PC0;
const int PIN_PC13              = PC13;
const int PIN_PE6_TIM9_CH2      = PE6;
const int PIN_PE5_TIM9_CH1      = PE5;
const int PIN_PE4               = PE4;
const int PIN_PE3               = PE3;
const int PIN_PE2               = PE2;
//----------------------------------------------------------------------------------
//
// ### Serial - RS232 ###
// Serial - RS232_1 - RS232Command
const int PIN_UART6_RXD                     = PIN_PC6_USART6_TX;
const int PIN_UART6_TXD                     = PIN_PC6_USART6_TX;
// Serial - RS232_2 - RS232Printer
// NC const int PIN_UART2_RXD                     = PIN_PA3_UART2_RX;
// NC const int PIN_UART2_TXD                     = PIN_PA2_UART2_TX;
// Serial - RS232_3 - RS232Debug
// NC const int PIN_UART3_RXD                     = PIN_PD9_USART3_RX;
// NC const int PIN_UART3_TXD                     = PIN_PD8_USART3_TX;
//
// ### Serial - SPI ###
// Serial - SPI - SPI_1
// NC const int PIN_SPI1_NSS                      = PIN_PA4_SPI1_NSS;
// NC const int PIN_SPI1_SCK                      = PIN_PA5_SPI1_SCK;
// NC const int PIN_SPI1_MISO                     = PIN_PA6_SPI1_MISO;
// NC const int PIN_SPI1_MOSI                     = PIN_PA7_SPI1_MOSI;
// Serial - SPI - SPI_2
// NC const int PIN_SPI2_NSS                      = PIN_PB9_SPI2_NSS;
// NC const int PIN_SPI2_SCK                      = PIN_PB13_SPI2_SCK;
// NC const int PIN_SPI2_MISO                     = PIN_PB14_SPI2_MISO;
// NC const int PIN_SPI2_MOSI                     = PIN_PB15_SPI2_MOSI;
//
// ### Serial - I2C ###
// Serial - I2C - I2C1
// NC const int PIN_I2C1_SCL                      = PIN_PB6_I2C1_SCL;
// NC const int PIN_I2C1_SDA                      = PIN_PB7_I2C1_SDA;
// Serial - I2C - I2C2
// NC const int PIN_I2C2_SCL                      = PIN_PB10_I2C2_SCL;
// NC const int PIN_I2C2_SDA                      = PIN_PB11_I2C2_SDA;
//
// ### Analog - ADC ###
// Analog - ADC - ADConverter
// const int PIN_ADC_ANALOGIN0                 = 
// const int PIN_ADC_ANALOGIN1                 = 
// const int PIN_ADC_ANALOGIN2                 = 
// const int PIN_ADC_ANALOGIN3                 = 
// const int PIN_ADC_ANALOGIN4                 = 
// const int PIN_ADC_ANALOGIN5                 = 
// const int PIN_ADC_ANALOGIN6                 = 
// const int PIN_ADC_ANALOGIN7                 = 
// const int PIN_ADC_ANALOGIN8                 = 
// const int PIN_ADC_ANALOGIN9                 = 
//
// ### Analog - DAC ###
// Analog - DAC - DAConverter
// const int PIN_DAC_ANALOGOUT                 = 
// const int PIN_DAC_ANALOGOUT                 = 
//
//
// ### DIO ###
// 
// Single Led System (fixed)
const int  PIN_LEDSYSTEM                    = PIN_PA1_UART4_RX;  
const bool LEDSYSTEM_INVERTED               = true;
const bool LEDSYSTEM_ON                     = LOW;
const bool LEDSYSTEM_OFF                    = HIGH;

//
// ### Peripheral ###
//------------------------------------------------------------------------------
// MotorL298N
//------------------------------------------------------------------------------
#if defined(MOTORL298N_ISPLUGGED)
const int PIN_MOTORX_ENA                  = PIN_PD0_CAN1_RX;
const int PIN_MOTORX_ENB                  = PIN_PD1_CAN1_TX;
const int PIN_MOTORX_PWM                  = PIN_PD12_TIM4_CH1;
//
const int PIN_MOTORY_ENA                  = PIN_PD2_UART5_RX;
const int PIN_MOTORY_ENB                  = PIN_PD3_USART2_CTS;
const int PIN_MOTORY_PWM                  = PIN_PD13_TIM4_CH2;
//
const int PIN_MOTORU_ENA                  = PIN_PD4_USART2_RTS;
const int PIN_MOTORU_ENB                  = PIN_PD5_USART2_TX;
const int PIN_MOTORU_PWM                  = PIN_PD14_TIM4_CH3;
//   
const int PIN_MOTORV_ENA                  = PIN_PD6_USART2_RX;
const int PIN_MOTORV_ENB                  = PIN_PD7_USART2_CK;
const int PIN_MOTORV_PWM                  = PIN_PD15_TIM4_CH4;
//
#endif // MOTORL298N_ISPLUGGED
//
#if defined(MOTORL298N_ISPLUGGED)
const Float32 INIT_PWMLOW_MOTORX          =  55.0f;
const Float32 INIT_PWMHIGH_MOTORX         =  95.0f;
const Float32 INIT_PWMLOW_MOTORY          =  56.0f;
const Float32 INIT_PWMHIGH_MOTORY         =  96.0f;
const Float32 INIT_PWMLOW_MOTORU          =  57.0f;
const Float32 INIT_PWMHIGH_MOTORU         =  97.0f;
const Float32 INIT_PWMLOW_MOTORV          =   4.0f;
const Float32 INIT_PWMHIGH_MOTORV         =  99.0f;
#endif // MOTORL298N_ISPLUGGED
//
#if defined(MOTORENCODER_ISPLUGGED)
const UInt32 INIT_POSITIONRAMP_X          =   1000; // China DC Gear
const UInt32 INIT_POSITIONRESOLUTION_X    =     10; // 1:200
const UInt32 INIT_POSITIONRAMP_Y          =   1000; // China DC Gear
const UInt32 INIT_POSITIONRESOLUTION_Y    =     10; // 1:200
const UInt32 INIT_POSITIONRAMP_U          =   1000; // China DC Gear
const UInt32 INIT_POSITIONRESOLUTION_U    =     10; // 1:200
const UInt32 INIT_POSITIONRAMP_V          =  10000; // PI RotationStage
const UInt32 INIT_POSITIONRESOLUTION_V    =     10; // no limits
#endif // MOTORENCODER_ISPLUGGED
//
//------------------------------------------------------------------------------
// EncoderIRQ
//------------------------------------------------------------------------------
#if defined(ENCODERIRQ_ISPLUGGED)
const int PIN_ENCODERX_ENA                = PIN_PE0_TIM4_ETR;
const int PIN_ENCODERX_ENB                = PIN_PE1;
//
const int PIN_ENCODERY_ENA                = PIN_PB8_TIM10_CH1;
const int PIN_ENCODERY_ENB                = PIN_PB9_SPI2_NSS;
//
const int PIN_ENCODERU_ENA                = PIN_PB6_I2C1_SCL;
const int PIN_ENCODERU_ENB                = PIN_PB7_I2C1_SDA;
//
const int PIN_ENCODERV_ENA                = PIN_PB3_SPI1_SCK;
const int PIN_ENCODERV_ENB                = PIN_PB5_SPI3_MOSI;
#endif // ENCODERIRQ_ISPLUGGED
//
//------------------------------------------------------------------------------
// TriggerIRQ
//------------------------------------------------------------------------------
#if defined(TRIGGER_ISPLUGGED)
const int PIN_TRIGGERX                    = PIN_LEDSYSTEM;
const int PIN_TRIGGERY                    = PIN_LEDSYSTEM;
const int PIN_TRIGGERU                    = PIN_LEDSYSTEM;
const int PIN_TRIGGERV                    = PIN_LEDSYSTEM;
#endif // TRIGGER_ISPLUGGED
//
#endif // PROCESSOR_STM32F407VG
//
#endif // Pinout_h
