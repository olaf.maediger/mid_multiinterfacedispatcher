//
#include "Defines.h"
//
#if defined(DISPATCHER_LASERPOSITION)
//
#ifndef DispatcherLaserPosition_h
#define DispatcherLaserPosition_h
//
#include "Dispatcher.h"
#include "Led.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - LaserPosition - SHORT
// //----------------------------------------------------------------
// #define SHORT_GLS   "GLS"
// #define SHORT_LSO   "LSO"
// #define SHORT_LSF   "LSF"
// #define SHORT_LSB   "LSB"
// #define SHORT_LSA   "LSA"
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LASERPOSITION  " Help (LaserPosition):"
// #define MASK_GLS                    " %-3s                : Get State LedSystem"
// #define MASK_LSO                    " %-3s                : LedSystem On"
// #define MASK_LSF                    " %-3s                : LedSystem oFf"
// #define MASK_LSB                    " %-3s <n> <p> <w>    : LedSystem Blink  <n>times <p>eriod{ms}/<w>ith{ms}"
// #define MASK_LSA                    " %-3s                : LedSystem Abort"
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition
//----------------------------------------------------------------
class CDispatcherLaserPosition : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLaserPosition(void);
  //
  // bool ExecuteGetLaserPosition(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLaserPosition(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemBlink(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemAbort(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherLaserPosition_h
//
#endif // DISPATCHER_LASERPOSITION
//
//###################################################################################
//###################################################################################
//###################################################################################
