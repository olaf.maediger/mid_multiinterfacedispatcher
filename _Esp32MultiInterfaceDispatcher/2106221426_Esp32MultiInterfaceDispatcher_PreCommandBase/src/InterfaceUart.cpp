//
#include "Defines.h"
//
#if defined(INTERFACE_UART)
//
#include "Error.h"
#include "InterfaceUart.h"
//
extern CError Error;
//
//####################################################
//  CUartBase
//####################################################
//----------------------------------------------------
//  CUartBase - Constructor
//----------------------------------------------------
CUartBase::CUartBase(void)
{
  FRXIndex = 0;
  FRXBlock[0] = 0;
  FRXEcho = UART_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CUartBase - Property
//----------------------------------------------------
void CUartBase::SetRXEcho(Boolean rxecho)
{
  FRXEcho = rxecho;
}
//
//----------------------------------------------------
//  CUartBase - Handler
//----------------------------------------------------
// Boolean Open(int baudrate);
// Boolean Close(void);
//
//----------------------------------------------------
//  CUartBase - Write
//----------------------------------------------------
// Boolean Write(char* text);
// Boolean WriteLine(void);
// Boolean WriteLine(char* line);
//
//----------------------------------------------------
//  CUartBase - Read
//----------------------------------------------------
// Boolean ReadLine(char* rxline, int *rxsize);
//








//
//####################################################
//  CUartHS
//####################################################
//----------------------------------------------------
//  CUartHS - Constructor
//----------------------------------------------------
CUartHS::CUartHS(HardwareSerial* pserial)
{
  FPSerial = pserial;
} 
//
//----------------------------------------------------
//  CUartHS - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CUartHS - Handler
//----------------------------------------------------
Boolean CUartHS::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  return true;
} 
Boolean CUartHS::Close(void)
{
  FPSerial->end();
  return true;
} 
//
//----------------------------------------------------
//  CUartHS - Write
//----------------------------------------------------
Boolean CUartHS::WriteCharacter(char character)
{
  FPSerial->print(character);
  return true;
}

Boolean CUartHS::WriteText(char* ptext)
{
  FPSerial->print(ptext);
  return true;
}

Boolean CUartHS::WriteLine(void)
{
  FPSerial->write(TERMINAL_NEWLINE);
  return true;  
} 

Boolean CUartHS::WriteLine(char* pline)
{
  FPSerial->write(pline);
  FPSerial->write(TERMINAL_NEWLINE);
  return true;
} 
//
//----------------------------------------------------
//  CUartHS - Read
//----------------------------------------------------
UInt8 CUartHS::GetRxCount(void)
{
  return FPSerial->available();
}

Boolean CUartHS::ReadCharacter(char &rxcharacter)
{
   if (0 < FPSerial->available())
   {
     rxcharacter = FPSerial->read();
     return true;
   }
   return false;
}

Boolean CUartHS::ReadLine(char* prxline, int &rxsize)
{ // Separator: CR or LF
  prxline[0] = SEPARATOR_ZERO;
  // !!! stay rxsize, use as limit !!!
  while (0 < FPSerial->available())
  {
    char RXC = (char)FPSerial->read();
    switch (RXC)
    {
      case (char)SEPARATOR_CR:
      case (char)SEPARATOR_LF:
        if (0 < FRXIndex)
        {          
          FRXBlock[FRXIndex] = SEPARATOR_ZERO;
          FRXIndex++;
          strcpy(prxline, FRXBlock);
          rxsize = 1 + FRXIndex;
          FRXIndex = 0;
          FRXBlock[FRXIndex] = SEPARATOR_ZERO;
        }
        return (0 < strlen(prxline));
      default:
        FRXBlock[FRXIndex] = RXC;
        FRXIndex++;
        if (rxsize <= (1 + FRXIndex))
        {
          Error.SetCode(ecReceiveBufferOverflow);
          return false;
        }
    }
  }
  return false;
} 
//
//----------------------------------------------------
//  CUartxx - Handler
//----------------------------------------------------
// Boolean Open(int baudrate);
// Boolean Close(void);
//
//----------------------------------------------------
//  CUartBase - Write
//----------------------------------------------------
// Boolean WriteLine(void);
// Boolean WriteLine(char* line);
//
//----------------------------------------------------
//  CUartBase - Read
//----------------------------------------------------
// Boolean Read(char* rxtext, int *rxsize);
// Boolean ReadLine(char* rxline, int *rxsize);











//
//####################################################
//  CUart
//####################################################
//----------------------------------------------------
//  CUart - Constructor
//----------------------------------------------------
CUart::CUart(HardwareSerial* pserial)
{
  FPUartBase = (CUartBase*)new CUartHS(pserial);
}
//
//----------------------------------------------------
//  CUart - Property
//----------------------------------------------------
// char* CUart::GetPRXBuffer(void)
// {
//   return FPUartBase->GetPRXBuffer();
// }
// ??? char* CUart::GetPTXBuffer(void)
// ??? {
// ???   return FPUartBase->GetPTXBuffer();
// ??? }

void CUart::SetRXEcho(Boolean rxecho)
{
  return FPUartBase->SetRXEcho(rxecho);
}
//
//----------------------------------------------------
//  CUart - Handler
//----------------------------------------------------
Boolean CUart::Open(int baudrate)
{
  return FPUartBase->Open(baudrate);
}
Boolean CUart::Close(void)
{
  return FPUartBase->Close();
}


//
//----------------------------------------------------
//  CUart - Read
//----------------------------------------------------
UInt8 CUart::GetRxCount(void)
{
  return FPUartBase->GetRxCount();
}

Boolean CUart::ReadLine(char* prxline, int &rxsize)
{
  return FPUartBase->ReadLine(prxline, rxsize);
}


// String CUart::ReadLine(void)
// {
//   return FPUartBase->ReadLine();
// }

// String CUart::ReadString(void)
// {
//   return FPUartBase->ReadString();
// }

// Byte CUart::ReadByte(void)
// {
//   return FPUartBase->ReadByte();
// }

// UInt16 CUart::ReadDual(void)
// {
//   return FPUartBase->ReadDual();
// }

// UInt32 CUart::ReadQuad(void)
// {
//   return FPUartBase->ReadQuad();
// }

// Int16 CUart::ReadInt16(void)
// {
//   return FPUartBase->ReadInt16();
// }
// UInt16 CUart::ReadUInt16(void)
// {
//   return FPUartBase->ReadUInt16();
// }

// Int32 CUart::ReadInt32(void)
// {
//   return FPUartBase->ReadInt32();
// }
// UInt32 CUart::ReadUInt32(void)
// {
//   return FPUartBase->ReadUInt32();
// }

// Float32 CUart::ReadFloat32(void)
// {
//   return FPUartBase->ReadFloat32();
// }
// Double64 CUart::ReadDouble64(void)
// {
//   return FPUartBase->ReadDouble64();
// }

//
//----------------------------------------------------
//  CUart - Write
//----------------------------------------------------
Boolean CUart::WriteLine(void)
{
  return FPUartBase->WriteLine();
}
Boolean CUart::WriteText(const char* ptext)
{
  return FPUartBase->WriteText((char*)ptext);
}
Boolean CUart::WriteText(const char* mask, const char* ptext)
{
  if (0 != ptext)
  {
    return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)ptext));
  }  
  return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)""));
}
Boolean CUart::WriteLine(const char* pline)
{
  return FPUartBase->WriteLine((char*)pline);
}
Boolean CUart::WriteLine(const char* mask, const char* pline)
{
  if (0 != pline)
  {
    return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)pline));
  }  
  return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)""));
}
Boolean CUart::WritePrompt(void)
{
  return FPUartBase->WriteText((char*)TERMINAL_PROMPT);
}
Boolean CUart::WriteComment(void)
{
  return FPUartBase->WriteCharacter(TERMINAL_COMMENT[0]);
}


// void CUart::WritePChar(const char* mask, char* value)
// {
//   if (0 != value)
//   {
//     FPUartBase->Write(FPUartBase->ConvertPChar(mask, value));
//     return;
//   }
//   FPUartBase->Write((char*)mask);
// }

// void CUart::WriteString(const char* mask, String value)
// {
//   FPUartBase->Write(FPUartBase->ConvertString(mask, value));
// }

// void CUart::WriteByte(const char* mask, Byte value)
// {
//   FPUartBase->Write(FPUartBase->ConvertByte(mask, value));
// }
// void CUart::WriteDual(const char* mask, UInt16 value)
// {
//   FPUartBase->Write(FPUartBase->ConvertDual(mask, value));
// }

// void CUart::WriteQuad(const char* mask, UInt32 value)
// {
//   FPUartBase->Write(FPUartBase->ConvertQuad(mask, value));
// }

// void CUart::WriteInt16(const char* mask, Int16 value)
// {
//   FPUartBase->Write(FPUartBase->ConvertInt16(mask, value));
// }

// void CUart::WriteUInt16(const char* mask, UInt16 value)
// {
//   FPUartBase->Write(FPUartBase->ConvertUInt16(mask, value));
// }

Boolean CUart::WriteInt32(const char* mask, Int32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertInt32(mask, value));
}

// void CUart::WriteUInt32(const char* mask, UInt32 value)
// {
//   FPUartBase->Write(FPUartBase->ConvertUInt32(mask, value));
// }

// void CUart::WriteFloat32(const char* mask, Float32 value)
// {  
//   FPUartBase->Write(FPUartBase->ConvertFloat32(mask, value));
// }

// void CUart::WriteDouble64(const char* mask, Double64 value)
// {  
//   FPUartBase->Write(FPUartBase->ConvertDouble64(mask, value));
// }
//






//
//-------------------------------------------------------------------
//  Uart - Execute
//-------------------------------------------------------------------
// Boolean CUart::Execute(void)
// {
//   // while (0 < GetRxCount())
//   // {
//   //   Char C = F
//   //   switch (C)
//   //   {
//   //     case (byte)SEPARATOR_CR:
//   //     case (byte)SEPARATOR_LF:
//   //       FRxdBufferIndex, SEPARATOR_ZERO);
//   //       FRxdBufferIndex = 0; // restart
//   //       strupr(FRxdBuffer);
//   //       strcpy(FCommandText, FRxdBuffer);
//   //       return true;
//   //     default: 
//   //       FRxdBuffer[FRxdBufferIndex] = C;
//   //       FRxdBufferIndex++;
//   //       break;
//   //   }
//   // }  
// }











#endif // INTERFACE_UART
//