//
#include "Defines.h"
//
#if defined(COMMAND_COMMON)
//
#ifndef CommandCommon_h
#define CommandCommon_h
//
#include <string.h>
//
//----------------------------------------------------------------
// Command - SHORT - Definition
//----------------------------------------------------------------
#define SHORT_GPH   "GPH"                   // GPH - GetProgramHeader
#define SHORT_GHV   "GHV"                   // GHV - GetHardwareVersion
#define SHORT_GSV   "GSV"                   // GSV - GetSoftwareVersion
#define SHORT_H     "H"                     // H - (this) Help
//
//----------------------------------------------------------------
// Command - MASK - Definition
//----------------------------------------------------------------
#define HELP_COMMAND_COMMON       " Help (Common):"
#define MASK_H                    " %-3s                  : This Help"
//
#define TITLE_LINE                "--------------------------------------------------"
#define MASK_PROJECT              "- Project:   %-35s -"
#define MASK_SOFTWARE             "- Version:   %-35s -"
#define MASK_HARDWARE             "- Hardware:  %-35s -"
#define MASK_DATE                 "- Date:      %-35s -"
#define MASK_TIME                 "- Time:      %-35s -"
#define MASK_AUTHOR               "- Author:    %-35s -"
#define MASK_PORT                 "- Port:      %-35s -"
#define MASK_PARAMETER            "- Parameter: %-35s -"
//
//----------------------------------------------------------------
// Command - Class
//----------------------------------------------------------------
class CCommandCommon
{
  public:
  CCommandCommon(void);
  //
  bool ExecuteGetProgramHeader(void);
  bool ExecuteGetHardwareVersion(void);
  bool ExecuteGetSoftwareVersion(void);
  bool ExecuteGetHelp(void);
};
//
#endif // Command_h
//
#endif // COMMAND_COMMON
//