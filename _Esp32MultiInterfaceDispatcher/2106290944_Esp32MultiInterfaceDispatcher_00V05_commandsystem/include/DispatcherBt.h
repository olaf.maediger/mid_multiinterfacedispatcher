//
#include "Defines.h"
//
#if defined(COMMAND_BT)
//
#ifndef DispatcherBt_h
#define DispatcherBt_h
// 
#include "Dispatcher.h"
//
class CDispatcherBt : public CDispatcher
{

};
//
#endif
//
#endif // COMMAND_BT
//
//


// //
// #include "Defines.h"
// //
// #if defined(COMMAND_BT)
// //
// #ifndef DispatcherBt_h
// #define DispatcherBt_h
// //
// #include "Dispatcher.h"
// //
// //----------------------------------------------------------------
// // Help - Dispatcher - SHORT - Definition
// //----------------------------------------------------------------
// #define SHORT_WCB   "WCB"                   // WCB - Write <c>ommand Bt
// #define SHORT_RCB   "RCB"                   // RCB - Read Dispatcher Bt
// // 
// //----------------------------------------------------------------
// // Help - Dispatcher - MASK - Definition
// //----------------------------------------------------------------
// #define HELP_COMMAND_BT          " Help (Bt):"
// #define MASK_WCB                  " %-3s <c>              : Write <c>ommand Bt"
// #define MASK_RCB                  " %-3s                  : Read Dispatcher Bt"
// //
// class CDispatcherBt : public CDispatcher
// {

// };
// //
// #endif // DispatcherBt_h
// //
// #endif // COMMAND_BT
// //