//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef Trigger_h
#define Trigger_h
//
//--------------------------------
//  Trigger - Constant
//--------------------------------
const int TRIGGER_SIZE_ID                         = 4;
const int TRIGGER_SIZE_BUFFER                     = 64;
//
const char* const TRIGGERIN_EVENT_MASK            = "TriggerIn[%s]=%s";
const char* const TRIGGERIN_EVENT_UNDEFINED       = "Undefined";
const char* const TRIGGERIN_EVENT_ACTIVE          = "Active";
const char* const TRIGGERIN_EVENT_PASSIVE         = "Passive";
const char* const TRIGGERIN_EVENT_WAITFORACTIVE   = "WaitForActive";
const char* const TRIGGERIN_EVENT_WAITFORPASSIVE  = "WaitForPassive";
const char* const TRIGGERIN_EVENT_TIMEOUT         = "TimeOut";
//
const char* const TRIGGEROUT_EVENT_MASK                 = "TriggerOut[%s]=%s";
const char* const TRIGGEROUT_EVENT_UNDEFINED            = "Undefined";
const char* const TRIGGEROUT_EVENT_ACTIVE               = "Active";
const char* const TRIGGEROUT_EVENT_PASSIVE              = "Passive";
const char* const TRIGGEROUT_EVENT_PULSEACTIVE          = "PulseActive";
const char* const TRIGGEROUT_EVENT_PULSEPASSIVE         = "PulsePassive";
const char* const TRIGGEROUT_EVENT_PULSESEQUENCEACTIVE  = "PulseSequenceActive";
const char* const TRIGGEROUT_EVENT_PULSESEQUENCEPASSIVE = "PulseSequencePassive";
//
const char* const STATE_TRIGGERIN[] = {TRIGGERIN_EVENT_UNDEFINED, 
                                       TRIGGERIN_EVENT_ACTIVE, 
                                       TRIGGERIN_EVENT_PASSIVE,
                                       TRIGGERIN_EVENT_WAITFORACTIVE, 
                                       TRIGGERIN_EVENT_WAITFORPASSIVE,
                                       TRIGGERIN_EVENT_TIMEOUT};
//
const char* const STATE_TRIGGEROUT[] = {TRIGGEROUT_EVENT_UNDEFINED, 
                                        TRIGGEROUT_EVENT_ACTIVE, 
                                        TRIGGEROUT_EVENT_PASSIVE,
                                        TRIGGEROUT_EVENT_PULSEACTIVE, 
                                        TRIGGEROUT_EVENT_PULSEPASSIVE,
                                        TRIGGEROUT_EVENT_PULSESEQUENCEACTIVE, 
                                        TRIGGEROUT_EVENT_PULSESEQUENCEPASSIVE};
//
//--------------------------------
//  Type - TriggerBase
//--------------------------------
enum ETriggerLevel
{
  tlUndefined = -1,
  tlPassive = 0,
  tlActive = 1
};
//
class CTriggerBase
{
  protected:
  char FID[TRIGGER_SIZE_ID];
  char FBuffer[TRIGGER_SIZE_BUFFER];
  int FPin;
  bool FInverted;
  ETriggerLevel FLevel;
  //  
  public:
  CTriggerBase(const char* pid, int pin,
               bool inverted = false);
  //
  ETriggerLevel GetLevel(void);
  void SetLevel(ETriggerLevel level);
  //
  Boolean virtual Open(void) = 0;
  Boolean virtual Close(void) = 0;
  //
  void virtual Execute(void) = 0;
};
//
//--------------------------------
//  Type - TriggerIn
//--------------------------------
//
class CTriggerIn;
typedef void (*DOnEventTriggerIn)(CTriggerIn* trigger, const char* event);
//
enum EStateTriggerIn
{
  stiUndefined = 0,
  stiActive = 1,
  stiPassive = 2,
  stiWaitForActive = 3,  // IsPassive
  stiWaitForPassive = 4  // IsActive
};
class CTriggerIn : public CTriggerBase
{
  protected:
  EStateTriggerIn FState;
  long unsigned FTimeWait;
  DOnEventTriggerIn FOnEvent;
  //  
  public:
  CTriggerIn(const char* pid, int pin,              
             bool inverted = false);
  //
  void SetOnEvent(DOnEventTriggerIn onevent);
  void SetState(EStateTriggerIn state);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  bool WaitForActive(long unsigned timeout);
  bool WaitForPassive(long unsigned timeout);
  //
  void Execute(void);
};
//
//--------------------------------
//  Type - TriggerOut
//--------------------------------
//
class CTriggerOut;
typedef void (*DOnEventTriggerOut)(CTriggerOut* trigger, const char* event);
//
enum EStateTriggerOut
{
  stoUndefined = 0,
  stoActive = 1,              // ->A
  stoPassive = 2,             // ->P
  stoPulseActive = 3,         // P->A->P
  stoPulsePassive = 4,        // A->P->A
  stoPulseSequenceActive = 5, // P->A-> .. P->A->P
  stoPulseSequencePassive = 6 // A->P-> .. A->P->A
};
class CTriggerOut : public CTriggerBase
{
  protected:
  EStateTriggerOut FState;
  DOnEventTriggerOut FOnEvent;
  UInt32 FTimeWait;
  UInt32 FPulsePeriod;
  UInt32 FPulseWidth;
  UInt32 FPulseCount;
  //  
  public:
  CTriggerOut(const char* pid, int pin, 
              bool inverted = false);
  //
  void SetOnEvent(DOnEventTriggerOut onevent);
  void SetState(EStateTriggerOut state);
  //
  bool Open(void);
  bool Close(void);
  bool SetActive(void);
  bool SetPassive(void);
  bool PulseActive(UInt32 timeout);
  bool PulsePassive(UInt32 timeout);
  bool PulseSequenceActive(UInt32 period, UInt32 width, UInt32 count);
  bool PulseSequencePassive(UInt32 period, UInt32 width, UInt32 count);
  //
  void Execute(void);
};
//
#endif // Trigger_h
//
#endif // DISPATCHER_TRIGGER
//
