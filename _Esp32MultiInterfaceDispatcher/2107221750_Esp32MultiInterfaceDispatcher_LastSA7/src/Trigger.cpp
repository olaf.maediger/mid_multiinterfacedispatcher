//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "Trigger.h"

#include "InterfaceUart.h"

extern CUart UartCommand;
//
//############################################################
//  TriggerBase
//############################################################
CTriggerBase::CTriggerBase(const char* pid, int pin,
                           bool inverted)
{
  strcpy(FID, pid);
  FPin = pin;
  FInverted = inverted;
  FLevel = tlUndefined;
}

ETriggerLevel CTriggerBase::GetLevel(void)
{
  return FLevel;
}
void CTriggerBase::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
//############################################################
//  TriggerIn
//############################################################
//
CTriggerIn::CTriggerIn(const char* pid, int pin, bool inverted)
  : CTriggerBase(pid, pin, inverted)
{
  FOnEvent = 0;
  FState = stiUndefined;
  FOnEvent = 0;
}
//
void CTriggerIn::SetOnEvent(DOnEventTriggerIn onevent)
{
  FOnEvent = onevent;
}
//
void CTriggerIn::SetState(EStateTriggerIn state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGERIN_EVENT_MASK, FID, STATE_TRIGGERIN[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}

Boolean CTriggerIn::Open()
{
  pinMode(FPin, INPUT);
  SetState(stiPassive);
  return true;
}

Boolean CTriggerIn::Close()
{
  pinMode(FPin, INPUT);
  SetLevel(tlPassive);
  SetState(stiPassive);
  return true;
}

bool CTriggerIn::WaitForActive(long unsigned timeout)
{
  if ((stiPassive == FState) || (stiActive == FState))
  {
    FTimeWait = millis();
    FTimeWait += timeout;
    SetState(stiWaitForActive);
    return true;
  }
  return false;
}
//
bool CTriggerIn::WaitForPassive(long unsigned timeout)
{
  if ((stiPassive == FState) || (stiActive == FState))
  {
    FTimeWait = millis();
    FTimeWait += timeout;
    SetState(stiWaitForPassive);
    return true;
  }
  return false;
}

void CTriggerIn::Execute(void)
{  
}
//
//############################################################
//  TriggerOut
//############################################################
CTriggerOut::CTriggerOut(const char* pid, int pin, 
                         bool inverted)
  : CTriggerBase(pid, pin, inverted)
{ 
  FState = stoUndefined;
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
}

void CTriggerOut::SetOnEvent(DOnEventTriggerOut onevent)
{
  FOnEvent = onevent;
}
void CTriggerOut::SetState(EStateTriggerOut state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGEROUT_EVENT_MASK, FID, STATE_TRIGGEROUT[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
Boolean CTriggerOut::Open()
{
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
  pinMode(FPin, OUTPUT);
  SetPassive();
  return true;
}
Boolean CTriggerOut::Close()
{
  FPulsePeriod = 0;
  FPulseWidth = 0;
  FPulseCount = 0;
  SetPassive();
  pinMode(FPin, INPUT);
  return true;
}
//
bool CTriggerOut::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stoActive);
  return true;
}
//
bool CTriggerOut::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stoPassive);
  return true;
}
//
bool CTriggerOut::PulseActive(UInt32 timewait)
{
  switch (FState)
  {
    case stoActive:
    case stoPassive:
    case stoPulseActive:
    case stoPulsePassive:
      SetPassive();
      FTimeWait = millis();
      FTimeWait += timewait;
      SetActive();
      return true;
    default:
      return false;
  }
}
//
bool CTriggerOut::PulsePassive(UInt32 timewait)
{
  switch (FState)
  {
    case stoActive:
    case stoPassive:
    case stoPulseActive:
    case stoPulsePassive:
      SetActive();
      FTimeWait = millis();
      FTimeWait += timewait;
      SetPassive();
      return true;
    default:
      return false;
  }
  return false;
}
//
bool CTriggerOut::PulseSequenceActive(UInt32 period, UInt32 width, UInt32 count)
{
  FPulsePeriod = period;
  FPulseWidth = width;
  FPulseCount = count;
  FPulseIndex = 0;
  SetPassive();
  SetState(stoPulseSequenceActive);
  return true;
}
bool CTriggerOut::PulseSequencePassive(UInt32 period, UInt32 width, UInt32 count)
{
  FPulsePeriod = period;
  FPulseWidth = width;
  FPulseCount = count;
  SetActive();
  SetState(stoPulseSequencePassive);
  return true;
}
//
void CTriggerOut::Execute(void)
{  
  switch (FState)
  {
    case stoActive:
      if (tlPassive == GetLevel())
      {
        SetState(stoPassive);
      }
      break;
    case stoPassive:
      if (tlActive == GetLevel())
      {
        SetState(stoActive);
      }
      break;
    case stoPulseActive:
      if (FTimeWait <= millis())
      {
        SetPassive();
      }
      break;
    case stoPulsePassive:
      if (FTimeWait <= millis())
      {
        SetActive();
      }
      break;
    case stoPulseSequenceActive:
      if (FPulseIndex < FPulseCount)
      {
        FPulseIndex++;
        SetLevel(tlActive);        
      }
      if (FTimeWait <= millis())
      {
        SetPassive();
      }
      break;
    case stoPulseSequencePassive:
      if (FTimeWait <= millis())
      {
        SetActive();
      }
      break;
    default:
      SetState(stoUndefined);
  }
}





















//
#endif // DISPATCHER_TRIGGER
//
/*

void CTriggerOut::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//





    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stoActive);
      }
      else
        if (FTimeWait <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stoActive);
          }
          else
          {
            SetState(stoPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;



//
void CTrigger::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
bool CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
  return true;
}
//
bool CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
  return true;
}
//
bool CTrigger::PulseActive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlPassive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulseActive);
      return true;
    default:
      return false;
  }
}
//
bool CTrigger::PulsePassive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlActive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulsePassive);
      return true;
    default:
      return false;
  }
  return false;
}
//

void CTriggerBase::SetState(EStateTrigger state)
{
  if (state != FState)
  {
    FState = state;
    if (FOnEvent) 
    {
      sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, STATE_TRIGGER[FState]);
      FOnEvent(this, FBuffer);
    }
  }
}
//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

//
ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}
*/



/*
Boolean override CTriggerBase::Open()
{
  base.Open();
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  SetLevel(tlPassive);
  return true;
}

Boolean CTriggerBase::Close()
{
  SetPassive();
  SetLevel(tlPassive);
  pinMode(FPin, INPUT);
  return true;
}




//
void CTriggerIn::Execute(void)
{  
  switch (FState)
  {
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    default:
      SetState(stiUndefined);
  }
}


Boolean CTrigger::Open()
{
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  SetLevel(tlPassive);
  SetState(stPassive);
  return true;
}

Boolean CTrigger::Close()
{
  SetPassive();
  SetLevel(tlPassive);
  pinMode(FPin, INPUT);
  SetState(stPassive);
  return true;
}



ool CTrigger::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
//
bool CTrigger::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}

//
void CTrigger::SetLevel(ETriggerLevel level)
{
  FLevel = level;
}
//
bool CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
  SetState(stActive);
  return true;
}
//
bool CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
  SetState(stPassive);
  return true;
}
//
bool CTrigger::PulseActive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlPassive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulseActive);
      return true;
    default:
      return false;
  }
}
//
bool CTrigger::PulsePassive(long unsigned timeout)
{
  switch (FState)
  {
    case stActive:
    case stPassive:
    case stPulseActive:
    case stPulsePassive:
      SetLevel(tlActive);
      FTimeOut = millis();
      FTimeOut += timeout;
      SetState(stPulsePassive);
      return true;
    default:
      return false;
  }
  return false;
}
//
bool CTrigger::PulseSequence(UInt32 period, UInt32 count)
{

}
bool CTrigger::PulseSequence(UInt32 period, UInt32 width, UInt32 count);
{

}
//
void CTrigger::Execute(void)
{  
  switch (FState)
  {
    case stActive:
      if ((tdInput == FDirection) && (tlPassive == GetLevel()))
      {
        SetState(stPassive);
      }
      break;
    case stPassive:
      if ((tdInput == FDirection) && (tlActive == GetLevel()))
      {
        SetState(stActive);
      }
      break;
    case stPulseActive:
      if (FTimeOut <= millis())
      {
        SetPassive();
      }
      break;
    case stPulsePassive:
      if (FTimeOut <= millis())
      {
        SetActive();
      }
      break;
    case stWaitForActive:
      if (tlActive == GetLevel())
      {
        SetState(stActive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    case stWaitForPassive:
      if (tlPassive == GetLevel())
      {
        SetState(stPassive);
      }
      else
        if (FTimeOut <= millis())
        {
          if (tlActive == GetLevel())
          {
            SetState(stActive);
          }
          else
          {
            SetState(stPassive);
          }
        }
      break;
    default:
      SetState(stUndefined);
  }
}

*/

/*
ool CTriggerOut::WaitForActive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForActive);
    return true;
  }
  return false;
}
//
bool CTriggerOut::WaitForPassive(long unsigned timeout)
{
  if ((stPassive == FState) || (stActive == FState))
  {
    FTimeOut = millis();
    FTimeOut += timeout;
    SetState(stWaitForPassive);
    return true;
  }
  return false;
}



ETriggerLevel CTriggerOut::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

*/