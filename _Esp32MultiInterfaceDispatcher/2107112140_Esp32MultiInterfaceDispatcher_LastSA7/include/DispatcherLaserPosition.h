//
#include "Defines.h"
//
#if defined(DISPATCHER_LASERPOSITION)
//
#ifndef DispatcherLaserPosition_h
#define DispatcherLaserPosition_h
//
#include "Dispatcher.h"
#include "Led.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - LaserPosition - SHORT
// //----------------------------------------------------------------
#define SHORT_PLP   ""
#define SHORT_ALP   ""
#define SHORT_PLR   ""
#define SHORT_ALR   ""
#define SHORT_PLC   ""
#define SHORT_ALC   ""
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LASERPOSITION  " Help (LaserPosition):"
#define MASK_PLP               " %-3s                : "
#define MASK_ALP               " %-3s                : "
#define MASK_PLR               " %-3s                : "
#define MASK_ALR               " %-3s <n> <p> <w>    : "
#define MASK_PLC               " %-3s                : "
#define MASK_ALC               " %-3s                : "
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition
//----------------------------------------------------------------
class CDispatcherLaserPosition : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLaserPosition(void);
  //
  // bool ExecuteGetLaserPosition(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLaserPosition(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemBlink(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemAbort(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherLaserPosition_h
//
#endif // DISPATCHER_LASERPOSITION
//
//###################################################################################
//###################################################################################
//###################################################################################
