//
#include "Defines.h"
//
#if defined(DISPATCHER_LASERRANGE)
//
#ifndef DispatcherLaserRange_h
#define DispatcherLaserRange_h
//
#include "Dispatcher.h"
#include "Led.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - LaserRange - SHORT
// //----------------------------------------------------------------
#define SHORT_GPX   "GPX"
#define SHORT_SPX   "SPX"
#define SHORT_GPY   "GPY"
#define SHORT_SPY   "SPY"
#define SHORT_GRX   "GRX"
#define SHORT_SRX   "SRX"
#define SHORT_GRY   "GRY"
#define SHORT_SRY   "SRY"
#define SHORT_GDM   "GDM"
#define SHORT_SDM   "SDM"
#define SHORT_GPW   "GPW"
#define SHORT_SPW   "SPW"
//
//----------------------------------------------------------------
// Dispatcher - LaserRange - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LASERRANGE " Help (LaserRange):"
#define MASK_GPX               " %-3s                : "
#define MASK_SPX               " %-3s                : "
#define MASK_GPY               " %-3s                : "
#define MASK_SPY               " %-3s <n> <p> <w>    : "
#define MASK_GRX               " %-3s                : "
#define MASK_SRX               " %-3s                : "
#define MASK_GRY               " %-3s                : "
#define MASK_SRY               " %-3s                : "
#define MASK_GDM               " %-3s <n> <p> <w>    : "
#define MASK_SDM               " %-3s                : "
#define MASK_GPW               " %-3s                : "
#define MASK_SPW               " %-3s                : "
//
//----------------------------------------------------------------
// Dispatcher - LedSystem
//----------------------------------------------------------------
class CDispatcherLaserRange : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLaserRange(void);
  //
  // bool ExecuteGetLedSystem(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOn(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemBlink(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemAbort(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherLaserRange_h
//
#endif // DISPATCHER_LASERRANGE
//
//###################################################################################
//###################################################################################
//###################################################################################
