//
//--------------------------------
//  Library DacMcp4725
//--------------------------------
//
#include "DefinitionSystem.h"
//
#ifndef DacMcp4725_h
#define DacMcp4725_h
//
#include <Wire.h>
//
//--------------------------------
//  Trigger - Constant
//--------------------------------
const static Byte MCP4725_I2CADDRESS_0x60   = 0x60;
const static Byte MCP4725_I2CADDRESS_0x61   = 0x61;
const static Byte MCP4725_I2CADDRESS_0x62   = 0x62;  // PSimon 1808301555_LASController_07V03_PS
const static Byte MCP4725_I2CADDRESS_0x63   = 0x63;
const static Byte MCP4725_I2CADDRESS_0x64   = 0x64;
const static Byte MCP4725_I2CADDRESS_0x65   = 0x65;
//
const static Byte MCP4725_WRITEDAC          = 0x40;
const static Byte MCP4725_WRITEDACEEPROM    = 0x20;
//
const int DACMCP4725_SIZE_ID                = 6;
const int DACMCP4725_SIZE_BUFFER            = 32;
//
const char* const DACMCP4725_EVENT_MASK     = "DacMCP4725[%s]=%u";
//
//--------------------------------
//  DacMcp4725 - Type
//--------------------------------
class CDacMcp4725;
typedef void (*DOnEventDacMcp4725)(CDacMcp4725* dac, const char* event);
//
class CDacMcp4725
{
  private:
  char FID[DACMCP4725_SIZE_ID];
  char FBuffer[DACMCP4725_SIZE_BUFFER];
  Byte FI2CAddress;
  UInt32 FValue;
  UInt32 FRangeLow;
  UInt32 FRangeHigh;
  UInt32 FRangeDelta;
  DOnEventDacMcp4725 FOnEvent;
  
  public:
  CDacMcp4725(const char* pid, Byte i2caddress);
  void SetOnEvent(DOnEventDacMcp4725 onevent);
  //
  Boolean Open();
  Boolean Close();
  //
  UInt32 GetValue();
  void SetValue(UInt32 value);
  //
  UInt32 GetRangeLow(void);
  void SetRangeLow(UInt32 value);
  UInt32 GetRangeHigh(void);
  void SetRangeHigh(UInt32 value);
  UInt32 GetRangeDelta(void);
  void SetRangeDelta(UInt32 value);
  //
  bool Execute(void);
};
//
#endif // DacMcp4725_h
