//
//--------------------------------
//  Library Error
//--------------------------------
//
#include "Defines.h"
//
#ifndef Error_h
#define Error_h
//
#include <Arduino.h>
//
//--------------------------------
//  Section - Definition
//--------------------------------
//##############################################################
//  Definition - Error-Codes
//##############################################################
enum EErrorCode
{
  ecNone                      = (int)0,  
  ecUnknown                   = (int)1,
  ecInvalidCommand            = (int)2,
  ecTimingFailure             = (int)3,
  ecInvalidParameter          = (int)4,
  ecToManyParameters          = (int)5,
  ecNotEnoughParameters       = (int)6,
  ecMissingParameter          = (int)7,
  ecMountSDCard               = (int)8,
  ecOpenCommandFile           = (int)9,
  ecParseCommandFile          = (int)10,
  ecWriteCommandFile          = (int)11,
  ecCloseCommandFile          = (int)12,
  ecUnmountSDCard             = (int)13,
  ecFileNotOpened             = (int)14
};
//
#define ERROR_NONE                    (char*)"None"
#define ERROR_UNKNOWN                 (char*)"Unknown"
#define ERROR_INVALIDCOMMAND          (char*)"Invalid Command"
#define ERROR_TIMINGFAILURE           (char*)"Timing Failure"
#define ERROR_INVALIDPARAMETER        (char*)"Invalid Parameter"
#define ERROR_TOMANYPARAMETERS        (char*)"To many Parameters"
#define ERROR_NOTENOUGHPARAMETERS     (char*)"Not enough Parameters"
#define ERROR_MISSINGPARAMETER        (char*)"Missing Parameter"
#define ERROR_MOUNTSDCARD             (char*)"Mount SDCard"
#define ERROR_OPENCOMMANDFILE         (char*)"Open Commandfile"
#define ERROR_PARSECOMMANDFILE        (char*)"Parse Commandfile"
#define ERROR_WRITECOMMANDFILE        (char*)"Write Commandfile"
#define ERROR_CLOSECOMMANDFILE        (char*)"Close Commandfile"
#define ERROR_UNMOUNTSDCARD           (char*)"Unmount SDCard"
#define ERROR_FILENOTOPENED           (char*)"File not opened"
//
//
const int ERROR_BUFFERSIZE = 48;
//
class CError
{
  private:
  enum EErrorCode FErrorCode;
  char FBuffer[ERROR_BUFFERSIZE];
  //
  const char* ErrorCodeText(EErrorCode errorcode);
  //
  public:
  CError(void);
  //
  EErrorCode GetCode(void);
  void SetCode(EErrorCode errorcode);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  Boolean Handle(void);
};
//
#endif // Error_h
