//
#include "Defines.h"
//
#if defined(COMMAND_COMMON)
//
#ifndef CommandCommon_h
#define CommandCommon_h
//
#include <string.h>
//
//----------------------------------------------------------------
// Help - Command - SHORT - Definition
//----------------------------------------------------------------
#define SHORT_H     "H"                     // H - (this) Help
//
//----------------------------------------------------------------
// Help - Command - MASK - Definition
//----------------------------------------------------------------
#define TITLE_LINE                "--------------------------------------------------"
#define MASK_PROJECT              "- Project:   %-35s -"
#define MASK_SOFTWARE             "- Version:   %-35s -"
#define MASK_HARDWARE             "- Hardware:  %-35s -"
#define MASK_DATE                 "- Date:      %-35s -"
#define MASK_TIME                 "- Time:      %-35s -"
#define MASK_AUTHOR               "- Author:    %-35s -"
#define MASK_PORT                 "- Port:      %-35s -"
#define MASK_PARAMETER            "- Parameter: %-35s -"
//
#define HELP_COMMAND_COMMON       " Help (Common):"
#define MASK_H                    " %-3s                  : This Help"
//

class CCommandCommon
{
  public:
  CCommandCommon(void);
  //
  bool ExecuteGetProgramHeader(void);
  //
  bool Execute(char* pcommand);
};

class CCommandBt
{
  public:
  void Hello(void)
  {
    Serial.println(34);
  }

};

//
#endif // Command_h
//
#endif // COMMAND_COMMON
//