//
#include "DefinitionSystem.h"
//
#ifndef Command_h
#define Command_h
//
#include <string.h>
#include <stdio.h>
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//----------------------------------------------------------------
// Command - Constant
//----------------------------------------------------------------
//
const int COMMAND_SIZE_BUFFER = 32;
//
#define MASK_STATECOMMAND "Command %s"
//
#if defined(INTERFACE_UART)
const int UART_COMMANDSIZE = 32;
const int UART_PARAMETERCOUNT = 4;
const int UART_PARAMETERSIZE = 32;
const int UART_BLOCKSIZE = 64;
#endif
#if defined(INTERFACE_BT)
const int BT_COMMANDSIZE = 32;
const int BT_PARAMETERCOUNT = 4;
const int BT_PARAMETERSIZE = 32;
const int BT_BLOCKSIZE = 64;
#endif
#if defined(INTERFACE_WLAN)
const int WLAN_COMMANDSIZE = 32;
const int WLAN_PARAMETERCOUNT = 4;
const int WLAN_PARAMETERSIZE = 32;
const int WLAN_BLOCKSIZE = 64;
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
const int MQTT_COMMANDSIZE = 32;
const int MQTT_PARAMETERCOUNT = 4;
const int MQTT_PARAMETERSIZE = 32;
const int MQTT_BLOCKSIZE = 64;
#endif
//
//
#define MASK_ENDLINE "###"
//
//----------------------------------------------------------------
// Command - Type
//----------------------------------------------------------------
enum EStateCommand
{
  scError     = -2,
  scUndefined = -1,
  scInit      = 0,
  scIdle      = 1,
  scBusy      = 2
};
//
class CCommand 
{
  private:
  EStateCommand FState;
  char FBuffer[COMMAND_SIZE_BUFFER];
  // Uart
#if defined (INTERFACE_UART)
  PCharacter FCommandUart;
  VPCharacter FParametersUart;
  Byte FParameterCountUart = 0;
  Character FBlockUart[UART_BLOCKSIZE];
#endif
  // Bt
#if defined(INTERFACE_BT)  
  PCharacter FCommandBt;
  VPCharacter FParametersBt;
  Byte FParameterCountBt = 0;
  Character FBlockBt[BT_BLOCKSIZE];
#endif
  // Wlan
#if defined(INTERFACE_WLAN)  
  PCharacter FCommandWlan;
  VPCharacter FParametersWlan;
  Byte FParameterCountWlan = 0;
  Character FBlockWlan[WLAN_BLOCKSIZE];
#endif
  // Lan
#if defined(INTERFACE_LAN)
#endif
  // Mqtt
#if defined(PROTOCOL_MQTT)
  PCharacter FMqttCommand;
  VPCharacter FMqttParameters;
  Byte FMqttParameterCount = 0;
  Character FMqttBlock[MQTT_BLOCKSIZE];
#endif
  //    
  public:
  // Constructor
  CCommand(void);
  ~CCommand(void);
  // Helper
  const char* StateText(EStateCommand state);
  // Property
  EStateCommand GetState(void);
  void SetState(EStateCommand state);
  //
  char* GetBuffer(void)
  {
    return FBuffer;
  }
  //
#if defined(INTERFACE_UART)
  PCharacter GetCommandUart(void);
  Int16 GetParameterCountUart(void);
  VPCharacter GetParametersUart(void);
  void ResetCommandUart(void);
#endif
#if defined(INTERFACE_BT)
  PCharacter GetCommandBt(void);
  Int16 GetParameterCountBt(void);
  VPCharacter GetParametersBt(void);
  void ResetCommandBt(void);
#endif
#if defined(INTERFACE_WLAN)
  PCharacter GetCommandWlan(void);
  Int16 GetParameterCountWlan(void);
  VPCharacter GetParametersWlan(void);
  void ResetCommandWlan(void);
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
  PCharacter GetMqttCommand(void);
  Int16 GetMqttParameterCount(void);
  VPCharacter GetMqttParameters(void);
  void ResetMqttCommand(void);
#endif
  // Manager
  bool Open(void);
  bool Close(void);
  //
  bool AnalyseInterfaceBlock(void);
};
//
#endif // Command_h
//
