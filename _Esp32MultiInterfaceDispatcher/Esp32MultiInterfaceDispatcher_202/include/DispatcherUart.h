//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_UART)
//
#ifndef DispatcherUart_h
#define DispatcherUart_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Uart - SHORT
//----------------------------------------------------------------
#define SHORT_WCU   "WCU"                   // WUC - Write Command Uart 
// NC #define SHORT_RCU   "RCU"                   // RUC - Read Command Uart 
//
//----------------------------------------------------------------
// Dispatcher - Uart - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_UART       " Help (Uart):"
#define MASK_WCU                " %-3s <c> <p>         : Write Uart <c>ommand <p>arameter"
// NC #define MASK_RCU                " %-3s                 : <r>ead <c>ommand <u>art"
//
//----------------------------------------------------------------
// Dispatcher - Uart
//----------------------------------------------------------------
class CDispatcherUart : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherUart(void);
  //
  bool ExecuteWriteCommandUart(char* command, int parametercount, char** parameters);
  // NC bool ExecuteReadCommandUart(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherUart_h
//
#endif // DISPATCHER_UART
//
