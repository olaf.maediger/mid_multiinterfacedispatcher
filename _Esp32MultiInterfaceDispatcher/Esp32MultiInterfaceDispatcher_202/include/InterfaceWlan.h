#include "DefinitionSystem.h"
//
#ifdef INTERFACE_WLAN
//
#ifndef InterfaceWlan_h
#define InterfaceWlan_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
#include "ConnectionWifi.h"
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const byte WLAN_SIZE_RXBLOCK = 32;
const byte WLAN_SIZE_TXBLOCK = 32;
const Boolean WLAN_INIT_RXECHO    = true;
//
//----------------------------------------------------
//  CInterfceWlanBase
//----------------------------------------------------
class CInterfaceWlanBase : public CInterfaceBase
{
  public:
  CInterfaceWlanBase(const char* deviceid);
  // Handler
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  // Read
  virtual UInt8 GetRxCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* pline, int &size) = 0;
  // Write
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
};
//
//----------------------------------------------------
//  CInterfaceWlanWIS
//----------------------------------------------------
class CInterfaceWlanWIS : public CInterfaceWlanBase
{
  protected:
  CWifiStation* FPWifiStation;
  
  public:
  CInterfaceWlanWIS(const char* deviceid, 
                    CWifiStation* pwifistation);
 // Handler
  virtual Boolean Open(int parameter);
  virtual Boolean Close(void);
  // Read
  virtual UInt8 GetRxCount(void);
  virtual Boolean Read(char &character);
  virtual Boolean ReadLine(char* pline, int &size);
  // Write
  virtual Boolean Write(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
};
//
//----------------------------------------------------
//  CInterfaceWlan
//----------------------------------------------------
class CInterfaceWlan
{
  protected:
  CInterfaceWlanWIS* FPWlanBase;
  //
  public:
  // Constructor
  CInterfaceWlan(const char* deviceid, 
                 CWifiStation* pwifistation);
  // Handler
  Boolean Open(int parameter);
  Boolean Close(void);
  // Property
  char* GetRxBlock(void)
  {
    return FPWlanBase->GetRxBlock();
  }
  char* GetTxBlock(void)
  {
    return FPWlanBase->GetTxBlock();
  }
  Boolean GetRxEcho(void)
  {
    return FPWlanBase->GetRxEcho();
  }
  void SetRxEcho(Boolean rxecho)
  {
    FPWlanBase->SetRxEcho(rxecho);
  }
  // Read
  UInt8 GetRxCount(void);
  Boolean Read(char &rxcharacter);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  //
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  //Boolean WritePrompt(void);
  //Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  Boolean Execute(void);
};
//
#endif // InterfaceWlan_h
//
#endif // INTERFACE_WLAN
//
//
