//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_BT)
//
#ifndef DispatcherBt_h
#define DispatcherBt_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Bt - SHORT
//----------------------------------------------------------------
#define SHORT_WCB   "WCB"                   // WCB - Write Command Bt
// NC #define SHORT_RCB   "RCB"                   // RCB - Read Command Bt 
//
//----------------------------------------------------------------
// Dispatcher - Bt - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_BT       " Help (Bt):"
#define MASK_WCB              " %-3s <c> <p>         : Write Bt <c>ommand <p>arameter"
// NC #define MASK_RCB        " %-3s                 : Read Bt Command / Parameter"
//
//----------------------------------------------------------------
// Dispatcher - Bt
//----------------------------------------------------------------
class CDispatcherBt : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherBt(void);
  //
  bool ExecuteWriteCommandBt(char* command, int parametercount, char** parameters);
  // NC bool ExecuteReadCommandBt(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherBt_h
//
#endif // DISPATCHER_BT
//
