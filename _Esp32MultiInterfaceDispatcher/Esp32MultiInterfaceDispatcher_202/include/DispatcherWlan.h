//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_WLAN)
//
#ifndef DispatcherWlan_h
#define DispatcherWlan_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Wlan - SHORT
//----------------------------------------------------------------
#define SHORT_WCW   "WCW"                   // WCW - Write Command Wlan
// NC #define SHORT_RCB   "RCB"             // RCW - Read Command Wlan
//
//----------------------------------------------------------------
// Dispatcher - Wlan - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_WLAN     " Help (Wlan):"
#define MASK_WCW              " %-3s <c> <p>         : Write Wlan <c>ommand <p>arameter"
// NC #define MASK_RCW        " %-3s                 : Read Wlan Command / Parameter"
//
//----------------------------------------------------------------
// Dispatcher - Wlan
//----------------------------------------------------------------
class CDispatcherWlan : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherWlan(void);
  //
  bool ExecuteWriteCommandWlan(char* command, int parametercount, char** parameters);
  // NC bool ExecuteReadCommandWlan(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherWlan_h
//
#endif // DISPATCHER_WLAN
//
