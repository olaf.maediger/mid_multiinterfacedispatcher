 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include <Arduino.h>
//
//##############################################################
//  Definition - Project
//##############################################################
//
#define ARGUMENT_PROJECT    "MID - Esp32MultiInterfaceDispatcher"
#define ARGUMENT_SOFTWARE   "00V40"
#define ARGUMENT_DATE       "210920"
#define ARGUMENT_TIME       "2003"
#define ARGUMENT_AUTHOR     "OMDevelop"
//
//##############################################################
//  Definition - Command
//##############################################################
//--------------------------------
//#undef INTERFACE_UART
#define INTERFACE_UART
#if defined(INTERFACE_UART)
#define ARGUMENT_UART_PORT       "Uart(COM)"
#define ARGUMENT_UART_PARAMETER  "115200, N, 8, 1"
#endif
//
#undef INTERFACE_BT
// #define INTERFACE_BT
#if defined(INTERFACE_BT)
#define ARGUMENT_BT_PORT         "Bt(COM)"
#define ARGUMENT_BT_PARAMETER    "Bt-Parameter"
#endif
//
#define CONNECTION_WIFI
// #undef  CONNECTION_WIFI
#if defined(CONNECTION_WIFI)
#define WIFI_SSID                "TPL1300SA7AZ"
#define WIFI_PASSWORD            "01234567890123456789"
#define INTERFACE_WLAN
// #undef  INTERFACE_WLAN
#endif
#if defined(INTERFACE_WLAN)
#define ARGUMENT_WLAN_PORT       "Wlan(192.168.178.23:80)"
#define ARGUMENT_WLAN_PARAMETER  "Wlan-Parameter"
#endif
//
#undef INTERFACE_LAN
// #define INTERFACE_LAN
#if defined(INTERFACE_LAN)
#define ARGUMENT_LAN_PORT        "Lan(192.168.178.25:80)"
#define ARGUMENT_LAN_PARAMETER   "Lan-Parameter"
#endif
//
#undef PROTOCOL_MQTT
// #define PROTOCOL_MQTT
//
#undef PROTOCOL_SDCARD
// // !!!# define PROTOCOL_SDCARD
//
//-------------------------------
// #undef DISPATCHER_COMMON
#define DISPATCHER_COMMON
// //
// #undef DISPATCHER_SYSTEM
#define DISPATCHER_SYSTEM
// 
// #undef DISPATCHER_UART
#define DISPATCHER_UART
//
#undef DISPATCHER_BT
// #define DISPATCHER_BT
//
// #undef DISPATCHER_WLAN
#define DISPATCHER_WLAN
//
#undef DISPATCHER_LAN
// #define DISPATCHER_LAN
//
#undef DISPATCHER_SDCARD
// #define DISPATCHER_SDCARD
//
#undef DISPATCHER_MQTT
// #define DISPATCHER_MQTT
//
#undef DISPATCHER_RTC
// #define DISPATCHER_RTC
//
#undef DISPATCHER_NTP
// #define DISPATCHER_NTP
//
#undef DISPATCHER_WATCHDOG
// #define DISPATCHER_WATCHDOG
//
#undef DISPATCHER_I2CDISPLAY
// #define DISPATCHER_I2CDISPLAY
//
// #undef DISPATCHER_LEDSYSTEM
#define DISPATCHER_LEDSYSTEM
//
//##############################################################
//  Definition - Processor
//##############################################################
//----------------------------------
// ARDUINO NANOR3 :
// #define PROCESSOR_NANOR3
//----------------------------------
//----------------------------------
// ARDUINO UNOR3 :
// #define PROCESSOR_UNOR3
//----------------------------------
// Arduino Mega2560
// #define PROCESSOR_MEGA2560
//----------------------------------
//----------------------------------
// Arduino DueM3
// #define PROCESSOR_DUEM3
//----------------------------------
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//----------------------------------
//----------------------------------
// STM32F407VG 
// #define PROCESSOR_STM32F407VG
//----------------------------------
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//----------------------------------
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//----------------------------------
//----------------------------------
// Esp8266
// #define PROCESSOR_ESP8266
//----------------------------------
//----------------------------------
// Esp32
// 
#define PROCESSOR_ESP32
//----------------------------------
//
//##############################################################
//  Definition - Output
//##############################################################
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_STM32F407VG
#define ARGUMENT_HARDWARE "Stm32F407VG"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
#ifdef PROCESSOR_ESP8266
#define ARGUMENT_HARDWARE "Esp8266"
#endif
#ifdef PROCESSOR_ESP32
#define ARGUMENT_HARDWARE "Esp32"
#endif
//
//##############################################################
//  Definition - Global
//##############################################################
//
#define TRUE                    1
#define FALSE                   0
#define INIT_ERRORCODE          ecNone
#define INIT_RXDECHO            false
#define NOT_INVERTED            false
#define INVERTED                true
//
#define SEPARATOR_ZERO          0x00
#define SEPARATOR_CR            0x0D
#define SEPARATOR_LF            0x0A
// ->
#define TERMINAL_COMMAND        "@"
// <-
#define TERMINAL_SPACE          " "
#define TERMINAL_RESPONSE       "&"
#define TERMINAL_EVENT          "!"
#define TERMINAL_COMMENT        "#"
#define TERMINAL_DEBUG          "|"
#define TERMINAL_ERROR          "?"
#define TERMINAL_WARNING        "%"
#define TERMINAL_TEXT           "\""
//
#define TOKEN_NEWLINE           "\r\n"
//
// 
// //##########################################
// //  PROCESSOR_NANOR3
// //##########################################
// #if defined(PROCESSOR_NANOR3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
//
// #endif // PROCESSOR_NANOR3
//
// //##########################################
// //  PROCESSOR_UNOR3
// //##########################################
// #if defined(PROCESSOR_UNOR3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_UNOR3
// //
// //##########################################
// //  PROCESSOR_MEGA2560
// //##########################################
// #if defined(PROCESSOR_MEGA2560)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 int 
// #define UInt16 unsigned int
// #define Int32 long int
// #define UInt32 long unsigned int
// #define Int64 long long int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_MEGA2560
// //
// //##########################################
// //  PROCESSOR_DUEM3
// //##########################################
// #if defined(PROCESSOR_DUEM3)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_DUEM3
// //
// //##########################################
// //  PROCESSOR_STM32F103C8
// //##########################################
// #if defined(PROCESSOR_STM32F103C8)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// // Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
// //
// // Forward-Declaration IRQ SlitPX
// // example: void ISREncoderSlitPXPulseA(void);
// // example: void ISREncoderSlitPXPulseB(void);
// //
// #define SERIALCOMMAND    Serial1   // CommunicationPC
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALPRINTER    Serial3   // Serial-Printer
// //
// #endif // PROCESSOR_STM32F103C8
// //
// //##########################################
// //  PROCESSOR_STM32F407VG
// //##########################################
// #if defined(PROCESSOR_STM32F407VG)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float32 float
// #define Double64 double
// //
// // Type - IRQ-Handler
// typedef void (*TInterruptFunction)(void);
// //
// // Definition - Serial 
// #define SERIALCOMMAND    Serial1   // CommunicationPC
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALPRINTER    Serial3   // Serial-Printer
// #define SERIALDEBUG4     Serial4   // Serial-Printer
// #define SERIALDEBUG5     Serial5   // Serial-Printer
// #define SERIALDEBUG6     Serial5   // Serial-Printer
// //
// #endif // PROCESSOR_STM32F103C8
// //
// //
// //##########################################
// //  PROCESSOR_TEENSY32
// //##########################################
// #if defined(PROCESSOR_TEENSY32)
// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// // Choose 72MHz and not 80MHz(Overclocked) !
// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 signed int 
// #define UInt16 unsigned int
// #define Int32 long signed int
// #define UInt32 long unsigned int
// #define Int64 long long signed int
// #define UInt64 long long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_TEENSY32
// //
// ////##########################################
// ////  PROCESSOR_TEENSY36
// ////##########################################
// #if defined(PROCESSOR_TEENSY36)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// #endif // PROCESSOR_TEENSY36
// //
// //##########################################
// //  PROCESSOR_ESP8266
// //##########################################
// #if defined(PROCESSOR_ESP8266)
// //
// #define Boolean bool
// #define Character char
// #define PCharacter char*
// #define Byte byte
// #define Int8 signed char
// #define UInt8 unsigned char
// #define Int16 short int 
// #define UInt16 short unsigned int
// #define Int32 int
// #define UInt32 unsigned int
// #define Int64 long int
// #define UInt64 long unsigned int
// #define Float float
// #define Double double
// //
// // Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
// // Forward-Declaration IRQ Function
// // example: void ISREncoderSlitPXPulseA(void);
// //
// #define SERIALUSBPROGRAM SerialUSB // Serial-USBProgramming
// #define SERIALPRINTER    Serial1   // Serial-Printer
// #define SERIALNETWORK    Serial2   // Serial-LAN
// #define SERIALCOMMAND    Serial3   // CommunicationPC
// //
// #endif 
// // PROCESSOR_ESP8266
//
//##########################################
//  PROCESSOR_ESP32
//##########################################
#if defined(PROCESSOR_ESP32)
//------------------------------------------------------
// Segment - Global Type
//------------------------------------------------------
typedef char Character;
typedef char* PCharacter;
typedef PCharacter* VPCharacter;
//
#define Boolean bool
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int16_t
#define UInt16 uint16_t
#define Int32 int32_t
#define UInt32 uint32_t
#define Int64 int64_t
#define UInt64 uint64_t
#define Float32 float
#define Double64 double
//-------------------------------------------------
// Type IRQ Function
// typedef void (*TInterruptFunction)(void); 
//-------------------------------------------------
#endif // PROCESSOR_ESP32
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
#endif // Defines_h
