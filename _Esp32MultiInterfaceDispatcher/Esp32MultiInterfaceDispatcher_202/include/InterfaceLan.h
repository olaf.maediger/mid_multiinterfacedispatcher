#include "Defines.h"
//
#ifdef INTERFACE_LAN
//
#ifndef InterfaceLan_h
#define InterfaceLan_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
//
//----------------------------------------------------
//  CLanBase
//----------------------------------------------------
class CLanBase : public CInterfaceBase
{
  private:
  //
  public:
  CLanBase(void);
  //
  Boolean Open(int baudrate);
  Boolean Close(void);
  //
  Boolean WriteLine(char* line);
  //
  Boolean ReadLine(char* rxline, int *rxsize);
};
//
//----------------------------------------------------
//  CLanHW
//----------------------------------------------------
class CLanHW : public CLanBase
{
  protected:
  // !!! HardwareLan* FPLan;
  
  public:
  CLanHW(void);// !!!  HardwareLan *lan);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  Boolean WriteLine(char* line);
  //
  Boolean ReadLine(char* rxline, int *rxsize);
};

class CLan
{
  private:
  CLanBase* FPLanBase;
  //
  public:
  CLan(void); // HardwareSerial* pserial);
  //
  // Property
  //
  // Handler
  Boolean Open(void); //int baudrate);
  Boolean Close(void);
  //
  // Write
  Boolean WriteNewLine(void);
  Boolean WriteText(const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WritePChar(const char* mask, char* value = 0);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WritePrompt(void);
  Boolean WriteEvent(const char* text, String event);
  Boolean WriteComment(const char* text, String comment);
  //
  // Read
  Boolean ReadLine(char* rxline, int *rxsize);
};
//
#endif // InterfaceLan_h
//
#endif // INTERFACE_LAN
//