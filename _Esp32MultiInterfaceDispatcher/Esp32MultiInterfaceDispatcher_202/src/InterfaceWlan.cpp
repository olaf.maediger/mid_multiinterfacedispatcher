//
#include "DefinitionSystem.h"
//
#if defined(INTERFACE_WLAN)
//
#include "Error.h"
#include "InterfaceWlan.h"
//
extern CError Error;
//
//####################################################
//  CInterfaceWlanBase
//####################################################
//----------------------------------------------------
//  CInterfaceWlanBase - Constructor
//----------------------------------------------------
// CInterfaceBtBase(abstract!) - CInterfaceBase(abstract!)
CInterfaceWlanBase::CInterfaceWlanBase(const char* deviceid)
  : CInterfaceBase(deviceid)
{
  FRxEcho = WLAN_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBtBase - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Read
//----------------------------------------------------


//
//----------------------------------------------------
//  CInterfaceWlanWIS
//----------------------------------------------------
CInterfaceWlanWIS::CInterfaceWlanWIS(const char* deviceid, 
                                     CWifiStation* pwifistation)
  : CInterfaceWlanBase(deviceid)
{
  FPWifiStation = pwifistation;
}                                     
// Handler
Boolean CInterfaceWlanWIS::Open(int parameter)
{
  return (FPWifiStation->IsConnected());
}
Boolean CInterfaceWlanWIS::Close(void)
{
  return false;
}
// Read
UInt8 CInterfaceWlanWIS::GetRxCount(void)
{
  return 0;
}
Boolean CInterfaceWlanWIS::Read(char &character)
{
  return false;
}
Boolean CInterfaceWlanWIS::ReadLine(char* pline, int &size)
{
  size = 0;
  return false;
}
// Write
Boolean CInterfaceWlanWIS::Write(char character)
{
  return false;
}
Boolean CInterfaceWlanWIS::WriteText(char* ptext)
{
  return false;
}
Boolean CInterfaceWlanWIS::WriteLine(void)
{
  return false;
}
Boolean CInterfaceWlanWIS::WriteLine(char* pline)
{
  return false;
}
//
//####################################################
//  CInterfaceWlan
//####################################################
//----------------------------------------------------
//  CInterfaceWlan - Constructor
//----------------------------------------------------
CInterfaceWlan::CInterfaceWlan(const char* deviceid,  
                               CWifiStation* pwifistation)
{
  FPWlanBase = new CInterfaceWlanWIS(deviceid, pwifistation);
}
//
//----------------------------------------------------
//  CInterfaceBt - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBt - Handler
//----------------------------------------------------
Boolean CInterfaceWlan::Open(int parameter)
{
  return FPWlanBase->Open(parameter);
}
Boolean CInterfaceWlan::Close(void)
{
  return FPWlanBase->Close();
}
//
//----------------------------------------------------
//  CInterfaceBt - Read
//----------------------------------------------------
UInt8 CInterfaceWlan::GetRxCount(void)
{
  return FPWlanBase->GetRxCount();
}
Boolean CInterfaceWlan::Read(char &character)
{
  return FPWlanBase->Read(character);
}
// true: whole line with cr/lf found
Boolean CInterfaceWlan::ReadLine(char* prxline, int &rxsize)
{
  return FPWlanBase->ReadLine(prxline, rxsize);
}
//
//----------------------------------------------------
// CInterfaceUart - Write
//----------------------------------------------------
Boolean CInterfaceWlan::WriteLine(void)
{
  return FPWlanBase->WriteLine();
}
Boolean CInterfaceWlan::WriteText(const char* ptext)
{
  return FPWlanBase->WriteText((char*)ptext);
}
Boolean CInterfaceWlan::WriteText(const char* mask, const char* ptext)
{
  if (0 != ptext)
  {
    return FPWlanBase->WriteText(FPWlanBase->ConvertPChar(mask, (char*)ptext));
  }  
  return FPWlanBase->WriteText(FPWlanBase->ConvertPChar(mask, (char*)""));
}
Boolean CInterfaceWlan::WriteLine(const char* pline)
{
  return FPWlanBase->WriteLine((char*)pline);
}
Boolean CInterfaceWlan::WriteLine(const char* mask, const char* pline)
{
  if (0 != pline)
  {
    return FPWlanBase->WriteLine(FPWlanBase->ConvertPChar(mask, (char*)pline));
  }  
  return FPWlanBase->WriteLine(FPWlanBase->ConvertPChar(mask, (char*)""));
}  
Boolean CInterfaceWlan::WritePChar(const char* mask, char* value)
{
  if (0 != value)
  {    
    return FPWlanBase->WriteText(FPWlanBase->ConvertPChar(mask, value));
  }
  return FPWlanBase->WriteText((char*)mask);
}
Boolean CInterfaceWlan::WriteString(const char* mask, String value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertString(mask, value));
}
Boolean CInterfaceWlan::WriteByte(const char* mask, Byte value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertByte(mask, value));
}
Boolean CInterfaceWlan::WriteDual(const char* mask, UInt16 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertDual(mask, value));
}
Boolean CInterfaceWlan::WriteQuad(const char* mask, UInt32 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertQuad(mask, value));
}
Boolean CInterfaceWlan::WriteInt16(const char* mask, Int16 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertInt16(mask, value));
}
Boolean CInterfaceWlan::WriteUInt16(const char* mask, UInt16 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertUInt16(mask, value));
}
Boolean CInterfaceWlan::WriteInt32(const char* mask, Int32 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertInt32(mask, value));
}
Boolean CInterfaceWlan::WriteUInt32(const char* mask, UInt32 value)
{
  return FPWlanBase->WriteText(FPWlanBase->ConvertUInt32(mask, value));
}
Boolean CInterfaceWlan::WriteFloat32(const char* mask, Float32 value)
{  
  return FPWlanBase->WriteText(FPWlanBase->ConvertFloat32(mask, value));
}
Boolean CInterfaceWlan::WriteDouble64(const char* mask, Double64 value)
{  
  return FPWlanBase->WriteText(FPWlanBase->ConvertDouble64(mask, value));
}
//
//------------------------------------------------------------------------
// Bt - Write - Specials
//------------------------------------------------------------------------
Boolean CInterfaceWlan::WriteComment(void)
{
  return FPWlanBase->Write(TERMINAL_COMMENT[0]);
}
Boolean CInterfaceWlan::WriteComment(String comment)
{
  sprintf(FPWlanBase->GetTxBlock(), "%s%s", TERMINAL_COMMENT, comment.c_str());
  return FPWlanBase->WriteText(FPWlanBase->GetTxBlock());
}
Boolean CInterfaceWlan::WriteResponse(String text)
{  
  sprintf(FPWlanBase->GetTxBlock(), "%s%s", TERMINAL_RESPONSE, text.c_str());
  return FPWlanBase->WriteLine(FPWlanBase->GetTxBlock());
}

Boolean CInterfaceWlan::WriteEvent(const char* event)
{
  sprintf(FPWlanBase->GetTxBlock(), "%s%s", TERMINAL_EVENT, event);
  return FPWlanBase->WriteLine(FPWlanBase->GetTxBlock());
}
Boolean CInterfaceWlan::WriteEvent(const char* mask, const char* event)
{  
  sprintf(FPWlanBase->GetTxBlock(), mask, event);
  FPWlanBase->WriteText((char*)TERMINAL_EVENT);
  return FPWlanBase->WriteLine(FPWlanBase->GetTxBlock());
}
//
//-------------------------------------------------------------------
//  Bt - Execute
//-------------------------------------------------------------------
Boolean CInterfaceWlan::Execute(void)
{
  return true;
}

// {
//   // while (0 < GetRxCount())
//   // {
//   //   Char C = F
//   //   switch (C)
//   //   {
//   //     case (byte)SEPARATOR_CR:
//   //     case (byte)SEPARATOR_LF:
//   //       FRxdBufferIndex, SEPARATOR_ZERO);
//   //       FRxdBufferIndex = 0; // restart
//   //       strupr(FRxdBuffer);
//   //       strcpy(FCommandText, FRxdBuffer);
//   //       return true;
//   //     default: 
//   //       FRxdBuffer[FRxdBufferIndex] = C;
//   //       FRxdBufferIndex++;
//   //       break;
//   //   }
//   // }  
// }
//
#endif // INTERFACE_BT
//
