//
#include "DefinitionSystem.h"
//
#if defined(PROTOCOL_MQTT)
//
//#include "DefinitionMqtt.h"
#include "ProtocolMqtt.h"
//
//
//----------------------------------------------------------------------
//  Global
//----------------------------------------------------------------------
CProtocolMqtt* PMqtt;
//
//----------------------------------------------------------------------
//  Callback
//----------------------------------------------------------------------
void GlobalOnReceiveTopic(char* topic, byte* payload, unsigned int payloadsize)
{
  PMqtt->OnReceiveTopic(topic, payload, payloadsize);
}
//
//----------------------------------------------------------------------
//  Mqtt - Constructor
//----------------------------------------------------------------------
CProtocolMqtt::CProtocolMqtt(WiFiClient* pwificlient, 
                             DMqttOnSetup onsetup, 
                             DMqttOnTryConnection ontryconnection, 
                             DMqttOnConnectionSuccess onconnectionsuccess, 
                             DMqttOnConnectionFailed onconnectionfailed, 
                             DMqttOnReceive onreceive)
{
  FErrorCode = ecmqNone;
  FOnSetup = onsetup;
  FOnTryConnection = ontryconnection;
  FOnConnectionSuccess = onconnectionsuccess;
  FOnConnectionFailed = onconnectionfailed;
  FOnReceive = onreceive;
  //  
  FPWifiClient = pwificlient;
  FPMqttClient = new PubSubClient(MQTT_BROKER_IPADDRESS, MQTT_BROKER_PORT, GlobalOnReceiveTopic, *FPWifiClient);
  // Global Instance of CProtocolMqtt for Callback:
  PMqtt = this;
} 
//
//----------------------------------------------------------------------
//  Mqtt - Property
//----------------------------------------------------------------------
EErrorCodeMqtt CProtocolMqtt::GetErrorCode(void)
{
  EErrorCodeMqtt EC = FErrorCode;
  FErrorCode = ecmqNone;
  return EC;
}
bool CProtocolMqtt::IsConnected(void)
{
  return FPMqttClient->connected();
}
int CProtocolMqtt::GetState(void)
{
  return FPMqttClient->state();
}
//
//----------------------------------------------------------------------
//  Mqtt - Callback
//----------------------------------------------------------------------
void CProtocolMqtt::OnReceiveTopic(char* topic, byte* payload, unsigned int payloadsize) 
{
  if (0 < strlen(topic))
  {
    strcpy(FTopic, topic);
    unsigned S = min((int)payloadsize, MQTT_SIZE_MESSAGE - 1);
    for (unsigned int CI = 0; CI < S; CI++) 
    {
      FMessage[CI] = (char)payload[CI];
    } 
    FMessage[S] = 0x00;
    if (0 < FOnReceive)
    {
      FOnReceive(this, (const char*)FTopic, (const char*)FMessage);
    }
  }
}
//
//----------------------------------------------------------------------
//  Mqtt - Management
//----------------------------------------------------------------------
void CProtocolMqtt::Setup(void)
{
  if (0 < FOnSetup)
  {
    FOnSetup(this);
  }
  FPMqttClient->setServer(MQTT_BROKER_IPADDRESS, MQTT_BROKER_PORT);
  FPMqttClient->setCallback(GlobalOnReceiveTopic);
}
//
void CProtocolMqtt::Connect(void)
{ 
  while (!FPMqttClient->connected())
  {
    if (0 < FOnTryConnection)
    {
      FOnTryConnection(this);
    }
    if (FPMqttClient->connect(MQTT_LOCAL_DEVICEID, "", ""))
    {
      FErrorCode = ecmqNone;
      if (0 < FOnConnectionSuccess)
      {
        FOnConnectionSuccess(this);
      }
    }
    else
    {
      FErrorCode = ecmqConnectionFailed;      
      if (0 < FOnConnectionFailed)
      {
        FOnConnectionFailed(this);
      }
    }
  }
}
//
//----------------------------------------------------------------------
//  Mqtt - Receive from Broker
//----------------------------------------------------------------------
// top callback!
// 
//----------------------------------------------------------------------
//  Mqtt - Send to Broker
//----------------------------------------------------------------------
bool CProtocolMqtt::Subscribe(const char* ptopic)
{ 
  if (FPMqttClient->subscribe(MQTT_TOPIC_COMMAND))
  {
    return true;
  }
  FErrorCode = ecmqSubscribeFailed;
  return false;
}
//
bool CProtocolMqtt::Publish(const char* ptopic, const char* pmessage)
{
  if (FPMqttClient->publish(ptopic, pmessage))
  {
    return true;
  }
  FErrorCode = ecmqPublishFailed;
  return false;
}
//
//------------------------------------------------------------------------
// Mqtt - Read
//------------------------------------------------------------------------
bool CProtocolMqtt::LineDetected(void)
{
  return (0 < strlen(FMessage));
}
bool CProtocolMqtt::ReadLine(char* buffer, unsigned size)
{
  strncpy(buffer, FMessage, size - 1);
  FTopic[0] = 0x00; // == Command
  FMessage[0] = 0x00;
  return true;
}
//
//------------------------------------------------------------------------
// Mqtt - Write - Specials
//------------------------------------------------------------------------
Boolean CProtocolMqtt::WriteComment(void)
{
  return false; //FPUartBase->Write(TERMINAL_COMMENT[0]);
}
Boolean CProtocolMqtt::WriteComment(String comment)
{
  // sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_COMMENT, comment.c_str());
  return false; //FPUartBase->WriteText(FPUartBase->GetTXBlock());
}
Boolean CProtocolMqtt::WriteResponse(String text)
{  
  // sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_RESPONSE, text.c_str());
  return false; //FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}
Boolean CProtocolMqtt::WriteEvent(const char* event)
{
  // sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_EVENT, event);
  return false; //FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}
Boolean CProtocolMqtt::WriteEvent(const char* mask, const char* event)
{  
  //sprintf(FPUartBase->GetTXBlock(), mask, event);
  //FPUartBase->WriteText((char*)TERMINAL_EVENT);
  return false; //FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}
//
//----------------------------------------------------------------------
//  Mqtt - Loop
//----------------------------------------------------------------------
void CProtocolMqtt::Execute(void)
{
  FPMqttClient->loop();
}
//
#endif // DISPATCHER_MQTT
//
