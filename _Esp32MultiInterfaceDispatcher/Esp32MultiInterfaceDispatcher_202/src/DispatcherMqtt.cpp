//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_MQTT)
//
#include "Error.h"
#include "DispatcherMqtt.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
extern CError Error;
extern CCommand Command;
// 
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CInterfaceBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CInterfaceWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CInterfaceLan LanCommand;
#endif
#if defined(PROTOCOL_MQTT)
extern CProtocolMqtt MqttCommand;
#endif
//
//#########################################################
//  Dispatcher - Mqtt - Constructor
//#########################################################
CDispatcherMqtt::CDispatcherMqtt(void)
{
}
//
//#########################################################
//  Dispatcher - Mqtt - Execution
//#########################################################
bool CDispatcherMqtt::ExecuteWriteCommandMqtt(char* command, int parametercount, char** parameters)
{ // <WCM> <topic==parameter0> <parameter1> <parameter2> <parameter3>
  const char* Topic = "";
  const char* Message = "";
  ExecuteBegin();
  // Analyse parameters: p0, p1, p2, p3
  if (4 <= parametercount)
  {
    Topic = parameters[0];
    sprintf(Command.GetBuffer(), "%s %s %s", parameters[1], parameters[2], parameters[3]);
    Message = Command.GetBuffer();
  }
  else
  if (3 <= parametercount)
  {
    Topic = parameters[0];
    sprintf(Command.GetBuffer(), "%s %s", parameters[1], parameters[2]);
    Message = Command.GetBuffer();
  }
  else
  if (2 == parametercount)
  {
    Topic = parameters[0];
    sprintf(Command.GetBuffer(), "%s", parameters[1]);
    Message = Command.GetBuffer();
  }
  else
  if (1 == parametercount)
  {
    Topic = parameters[0];
    Message = "";
  }
  else
  {
    sprintf(Command.GetBuffer(), "Missing Parameter!");
  }
  // Response:
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (0 < strlen(Topic))
  {
    MqttCommand.Publish(Topic, Message);
  }
  ExecuteEnd();
  return true;
}

bool CDispatcherMqtt::ExecuteSubscribeTopicMqtt(char* command, int parametercount, char** parameters)
{ // <STM> <topic==parameter0>
  const char* Topic = "";
  ExecuteBegin();
  // Analyse parameters: topic
  if (1 <= parametercount)
  {
    Topic = parameters[0];
    sprintf(Command.GetBuffer(), "%s", parameters[0]);
  }
  else
  {
    sprintf(Command.GetBuffer(), "Missing Parameter!");
  }
  // Response:
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (0 < strlen(Topic))
  {
    MqttCommand.Subscribe(Topic);
  }
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Mqtt - Handler
//#########################################################
bool CDispatcherMqtt::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_WCM, command))
  {
    return ExecuteWriteCommandMqtt(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_STM, command))
  {
    return ExecuteSubscribeTopicMqtt(command, parametercount, parameters);
  }
  return false;
}
//
bool CDispatcherMqtt::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_MQTT
//
// // CommandDispatcher:
//   Serial.print("Mqtt: CommandDispatcher[");
//   Serial.print(ptopic);
//   Serial.print("][");  
//   Serial.print(pmessage);
//   Serial.println("]");
//   //  
//   if (0 == strcmp(MQTT_TOPIC_EVENT, ptopic))
//   {
//     Serial.print("Mqtt: Received Event[");
//     Serial.print(pmessage);
//     Serial.println("]");
//     return true;
//   }
//   if (0 == strcmp(MQTT_TOPIC_COMMAND, ptopic))
//   {
//     if (0 == strlen(pmessage))
//     {
//       MqttError(ecmqInvalidCommand);
//       return false;
//     }
//     Serial.print("Mqtt: Received Command[");
//     Serial.print(pmessage);
//     Serial.println("]");
//     if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMON, pmessage))
//     { // debug Serial.print("!!!!!!!!!! ON");  
//       digitalWrite(PIN_LEDSYSTEM, HIGH);
//       return true;
//     }
//     if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMOFF, pmessage))
//     { // debug Serial.print("!!!!!!!!!! OFF");  
//       digitalWrite(PIN_LEDSYSTEM, LOW);
//       return true;
//     }
//     return MqttError(ecmqInvalidCommand);
//   }
//   return MqttError(ecmqInvalidTopic);