//
#include "DefinitionSystem.h"
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  CInterfaceBase - Constructor
//----------------------------------------------------
CInterfaceBase::CInterfaceBase(const char* deviceid)
{
  strncpy(FDeviceID, deviceid, INTERFACE_SIZE_DEVICEID - 1);
  // NC FDeviceID[INTERFACE_SIZE_DEVICEID - 1] = 0x00;
  FRxIndex = 0;
  FRxBlock[FRxIndex] = 0x00;
  FRxEcho = INTERFACE_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBase - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceBase - Helper
//----------------------------------------------------
char* CInterfaceBase::ConvertPChar(const char* mask, char* value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;
}
char* CInterfaceBase::ConvertString(const char* mask, String value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer; 
}
char* CInterfaceBase::ConvertByte(const char* mask, Byte value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDual(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertQuad(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt16(const char* mask, Int16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt16(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt32(const char* mask, Int32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt32(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertFloat32(const char* mask, Float32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDouble64(const char* mask, Double64 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
//
//----------------------------------------------------
//  CInterfaceBase - Handler
//----------------------------------------------------
// virtual Boolean Open(int parameter) = 0;
// virtual Boolean Close(void) = 0;
//
//----------------------------------------------------
//  CInterfaceBase - Read
//----------------------------------------------------
// virtual UInt8 GetRXCount(void) = 0;
// virtual Boolean Read(char &character) = 0;
// virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
//  
//----------------------------------------------------
//  CInterfaceBase - Write
//----------------------------------------------------
// virtual Boolean Write(char character) = 0;
// virtual Boolean WriteText(char* ptext) = 0;
// virtual Boolean WriteLine(void) = 0;
// virtual Boolean WriteLine(char* pline) = 0;
// 