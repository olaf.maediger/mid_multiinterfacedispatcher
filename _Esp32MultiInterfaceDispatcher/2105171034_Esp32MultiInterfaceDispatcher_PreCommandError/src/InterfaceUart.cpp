//
#include "Defines.h"
//
#if defined(INTERFACE_UART)
//
#include "InterfaceUart.h"
//
using namespace std;
//
//----------------------------------------------------
//  CUartBase
//----------------------------------------------------
CUartBase::CUartBase(void)
{
  FRXEcho = UART_INIT_RXECHO;
}

void CUartBase::SetRXEcho(Boolean rxecho)
{
  FRXEcho = rxecho;
}

char* CUartBase::GetPRXBuffer(void)
{
  return FRXBuffer;
}
char* CUartBase::GetPTXBuffer(void)
{
  return FTXBuffer;
}

char* CUartBase::ConvertPChar(const char* mask, char* value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;
}
char* CUartBase::ConvertString(const char* mask, String value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer; 
}
char* CUartBase::ConvertByte(const char* mask, Byte value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertDual(const char* mask, UInt16 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertQuad(const char* mask, UInt32 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertInt16(const char* mask, Int16 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertUInt16(const char* mask, UInt16 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertInt32(const char* mask, Int32 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertUInt32(const char* mask, UInt32 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertFloat32(const char* mask, Float32 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
char* CUartBase::ConvertDouble64(const char* mask, Double64 value)
{
  sprintf(FTXBuffer, mask, value);
  return FTXBuffer;   
}
//
//----------------------------------------------------
//  CUartHS
//----------------------------------------------------
CUartHS::CUartHS(HardwareSerial* pserial)
{
  FPSerial = pserial;
} 

Boolean CUartHS::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  return true;
} 
Boolean CUartHS::Close(void)
{
  FPSerial->end();
  return true;
} 

void CUartHS::Write(char* text)
{
  FPSerial->print(text);
}
//
//----------------------------------------------------
//  CUart - Constructor
//----------------------------------------------------
CUart::CUart(HardwareSerial* pserial)
{
  FPUartBase = (CUartBase*)new CUartHS(pserial);
}
//
//----------------------------------------------------
//  CUart - Property
//----------------------------------------------------
char* CUart::GetPRXBuffer(void)
{
  return FPUartBase->GetPRXBuffer();
}
char* CUart::GetPTXBuffer(void)
{
  return FPUartBase->GetPTXBuffer();
}

void CUart::SetRXEcho(Boolean rxecho)
{
  return FPUartBase->SetRXEcho(rxecho);
}
//
//----------------------------------------------------
//  CUart - Handler
//----------------------------------------------------
Boolean CUart::Open(int baudrate)
{
  return FPUartBase->Open(baudrate);
}
Boolean CUart::Close(void)
{
  return FPUartBase->Close();
}

void CUart::WritePChar(const char* mask, char* value)
{
  if (0 != value)
  {
    FPUartBase->Write(FPUartBase->ConvertPChar(mask, value));
    return;
  }
  FPUartBase->Write((char*)mask);
}

void CUart::WriteString(const char* mask, String value)
{
  FPUartBase->Write(FPUartBase->ConvertString(mask, value));
}

void CUart::WriteByte(const char* mask, Byte value)
{
  FPUartBase->Write(FPUartBase->ConvertByte(mask, value));
}
void CUart::WriteDual(const char* mask, UInt16 value)
{
  FPUartBase->Write(FPUartBase->ConvertDual(mask, value));
}

void CUart::WriteQuad(const char* mask, UInt32 value)
{
  FPUartBase->Write(FPUartBase->ConvertQuad(mask, value));
}

void CUart::WriteInt16(const char* mask, Int16 value)
{
  FPUartBase->Write(FPUartBase->ConvertInt16(mask, value));
}

void CUart::WriteUInt16(const char* mask, UInt16 value)
{
  FPUartBase->Write(FPUartBase->ConvertUInt16(mask, value));
}

void CUart::WriteInt32(const char* mask, Int32 value)
{
  FPUartBase->Write(FPUartBase->ConvertInt32(mask, value));
}

void CUart::WriteUInt32(const char* mask, UInt32 value)
{
  FPUartBase->Write(FPUartBase->ConvertUInt32(mask, value));
}

void CUart::WriteFloat32(const char* mask, Float32 value)
{  
  FPUartBase->Write(FPUartBase->ConvertFloat32(mask, value));
}

void CUart::WriteDouble64(const char* mask, Double64 value)
{  
  FPUartBase->Write(FPUartBase->ConvertDouble64(mask, value));
}
//
#endif // INTERFACE_UART
//