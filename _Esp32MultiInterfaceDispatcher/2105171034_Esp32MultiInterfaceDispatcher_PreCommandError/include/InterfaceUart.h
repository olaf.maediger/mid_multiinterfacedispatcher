#include "Defines.h"
//
#ifdef INTERFACE_UART
//
#ifndef InterfaceUart_h
#define InterfaceUart_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <HardwareSerial.h>
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
const Int16 UART_SIZE_RXBUFFER    = 64;
const Int16 UART_SIZE_TXBUFFER    = 64;
//
const Boolean UART_INIT_RXECHO    = false;
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CUartBase
{
  private:
  char FRXBuffer[UART_SIZE_RXBUFFER];
  char FTXBuffer[UART_SIZE_TXBUFFER];
  //
  Boolean FRXEcho;
  //
  public:
  CUartBase(void);
  //
  char* GetPRXBuffer(void);
  char* GetPTXBuffer(void);
  void SetRXEcho(Boolean rxecho);
  //
  virtual Boolean Open(int baudrate) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual void Write(char* text) = 0;
  //
  char* ConvertPChar(const char* mask, char* value);
  char* ConvertString(const char* mask, String value);
  char* ConvertByte(const char* mask, Byte value);
  char* ConvertDual(const char* mask, UInt16 value);
  char* ConvertQuad(const char* mask, UInt32 value);
  char* ConvertInt16(const char* mask, Int16 value);
  char* ConvertUInt16(const char* mask, UInt16 value);
  char* ConvertInt32(const char* mask, Int32 value);
  char* ConvertUInt32(const char* mask, UInt32 value);
  char* ConvertFloat32(const char* mask, Float32 value);
  char* ConvertDouble64(const char* mask, Double64 value);
};

class CUartHS : public CUartBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CUartHS(HardwareSerial *serial);
  //
  Boolean Open(int baudrate);
  Boolean Close(void);
  //
  void Write(char* text);
};

// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
// class CUartU : public CUartBase
// {
//   protected:
//   usb_serial_class* FPSerial;
//   //  
//   public:
//   CUartU(usb_serial_class *serial)
//   {
//     FPSerial = serial;
//   } 
// };
// #endif

class CUart
{
  private:
  CUartBase* FPUartBase;
  int FPinRXD, FPinTXD;
  //
  public:
  CUart(HardwareSerial* pserial);
  CUart(HardwareSerial* pserial, int pinrxd, int pintxd);
// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
//   CUart(usb_serial_class &serial);
// #endif
  //
  Boolean Open(int baudrate);
  Boolean Close(void);
  //
  char* GetPRXBuffer(void);
  char* GetPTXBuffer(void);
  void SetRXEcho(Boolean rxecho);
  //
  void WritePChar(const char* mask, char* value = 0);
  void WriteString(const char* mask, String value);
  void WriteByte(const char* mask, Byte value);
  void WriteDual(const char* mask, UInt16 value);
  void WriteQuad(const char* mask, UInt32 value);
  void WriteInt16(const char* mask, Int16 value);
  void WriteUInt16(const char* mask, UInt16 value);
  void WriteInt32(const char* mask, Int32 value);
  void WriteUInt32(const char* mask, UInt32 value);
  void WriteFloat32(const char* mask, Float32 value);
  void WriteDouble64(const char* mask, Double64 value);
};
//
#endif // InterfaceUart_h
//
#endif // INTERFACE_UART
//



  //
