#include "DefinitionSystem.h"
//
#ifdef INTERFACE_UART
//
#ifndef InterfaceUart_h
#define InterfaceUart_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <HardwareSerial.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
// const byte UART_SIZE_RXBLOCK = 32;
// const byte UART_SIZE_TXBLOCK = 32;
const Boolean UART_INIT_RXECHO = false;
//
//----------------------------------------------------
//  CInterfaceUartBase
//----------------------------------------------------
class CInterfaceUartBase : public CInterfaceBase
{
  public:
  CInterfaceUartBase(const char* deviceid);
   // Handler
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  // Read
  virtual UInt8 GetRxCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* pline, int &size) = 0;
  // Write
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
};
//
//----------------------------------------------------
//  CInterfaceUartHS
//----------------------------------------------------
class CInterfaceUartHS : public CInterfaceUartBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CInterfaceUartHS(const char* deviceid, HardwareSerial *serial);
  // Handler
  virtual Boolean Open(int baudrate);
  virtual Boolean Close(void);
  // Read
  virtual UInt8 GetRxCount(void);
  virtual Boolean Read(char &character);
  virtual Boolean ReadLine(char* pline, int &size);
  // Write
  virtual Boolean Write(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
};

// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
// class CInterfaceUartUS : public CInterfaceUartBase
// {
//   protected:
//   usb_serial_class* FPSerial;
//   //  
//   public:
//   CInterfaceUartU(usb_serial_class *serial)
//   {
//     FPSerial = serial;
//   } 
// };
// #endif
//
//----------------------------------------------------
//  CInterfaceUart
//----------------------------------------------------
class CInterfaceUart
{
  private:
  CInterfaceUartBase* FPUartBase;
  int FPinRXD, FPinTXD;
  //
  public:
  CInterfaceUart(const char* deviceid, HardwareSerial* pserial);
  CInterfaceUart(const char* deviceid, HardwareSerial* pserial, int pinrxd, int pintxd);
// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
//   CInterfaceUart(usb_serial_class &serial);
// #endif
  //
  // Property
  char* GetRxBlock(void)
  {
    return FPUartBase->GetRxBlock();
  }
  char* GetTxBlock(void)
  {
    return FPUartBase->GetTxBlock();
  }
  void SetRxEcho(Boolean rxecho)
  {
    FPUartBase->SetRxEcho(rxecho);
  }
  Boolean GetRxEcho(void)
  {
    return FPUartBase->GetRxEcho();
  }
  //
  // Handler
  Boolean Open(int baudrate);
  Boolean Close(void);
  // Read
  UInt8 GetRxCount(void);
  //Boolean LineDetected(void);
  Boolean Read(char &character);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  // Boolean WritePrompt(void);
  // Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  // ??? Boolean Execute(void);
};
//
#endif // InterfaceUart_h
//
#endif // INTERFACE_UART
//
//
