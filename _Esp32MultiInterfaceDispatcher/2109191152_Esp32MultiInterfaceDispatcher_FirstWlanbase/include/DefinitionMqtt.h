//
#if defined(PROTOCOL_MQTT)
//
#ifndef DefinitionMqtt_h
#define DefinitionMqtt_h
//------------------------------------------------------
// Segment - Global Constant - Mqtt
//------------------------------------------------------
#define WLAN_SSID                 "TPL1300SA7AZ"
// alternative #define WLAN_SSID  "FritzBoxSA7"
#define WLAN_PASSWORD             "01234567890123456789"
#define MQTT_BROKER_IPADDRESS     "192.168.178.53"
#define MQTT_BROKER_PORT          1883
#define MQTT_LOCAL_DEVICEID       "Esp32PubSubClient"
//
#define MQTT_TOPIC_EVENT          "Esp32PubSubClient/Event"
#define MQTT_EVENT_CONNECTED      "IsConnected"
#define MQTT_EVENT_ACTIVE         "IsActive"
//
#define MQTT_TOPIC_COMMAND        "Esp32PubSubClient/Command"
#define MQTT_COMMAND_LEDSYSTEMON  "LSO"
#define MQTT_COMMAND_LEDSYSTEMOFF "LSF"
//
const int MQTT_SIZE_TOPIC         = 64; // Enough space for Topic!!!
const int MQTT_SIZE_MESSAGE       = 64;
//
const long unsigned MQTT_INTERVAL_ACTIVE = 60000L; // [ms]
//
#endif // DefinitionMqtt_h
//
#endif // PROTOCOL_MQTT
//