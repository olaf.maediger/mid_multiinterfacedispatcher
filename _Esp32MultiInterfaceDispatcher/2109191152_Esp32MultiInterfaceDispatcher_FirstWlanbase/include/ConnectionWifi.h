//
#include "DefinitionSystem.h"
//
#if defined(CONNECTION_WIFI)
//
#ifndef Wifi_h
#define Wifi_h
//
#include <Arduino.h>
#include "WiFi.h"
//
const int CONNECTION_SIZE_DEVICEID = 12;
const int WIFI_COUNT_TRYCONNECTION = 10;
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
class CWifiBase
{
  protected:
  char FDeviceID[CONNECTION_SIZE_DEVICEID];
  WiFiClass*  FPWiFi;
  const char* FSSID;
  const char* FPassword;
  public:
  // Constructor
  CWifiBase(const char* deviceid, WiFiClass* pwifi, 
            const char* ssid, const char* password);
  // Property
  const char* GetDevideID(void)
  {
    return FDeviceID;
  }
  bool IsConnected(void);
  const char* GetSSID(void);  
  const char* GetPassword(void);  
  IPAddress GetIPAddress(void);  
  // Management
  virtual bool Open(void);
  virtual bool Close(void);
  virtual bool Reconnect(void);
};
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
class CWifiStation : public CWifiBase
{
  protected:
  public:
  // Constructor
  CWifiStation(const char* deviceid, WiFiClass* pwifi, 
               const char* ssid, const char* password);
  // Management
  bool Open(void);
  bool Close(void);
  bool Reconnect(void);
};
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
class CWifiAccessPoint : public CWifiBase
{
  private:
  protected:
  public:
  // Constructor
  CWifiAccessPoint(const char* deviceid, WiFiClass* pwifi, 
                   const char* ssid, const char* password);
  // Management
  bool Open(void);
  bool Close(void);
  bool Reconnect(void);
};
//
//-------------------------------------------------------------
//  WifiStationAccessPoint
//-------------------------------------------------------------
//...
//
#endif // DefinitionSystem_h
//
#endif // INTERFACE_WLAN
//