//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Trigger.h"
#include "DacMcp4725.h"
//
//--------------------------------
//  LaserScanner - Constant
//--------------------------------
const int LASERSCANNER_SIZE_ID                   = 4;
const int LASERSCANNER_SIZE_BUFFER               = 64;
//
const char* const LASERSCANNER_EVENT_MASK        = "LaserScanner[%s]=%s";
//
// const char* const TRIGGER_EVENT_UNDEFINED       = "Undefined";
// const char* const TRIGGER_EVENT_ACTIVE          = "Active";
// const char* const TRIGGER_EVENT_PASSIVE         = "Passive";
// const char* const TRIGGER_EVENT_PULSEACTIVE     = "PulseActive";
// const char* const TRIGGER_EVENT_PULSEPASSIVE    = "PulsePassive";
// const char* const TRIGGER_EVENT_WAITFORACTIVE   = "WaitForActive";
// const char* const TRIGGER_EVENT_WAITFORPASSIVE  = "WaitForPassive";
// const char* const TRIGGER_EVENT_TIMEOUT         = "TimeOut";
// //
// const char* const STATE_TRIGGER[] = {TRIGGER_EVENT_UNDEFINED, 
//                                      TRIGGER_EVENT_ACTIVE, TRIGGER_EVENT_PASSIVE,
//                                      TRIGGER_EVENT_PULSEACTIVE, TRIGGER_EVENT_PULSEPASSIVE,
//                                      TRIGGER_EVENT_WAITFORACTIVE, TRIGGER_EVENT_WAITFORPASSIVE};
//
//--------------------------------
//  LaserScanner - Type
//--------------------------------
class CLaserScanner;
typedef void (*DOnEventLaserScanner)(CLaserScanner* laserscanner, const char* event);
//
// enum ELaserScannerDirection
// {
//   tdInput = 0,
//   tdOutput = 1
// };
// enum ELaserScannerLevel
// {
//   tlUndefined = -1,
//   tlPassive = 0,
//   tlActive = 1
// };
// enum EStateLaserScanner
// {
//   stUndefined = 0,
//   stActive = 1,
//   stPassive = 2,
//   stPulseActive = 3,    // P->A->P
//   stPulsePassive = 4,   // A->P->A
//   stWaitForActive = 5,  // IsPassive
//   stWaitForPassive = 6  // IsActive
// };
//
class CLaserScanner
{
  private:
  // EStateTrigger FState;
  char FID[LASERSCANNER_SIZE_ID];
  char FBuffer[LASERSCANNER_SIZE_BUFFER];
  DOnEventLaserScanner FOnEvent;
  CTrigger* FPTrigger;
  CDacMcp4725* FPDacX;
  CDacMcp4725* FPDacY;
  //  
  public:
  CLaserScanner(const char* pid, CTrigger* ptrigger, CDacMcp4725* pdacx, CDacMcp4725* pdacy);
  // //
  // void SetLevel(ETriggerLevel level);
  // void SetState(EStateTrigger state);
  // void SetOnEvent(DOnEventTrigger onevent);
  // ETriggerLevel GetLevel(void);
  // //
  Boolean Open(void);
  Boolean Close(void);
  // bool SetActive(void);
  // bool SetPassive(void);
  // bool PulseActive(long unsigned timeout);
  // bool PulsePassive(long unsigned timeout);
  // bool WaitForActive(long unsigned timeout);
  // bool WaitForPassive(long unsigned timeout);
  Boolean PulseLaserPosition(int positionx, int positiony, int pulseperiod, int pulsecount);
  //
  void Execute(void);
};
//
#endif // LaserScanner_h
//
#endif // DISPATCHER_LASERSCANNER
//
