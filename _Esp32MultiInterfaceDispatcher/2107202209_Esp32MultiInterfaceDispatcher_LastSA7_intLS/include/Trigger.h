//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef Trigger_h
#define Trigger_h
//
//--------------------------------
//  Trigger - Constant
//--------------------------------
const int TRIGGER_SIZE_ID                   = 4;
const int TRIGGER_SIZE_BUFFER               = 64;
//
const char* const TRIGGER_EVENT_MASK            = "Trigger[%s]=%s";
//
const char* const TRIGGER_EVENT_UNDEFINED       = "Undefined";
const char* const TRIGGER_EVENT_ACTIVE          = "Active";
const char* const TRIGGER_EVENT_PASSIVE         = "Passive";
const char* const TRIGGER_EVENT_PULSEACTIVE     = "PulseActive";
const char* const TRIGGER_EVENT_PULSEPASSIVE    = "PulsePassive";
const char* const TRIGGER_EVENT_WAITFORACTIVE   = "WaitForActive";
const char* const TRIGGER_EVENT_WAITFORPASSIVE  = "WaitForPassive";
const char* const TRIGGER_EVENT_TIMEOUT         = "TimeOut";
//
const char* const STATE_TRIGGER[] = {TRIGGER_EVENT_UNDEFINED, 
                                     TRIGGER_EVENT_ACTIVE, TRIGGER_EVENT_PASSIVE,
                                     TRIGGER_EVENT_PULSEACTIVE, TRIGGER_EVENT_PULSEPASSIVE,
                                     TRIGGER_EVENT_WAITFORACTIVE, TRIGGER_EVENT_WAITFORPASSIVE};
//
//--------------------------------
//  Trigger - Type
//--------------------------------
class CTrigger;
typedef void (*DOnEventTrigger)(CTrigger* trigger, const char* event);
//
enum ETriggerDirection
{
  tdInput = 0,
  tdOutput = 1
};
enum ETriggerLevel
{
  tlUndefined = -1,
  tlPassive = 0,
  tlActive = 1
};
enum EStateTrigger
{
  stUndefined = 0,
  stActive = 1,
  stPassive = 2,
  stPulseActive = 3,    // P->A->P
  stPulsePassive = 4,   // A->P->A
  stWaitForActive = 5,  // IsPassive
  stWaitForPassive = 6  // IsActive
};
//
class CTrigger 
{
  private:
  EStateTrigger FState;
  char FID[TRIGGER_SIZE_ID];
  char FBuffer[TRIGGER_SIZE_BUFFER];
  DOnEventTrigger FOnEvent;
  int FPin;
  bool FInverted;
  ETriggerDirection FDirection;
  ETriggerLevel FLevel;
  long unsigned FTimeOut;
  //  
  public:
  CTrigger(const char* pid, int pin,
           ETriggerDirection triggerdirection);
  CTrigger(const char* pid, int pin, 
           ETriggerDirection triggerdirection,
           bool inverted);
  //
  void SetLevel(ETriggerLevel level);
  void SetState(EStateTrigger state);
  void SetOnEvent(DOnEventTrigger onevent);
  ETriggerLevel GetLevel(void);
  //
  Boolean Open(void);
  Boolean Close(void);
  bool SetActive(void);
  bool SetPassive(void);
  bool PulseActive(long unsigned timeout);
  bool PulsePassive(long unsigned timeout);
  bool WaitForActive(long unsigned timeout);
  bool WaitForPassive(long unsigned timeout);
  //
  void Execute(void);
};
//
#endif // Trigger_h
//
#endif // DISPATCHER_TRIGGER
//
