//
//--------------------------------
//  Library TimeSpan
//--------------------------------
//
#include "DefinitionSystem.h"
//
// no dispatcher dirctive!
//
#ifndef TimeSpan_h
#define TimeSpan_h
//
#include <Arduino.h>
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Float32 INIT_TIMESPAN_MS = 0.0f;  // [ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateTimeSpan
{
  stsZero = 0,
  stsInit = 1,
  stsBusy = 2
};
//
class CTimeSpan
{
  private:
  EStateTimeSpan FState;
  Float32 FTimePeriodms;
  Float32 FTimeStartms;
  //
  public:
  CTimeSpan(void);
  //
  EStateTimeSpan GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  Float32 GetTimePeriodms(void);
  Float32 GetTimeActualms(void);
  void Start(Float32 timeperiodms);
  void Abort(void);
  Boolean Execute(void); // True:Active False:Passive
};
//
#endif // TimeSpan_h
//
