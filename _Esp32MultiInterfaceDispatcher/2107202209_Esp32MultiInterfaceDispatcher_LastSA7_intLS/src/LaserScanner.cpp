//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#include "LaserScanner.h"

#include "InterfaceUart.h"

extern CUart UartCommand;
//
CLaserScanner::CLaserScanner(const char* pid, CTrigger* ptrigger, 
                             CDacMcp4725* pdacx, CDacMcp4725* pdacy)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPTrigger = ptrigger;
  FPDacX = pdacx;
  FPDacY = pdacy;
}

// void CLaserScanner::SetOnEvent(DOnEventLaserScanner onevent)
// {
//   FOnEvent = onevent;
// }
// //
// void CLaserScanner::SetState(EStateLaserScanner state)
// {
//   if (state != FState)
//   {
//     FState = state;
//     if (FOnEvent) 
//     {
//       sprintf(FBuffer, TRIGGER_EVENT_MASK, FID, STATE_TRIGGER[FState]);
//       FOnEvent(this, FBuffer);
//     }
//   }
// }
// //
// ELaserScannerLevel CLaserScanner::GetLevel()
// {
//   if (tdInput == FDirection)
//   {
//     if (FInverted)
//     {
//       if (0 < digitalRead(FPin))
//       {
//         FLevel = tlPassive;
//       }
//       else
//       {
//         FLevel = tlActive;
//       }
//     }
//     else
//     {
//       if (0 < digitalRead(FPin))
//       {
//         FLevel = tlActive;
//       }
//       else
//       {
//         FLevel = tlPassive;
//       }      
//     }
//   }
//   return FLevel;
// }
//
Boolean CLaserScanner::Open()
{
  // switch (FDirection)
  // {
  //   case tdOutput:
  //     pinMode(FPin, OUTPUT);
  //     break;
  //   default: // tdInput
  //     pinMode(FPin, INPUT);
  //     break;
  // }
  // SetPassive();
  // SetLevel(tlPassive);
  // SetState(stPassive);
  return true;
}
//
Boolean CLaserScanner::Close()
{
  // SetPassive();
  // SetLevel(tlPassive);
  // pinMode(FPin, INPUT);
  // SetState(stPassive);
  return true;
}
//
// void CLaserScanner::SetLevel(ELaserScannerLevel level)
// {
//   FLevel = level;
// }
// //
// bool CLaserScanner::SetActive()
// {
//   if (FInverted)
//   {
//     digitalWrite(FPin, LOW);
//   }
//   else
//   {
//     digitalWrite(FPin, HIGH);
//   }
//   FLevel = tlActive;
//   SetState(stActive);
//   return true;
// }
// //
// bool CLaserScanner::SetPassive()
// {
//   if (FInverted)
//   {
//     digitalWrite(FPin, HIGH);
//   }
//   else
//   {
//     digitalWrite(FPin, LOW);
//   }
//   FLevel = tlPassive;
//   SetState(stPassive);
//   return true;
// }
// //
// bool CLaserScanner::PulseActive(long unsigned timeout)
// {
//   switch (FState)
//   {
//     case stActive:
//     case stPassive:
//     case stPulseActive:
//     case stPulsePassive:
//       SetLevel(tlPassive);
//       FTimeOut = millis();
//       FTimeOut += timeout;
//       SetState(stPulseActive);
//       return true;
//     default:
//       return false;
//   }
// }
// //
// bool CLaserScanner::PulsePassive(long unsigned timeout)
// {
//   switch (FState)
//   {
//     case stActive:
//     case stPassive:
//     case stPulseActive:
//     case stPulsePassive:
//       SetLevel(tlActive);
//       FTimeOut = millis();
//       FTimeOut += timeout;
//       SetState(stPulsePassive);
//       return true;
//     default:
//       return false;
//   }
//   return false;
// }
// //
// bool CLaserScanner::WaitForActive(long unsigned timeout)
// {
//   if ((stPassive == FState) || (stActive == FState))
//   {
//     FTimeOut = millis();
//     FTimeOut += timeout;
//     SetState(stWaitForActive);
//     return true;
//   }
//   return false;
// }
// //
// bool CLaserScanner::WaitForPassive(long unsigned timeout)
// {
//   if ((stPassive == FState) || (stActive == FState))
//   {
//     FTimeOut = millis();
//     FTimeOut += timeout;
//     SetState(stWaitForPassive);
//     return true;
//   }
//   return false;
// }
//



Boolean CLaserScanner::PulseLaserPosition(int positionx, int positiony, 
                                          int pulseperiod, int pulsecount)
{
  
}



void CLaserScanner::Execute(void)
{  
  // switch (FState)
  // {
  //   case stActive:
  //     if ((tdInput == FDirection) && (tlPassive == GetLevel()))
  //     {
  //       SetState(stPassive);
  //     }
  //     break;
  //   case stPassive:
  //     if ((tdInput == FDirection) && (tlActive == GetLevel()))
  //     {
  //       SetState(stActive);
  //     }
  //     break;
  //   case stPulseActive:
  //     if (FTimeOut <= millis())
  //     {
  //       SetPassive();
  //     }
  //     break;
  //   case stPulsePassive:
  //     if (FTimeOut <= millis())
  //     {
  //       SetActive();
  //     }
  //     break;
  //   case stWaitForActive:
  //     if (tlActive == GetLevel())
  //     {
  //       SetState(stActive);
  //     }
  //     else
  //       if (FTimeOut <= millis())
  //       {
  //         if (tlActive == GetLevel())
  //         {
  //           SetState(stActive);
  //         }
  //         else
  //         {
  //           SetState(stPassive);
  //         }
  //       }
  //     break;
  //   case stWaitForPassive:
  //     if (tlPassive == GetLevel())
  //     {
  //       SetState(stPassive);
  //     }
  //     else
  //       if (FTimeOut <= millis())
  //       {
  //         if (tlActive == GetLevel())
  //         {
  //           SetState(stActive);
  //         }
  //         else
  //         {
  //           SetState(stPassive);
  //         }
  //       }
  //     break;
  //   default:
  //     SetState(stUndefined);
  // }
}
//
#endif // DISPATCHER_TRIGGER
//
