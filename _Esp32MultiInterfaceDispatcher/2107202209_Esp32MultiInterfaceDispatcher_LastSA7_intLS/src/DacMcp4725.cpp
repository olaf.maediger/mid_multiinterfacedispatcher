//
//--------------------------------
//  Library Dac
//--------------------------------
//
#include "DacMcp4725.h"
//
CDacMcp4725::CDacMcp4725(const char* pid, Byte i2caddress)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FI2CAddress = i2caddress;
  FValue = 0x0000;
}


void CDacMcp4725::SetOnEvent(DOnEventDacMcp4725 onevent)
{
  FOnEvent = onevent;
}


Boolean CDacMcp4725::Open()
{
  Wire.begin();
  SetValue(0x0000);
  return true;
}

Boolean CDacMcp4725::Close()
{
  // Wire.end();
  return true;
}
//
UInt32 CDacMcp4725::GetValue()
{  
  return FValue;
}
void CDacMcp4725::SetValue(UInt32 value)
{
  FValue = value;
//#ifdef TWBR
//  uint8_t twbrback = TWBR;
//  TWBR = ((F_CPU / 400000L) - 16) / 2; // Set I2C frequency to 400kHz
//#endif
  Wire.beginTransmission(FI2CAddress);
  Wire.write(MCP4725_WRITEDAC);
  Wire.write((Byte)(value >> 4));
  Wire.write((Byte)((0x000F & value) << 4));
  Wire.endTransmission();
//#ifdef TWBR
//  TWBR = twbrback;
//#endif
  if (FOnEvent) 
  {
    sprintf(FBuffer, DACMCP4725_EVENT_MASK, FID, FValue);
    FOnEvent(this, FBuffer);
  }
}
//
UInt32 CDacMcp4725::GetRangeLow(void)
{
  return FRangeLow;
}
void CDacMcp4725::SetRangeLow(UInt32 value)
{
  FRangeLow = value;
}
//
UInt32 CDacMcp4725::GetRangeHigh(void)
{
  return FRangeHigh;
}
void CDacMcp4725::SetRangeHigh(UInt32 value)
{
  FRangeHigh = value;
}
//
UInt32 CDacMcp4725::GetRangeDelta(void)
{
  return FRangeDelta;
}
void CDacMcp4725::SetRangeDelta(UInt32 value)
{
  FRangeDelta = value;
}
//
bool CDacMcp4725::Execute(void)
{
  // ...
  return true;
}
//