//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "DispatcherTrigger.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
extern CError Error;
extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
extern CTrigger TriggerOut;
extern CTrigger TriggerIn;
//
//#########################################################
//  Dispatcher - Trigger - Constructor
//#########################################################
CDispatcherTrigger::CDispatcherTrigger(void)
{
}
//
//#########################################################
//  Dispatcher - Trigger - Execution
//#########################################################
bool CDispatcherTrigger::ExecuteSetTriggerOutActive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerOut.SetActive())
  {
    Error.SetCode(ecCommandFailed);
  }
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteSetTriggerOutPassive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerOut.SetPassive())
  {
    Error.SetCode(ecCommandFailed);
  }
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecutePulseTriggerOutActive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return true;
  }
  long unsigned TMS = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %lu", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerOut.PulseActive(TMS))
  {
    Error.SetCode(ecUndefinedState);
  }
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecutePulseTriggerOutPassive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return true;
  }
  long unsigned TMS = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %lu", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerOut.PulsePassive(TMS))
  {
    Error.SetCode(ecUndefinedState);
  }
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteGetTriggerInLevel(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int TL = 0;//TriggerIn.GetLevel();
  sprintf(Command.GetBuffer(), "%s %i", command, TL);
  ExecuteResponse(Command.GetBuffer());
  // Action: -
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteWaitTriggerInActive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  long unsigned TMS = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %lu", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerIn.WaitForActive(TMS))
  {
    Error.SetCode(ecUndefinedState);
  }
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteWaitTriggerInPassive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  long unsigned TMS = atol(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %lu", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  if (!TriggerIn.WaitForPassive(TMS))
  {
    Error.SetCode(ecUndefinedState);
  }
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Trigger - Handler
//#########################################################
bool CDispatcherTrigger::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_STA, command))
  {
    return ExecuteSetTriggerOutActive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_STP, command))
  {
    return ExecuteSetTriggerOutPassive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PTA, command))
  {
    return ExecutePulseTriggerOutActive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_PTP, command))
  {
    return ExecutePulseTriggerOutPassive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GTL, command))
  {
    return ExecuteGetTriggerInLevel(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_WTA, command))
  {
    return ExecuteWaitTriggerInActive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_WTP, command))
  {
    return ExecuteWaitTriggerInPassive(command, parametercount, parameters);
  }
  return false;
}
//
#endif // DISPATCHER_TRIGGER
//
//####################################################################
//####################################################################
//####################################################################
//
