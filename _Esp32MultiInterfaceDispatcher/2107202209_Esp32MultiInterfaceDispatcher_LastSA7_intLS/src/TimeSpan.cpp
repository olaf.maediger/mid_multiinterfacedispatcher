//
//--------------------------------
//  Library TimeSpan
//--------------------------------
//
#include "DefinitionSystem.h"
//
// no directive 
//
#include "TimeSpan.h"
//
CTimeSpan::CTimeSpan(void)
{
  FState = strZero;
  FTimePeriodms = INIT_TIMEPERIOD_MS;
  FTimeStartms = INIT_TIMEPERIOD_MS;
}

EStateTimeSpan CTimeSpan::GetState(void)
{
  return FState;
}  

Boolean CTimeSpan::Open()
{
  FState = strZero;
  return true;
}

Boolean CTimeSpan::Close()
{
  FState = strZero;
  return true;
}
//
/*
void CTimeSpan::SetTimeStartActual(void)
{
  FTimeStartms = (Float32)micros() / 1000.0f;
}
Float32 CTimeRelative::GetTimePeriodms(void)
{
  return FTimePeriodms;
}
Float32 CTimeRelative::GetTimeStartms(void)
{
  return FTimeStartms;
}
Float32 CTimeRelative::GetTimeActualms(void)
{
  return (Float32)micros() / 1000.0f - FTimeStartms;
}
//
UInt32 CTimeRelative::GetTimePeriodus(void)
{
  return (UInt32)(1000.0f * FTimePeriodms);  
}
UInt32 CTimeRelative::GetTimeStartus(void)
{
  return (UInt32)(1000.0f * FTimeStartms);
}
UInt32 CTimeRelative::GetTimeActualus(void)
{
  return (UInt32)(micros() - GetTimeStartus());
}
*/
//
//-------------------------------------------------------------------------------
//  TimeRelative - Wait
//-------------------------------------------------------------------------------
//
void CTimeSpan::Start(Float32 timeperiodms)
{
  FTimePeriodms = timeperiodms;
  FState = strInit;
}

void CTimeSpan::Abort(void)
{
  FState = strZero;
}

Boolean CTimeSpan::Execute(void)
{
  switch (FState)
  {
    case strZero:
      return false;
    case strInit:
      SetTimeStartActual();
      FState = strBusy;
      return true;
    case strBusy:
      if (FTimePeriodms <= GetTimeActualms())
      {
        FState = strZero;
        return false;
      }
      return true;  
  }
  return false;
}
//
