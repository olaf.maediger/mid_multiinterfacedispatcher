//
#include "Defines.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef DispatcherTrigger_h
#define DispatcherTrigger_h
//
#include "Dispatcher.h"
#include "Led.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - Trigger - SHORT
// //----------------------------------------------------------------
// #define SHORT_GLS   "GLS"
// #define SHORT_LSO   "LSO"
// #define SHORT_LSF   "LSF"
// #define SHORT_LSB   "LSB"
// #define SHORT_LSA   "LSA"
//
//----------------------------------------------------------------
// Dispatcher - Trigger - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_TRIGGER " Help (LedSystem):"
// #define MASK_GLS               " %-3s                : Get State LedSystem"
// #define MASK_LSO               " %-3s                : LedSystem On"
// #define MASK_LSF               " %-3s                : LedSystem oFf"
// #define MASK_LSB               " %-3s <n> <p> <w>    : LedSystem Blink  <n>times <p>eriod{ms}/<w>ith{ms}"
// #define MASK_LSA               " %-3s                : LedSystem Abort"
//
//----------------------------------------------------------------
// Dispatcher - Trigger
//----------------------------------------------------------------
class CDispatcherTrigger : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherTrigger(void);
  //
  // bool ExecuteGetLedSystem(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOn(char* command, int parametercount, char** parameters);
  // bool ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemBlink(char* command, int parametercount, char** parameters);
  // bool ExecuteLedSystemAbort(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool virtual Execute();
};
//
#endif // DispatcherLedSystem_h
//
#endif // DISPATCHER_TRIGGER
//
//###################################################################################
//###################################################################################
//###################################################################################
