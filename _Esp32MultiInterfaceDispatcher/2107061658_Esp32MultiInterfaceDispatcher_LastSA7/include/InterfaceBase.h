#include "Defines.h"
//
#ifndef InterfaceBase_h
#define InterfaceBase_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
const Int16 INTERFACE_SIZE_RXBLOCK    = 64;
const Int16 INTERFACE_SIZE_TXBLOCK    = 64;
//
const Int16 INTERFACE_SIZE_CONVERSION = 64;
//
//
//----------------------------------------------------
//  CInterfaceBase
//----------------------------------------------------
class CInterfaceBase
{
  protected:
  char FRXBlock[INTERFACE_SIZE_RXBLOCK];
  int  FRXIndex;
  char FTXBlock[INTERFACE_SIZE_TXBLOCK];
  char FCVBuffer[INTERFACE_SIZE_CONVERSION];
  //
  public:
  //
  // Constructor
  CInterfaceBase(void);
  //
  // Property
  char* GetPRXBlock(void)
  {
    return FRXBlock;
  }
  char* GetPTXBlock(void)
  {
    return FTXBlock;
  }
  //
  // Helper
  char* ConvertPChar(const char* mask, char* value);
  char* ConvertString(const char* mask, String value);
  char* ConvertByte(const char* mask, Byte value);
  char* ConvertDual(const char* mask, UInt16 value);
  char* ConvertQuad(const char* mask, UInt32 value);
  char* ConvertInt16(const char* mask, Int16 value);
  char* ConvertUInt16(const char* mask, UInt16 value);
  char* ConvertInt32(const char* mask, Int32 value);
  char* ConvertUInt32(const char* mask, UInt32 value);
  char* ConvertFloat32(const char* mask, Float32 value);
  char* ConvertDouble64(const char* mask, Double64 value);
  //
  // Handler
  virtual Boolean Open(int baudrate) = 0;
  virtual Boolean Close(void) = 0;
  //
  // Write
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  // Read
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};
//
#endif // InterfaceBase_h
//
//
