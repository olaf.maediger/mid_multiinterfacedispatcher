//
#include "Defines.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "Error.h"
#include "DispatcherTrigger.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
// extern CError Error;
// extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
// extern CLed LedSystem;
//
//#########################################################
//  Dispatcher - Trigger - Constructor
//#########################################################
CDispatcherTrigger::CDispatcherTrigger(void)
{
}
//
//#########################################################
//  Dispatcher - Trigger - Execution
//#########################################################
// bool CDispatcherTrigger::ExecuteGetLedSystem(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: -
//   // Response:
//   bool State = LedSystem.GetState();
//   sprintf(Command.GetBuffer(), "%s %i", command, State);
//   ExecuteResponse(Command.GetBuffer());
//   // Action: -  
//   ExecuteEnd();
//   return true;
// }
// //
// bool CDispatcherTrigger::ExecuteSetLedSystemOn(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: -
//   // Response:
//   sprintf(Command.GetBuffer(), "%s", command);
//   ExecuteResponse(Command.GetBuffer());
//   // Action:
//   LedSystem.SetOn();
//   ExecuteEnd();
//   return true;
// }
// //
// bool CDispatcherTrigger::ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: -
//   // Response:
//   sprintf(Command.GetBuffer(), "%s", command);
//   ExecuteResponse(Command.GetBuffer());
//   // Action:
//   LedSystem.SetOff();
//   ExecuteEnd();
//   return true;

// }
// bool CDispatcherTrigger::ExecuteLedSystemBlink(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: <n> <p> <w>
//   if (parametercount < 3)
//   {
//     Error.SetCode(ecMissingParameter);
//     return false;
//   }
//   int PC = atoi(parameters[0]);
//   int PP = atoi(parameters[1]);
//   int PW = atoi(parameters[2]);
//   // Response:
//   sprintf(Command.GetBuffer(), "%s %i %i %i", command, PC, PP, PW);
//   ExecuteResponse(Command.GetBuffer());
//   // Action:
//   LedSystem.Blink_Start(PC, PP, PW);
//   ExecuteEnd();
//   return true;
// }
// bool CDispatcherTrigger::ExecuteLedSystemAbort(char* command, int parametercount, char** parameters)
// {
//   ExecuteBegin();
//   // Analyse parameters: -
//   // Response:
//   sprintf(Command.GetBuffer(), "%s", command);
//   ExecuteResponse(Command.GetBuffer());
//   // Action:
//   LedSystem.Blink_Abort();
//   ExecuteEnd();
//   return true;
// }
//
//#########################################################
//  Dispatcher - Trigger - Handler
//#########################################################
bool CDispatcherTrigger::HandleInterface(char* command, int parametercount, char** parameters) 
{
  // if (!strcmp(SHORT_GLS, command))
  // {
  //   return ExecuteGetLedSystem(command, parametercount, parameters);
  // }
  // if (!strcmp(SHORT_LSO, command))
  // {
  //   return ExecuteSetLedSystemOn(command, parametercount, parameters);
  // }
  // if (!strcmp(SHORT_LSF, command))
  // {
  //   return ExecuteSetLedSystemOff(command, parametercount, parameters);
  // }
  // if (!strcmp(SHORT_LSB, command))
  // {
  //   return ExecuteLedSystemBlink(command, parametercount, parameters);
  // }
  // if (!strcmp(SHORT_LSA, command))
  // {
  //   return ExecuteLedSystemAbort(command, parametercount, parameters);
  // }
  return false;
}
//
bool CDispatcherTrigger::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_TRIGGER
//
//####################################################################
//####################################################################
//####################################################################
//
