//
#include "Defines.h"
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  CInterfaceBase - Constructor
//----------------------------------------------------
CInterfaceBase::CInterfaceBase(void)
{
}
//
//----------------------------------------------------
//  CInterfaceBase - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceBase - Helper
//----------------------------------------------------
char* CInterfaceBase::ConvertPChar(const char* mask, char* value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;
}
char* CInterfaceBase::ConvertString(const char* mask, String value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer; 
}
char* CInterfaceBase::ConvertByte(const char* mask, Byte value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDual(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertQuad(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt16(const char* mask, Int16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt16(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt32(const char* mask, Int32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt32(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertFloat32(const char* mask, Float32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDouble64(const char* mask, Double64 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
//
//----------------------------------------------------
//  CInterfaceBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBase - Read
//----------------------------------------------------
//
//