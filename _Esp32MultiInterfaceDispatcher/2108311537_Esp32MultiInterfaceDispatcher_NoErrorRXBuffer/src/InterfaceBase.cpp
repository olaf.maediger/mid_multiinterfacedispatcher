//
#include "DefinitionSystem.h"
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  CInterfaceBase - Constructor
//----------------------------------------------------
CInterfaceBase::CInterfaceBase(const char* deviceid)
{
  strncpy(FDeviceID, deviceid, INTERFACE_SIZE_DEVICEID - 1);
  FDeviceID[INTERFACE_SIZE_DEVICEID - 1] = 0x00;
  FRXIndex = 0;
  FRXBlock[0] = 0;
  FRXEcho = INTERFACE_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBase - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceBase - Helper
//----------------------------------------------------
char* CInterfaceBase::ConvertPChar(const char* mask, char* value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;
}
char* CInterfaceBase::ConvertString(const char* mask, String value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer; 
}
char* CInterfaceBase::ConvertByte(const char* mask, Byte value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDual(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertQuad(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt16(const char* mask, Int16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt16(const char* mask, UInt16 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertInt32(const char* mask, Int32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertUInt32(const char* mask, UInt32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertFloat32(const char* mask, Float32 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
char* CInterfaceBase::ConvertDouble64(const char* mask, Double64 value)
{
  sprintf(FCVBuffer, mask, value);
  return FCVBuffer;   
}
//
//----------------------------------------------------
//  CInterfaceBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBase - Write
//----------------------------------------------------
// Boolean CInterfaceBase::WriteCharacter(char character)

// Boolean CInterfaceBase::WriteText(char* ptext)
// {
//   FPSerial->print(ptext);
//   return true;
// }

// Boolean CInterfaceBase::WriteLine(void)
// {
//   FPSerial->write(TERMINAL_NEWLINE);
//   return true;  
// } 

// Boolean CInterfaceBase::WriteLine(char* pline)
// {
//   FPSerial->write(pline);
//   FPSerial->write(TERMINAL_NEWLINE);
//   return true;
// } 

// //
// //----------------------------------------------------
// //  CInterfaceBase - Read
// //----------------------------------------------------
// UInt8 CInterfaceBase::GetRxCount(void)
// {
//   return FPSerial->available();
// }

// Boolean CInterfaceBase::ReadCharacter(char &rxcharacter)
// {
//   rxcharacter = (char)0x00;
//   if (0 < FPSerial->available())
//   {
//     rxcharacter = FPSerial->read();
//     if (FRXEcho)
//     {
//       switch (rxcharacter)
//       {
//         case (char)0x0D:
//         case (char)0x0A:
//           break;
//         default:
//           FPSerial->write(rxcharacter);
//           break;
//       }
//     }
//     return true;
//    }
//    return false;
// }

// Boolean CInterfaceBase::ReadLine(char* prxline, int &rxsize)
// { // Separator: CR or LF
//   // !!! stay rxsize, use as limit !!!
//   while (0 < FPSerial->available())
//   {
//     char RXC = (char)FPSerial->read();
//     if (FRXEcho)
//     {
//       switch (RXC)
//       {
//         case (char)0x0D:
//         case (char)0x0A:
//           break;
//         default:
//           FPSerial->write(RXC);
//           break;
//       }
//     }
//     switch (RXC)
//     {
//       case (char)SEPARATOR_CR:
//       case (char)SEPARATOR_LF:
//         if (0 < FRXIndex)
//         {          
//           FRXBlock[FRXIndex] = SEPARATOR_ZERO;
//           if (0 < strlen(FRXBlock))
//           {
//             strcpy(prxline, FRXBlock);
//             rxsize = 1 + FRXIndex;
//             FRXIndex = 0;
//             FRXBlock[FRXIndex] = SEPARATOR_ZERO;
//             return true;
//           }
//         }
//         return false;
//       default:
//         FRXBlock[FRXIndex] = RXC;
//         FRXIndex++;
//         if (rxsize <= (1 + FRXIndex))
//         {
//           Error.SetCode(ecReceiveBufferOverflow);
//           return false;
//         }
//     }
//   }
//   return false;
// } 
//
//




