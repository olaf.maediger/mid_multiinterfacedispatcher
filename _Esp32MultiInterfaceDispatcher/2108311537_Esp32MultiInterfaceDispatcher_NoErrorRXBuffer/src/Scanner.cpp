//
//--------------------------------
//  Library Scanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_SCANNER)
//
#include "Scanner.h"
#include "InterfaceUart.h"
//
//-------------------------------------------------------------------------------
//  Scanner - Constructor
//-------------------------------------------------------------------------------
CScanner::CScanner(const char* pid, 
                   CDacMcp4725* pdacx, 
                   CDacMcp4725* pdacy,
                   DOnEventScanner oneventlaserscanner,
                   DOnEventTimeSpan oneventtimespan)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPDacX = pdacx;
  FPDacY = pdacy;
  FOnEvent = oneventlaserscanner;
  FState = sscUndefined;
  FPDelayMotion = new CTimeSpan("SCTS", oneventtimespan);
  //
  FPositionX = INIT_SCANNER_POSITIONX; // [stp]
  FPositionY = INIT_SCANNER_POSITIONY; // [stp]
  FDelayMotion = INIT_SCANNER_DELAYMOTION;   // [ms]
  //
  FPositionXL = INIT_SCANNER_RANGEXL;   // [stp]
  FPositionXH = INIT_SCANNER_RANGEXH;   // [stp]
  FPositionDX = INIT_SCANNER_RANGEDX;   // [ms]
  //
  FPositionYL = INIT_SCANNER_RANGEYL;   // [stp]
  FPositionYH = INIT_SCANNER_RANGEYH;   // [stp]
  FPositionDY = INIT_SCANNER_RANGEDY;   // [ms]
}
//
//-------------------------------------------------------------------------------
//  Scanner - Property
//-------------------------------------------------------------------------------
const char* CScanner::StateText(EStateScanner state)
{
  if (sscIdle == state) return SCANNER_EVENT_IDLE;
  if (sscMove == state) return SCANNER_EVENT_MOVE;
  if (sscWait == state) return SCANNER_EVENT_WAIT;
  if (sscScanX == state) return SCANNER_EVENT_SCANX;
  if (sscWaitX == state) return SCANNER_EVENT_WAITX;
  if (sscScanY == state) return SCANNER_EVENT_SCANY;
  if (sscWaitY == state) return SCANNER_EVENT_WAITY;
  if (sscScanXY == state) return SCANNER_EVENT_SCANXY;
  if (sscWaitXY == state) return SCANNER_EVENT_WAITXY;
  return SCANNER_EVENT_UNDEFINED;
}
EStateScanner CScanner::GetState(void)
{
  return FState;
}
void CScanner::SetState(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s", FID, StateText(FState));
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateScanX(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %u", FID, StateText(FState), FPositionX);
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateWaitX(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateScanY(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %u", FID, StateText(FState), FPositionY);
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateWaitY(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateScanXY(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %u %u", FID, StateText(FState), FPositionX, FPositionY);
      FOnEvent(this, FBuffer);
    }
  }
}
void CScanner::SetStateWaitXY(EStateScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "Scanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
UInt32 CScanner::GetPositionX(void)
{
  return FPositionX;
}
void CScanner::SetPositionX(UInt32 value)
{
  FPositionX = value;
}
//
UInt32 CScanner::GetPositionXL(void)
{
  return FPositionXL;
}
void CScanner::SetPositionXL(UInt32 value)
{
  FPositionXL = value;
}
//
UInt32 CScanner::GetPositionXH(void)
{
  return FPositionXH;
}
void CScanner::SetPositionXH(UInt32 value)
{
  FPositionXH = value;
}
//
UInt32 CScanner::GetPositionDX(void)
{
  return FPositionDX;
}
void CScanner::SetPositionDX(UInt32 value)
{
  FPositionDX = value;
}
//
UInt32 CScanner::GetPositionY(void)
{
  return FPositionY;
}
void CScanner::SetPositionY(UInt32 value)
{
  FPositionY = value;
}
//
UInt32 CScanner::GetPositionYL(void)
{
  return FPositionYL;
}
void CScanner::SetPositionYL(UInt32 value)
{
  FPositionYL = value;
}
//
UInt32 CScanner::GetPositionYH(void)
{
  return FPositionYH;
}
void CScanner::SetPositionYH(UInt32 value)
{
  FPositionYH = value;
}
//
UInt32 CScanner::GetPositionDY(void)
{
  return FPositionDY;
}
void CScanner::SetPositionDY(UInt32 value)
{
  FPositionDY = value;
}
//
Float32 CScanner::GetDelayMotion(void)
{
  return FDelayMotion;
}
void CScanner::SetDelayMotion(Float32 value)
{
  FDelayMotion = value;
}
//
void CScanner::GetRangeX(UInt32 &positionxl, 
                         UInt32 &positionxh, 
                         UInt32 &positiondx)
{
  positionxl = FPositionXL;  
  positionxh = FPositionXH;  
  positiondx = FPositionDX;  
}
void CScanner::SetRangeX(UInt32 positionxl,
                         UInt32 positionxh, 
                         UInt32 positiondx)
{
  FPositionXL = positionxl;
  FPositionXH = positionxh;  
  FPositionDX = positiondx;
}
void CScanner::GetRangeY(UInt32 &positionyl, 
                         UInt32 &positionyh, 
                         UInt32 &positiondy)
{
  positionyl = FPositionYL;  
  positionyh = FPositionYH;  
  positiondy = FPositionDY;  
}
void CScanner::SetRangeY(UInt32 positionyl, 
                         UInt32 positionyh, 
                         UInt32 positiondy)
{
  FPositionYL = positionyl;
  FPositionYH = positionyh;  
  FPositionDY = positiondy; 
}
//
//-------------------------------------------------------------------------------
//  Scanner - Method
//-------------------------------------------------------------------------------
Boolean CScanner::Open()
{
  FPDelayMotion->Open();
  //
  FPositionX = INIT_SCANNER_POSITIONX;      // [stp]
  FPositionY = INIT_SCANNER_POSITIONY;      // [stp]
  FDelayMotion = INIT_SCANNER_DELAYMOTION;  // [ms]
  //
  FPositionXL = INIT_SCANNER_RANGEXL;   // [stp]
  FPositionXH = INIT_SCANNER_RANGEXH;   // [stp]
  FPositionDX = INIT_SCANNER_RANGEDX;   // [ms]
  //
  FPositionYL = INIT_SCANNER_RANGEYL;   // [stp]
  FPositionYH = INIT_SCANNER_RANGEYH;   // [stp]
  FPositionDY = INIT_SCANNER_RANGEDY;   // [ms]
  //
  SetState(sscIdle);
  return true;
}
//
Boolean CScanner::Close()
{
  SetState(sscUndefined);
  return true;
}
//
Boolean CScanner::Move(UInt32 positionx, UInt32 positiony)
{
  FPositionX = positionx;
  FPositionY = positiony;
  // FTimeWait - no change!
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetState(sscMove);
  return true;
}  
Boolean CScanner::Wait(Float32 delaymotion)
{
  // FPositionX - no change!
  // FPositionY - no change!
  FDelayMotion = delaymotion;
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetState(sscWait);
  return true;
}  
Boolean CScanner::MoveWait(UInt32 positionx, UInt32 positiony, Float32 delaymotion)
{
  FPositionX = positionx;
  FPositionY = positiony;
  FDelayMotion = delaymotion;
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetState(sscWait);
  return true;
}
//
Boolean CScanner::ScanX(void)   // SCX(XL, XH, DX, DM)
{
  FPositionX = FPositionXL;
  FPositionY = FPositionY;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanX(sscScanX);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitX(sscWaitX);
  return true;
}
Boolean CScanner::ScanY(void)   // SCY(YL, YH, DY, DM)
{
  FPositionX = FPositionX;
  FPositionY = FPositionYL;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanY(sscScanY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitY(sscWaitY);
  return true;
}
Boolean CScanner::ScanXY(void)  // SXY(XL, XH, DX, YL, YH, DY, DM)
{
  FPositionX = FPositionXL;
  FPositionY = FPositionYL;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanXY(sscScanXY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitXY(sscWaitXY);
  return true;
}
//
//-------------------------------------------------------------------------------
//  Scanner - Execute
//-------------------------------------------------------------------------------
void CScanner::Execute(void)
{ // debug Serial.print(".");
  // debug delay(2999);
  FPDelayMotion->Execute();
  switch (FState)
  {
    case sscUndefined:
      return;
    case sscIdle:
      return;
    case sscMove:
      // NO wait !!!FPDelayMotion->StartPeriod(FDelayMotion);
      // SetState(slsWait);
      SetState(sscIdle);
      return;
    case sscWait:
      if (FPDelayMotion->IsIdle())
      {
        SetState(sscIdle);
      }
      return;
    //-------------------------------------
    case sscScanX:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitX(sscWaitX);
      return;
    case sscWaitX:
      if (FPDelayMotion->IsIdle())
      {
        FPositionX += FPositionDX;
        if (FPositionX <= FPositionXH)
        {
          SetStateScanX(sscScanX);
        }
        else
        {
          SetState(sscIdle);
        }        
      }
      return;
    //-------------------------------------
    case sscScanY:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitY(sscWaitY);
      return;
    case sscWaitY:
      if (FPDelayMotion->IsIdle())
      {
        FPositionY += FPositionDY;
        if (FPositionY <= FPositionYH)
        {
          SetStateScanY(sscScanY);
        }
        else
        {
          SetState(sscIdle);
        }        
      }
      return;
    //-------------------------------------
    case sscScanXY:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitXY(sscWaitXY);
      return;
    case sscWaitXY:
      if (FPDelayMotion->IsIdle())
      {
        FPositionX += FPositionDX;
        if (FPositionX <= FPositionXH)
        {
          SetStateScanXY(sscScanXY);
        }
        else
        {
          FPositionX = FPositionXL;
          FPositionY += FPositionDY;
          if (FPositionY <= FPositionYH)
          {
            SetStateScanXY(sscScanXY);
          }
          else
          {
            FPositionY = FPositionYL;
            SetState(sscIdle);
          }
        }
      }
      return;    
    //-------------------------------------
  }
}
//
#endif // DISPATCHER_SCANNER
//
