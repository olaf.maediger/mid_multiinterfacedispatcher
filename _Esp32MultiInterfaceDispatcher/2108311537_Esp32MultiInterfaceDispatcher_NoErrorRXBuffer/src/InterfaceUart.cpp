//
#include "DefinitionSystem.h"
//
#if defined(INTERFACE_UART)
//
#include "Error.h"
#include "InterfaceUart.h"
//
extern CError Error;
//
//####################################################
//  CUartBase
//####################################################
//----------------------------------------------------
//  CUartBase - Constructor
//----------------------------------------------------
CUartBase::CUartBase(const char* deviceid)
  : CInterfaceBase(deviceid)
{
  FRXEcho = UART_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CUartBase - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CUartBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CUartBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CUartBase - Read
//----------------------------------------------------
//
//
//####################################################
//  CUartHS
//####################################################
//----------------------------------------------------
//  CUartHS - Constructor
//----------------------------------------------------
CUartHS::CUartHS(const char* deviceid, HardwareSerial* pserial)
  : CUartBase(deviceid)
{
  FPSerial = pserial;
} 
//
//----------------------------------------------------
//  CUartHS - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CUartHS - Handler
//----------------------------------------------------
Boolean CUartHS::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  return true;
} 
Boolean CUartHS::Close(void)
{
  FPSerial->end();
  return true;
} 
//
//----------------------------------------------------
//  CUartHS - Read
//----------------------------------------------------
UInt8 CUartHS::GetRXCount(void)
{
  return FPSerial->available();
}

Boolean CUartHS::Read(char &character)
{
  character = (char)0x00;
  if (0 < FPSerial->available())
  {
    character = FPSerial->read();
    if (FRXEcho)
    {
      switch (character)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(character);
          break;
      }
    }
    return true;
   }
   return false;
}

Boolean CUartHS::ReadLine(char* prxline, int &rxsize)
{ // Separator: CR or LF
  // !!! stay rxsize, use as limit !!!
  while (0 < FPSerial->available())
  {
    char RXC = (char)FPSerial->read();
    if (FRXEcho)
    {
      switch (RXC)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(RXC);
          break;
      }
    }
    switch (RXC)
    {
      case (char)SEPARATOR_CR:
      case (char)SEPARATOR_LF:
        if (0 < FRXIndex)
        {          
          FRXBlock[FRXIndex] = SEPARATOR_ZERO;
          if (0 < strlen(FRXBlock))
          {
            strcpy(prxline, FRXBlock);
            rxsize = 1 + FRXIndex;
            FRXIndex = 0;
            FRXBlock[FRXIndex] = SEPARATOR_ZERO;
            return true;
          }
        }
        return false;
      default:
        FRXBlock[FRXIndex] = RXC;
        FRXIndex++;
        if (rxsize <= (1 + FRXIndex))
        {
          Error.SetCode(ecReceiveBufferOverflow);
          return false;
        }
    }
  }
  return false;
} 
//
//----------------------------------------------------
//  CUartHS - Write
//----------------------------------------------------
Boolean CUartHS::Write(char character)
{
  FPSerial->print(character);
  return true;
}

Boolean CUartHS::WriteText(char* ptext)
{
  FPSerial->print(ptext);
  return true;
}

Boolean CUartHS::WriteLine(void)
{
  FPSerial->write(TERMINAL_NEWLINE);
  return true;  
} 

Boolean CUartHS::WriteLine(char* pline)
{
  FPSerial->write(pline);
  FPSerial->write(TERMINAL_NEWLINE);
  return true;
} 
//
//####################################################
//  CUart
//####################################################
//----------------------------------------------------
//  CUart - Constructor
//----------------------------------------------------
CUart::CUart(const char* deviceid, HardwareSerial* pserial)
{
  FPUartBase = (CUartBase*)new CUartHS(deviceid, pserial);
}
//
//----------------------------------------------------
//  CUart - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CUart - Handler
//----------------------------------------------------
Boolean CUart::Open(int baudrate)
{
  return FPUartBase->Open(baudrate);
}
Boolean CUart::Close(void)
{
  return FPUartBase->Close();
}
//
//----------------------------------------------------
//  CUart - Read
//----------------------------------------------------
UInt8 CUart::GetRXCount(void)
{
  return FPUartBase->GetRXCount();
}
Boolean CUart::Read(char &character)
{
  return FPUartBase->Read(character);
}
Boolean CUart::ReadLine(char* prxline, int &rxsize)
{
  return FPUartBase->ReadLine(prxline, rxsize);
}
//
//----------------------------------------------------
//  CUart - Write
//----------------------------------------------------
Boolean CUart::WriteLine(void)
{
  return FPUartBase->WriteLine();
}
Boolean CUart::WriteText(const char* ptext)
{
  return FPUartBase->WriteText((char*)ptext);
}
Boolean CUart::WriteText(const char* mask, const char* ptext)
{
  if (0 != ptext)
  {
    return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)ptext));
  }  
  return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)""));
}
Boolean CUart::WriteLine(const char* pline)
{
  return FPUartBase->WriteLine((char*)pline);
}
Boolean CUart::WriteLine(const char* mask, const char* pline)
{
  if (0 != pline)
  {
    return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)pline));
  }  
  return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)""));
}  
Boolean CUart::WritePChar(const char* mask, char* value)
{
  if (0 != value)
  {    
    return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, value));
  }
  return FPUartBase->WriteText((char*)mask);
}
Boolean CUart::WriteString(const char* mask, String value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertString(mask, value));
}
Boolean CUart::WriteByte(const char* mask, Byte value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertByte(mask, value));
}
Boolean CUart::WriteDual(const char* mask, UInt16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertDual(mask, value));
}
Boolean CUart::WriteQuad(const char* mask, UInt32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertQuad(mask, value));
}
Boolean CUart::WriteInt16(const char* mask, Int16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertInt16(mask, value));
}
Boolean CUart::WriteUInt16(const char* mask, UInt16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertUInt16(mask, value));
}
Boolean CUart::WriteInt32(const char* mask, Int32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertInt32(mask, value));
}
Boolean CUart::WriteUInt32(const char* mask, UInt32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertUInt32(mask, value));
}
Boolean CUart::WriteFloat32(const char* mask, Float32 value)
{  
  return FPUartBase->WriteText(FPUartBase->ConvertFloat32(mask, value));
}
Boolean CUart::WriteDouble64(const char* mask, Double64 value)
{  
  return FPUartBase->WriteText(FPUartBase->ConvertDouble64(mask, value));
}
//
//------------------------------------------------------------------------
// Uart - Write - Specials
//------------------------------------------------------------------------
Boolean CUart::WriteComment(void)
{
  return FPUartBase->Write(TERMINAL_COMMENT[0]);
}
Boolean CUart::WriteComment(String comment)
{
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_COMMENT, comment.c_str());
  return FPUartBase->WriteText(FPUartBase->GetTXBlock());
}
Boolean CUart::WriteResponse(String text)
{  
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_RESPONSE, text.c_str());
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}

Boolean CUart::WriteEvent(const char* event)
{
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_EVENT, event);
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}
Boolean CUart::WriteEvent(const char* mask, const char* event)
{  
  sprintf(FPUartBase->GetTXBlock(), mask, event);
  FPUartBase->WriteText((char*)TERMINAL_EVENT);
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}

//
//-------------------------------------------------------------------
//  Uart - Execute
//-------------------------------------------------------------------
// Boolean CUart::Execute(void)
// {
//   // while (0 < GetRxCount())
//   // {
//   //   Char C = F
//   //   switch (C)
//   //   {
//   //     case (byte)SEPARATOR_CR:
//   //     case (byte)SEPARATOR_LF:
//   //       FRxdBufferIndex, SEPARATOR_ZERO);
//   //       FRxdBufferIndex = 0; // restart
//   //       strupr(FRxdBuffer);
//   //       strcpy(FCommandText, FRxdBuffer);
//   //       return true;
//   //     default: 
//   //       FRxdBuffer[FRxdBufferIndex] = C;
//   //       FRxdBufferIndex++;
//   //       break;
//   //   }
//   // }  
// }











#endif // INTERFACE_UART
//