//
//--------------------------------
//  Library Scanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#include "LaserScanner.h"
#include "InterfaceUart.h"
//
//-------------------------------------------------------------------------------
//  Scanner - Constructor
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(const char* pid, 
                             CDacMcp4725* pdacx, 
                             CDacMcp4725* pdacy,
                             DOnEventLaserScanner oneventlaserscanner,
                             DOnEventTimeSpan oneventtimespan)
{
  strcpy(FID, pid);
  FOnEvent = 0;
  FPDacX = pdacx;
  FPDacY = pdacy;
  FOnEvent = oneventlaserscanner;
  FState = slsUndefined;
  FPDelayMotion = new CTimeSpan("SCTS", oneventtimespan);
  //
  FPositionX = INIT_LASERSCANNER_POSITIONX; // [stp]
  FPositionY = INIT_LASERSCANNER_POSITIONY; // [stp]
  FDelayMotion = INIT_LASERSCANNER_DELAYMOTION;   // [ms]
  //
  FPositionXL = INIT_LASERSCANNER_RANGEXL;   // [stp]
  FPositionXH = INIT_LASERSCANNER_RANGEXH;   // [stp]
  FPositionDX = INIT_LASERSCANNER_RANGEDX;   // [ms]
  //
  FPositionYL = INIT_LASERSCANNER_RANGEYL;   // [stp]
  FPositionYH = INIT_LASERSCANNER_RANGEYH;   // [stp]
  FPositionDY = INIT_LASERSCANNER_RANGEDY;   // [ms]
}
//
//-------------------------------------------------------------------------------
//  Scanner - Property
//-------------------------------------------------------------------------------
const char* CLaserScanner::StateText(EStateLaserScanner state)
{
  if (slsIdle == state) return SCANNER_EVENT_IDLE;
  if (slsMove == state) return SCANNER_EVENT_MOVE;
  if (slsWait == state) return SCANNER_EVENT_WAIT;
  if (slsScanX == state) return SCANNER_EVENT_SCANX;
  if (slsWaitX == state) return SCANNER_EVENT_WAITX;
  if (slsScanY == state) return SCANNER_EVENT_SCANY;
  if (slsWaitY == state) return SCANNER_EVENT_WAITY;
  if (slsScanXY == state) return SCANNER_EVENT_SCANXY;
  if (slsWaitXY == state) return SCANNER_EVENT_WAITXY;
  return SCANNER_EVENT_UNDEFINED;
}
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}
void CLaserScanner::SetState(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s", FID, StateText(FState));
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateScanX(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %u", FID, StateText(FState), FPositionX);
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateWaitX(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateScanY(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %u", FID, StateText(FState), FPositionY);
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateWaitY(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateScanXY(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %u %u", FID, StateText(FState), FPositionX, FPositionY);
      FOnEvent(this, FBuffer);
    }
  }
}
void CLaserScanner::SetStateWaitXY(EStateLaserScanner state)
{
  if (state != FState)
  {
    FState = state;
    if (0 != FOnEvent)
    {
      sprintf(FBuffer, "LaserScanner[%s]: %s %f", FID, StateText(FState), FDelayMotion);
      FOnEvent(this, FBuffer);
    }
  }
}
UInt32 CLaserScanner::GetPositionX(void)
{
  return FPositionX;
}
void CLaserScanner::SetPositionX(UInt32 value)
{
  FPositionX = value;
}
//
UInt32 CLaserScanner::GetPositionXL(void)
{
  return FPositionXL;
}
void CLaserScanner::SetPositionXL(UInt32 value)
{
  FPositionXL = value;
}
//
UInt32 CLaserScanner::GetPositionXH(void)
{
  return FPositionXH;
}
void CLaserScanner::SetPositionXH(UInt32 value)
{
  FPositionXH = value;
}
//
UInt32 CLaserScanner::GetPositionDX(void)
{
  return FPositionDX;
}
void CLaserScanner::SetPositionDX(UInt32 value)
{
  FPositionDX = value;
}
//
UInt32 CLaserScanner::GetPositionY(void)
{
  return FPositionY;
}
void CLaserScanner::SetPositionY(UInt32 value)
{
  FPositionY = value;
}
//
UInt32 CLaserScanner::GetPositionYL(void)
{
  return FPositionYL;
}
void CLaserScanner::SetPositionYL(UInt32 value)
{
  FPositionYL = value;
}
//
UInt32 CLaserScanner::GetPositionYH(void)
{
  return FPositionYH;
}
void CLaserScanner::SetPositionYH(UInt32 value)
{
  FPositionYH = value;
}
//
UInt32 CLaserScanner::GetPositionDY(void)
{
  return FPositionDY;
}
void CLaserScanner::SetPositionDY(UInt32 value)
{
  FPositionDY = value;
}
//
Float32 CLaserScanner::GetDelayMotion(void)
{
  return FDelayMotion;
}
void CLaserScanner::SetDelayMotion(Float32 value)
{
  FDelayMotion = value;
}
//
void CLaserScanner::GetRangeX(UInt32 &positionxl, 
                              UInt32 &positionxh, 
                              UInt32 &positiondx)
{
  positionxl = FPositionXL;  
  positionxh = FPositionXH;  
  positiondx = FPositionDX;  
}
void CLaserScanner::SetRangeX(UInt32 positionxl,
                              UInt32 positionxh, 
                              UInt32 positiondx)
{
  FPositionXL = positionxl;
  FPositionXH = positionxh;  
  FPositionDX = positiondx;
}
void CLaserScanner::GetRangeY(UInt32 &positionyl, 
                              UInt32 &positionyh, 
                              UInt32 &positiondy)
{
  positionyl = FPositionYL;  
  positionyh = FPositionYH;  
  positiondy = FPositionDY;  
}
void CLaserScanner::SetRangeY(UInt32 positionyl, 
                              UInt32 positionyh, 
                              UInt32 positiondy)
{
  FPositionYL = positionyl;
  FPositionYH = positionyh;  
  FPositionDY = positiondy; 
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Method
//-------------------------------------------------------------------------------
Boolean CLaserScanner::Open()
{
  FPDelayMotion->Open();
  //
  FPositionX = INIT_LASERSCANNER_POSITIONX;      // [stp]
  FPositionY = INIT_LASERSCANNER_POSITIONY;      // [stp]
  FDelayMotion = INIT_LASERSCANNER_DELAYMOTION;  // [ms]
  //
  FPositionXL = INIT_LASERSCANNER_RANGEXL;   // [stp]
  FPositionXH = INIT_LASERSCANNER_RANGEXH;   // [stp]
  FPositionDX = INIT_LASERSCANNER_RANGEDX;   // [ms]
  //
  FPositionYL = INIT_LASERSCANNER_RANGEYL;   // [stp]
  FPositionYH = INIT_LASERSCANNER_RANGEYH;   // [stp]
  FPositionDY = INIT_LASERSCANNER_RANGEDY;   // [ms]
  //
  SetState(slsIdle);
  return true;
}
//
Boolean CLaserScanner::Close()
{
  SetState(slsUndefined);
  return true;
}
//
Boolean CLaserScanner::Move(UInt32 positionx, UInt32 positiony)
{
  FPositionX = positionx;
  FPositionY = positiony;
  // FTimeWait - no change!
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetState(slsMove);
  return true;
}  
Boolean CLaserScanner::Wait(Float32 delaymotion)
{
  // FPositionX - no change!
  // FPositionY - no change!
  FDelayMotion = delaymotion;
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetState(slsWait);
  return true;
}  
Boolean CLaserScanner::MoveWait(UInt32 positionx, UInt32 positiony, Float32 delaymotion)
{
  FPositionX = positionx;
  FPositionY = positiony;
  FDelayMotion = delaymotion;
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetState(slsWait);
  return true;
}
//
Boolean CLaserScanner::ScanX(void)   // SCX(XL, XH, DX, DM)
{
  FPositionX = FPositionXL;
  FPositionY = FPositionY;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanX(slsScanX);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitX(slsWaitX);
  return true;
}
Boolean CLaserScanner::ScanY(void)   // SCY(YL, YH, DY, DM)
{
  FPositionX = FPositionX;
  FPositionY = FPositionYL;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanY(slsScanY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitY(slsWaitY);
  return true;
}
Boolean CLaserScanner::ScanXY(void)  // SXY(XL, XH, DX, YL, YH, DY, DM)
{
  FPositionX = FPositionXL;
  FPositionY = FPositionYL;
  // FDelayMotion
  FPDacX->SetValue(FPositionX);
  FPDacY->SetValue(FPositionY);
  SetStateScanXY(slsScanXY);
  FPDelayMotion->StartPeriod(FDelayMotion);
  SetStateWaitXY(slsWaitXY);
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Execute
//-------------------------------------------------------------------------------
void CLaserScanner::Execute(void)
{ // debug Serial.print(".");
  // debug delay(2999);
  FPDelayMotion->Execute();
  switch (FState)
  {
    case slsUndefined:
      return;
    case slsIdle:
      return;
    case slsMove:
      // NO wait !!!FPDelayMotion->StartPeriod(FDelayMotion);
      // SetState(slsWait);
      SetState(slsIdle);
      return;
    case slsWait:
      if (FPDelayMotion->IsIdle())
      {
        SetState(slsIdle);
      }
      return;
    //-------------------------------------
    case slsScanX:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitX(slsWaitX);
      return;
    case slsWaitX:
      if (FPDelayMotion->IsIdle())
      {
        FPositionX += FPositionDX;
        if (FPositionX <= FPositionXH)
        {
          SetStateScanX(slsScanX);
        }
        else
        {
          SetState(slsIdle);
        }        
      }
      return;
    //-------------------------------------
    case slsScanY:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitY(slsWaitY);
      return;
    case slsWaitY:
      if (FPDelayMotion->IsIdle())
      {
        FPositionY += FPositionDY;
        if (FPositionY <= FPositionYH)
        {
          SetStateScanY(slsScanY);
        }
        else
        {
          SetState(slsIdle);
        }        
      }
      return;
    //-------------------------------------
    case slsScanXY:
      FPDacX->SetValue(FPositionX);
      FPDacY->SetValue(FPositionY);
      FPDelayMotion->StartPeriod(FDelayMotion);
      SetStateWaitXY(slsWaitXY);
      return;
    case slsWaitXY:
      if (FPDelayMotion->IsIdle())
      {
        FPositionX += FPositionDX;
        if (FPositionX <= FPositionXH)
        {
          SetStateScanXY(slsScanXY);
        }
        else
        {
          FPositionX = FPositionXL;
          FPositionY += FPositionDY;
          if (FPositionY <= FPositionYH)
          {
            SetStateScanXY(slsScanXY);
          }
          else
          {
            FPositionY = FPositionYL;
            SetState(slsIdle);
          }
        }
      }
      return;    
    //-------------------------------------
  }
}
//
#endif // DISPATCHER_LASERSCANNER
//
