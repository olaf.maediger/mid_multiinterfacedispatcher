//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#ifndef DispatcherTrigger_h
#define DispatcherTrigger_h
//
#include "Error.h"
#include "Dispatcher.h"
#include "Trigger.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - Trigger - SHORT
// //----------------------------------------------------------------
#define SHORT_STA   "STA"
#define SHORT_STP   "STP"
#define SHORT_PTA   "PTA"
#define SHORT_PTP   "PTP"
#define SHORT_PSA   "PSA"
#define SHORT_PSP   "PSP"
#define SHORT_GTL   "GTL"
#define SHORT_WTA   "WTA"
#define SHORT_WTP   "WTP"
//
//----------------------------------------------------------------
// Dispatcher - Trigger - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_TRIGGER " Help (Trigger):"
#define MASK_STA    " %-3s                 : Set TriggerOut Active"
#define MASK_STP    " %-3s                 : Set TriggerOut Passive"
#define MASK_PTA    " %-3s <t>             : Pulse TriggerOut Active <t>ime{ms}"
#define MASK_PTP    " %-3s <t>             : Pulse TriggerOut Passive <t>ime{ms}"
#define MASK_PSA    " %-3s <p> <w> <c>     : Pulse Sequence Active <p>eriod{ms} <w>idth{ms} <c>ount"
#define MASK_PSP    " %-3s <p> <w> <c>     : Pulse Sequence Passive <p>eriod{ms} <w>idth{ms} <c>ount"
#define MASK_GTL    " %-3s                 : Get TriggerIn Level"
#define MASK_WTA    " %-3s <t>             : Wait TriggerIn Active <t>ime{ms}"
#define MASK_WTP    " %-3s <t>             : Wait TriggerIn Passive <t>ime{ms}"
//
//----------------------------------------------------------------
// Dispatcher - Trigger
//----------------------------------------------------------------
class CDispatcherTrigger : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherTrigger(void);
  //
  bool ExecuteSetTriggerOutActive(char* command, int parametercount, char** parameters);
  bool ExecuteSetTriggerOutPassive(char* command, int parametercount, char** parameters);
  bool ExecutePulseTriggerOutActive(char* command, int parametercount, char** parameters);
  bool ExecutePulseTriggerOutPassive(char* command, int parametercount, char** parameters);  
  bool ExecutePulseSequenceActive(char* command, int parametercount, char** parameters);  
  bool ExecutePulseSequencePassive(char* command, int parametercount, char** parameters);  
  bool ExecuteGetTriggerInLevel(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerInActive(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTriggerInPassive(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
};
//
#endif // DispatcherLedSystem_h
//
#endif // DISPATCHER_TRIGGER
//
//###################################################################################
//###################################################################################
//###################################################################################
