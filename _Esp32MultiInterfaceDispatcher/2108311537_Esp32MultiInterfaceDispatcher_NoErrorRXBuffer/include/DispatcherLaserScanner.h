//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#ifndef DispatcherLaserScanner_h
#define DispatcherLaserScanner_h
//
#include "Error.h"
#include "Dispatcher.h"
#include "LaserScanner.h"
//
#if defined(TRIGGEROUT_LASER) | defined(TRIGGERIN_SYNCHRONIZE)
#include "Trigger.h"
#endif
//
#if defined(DACX_INTERNAL) | defined(DACY_INTERNAL)
#include "DacInternal.h"
#endif
#if defined(DACX_MCP4725) | defined(DACY_MCP4725)
#include "DacMcp4725.h"
#endif
// //
// //----------------------------------------------------------------
// // Dispatcher - LaserScanner - SHORT
// //----------------------------------------------------------------
#define SHORT_PLP   "PLP"
#define SHORT_ALP   "ALP"
#define SHORT_PLR   "PLR"
#define SHORT_ALR   "ALP"
#define SHORT_PLC   "PLC"
#define SHORT_ALC   "ALC"
//
//----------------------------------------------------------------
// Dispatcher - LaserScanner - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LASERSCANNER   " Help (LaserScanner):"
#define MASK_PLP                    " %-3s <x> <y> <p> <c> : Pulse Laser Position<x><y> <p>eriod{ms} <c>ount"
#define MASK_ALP                    " %-3s                 : Abort Laser Position"
#define MASK_PLR                    " %-3s <p> <n>         : Pulse Laser Row <p>eriod{ms} <n>Pulses"
#define MASK_ALR                    " %-3s                 : Abort Laser Row"
#define MASK_PLC                    " %-3s <p> <n>         : Pulse Laser Column <p>eriod{ms} <n>Pulses"
#define MASK_ALC                    " %-3s                 : Abort Laser Column"
//
//----------------------------------------------------------------
// Dispatcher - LaserScanner
//----------------------------------------------------------------
class CDispatcherLaserScanner : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLaserScanner(void);
  //
  bool ExecutePulseLaserPosition(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserPosition(char* command, int parametercount, char** parameters);
  bool ExecutePulseLaserRow(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserRow(char* command, int parametercount, char** parameters);
  bool ExecutePulseLaserColumn(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserColumn(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
};
//
#endif // DispatcherLaserScanner_h
//
#endif // DISPATCHER_LASERSCANNER
//

