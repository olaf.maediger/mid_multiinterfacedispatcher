//
//--------------------------------
//  Library Scanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_SCANNER)
//
#ifndef Scanner_h
#define Scanner_h
//
#include "DacMcp4725.h"
#include "TimeSpan.h"
//
//--------------------------------
//  Scanner - Constant
//--------------------------------
const int SCANNER_SIZE_ID                   = 6;
const int SCANNER_SIZE_BUFFER               = 48;
//
const char* const SCANNER_EVENT_MASK        = "Scanner[%s]=%s";
//
const char* const SCANNER_EVENT_UNDEFINED   = "Undefined";
const char* const SCANNER_EVENT_IDLE        = "Idle";
const char* const SCANNER_EVENT_MOVE        = "Move";
const char* const SCANNER_EVENT_WAIT        = "Wait";
const char* const SCANNER_EVENT_SCANX       = "ScanX";
const char* const SCANNER_EVENT_WAITX       = "WaitX";
const char* const SCANNER_EVENT_SCANY       = "ScanY";
const char* const SCANNER_EVENT_WAITY       = "WaitY";
const char* const SCANNER_EVENT_SCANXY      = "ScanXY";
const char* const SCANNER_EVENT_WAITXY      = "WaitXY";
//
const char* const STATE_SCANNER[] = {SCANNER_EVENT_UNDEFINED, 
                                     SCANNER_EVENT_IDLE, 
                                     SCANNER_EVENT_MOVE,
                                     SCANNER_EVENT_WAIT,
                                     SCANNER_EVENT_SCANX,
                                     SCANNER_EVENT_WAITX,
                                     SCANNER_EVENT_SCANY,
                                     SCANNER_EVENT_WAITY,
                                     SCANNER_EVENT_SCANXY,
                                     SCANNER_EVENT_WAITXY};
//
const UInt32 INIT_SCANNER_POSITIONX = 2000;   // [stp]
const UInt32 INIT_SCANNER_POSITIONY = 2000;   // [stp]
const Float32 INIT_SCANNER_DELAYMOTION = 100.0;  // [ms]
//
const UInt32 INIT_SCANNER_RANGEXL = 1800;   // [stp]
const UInt32 INIT_SCANNER_RANGEXH = 2200;   // [stp]
const UInt32 INIT_SCANNER_RANGEDX = 100;    // [ms]
//
const UInt32 INIT_SCANNER_RANGEYL = 1800;   // [stp]
const UInt32 INIT_SCANNER_RANGEYH = 2200;   // [stp]
const UInt32 INIT_SCANNER_RANGEDY = 100;    // [ms]
//
//--------------------------------
//  Scanner - Type
//--------------------------------
class CScanner;
typedef void (*DOnEventScanner)(CScanner* plaserscanner, const char* pevent);
//
enum EStateScanner
{
  sscUndefined = 0,
  sscIdle = 1,
  sscMove = 2,
  sscWait = 3,
  sscScanX = 4,
  sscWaitX = 5,
  sscScanY = 6,
  sscWaitY = 7,
  sscScanXY = 8,
  sscWaitXY = 9
};
//
class CScanner
{
  private:
  EStateScanner FState;
  char FID[SCANNER_SIZE_ID];
  char FBuffer[SCANNER_SIZE_BUFFER];
  CDacMcp4725* FPDacX;
  CDacMcp4725* FPDacY;
  CTimeSpan* FPDelayMotion;
  DOnEventScanner FOnEvent;
  UInt32 FPositionX, FPositionY;
  UInt32 FPositionXL, FPositionXH, FPositionDX;
  UInt32 FPositionYL, FPositionYH, FPositionDY;
  Float32 FDelayMotion;
  //  
  public:
  // Constructor
  CScanner(const char* pid, 
           CDacMcp4725* pdacx, 
           CDacMcp4725* pdacy,
           DOnEventScanner oneventscanner,
           DOnEventTimeSpan oneventdelaymotion);
  // Property
  const char* StateText(EStateScanner state);
  EStateScanner GetState(void);
  void SetState(EStateScanner state);
  void SetStateScanX(EStateScanner state);
  void SetStateWaitX(EStateScanner state);
  void SetStateScanY(EStateScanner state);
  void SetStateWaitY(EStateScanner state);
  void SetStateScanXY(EStateScanner state);
  void SetStateWaitXY(EStateScanner state);
  UInt32 GetPositionX(void);
  void SetPositionX(UInt32 value);
  UInt32 GetPositionXL(void);
  void SetPositionXL(UInt32 value);
  UInt32 GetPositionXH(void);
  void SetPositionXH(UInt32 value);
  UInt32 GetPositionDX(void);
  void SetPositionDX(UInt32 value);
  UInt32 GetPositionY(void);
  void SetPositionY(UInt32 value);
  UInt32 GetPositionYL(void);
  void SetPositionYL(UInt32 value);
  UInt32 GetPositionYH(void);
  void SetPositionYH(UInt32 value);
  UInt32 GetPositionDY(void);
  void SetPositionDY(UInt32 value);
  Float32 GetDelayMotion(void);
  void SetDelayMotion(Float32 value);
  void GetRangeX(UInt32 &positionxl, 
                 UInt32 &positionxh, 
                 UInt32 &positiondx);
  void SetRangeX(UInt32 positionxl, 
                 UInt32 positionxh, 
                 UInt32 positiondx);
  void GetRangeY(UInt32 &positionyl, 
                 UInt32 &positionyh, 
                 UInt32 &positiondy);
  void SetRangeY(UInt32 positionyl, 
                 UInt32 positionyh, 
                 UInt32 positiondy);
  // Method
  Boolean Open(void);
  Boolean Close(void);
  //
  Boolean Wait(Float32 delaymotion);
  Boolean Move(UInt32 positionx, UInt32 positiony);
  Boolean MoveWait(UInt32 positionx, UInt32 positiony, Float32 delaymotion);
  //
  Boolean ScanX(void);   // SCX(XL, XH, DX, DM)
  Boolean ScanY(void);   // SCY(YL, YH, DY, DM)
  Boolean ScanXY(void);  // SXY(XL, XH, DX, YL, YH, DY, DM)
  //
  void Execute(void);
};
//
#endif // Scanner_h
//
#endif // DISPATCHER_SCANNER
//
