//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "DefinitionSystem.h"
//
#if defined(DISPATCHER_LASERSCANNER)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Scanner.h"
//
//--------------------------------
//  Scanner - Constant
//--------------------------------
const int LASERSCANNER_SIZE_ID                   = 6;
const int LASERSCANNER_SIZE_BUFFER               = 48;
//
const char* const LASERSCANNER_EVENT_MASK        = "LaserScanner[%s]=%s";
//
const char* const LASERSCANNER_EVENT_UNDEFINED   = "Undefined";
const char* const LASERSCANNER_EVENT_IDLE        = "Idle";
const char* const LASERSCANNER_EVENT_MOVE        = "Move";
const char* const LASERSCANNER_EVENT_WAIT        = "Wait";
const char* const LASERSCANNER_EVENT_SCANX       = "ScanX";
const char* const LASERSCANNER_EVENT_WAITX       = "WaitX";
const char* const LASERSCANNER_EVENT_SCANY       = "ScanY";
const char* const LASERSCANNER_EVENT_WAITY       = "WaitY";
const char* const LASERSCANNER_EVENT_SCANXY      = "ScanXY";
const char* const LASERSCANNER_EVENT_WAITXY      = "WaitXY";
//
const char* const STATE_LASERSCANNER[] = {LASERSCANNER_EVENT_UNDEFINED, 
                                          LASERSCANNER_EVENT_IDLE, 
                                          LASERSCANNER_EVENT_MOVE,
                                          LASERSCANNER_EVENT_WAIT,
                                          LASERSCANNER_EVENT_SCANX,
                                          LASERSCANNER_EVENT_WAITX,
                                          LASERSCANNER_EVENT_SCANY,
                                          LASERSCANNER_EVENT_WAITY,
                                          LASERSCANNER_EVENT_SCANXY,
                                          LASERSCANNER_EVENT_WAITXY};
//
const UInt32 INIT_LASERSCANNER_POSITIONX = 2000;   // [stp]
const UInt32 INIT_LASERSCANNER_POSITIONY = 2000;   // [stp]
const Float32 INIT_LASERSCANNER_DELAYMOTION = 100.0;  // [ms]
//
const UInt32 INIT_LASERSCANNER_RANGEXL = 1800;   // [stp]
const UInt32 INIT_LASERSCANNER_RANGEXH = 2200;   // [stp]
const UInt32 INIT_LASERSCANNER_RANGEDX = 100;    // [ms]
//
const UInt32 INIT_LASERSCANNER_RANGEYL = 1800;   // [stp]
const UInt32 INIT_LASERSCANNER_RANGEYH = 2200;   // [stp]
const UInt32 INIT_LASERSCANNER_RANGEDY = 100;    // [ms]
//
//--------------------------------
//  Scanner - Type
//--------------------------------
class CLaserScanner;
typedef void (*DOnEventLaserScanner)(CLaserScanner* plaserscanner, const char* pevent);
//
enum EStateLaserScanner
{
  slsUndefined = 0,
  slsIdle = 1,
  slsMove = 2,
  slsWait = 3,
  slsScanX = 4,
  slsWaitX = 5,
  slsScanY = 6,
  slsWaitY = 7,
  slsScanXY = 8,
  slsWaitXY = 9
};
//
class CLaserScanner
{
  private:
  EStateLaserScanner FState;
  char FID[LASERSCANNER_SIZE_ID];
  char FBuffer[LASERSCANNER_SIZE_BUFFER];
  CDacMcp4725* FPDacX;
  CDacMcp4725* FPDacY;
  CTimeSpan* FPDelayMotion;
  DOnEventLaserScanner FOnEvent;
  UInt32 FPositionX, FPositionY;
  UInt32 FPositionXL, FPositionXH, FPositionDX;
  UInt32 FPositionYL, FPositionYH, FPositionDY;
  Float32 FDelayMotion;
  //  
  public:
  // Constructor
  CLaserScanner(const char* pid, 
                CDacMcp4725* pdacx, 
                CDacMcp4725* pdacy,
                DOnEventLaserScanner oneventlaserscanner,
                DOnEventTimeSpan oneventdelaymotion);
  // Property
  const char* StateText(EStateLaserScanner state);
  EStateLaserScanner GetState(void);
  void SetState(EStateLaserScanner state);
  void SetStateScanX(EStateLaserScanner state);
  void SetStateWaitX(EStateLaserScanner state);
  void SetStateScanY(EStateLaserScanner state);
  void SetStateWaitY(EStateLaserScanner state);
  void SetStateScanXY(EStateLaserScanner state);
  void SetStateWaitXY(EStateLaserScanner state);
  UInt32 GetPositionX(void);
  void SetPositionX(UInt32 value);
  UInt32 GetPositionXL(void);
  void SetPositionXL(UInt32 value);
  UInt32 GetPositionXH(void);
  void SetPositionXH(UInt32 value);
  UInt32 GetPositionDX(void);
  void SetPositionDX(UInt32 value);
  UInt32 GetPositionY(void);
  void SetPositionY(UInt32 value);
  UInt32 GetPositionYL(void);
  void SetPositionYL(UInt32 value);
  UInt32 GetPositionYH(void);
  void SetPositionYH(UInt32 value);
  UInt32 GetPositionDY(void);
  void SetPositionDY(UInt32 value);
  Float32 GetDelayMotion(void);
  void SetDelayMotion(Float32 value);
  void GetRangeX(UInt32 &positionxl, 
                 UInt32 &positionxh, 
                 UInt32 &positiondx);
  void SetRangeX(UInt32 positionxl, 
                 UInt32 positionxh, 
                 UInt32 positiondx);
  void GetRangeY(UInt32 &positionyl, 
                 UInt32 &positionyh, 
                 UInt32 &positiondy);
  void SetRangeY(UInt32 positionyl, 
                 UInt32 positionyh, 
                 UInt32 positiondy);
  // Method
  Boolean Open(void);
  Boolean Close(void);
  //
  Boolean Wait(Float32 delaymotion);
  Boolean Move(UInt32 positionx, UInt32 positiony);
  Boolean MoveWait(UInt32 positionx, UInt32 positiony, Float32 delaymotion);
  //
  Boolean ScanX(void);   // SCX(XL, XH, DX, DM)
  Boolean ScanY(void);   // SCY(YL, YH, DY, DM)
  Boolean ScanXY(void);  // SXY(XL, XH, DX, YL, YH, DY, DM)
  //
  void Execute(void);
};
//
#endif // LaserScanner_h
//
#endif // DISPATCHER_LASERSCANNER
//
