#include "DefinitionSystem.h"
//
#ifdef INTERFACE_UART
//
#ifndef InterfaceUart_h
#define InterfaceUart_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <HardwareSerial.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
// const byte UART_SIZE_RXBLOCK = 32;
// const byte UART_SIZE_TXBLOCK = 32;
const Boolean UART_INIT_RXECHO = false;
//
//----------------------------------------------------
//  CUartBase
//----------------------------------------------------
class CUartBase : public CInterfaceBase
{
  public:
  CUartBase(const char* deviceid);
  //
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  virtual UInt8 GetRXCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};
//
//----------------------------------------------------
//  CUartHS
//----------------------------------------------------
class CUartHS : public CUartBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CUartHS(const char* deviceid, HardwareSerial *serial);
  // Handler
  virtual Boolean Open(int baudrate);
  virtual Boolean Close(void);
  // Read
  virtual UInt8 GetRXCount(void);
  virtual Boolean Read(char &rxcharacter);
  virtual Boolean ReadLine(char* prxline, int &rxsize);
  // Write
  virtual Boolean Write(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
};

// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
// class CUartUS : public CUartBase
// {
//   protected:
//   usb_serial_class* FPSerial;
//   //  
//   public:
//   CUartU(usb_serial_class *serial)
//   {
//     FPSerial = serial;
//   } 
// };
// #endif
//
//----------------------------------------------------
//  CUart
//----------------------------------------------------
class CUart
{
  private:
  CUartBase* FPUartBase;
  int FPinRXD, FPinTXD;
  //
  public:
  CUart(const char* deviceid, HardwareSerial* pserial);
  CUart(const char* deviceid, HardwareSerial* pserial, int pinrxd, int pintxd);
// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
//   CUart(usb_serial_class &serial);
// #endif
  //
  // Property
  char* GetRXBlock(void)
  {
    return FPUartBase->GetRXBlock();
  }
  char* GetTXBlock(void)
  {
    return FPUartBase->GetTXBlock();
  }
  void SetRXEcho(Boolean rxecho)
  {
    FPUartBase->SetRXEcho(rxecho);
  }
  Boolean GetRXEcho(void)
  {
    return FPUartBase->GetRXEcho();
  }
  //
  // Handler
  Boolean Open(int baudrate);
  Boolean Close(void);
  // Read
  UInt8 GetRXCount(void);
  Boolean Read(char &character);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  // Boolean WritePrompt(void);
  // Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  // ??? Boolean Execute(void);
};
//
#endif // InterfaceUart_h
//
#endif // INTERFACE_UART
//
//
