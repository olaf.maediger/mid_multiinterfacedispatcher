//
#include "Defines.h"
//
#if defined(DISPATCHER_TRIGGER)
//
#include "DispatcherTrigger.h"
//
// #if defined(INTERFACE_UART)
// #include "InterfaceUart.h"
// #endif
// #if defined(INTERFACE_BT)
// #include "InterfaceBt.h"
// #endif
// #if defined(INTERFACE_WLAN)
// #include "InterfaceWlan.h"
// #endif
// #if defined(INTERFACE_LAN)
// #include "InterfaceLan.h"
// #endif
//
//
extern CError Error;
extern CCommand Command;
// // 
// #if defined(INTERFACE_UART)
// extern CUart UartCommand;
// #endif
// #if defined(INTERFACE_BT)
// extern CBt BtCommand;
// #endif
// #if defined(INTERFACE_WLAN)
// extern CWlan WlanCommand;
// #endif
// #if defined(INTERFACE_LAN)
// extern CLan LanCommand;
// #endif
//
// extern CLed LedSystem;
//
//#########################################################
//  Dispatcher - Trigger - Constructor
//#########################################################
CDispatcherTrigger::CDispatcherTrigger(void)
{
}
//
//#########################################################
//  Dispatcher - Trigger - Execution
//#########################################################
bool CDispatcherTrigger::ExecuteSetTriggerActive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteSetTriggerPassive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteGetTriggerLevel(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  int TL = 0;
  sprintf(Command.GetBuffer(), "%s %i", command, TL);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteWaitTriggerActive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int TMS = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteWaitTriggerPassive(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int TMS = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
bool CDispatcherTrigger::ExecuteBreakForTime(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <tms>
  if (parametercount < 1)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int TMS = atoi(parameters[0]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i", command, TMS);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  // action ...
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - Trigger - Handler
//#########################################################
bool CDispatcherTrigger::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_STA, command))
  {
    return ExecuteSetTriggerActive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_STP, command))
  {
    return ExecuteSetTriggerPassive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_GTL, command))
  {
    return ExecuteGetTriggerLevel(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_WTA, command))
  {
    return ExecuteWaitTriggerActive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_WTP, command))
  {
    return ExecuteWaitTriggerPassive(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_BFT, command))
  {
    return ExecuteBreakForTime(command, parametercount, parameters);
  }
  return false;
}
//
bool CDispatcherTrigger::Execute(void)
{
  return true;
}
//
#endif // DISPATCHER_TRIGGER
//
//####################################################################
//####################################################################
//####################################################################
//
