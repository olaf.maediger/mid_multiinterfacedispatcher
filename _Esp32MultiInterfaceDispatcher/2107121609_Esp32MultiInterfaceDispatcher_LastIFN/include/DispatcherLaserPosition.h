//
#include "Defines.h"
//
#if defined(DISPATCHER_LASERPOSITION)
//
#ifndef DispatcherLaserPosition_h
#define DispatcherLaserPosition_h
//
#include "Error.h"
#include "Dispatcher.h"
// //
// //----------------------------------------------------------------
// // Dispatcher - LaserPosition - SHORT
// //----------------------------------------------------------------
#define SHORT_PLP   "PLP"
#define SHORT_ALP   "ALP"
#define SHORT_PLR   "PLR"
#define SHORT_ALR   "ALP"
#define SHORT_PLC   "PLC"
#define SHORT_ALC   "ALC"
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LASERPOSITION  " Help (LaserPosition):"
#define MASK_PLP                    " %-3s <x> <y> <p> <c> : Pulse Laser Position<x><y> <p>eriod{ms} <c>ount"
#define MASK_ALP                    " %-3s                 : Abort Laser Position"
#define MASK_PLR                    " %-3s <p> <n>         : Pulse Laser Row <p>eriod{ms} <n>Pulses"
#define MASK_ALR                    " %-3s                 : Abort Laser Row"
#define MASK_PLC                    " %-3s <p> <n>         : Pulse Laser Column <p>eriod{ms} <n>Pulses"
#define MASK_ALC                    " %-3s                 : Abort Laser Column"
//
//----------------------------------------------------------------
// Dispatcher - LaserPosition
//----------------------------------------------------------------
class CDispatcherLaserPosition : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLaserPosition(void);
  //
  bool ExecutePulseLaserPosition(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserposition(char* command, int parametercount, char** parameters);
  bool ExecutePulselaserRow(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserRow(char* command, int parametercount, char** parameters);
  bool ExecutePulseLaserColumn(char* command, int parametercount, char** parameters);
  bool ExecuteAbortLaserColumn(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherLaserPosition_h
//
#endif // DISPATCHER_LASERPOSITION
//
//###################################################################################
//###################################################################################
//###################################################################################
