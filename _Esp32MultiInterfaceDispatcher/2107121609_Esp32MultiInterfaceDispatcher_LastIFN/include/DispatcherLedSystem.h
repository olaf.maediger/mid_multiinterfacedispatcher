//
#include "Defines.h"
//
#if defined(DISPATCHER_LEDSYSTEM)
//
#ifndef DispatcherLedSystem_h
#define DispatcherLedSystem_h
//
#include "Dispatcher.h"
#include "Led.h"
//
//----------------------------------------------------------------
// Dispatcher - LedSystem - SHORT
//----------------------------------------------------------------
#define SHORT_GLS   "GLS"
#define SHORT_LSO   "LSO"
#define SHORT_LSF   "LSF"
#define SHORT_LSB   "LSB"
#define SHORT_LSA   "LSA"
//
//----------------------------------------------------------------
// Dispatcher - LedSystem - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_LEDSYSTEM " Help (LedSystem):"
#define MASK_GLS               " %-3s                : Get State LedSystem"
#define MASK_LSO               " %-3s                : LedSystem On"
#define MASK_LSF               " %-3s                : LedSystem oFf"
#define MASK_LSB               " %-3s <n> <p> <w>    : LedSystem Blink  <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_LSA               " %-3s                : LedSystem Abort"
//
//----------------------------------------------------------------
// Dispatcher - LedSystem
//----------------------------------------------------------------
class CDispatcherLedSystem : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherLedSystem(void);
  //
  bool ExecuteGetLedSystem(char* command, int parametercount, char** parameters);
  bool ExecuteSetLedSystemOn(char* command, int parametercount, char** parameters);
  bool ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters);
  bool ExecuteLedSystemBlink(char* command, int parametercount, char** parameters);
  bool ExecuteLedSystemAbort(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
  bool Execute(void);
};
//
#endif // DispatcherLedSystem_h
//
#endif // DISPATCHER_LEDSYSTEM
//
//###################################################################################
//###################################################################################
//###################################################################################
