//
#include "Defines.h"
//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Automation.h"
//
#if defined(COMMAND_COMMON)
#include "CommandCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "CommandSystem.h"
#endif
#if defined(COMMAND_UART)
#include "CommandUART.h"
#endif
#if defined(COMMAND_WLAN)
#include "CommandWLAN.h"
#endif
#if defined(COMMAND_LAN)
#include "CommandLAN.h"
#endif
#if defined(COMMAND_BT)
#include "CommandBT.h"
#endif
#if defined(COMMAND_SDCARD)
#include "SDCard.h"
#include "XmlFile.h"
#include "CommandFile.h"
#endif
#if defined(COMMAND_MQTT)
#include "CommandMQTT.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "CommandLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "CommandRTC.h"
#endif
#if defined(COMMAND_NTP)
#include "CommandNTP.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "CommandWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "CommandI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
////
//----------------------------------------------------------------
// Segment - Constant
//----------------------------------------------------------------
//
//
const char* FORMAT_EVENT_MOTORENCODERLM393 = "GEPB %li %li %i %i";
//
// ----------------------------------------------------------------
// Segment - Field
// ----------------------------------------------------------------
//
//!!!!!!!extern CCommand Command;
//
//
//
//----------------------------------------------------------------
// Segment - Automation
//----------------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimePreset = 0L;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}
//
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  FTimePreset = 0L;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(void)
{ 
  delay(100);  
#if defined(COMMAND_UART)
#endif
}

// void CAutomation::TransmitEvent(String text)
// {
// #if defined(COMMAND_UART)
//   serial.WriteLine(text.c_str());
//   serial.WritePrompt();
// #endif
// }

// void CAutomation::WriteEvent(String mask, int value)
// {
// #if defined(COMMAND_UART)
//   serial.Write(mask.c_str(), value);
//   serial.Write("\r\n");
//   serial.WritePrompt();
// #endif
// }

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(void)
{ 
#if defined(COMMAND_UART)
#endif
//
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.RefreshConnection();
  MQTTClient.Execute();  
#endif
}

void CAutomation::HandleReset(void)
{
  delay(100);
  SetState(saIdle);
#if defined(COMMAND_UART)
#endif
}

void CAutomation::Handle(void)
{
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Trigger();    
#endif 
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle();
      break;
    case saReset:
      HandleReset();
      break;
    default: // saUndefined
      HandleUndefined();
      break;
  }  
}
