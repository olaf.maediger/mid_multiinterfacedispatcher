//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Defines.h"
//
#ifndef Automation_h
#define Automation_h
//
//--------------------------------
//  Section - Constant
//--------------------------------
const int TIME_AUTOMATION_MS = 1000; // [ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  saReset = 1
};
//
class CAutomation
{
  private:
  // Field
  EStateAutomation FState;
  long unsigned FTimePreset;
  //
  public:
  // Constructor
  CAutomation(void);
  // Property
  EStateAutomation GetState(void);
  void SetState(EStateAutomation state);
  //
  // Management
  Boolean Open(void);
  Boolean Close(void);
  private:
  // State
  void HandleUndefined(void);
  void HandleIdle(void);
  void HandleReset(void); 
  //
  public:
  void Handle(void);
};
//
#endif
//