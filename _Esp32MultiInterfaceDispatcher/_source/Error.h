//
//--------------------------------
//  Library Error
//--------------------------------
//
#include "Defines.h"
//
#ifndef Error_h
#define Error_h
//
#include <Arduino.h>
//
//--------------------------------
//  Section - Definition
//--------------------------------
//
enum EErrorCode
{
  ecNone =                    (int)0,  
  ecUnknown =                 (int)1,
  ecInvalidCommand =          (int)2,
  ecCommandTimingFailure =    (int)3,
  ecCommandInvalidParameter = (int)4,
  ecToManyParameters =        (int)5,
  ecNotEnoughParameters =     (int)6,
  ecMissingTargetParameter =  (int)7,
  ecFailMountard =            (int)8,
  ecFailOpenCommandFile =     (int)9,
  ecFailParseCommandFile =    (int)10,
  ecFailWriteCommandFile =    (int)11,
  ecFailCloseCommandFile =    (int)12,
  ecFailUnmountard =          (int)13,
  ecCommandFileNotOpened =    (int)14,
  ecInvalidAxis =             (int)15  
};
//
class CError
{
  private:
  enum EErrorCode FErrorCode;
  //
  const char* ErrorCodeText(EErrorCode errorcode);
  //
  public:
  CError(void);
  //
  EErrorCode GetCode(void);
  void SetCode(EErrorCode errorcode);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
#if defined(COMMAND_UART)  
  Boolean Handle(CSerial &serial);
#endif
};
//
#endif // Error_h
