//
#include "DefinitionSystem.h"
//
#if defined(INTERFACE_UART)
//
#include "Error.h"
#include "InterfaceUart.h"
//
extern CError Error;
//
//####################################################
//  CInterfaceUartBase
//####################################################
//----------------------------------------------------
//  CInterfaceUartBase - Constructor
//----------------------------------------------------
// CInterfaceUartBase(abstract!) - CInterfaceBase(abstract!)
CInterfaceUartBase::CInterfaceUartBase(const char* deviceid)
  : CInterfaceBase(deviceid)
{
  FRXEcho = UART_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceUartBase - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceUartBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceUartBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceUartBase - Read
//----------------------------------------------------
//
//
//####################################################
//  CInterfaceUartHS
//####################################################
//----------------------------------------------------
//  CInterfaceUartHS - Constructor
//----------------------------------------------------
CInterfaceUartHS::CInterfaceUartHS(const char* deviceid, HardwareSerial* pserial)
  : CInterfaceUartBase(deviceid)
{
  FPSerial = pserial;
} 
//
//----------------------------------------------------
//  CInterfaceUartHS - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceUartHS - Handler
//----------------------------------------------------
Boolean CInterfaceUartHS::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  return true;
} 
Boolean CInterfaceUartHS::Close(void)
{
  FPSerial->end();
  return true;
} 
//
//----------------------------------------------------
//  CInterfaceUartHS - Read
//----------------------------------------------------
UInt8 CInterfaceUartHS::GetRXCount(void)
{
  return FPSerial->available();
}

Boolean CInterfaceUartHS::Read(char &character)
{
  character = (char)0x00;
  if (0 < FPSerial->available())
  {
    character = FPSerial->read();
    if (FRXEcho)
    {
      switch (character)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(character);
          break;
      }
    }
    return true;
   }
   return false;
}
// reads a whole line with CR/LF, 
// if <line then rxdata is buffered in FRXBlock,
// return true: found whole line
// prxline points to extern buffer with copy of line 
Boolean CInterfaceUartHS::ReadLine(char* prxline, int &rxsize)
{ // Separator: CR or LF
  // !!! stay rxsize, use as limit !!!
  while (0 < FPSerial->available())
  {
    char RXC = (char)FPSerial->read();
    if (FRXEcho)
    {
      switch (RXC)
      {
        case (char)0x0D:
        case (char)0x0A:
          break;
        default:
          FPSerial->write(RXC);
          break;
      }
    }
    switch (RXC)
    {
      case (char)SEPARATOR_CR:
      case (char)SEPARATOR_LF:
        if (0 < FRXIndex)
        {          
          FRXBlock[FRXIndex] = SEPARATOR_ZERO;
          if (0 < strlen(FRXBlock))
          { // secure-copy to targetbuffer
            strcpy(prxline, FRXBlock);
            // zero FRXBlock
            rxsize = 1 + FRXIndex;
            FRXIndex = 0;
            FRXBlock[FRXIndex] = SEPARATOR_ZERO;
            return true;
          }
        }
        return false;
      default:
        FRXBlock[FRXIndex] = RXC;
        FRXIndex++;
        if (rxsize <= (1 + FRXIndex))
        {
          Error.SetCode(ecReceiveBufferOverflow);
          return false;
        }
    }
  }
  return false;
} 
//
//----------------------------------------------------
//  CInterfaceUartHS - Write
//----------------------------------------------------
Boolean CInterfaceUartHS::Write(char character)
{
  FPSerial->print(character);
  return true;
}

Boolean CInterfaceUartHS::WriteText(char* ptext)
{
  FPSerial->print(ptext);
  return true;
}

Boolean CInterfaceUartHS::WriteLine(void)
{
  FPSerial->write(TERMINAL_NEWLINE);
  return true;  
} 

Boolean CInterfaceUartHS::WriteLine(char* pline)
{
  FPSerial->write(pline);
  FPSerial->write(TERMINAL_NEWLINE);
  return true;
} 
//
//####################################################
//  CInterfaceUart
//####################################################
//----------------------------------------------------
//  CInterfaceUart - Constructor
//----------------------------------------------------
CInterfaceUart::CInterfaceUart(const char* deviceid, HardwareSerial* pserial)
{
  FPUartBase = (CInterfaceUartBase*)new CInterfaceUartHS(deviceid, pserial);
}
//
//----------------------------------------------------
//  CInterfaceUart - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceUart - Handler
//----------------------------------------------------
Boolean CInterfaceUart::Open(int baudrate)
{
  return FPUartBase->Open(baudrate);
}
Boolean CInterfaceUart::Close(void)
{
  return FPUartBase->Close();
}
//
//----------------------------------------------------
//  CInterfaceUart - Read
//----------------------------------------------------
UInt8 CInterfaceUart::GetRXCount(void)
{
  return FPUartBase->GetRXCount();
}
Boolean CInterfaceUart::Read(char &character)
{
  return FPUartBase->Read(character);
}
// true: whole line with cr/lf found
Boolean CInterfaceUart::ReadLine(char* prxline, int &rxsize)
{
  return FPUartBase->ReadLine(prxline, rxsize);
}
//
//----------------------------------------------------
//  CInterfaceUart - Write
//----------------------------------------------------
Boolean CInterfaceUart::WriteLine(void)
{
  return FPUartBase->WriteLine();
}
Boolean CInterfaceUart::WriteText(const char* ptext)
{
  return FPUartBase->WriteText((char*)ptext);
}
Boolean CInterfaceUart::WriteText(const char* mask, const char* ptext)
{
  if (0 != ptext)
  {
    return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)ptext));
  }  
  return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, (char*)""));
}
Boolean CInterfaceUart::WriteLine(const char* pline)
{
  return FPUartBase->WriteLine((char*)pline);
}
Boolean CInterfaceUart::WriteLine(const char* mask, const char* pline)
{
  if (0 != pline)
  {
    return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)pline));
  }  
  return FPUartBase->WriteLine(FPUartBase->ConvertPChar(mask, (char*)""));
}  
Boolean CInterfaceUart::WritePChar(const char* mask, char* value)
{
  if (0 != value)
  {    
    return FPUartBase->WriteText(FPUartBase->ConvertPChar(mask, value));
  }
  return FPUartBase->WriteText((char*)mask);
}
Boolean CInterfaceUart::WriteString(const char* mask, String value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertString(mask, value));
}
Boolean CInterfaceUart::WriteByte(const char* mask, Byte value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertByte(mask, value));
}
Boolean CInterfaceUart::WriteDual(const char* mask, UInt16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertDual(mask, value));
}
Boolean CInterfaceUart::WriteQuad(const char* mask, UInt32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertQuad(mask, value));
}
Boolean CInterfaceUart::WriteInt16(const char* mask, Int16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertInt16(mask, value));
}
Boolean CInterfaceUart::WriteUInt16(const char* mask, UInt16 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertUInt16(mask, value));
}
Boolean CInterfaceUart::WriteInt32(const char* mask, Int32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertInt32(mask, value));
}
Boolean CInterfaceUart::WriteUInt32(const char* mask, UInt32 value)
{
  return FPUartBase->WriteText(FPUartBase->ConvertUInt32(mask, value));
}
Boolean CInterfaceUart::WriteFloat32(const char* mask, Float32 value)
{  
  return FPUartBase->WriteText(FPUartBase->ConvertFloat32(mask, value));
}
Boolean CInterfaceUart::WriteDouble64(const char* mask, Double64 value)
{  
  return FPUartBase->WriteText(FPUartBase->ConvertDouble64(mask, value));
}
//
//------------------------------------------------------------------------
// Uart - Write - Specials
//------------------------------------------------------------------------
Boolean CInterfaceUart::WriteComment(void)
{
  return FPUartBase->Write(TERMINAL_COMMENT[0]);
}
Boolean CInterfaceUart::WriteComment(String comment)
{
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_COMMENT, comment.c_str());
  return FPUartBase->WriteText(FPUartBase->GetTXBlock());
}
Boolean CInterfaceUart::WriteResponse(String text)
{  
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_RESPONSE, text.c_str());
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}

Boolean CInterfaceUart::WriteEvent(const char* event)
{
  sprintf(FPUartBase->GetTXBlock(), "%s%s", TERMINAL_EVENT, event);
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}
Boolean CInterfaceUart::WriteEvent(const char* mask, const char* event)
{  
  sprintf(FPUartBase->GetTXBlock(), mask, event);
  FPUartBase->WriteText((char*)TERMINAL_EVENT);
  return FPUartBase->WriteLine(FPUartBase->GetTXBlock());
}

//
//-------------------------------------------------------------------
//  Uart - Execute
//-------------------------------------------------------------------
// Boolean CInterfaceUart::Execute(void)
// {
//   // while (0 < GetRxCount())
//   // {
//   //   Char C = F
//   //   switch (C)
//   //   {
//   //     case (byte)SEPARATOR_CR:
//   //     case (byte)SEPARATOR_LF:
//   //       FRxdBufferIndex, SEPARATOR_ZERO);
//   //       FRxdBufferIndex = 0; // restart
//   //       strupr(FRxdBuffer);
//   //       strcpy(FCommandText, FRxdBuffer);
//   //       return true;
//   //     default: 
//   //       FRxdBuffer[FRxdBufferIndex] = C;
//   //       FRxdBufferIndex++;
//   //       break;
//   //   }
//   // }  
// }











#endif // INTERFACE_UART
//