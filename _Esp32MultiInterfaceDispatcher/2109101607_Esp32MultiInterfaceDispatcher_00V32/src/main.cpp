//
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//
#include "DefinitionSystem.h"
#include "Error.h"
#include "Automation.h"
#include "Command.h"
#include "DefinitionPin.h"
#include "TimeSpan.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#include "Dispatcher.h"
#if defined(DISPATCHER_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(DISPATCHER_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(DISPATCHER_UART)
#include "DispatcherUart.h"
#endif
#if defined(DISPATCHER_BT)
#include "DispatcherBt.h"
#endif
#if defined(DISPATCHER_WLAN)
#include "DispatcherWlan.h"
#endif
#if defined(DISPATCHER_LAN)
#include "DispatcherLan.h"
#endif
#if defined(DISPATCHER_SDCARD)
#include "DispatcherSDCard.h"
#endif
#if defined(DISPATCHER_MQTT)
#include "DispatcherMqtt.h"
#endif
#if defined(DISPATCHER_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(DISPATCHER_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(DISPATCHER_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(DISPATCHER_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(DISPATCHER_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
//-----------------------------------------------------------
// Segment - Global Variables - Interface / Protocol
//-----------------------------------------------------------
#if defined(INTERFACE_UART)
CInterfaceUart UartCommand("UTC", &Serial);
// CInterfaceUart UartProgram(&Serial1, PIN_UART0_RXD, PIN_UART0_TXD);
#endif
#if defined(INTERFACE_BT)
CBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
CLan LanCommand;
#endif
#if defined(PROTOCOL_SDCARD)
CSDCard SDCardProtocol;
// CSDCard SDCardProtocol(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
#if defined(PROTOCOL_MQTT)
void MqttOnSetup(CProtocolMqtt* pmqtt);
void MqttOnTryConnection(CProtocolMqtt* pmqtt);
void MqttOnConnectionSuccess(CProtocolMqtt* pmqtt);
void MqttOnConnectionFailed(CProtocolMqtt* pmqtt);
bool MqttOnReceive(CProtocolMqtt* pmqtt, const char* topic, const char* message);
WiFiClient WifiClient;
CProtocolMqtt MqttCommand(&WifiClient, MqttOnSetup, 
                          MqttOnTryConnection, MqttOnConnectionSuccess, 
                          MqttOnConnectionFailed, MqttOnReceive);
long unsigned MillisPreset;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Dispatcher
//-----------------------------------------------------------
CError        Error;
CAutomation   Automation;
CCommand      Command;
//
#if defined(DISPATCHER_COMMON)
CDispatcherCommon DispatcherCommon;
#endif
#if defined(DISPATCHER_SYSTEM)
CTimeRelative TimeRelativeSystem(TIMERELATIVE_ID, TIMERELATIVE_MESSAGE_ON);
CDispatcherSystem DispatcherSystem;
#endif
#if defined(DISPATCHER_UART)
CDispatcherUart DispatcherUart;
#endif
#if defined(DISPATCHER_BT)
CDispatcherBT DispatcherBT;
#endif
#if defined(DISPATCHER_WLAN)
CDispatcherWlan DispatcherWlan;
#endif
#if defined(DISPATCHER_LAN)
CDispatcherLan DispatcherLan;
#endif
#if defined(DISPATCHER_SDCARD)
CDispatcherSDCard DispatcherSDCard;
#endif
#if defined(DISPATCHER_MQTT)
CDispatcherMqtt DispatcherMqtt;
#endif
#if defined(DISPATCHER_LEDSYSTEM)
CLed LedSystem("LEDS", PIN_LEDSYSTEM, LEDSYSTEM_INVERTED); 
CDispatcherLedSystem DispatcherLedSystem;
#endif
#if defined(DISPATCHER_RTC)
CRTCInternal RTCInternal; // not here
CDispatcherRtc DispatcherRtc;
#endif
#if defined(DISPATCHER_NTP)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
CDispatcherNtp DispatcherNtp;
#endif
#if defined(DISPATCHER_WATCHDOG)
CDispatcherWatchDog DispatcherWatchDog;
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
CDispatcherI2CDisplay DispatcherI2CDisplay;
C2CDisplay I2CDisplay;
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
CMenu MenuSystem("MS");                       
#endif
//
//#################################################################
//  Global - Event
//#################################################################
void OnWriteHelp(void)
{
#if defined(INTERFACE_UART)
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine();
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_COMMON);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_H, SHORT_H); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPH, SHORT_GPH); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GSV, SHORT_GSV); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GHV, SHORT_GHV); 
  #endif
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_APE, SHORT_APE); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_RSS, SHORT_RSS); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WTR, SHORT_WTR); 
  #endif
  #if defined(DISPATCHER_UART)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_UART);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCU, SHORT_WCU);
  #endif
  #if defined(DISPATCHER_BT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_BT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_WLAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WLAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_LAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_SDCARD)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SDCARD);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_MQTT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_MQTT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCM, SHORT_WCM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_STM, SHORT_STM);
  #endif
  #if defined(DISPATCHER_LEDSYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LEDSYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GLS, SHORT_GLS);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSO, SHORT_LSO);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSF, SHORT_LSF);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSB, SHORT_LSB);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSA, SHORT_LSA);
  #endif
  #if defined(DISPATCHER_RTC)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_RTC);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_NTP)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_NTP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_WATCHDOG)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WATCHDOG);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_I2CDISPLAY)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_I2CDISPLAY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif  
  UartCommand.WriteLine(MASK_ENDLINE);
#endif
#if defined(INTERFACE_BT)
  ...
#endif
#if defined(INTERFACE_WLAN) 
  ...
#endif
#if defined(INTERFACE_LAN) 
  ...
#endif
#if defined(PROTOCOL_MQTT)
#endif
#if defined(PROTOCOL_SDCARD)
  ...
#endif
}
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
// void OnEventTimeSpan(CTimeSpan* timespan, char* event)
// {
//   #if defined(INTERFACE_UART)
//   UartCommand.WriteEvent(event);
//   #endif  
// }
// CTimeSpan TimeSpan("TSS", OnEventTimeSpan);
//
//#################################################################
//  Global - Callback - Mqtt
//#################################################################
#if defined(PROTOCOL_MQTT)
bool MqttError(const int errorcode)
{
  Serial.print("Error[");
  Serial.print(errorcode);
  Serial.print("]: ");
  Serial.print(ERROR_MQTT[errorcode]);
  Serial.println("!");
  return false;
}
void MqttOnSetup(CProtocolMqtt* pmqtt)
{  
  Serial.println("Mqtt: Wifi-Setup");
  Serial.print("Mqtt: Wifi-Connection to: ");
  Serial.print(WLAN_SSID);
  Serial.print(" ");
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  while (WL_CONNECTED != WiFi.status())
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.print("\r\nMqtt: Wifi-Connection to LocalIPAddress[");
  Serial.print(WiFi.localIP());
  Serial.println("]");
}
void MqttOnTryConnection(CProtocolMqtt* pmqtt)
{  
  Serial.print("Mqtt: Try Connection to Broker[");
  Serial.print(MQTT_BROKER_IPADDRESS);    
  Serial.println("]");
}
void MqttOnConnectionSuccess(CProtocolMqtt* pmqtt)
{  
  Serial.println("Mqtt: Broker connected");      
  //  Subscribe: Event
  pmqtt->Subscribe(MQTT_TOPIC_EVENT);
  Serial.print("Mqtt: Subscribe Topic[");
  Serial.print(MQTT_TOPIC_EVENT);
  Serial.println("]");
  //  Subscribe: Command
  pmqtt->Subscribe(MQTT_TOPIC_COMMAND);
  Serial.print("Mqtt: Subscribe Topic[");
  Serial.print(MQTT_TOPIC_COMMAND);
  Serial.println("]");
  //  Publish: Connected
  pmqtt->Publish(MQTT_TOPIC_EVENT, MQTT_EVENT_CONNECTED);
  Serial.print("Mqtt: Connection published[");
  Serial.print(MQTT_TOPIC_EVENT);
  Serial.print("][");
  Serial.print(MQTT_EVENT_CONNECTED);
  Serial.println("]");  
}
void MqttOnConnectionFailed(CProtocolMqtt* pmqtt)
{  
  Serial.print("Mqtt: State[");
  Serial.print(pmqtt->GetState());
  Serial.println("]");
  MqttError(pmqtt->GetErrorCode());
  Serial.println("Mqtt: Retry after 5s...");
  delay(5000);
}
bool MqttOnReceive(CProtocolMqtt* pmqtt, const char* ptopic, const char* pmessage)
{
// CommandDispatcher:
  Serial.print("Mqtt: CommandDispatcher[");
  Serial.print(ptopic);
  Serial.print("][");  
  Serial.print(pmessage);
  Serial.println("]");
  //  
  if (0 == strcmp(MQTT_TOPIC_EVENT, ptopic))
  {
    Serial.print("Mqtt: Received Event[");
    Serial.print(pmessage);
    Serial.println("]");
    return true;
  }
  if (0 == strcmp(MQTT_TOPIC_COMMAND, ptopic))
  {
    if (0 == strlen(pmessage))
    {
      MqttError(ecmqInvalidCommand);
      return false;
    }
    Serial.print("Mqtt: Received Command[");
    Serial.print(pmessage);
    Serial.println("]");
    if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMON, pmessage))
    { // debug Serial.print("!!!!!!!!!! ON");  
      digitalWrite(PIN_LEDSYSTEM, HIGH);
      return true;
    }
    if (0 == strcmp(MQTT_COMMAND_LEDSYSTEMOFF, pmessage))
    { // debug Serial.print("!!!!!!!!!! OFF");  
      digitalWrite(PIN_LEDSYSTEM, LOW);
      return true;
    }
    return MqttError(ecmqInvalidCommand);
  }
  return MqttError(ecmqInvalidTopic);
}
#endif
//
//#################################################################
//  Global - Setup - Main
//#################################################################
void setup() 
{ 
  Error.Open();
//----------------------------------------------------
// Interface - Uart
//----------------------------------------------------
#if defined(INTERFACE_UART)
  UartCommand.Open(115200);
  delay(300);
  UartCommand.SetRXEcho(UART_INIT_RXECHO);
  UartCommand.WriteText("\r\n# Uart-Connection[");
  if (UartCommand.GetRXEcho())
  {
    UartCommand.WriteText("RXEcho=true");  
  }
  else
  {
    UartCommand.WriteText("RXEcho=false");  
  }
  UartCommand.WriteLine("] established.");  
#endif
//
//----------------------------------------------------
// Interface - Bt
//----------------------------------------------------
#if defined(INTERFACE_BT)
#endif
//
//----------------------------------------------------
// Interface - Wlan
//----------------------------------------------------
#if defined(INTERFACE_WLAN)
#endif
//
//----------------------------------------------------
// Interface - Lan
//----------------------------------------------------
#if defined(INTERFACE_LAN)
#endif
//
//----------------------------------------------------
// Protocol - SDCard
//----------------------------------------------------
#if defined(PROTOCOL_SDCARD)
  SDCard.Open();
#endif
//
//----------------------------------------------------
// Protocol - Mqtt
//----------------------------------------------------
#if defined(PROTOCOL_MQTT)
  MqttCommand.Setup(); 
#endif
//----------------------------------------------------
// Dispatcher - Common
//----------------------------------------------------
  Command.Open();
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.SetOnWriteHelp(OnWriteHelp);
  DispatcherCommon.Open();
#endif
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Open();
  DispatcherSystem.Open();
#endif
#if defined(DISPATCHER_UART)
#endif
#if defined(DISPATCHER_BT)
#endif
#if defined(DISPATCHER_WLAN)
#endif
#if defined(DISPATCHER_LAN)
#endif
#if defined(DISPATCHER_SDCARD)
#endif
#if defined(DISPATCHER_MQTT)
  DispatcherMqtt.Open();
#endif
#if defined(DISPATCHER_LEDSYSTEM)
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(50);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff(); 
#endif
#if defined(DISPATCHER_RTC)
  RTCInternal.Open();
#endif
#if defined(DISPATCHER_NTP)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  TimeAbsoluteSystem.Open();  
#endif
#if defined(DISPATCHER_WATCHDOG)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  MenuSystem.Open();
#endif
//--------------------------------------------------------------
// Init Application...
//--------------------------------------------------------------
  Automation.Open();
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.WriteProgramHeader();
  DispatcherCommon.WriteHelp();
#endif
} 
//-----------------------------------------------------------------
//
//
//#################################################################
//  Global - Loop - Main
//#################################################################
char* CC = 0;
int CPC = 0;
char** CPS = 0;
void loop()
{
  //##########################################
  // Interface / Protocol
  //##########################################
#if defined(INTERFACE_UART)
  // NC UartCommand.Execute();
#endif
#if defined(INTERFACE_BT)
  BtCommand.Execute();
#endif
#if defined(INTERFACE_LAN)
  LanCommand.Execute();
#endif
#if defined(INTERFACE_WLAN)
  WlanCommand.Execute();
#endif
#if defined(PROTOCOL_MQTT)
  if (!MqttCommand.IsConnected()) 
  { // -> open
    MqttCommand.Connect();
  }
  else
  { // -> execute
    MqttCommand.Execute();
    if (MillisPreset <= millis())
    {
      MillisPreset = millis() + MQTT_INTERVAL_ACTIVE;
      Serial.print("Mqtt: Publish[");
      Serial.print(MQTT_TOPIC_EVENT);
      Serial.print("][");
      Serial.print(MQTT_EVENT_ACTIVE);
      Serial.println("]");        
      if (!MqttCommand.Publish(MQTT_TOPIC_EVENT, MQTT_EVENT_ACTIVE))
      {
        MqttError(MqttCommand.GetErrorCode());
      }
    }
  }
#endif
//##########################################
// Dispatcher
//##########################################
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Wait_Execute();
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
  LedSystem.Blink_Execute(Command.GetBuffer());
#endif
//##########################################
// Command
//##########################################
  if (Command.AnalyseInterfaceBlock())
  { 
    Boolean R = false;
    CC = 0;
    CPC = 0;
    CPS = 0;
#if defined(INTERFACE_UART)
    if (0 == CC)
    {
      CC = Command.GetUartCommand();
      CPC = Command.GetUartParameterCount();
      CPS = Command.GetUartParameters();
      if (0 < CC)
      {
        Serial.print("*** INTERFACE_UART CC[");
        Serial.print(CC);
        Serial.println("]");
      }
    }
#endif
#if defined(INTERFACE_BT)
    if (0 == CC)
    {
    }
#endif
#if defined(INTERFACE_WLAN)
    if (0 == CC)
    {
    }
#endif
#if defined(INTERFACE_LAN)
    if (0 == CC)
    {
    }
#endif    
#if defined(PROTOCOL_MQTT)
    if (0 == CC)
    {
      CC = Command.GetMqttCommand();
      CPC = Command.GetMqttParameterCount();
      CPS = Command.GetMqttParameters();
      if (0 < CC)
      {
        Serial.print("*** INTERFACE_MQTT CC[");
        Serial.print(CC);
        Serial.println("]");
      }
    }
#endif
    //
#if defined(DISPATCHER_COMMON)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherCommon.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_SYSTEM)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherSystem.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_UART)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherUart.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_BT)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherBt.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_WLAN)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherWlan.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_LAN)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherLan.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_SDCARD)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherSDCard.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_MQTT)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherMqtt.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherLedSystem.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_RTC)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherRtc.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_NTP)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherNtp.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_WATCHDOG)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherWatchdog.HandleInterface(CC, CPC, CPS);
    }
#endif
#if defined(DISPATCHER_I2CDISPLAY)
    if ((0 < CC) && (!R) && (!Error.IsActive()))
    {
      R = DispatcherI2CDisplay.HandleInterface(CC, CPC, CPS);
    }
#endif
    if (!R)
    {
      Error.SetCode(ecUnknownCommand);
    }
    Error.Handle();
    Command.SetState(scIdle);
  }
}
//