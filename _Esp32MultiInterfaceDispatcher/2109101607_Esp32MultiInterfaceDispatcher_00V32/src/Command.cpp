// //
#include "DefinitionSystem.h"
//
#include "Error.h"
#include "Command.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
extern CError     Error;
//
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CInterfaceBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CInterfaceWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CInterfaceLan LanCommand;
#endif
#if defined(PROTOCOL_MQTT)
extern CProtocolMqtt MqttCommand;
#endif
//
//#########################################################
//  Command - Constructor
//#########################################################
CCommand::CCommand(void)
{
  FState = scUndefined;
#if defined(INTERFACE_UART)
  FUartCommand = new Character[UART_COMMANDSIZE];
  FUartParameters = new PCharacter[UART_PARAMETERCOUNT];
  FUartParameterCount = 0;
  for (int IUP = 0; IUP < UART_PARAMETERCOUNT; IUP++)
  {
    FUartParameters[IUP] = new Character[UART_PARAMETERSIZE];
  }
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
  FMqttCommand = new Character[MQTT_COMMANDSIZE];
  FMqttParameters = new PCharacter[MQTT_PARAMETERCOUNT];
  FMqttParameterCount = 0;
  for (int IUP = 0; IUP < MQTT_PARAMETERCOUNT; IUP++)
  {
    FMqttParameters[IUP] = new Character[MQTT_PARAMETERSIZE];
  }
#endif
}
CCommand::~CCommand(void)
{
  FState = scUndefined;
}
//
//#########################################################
//  Command - Property
//#########################################################
EStateCommand CCommand::GetState(void)
{
  return FState;
}
//
void CCommand::SetState(EStateCommand state)
{
  if (state != FState)
  {
    FState = state;
#ifdef INTERFACE_UART
    UartCommand.WriteEvent(MASK_STATECOMMAND, StateText(FState));
#endif
// #ifdef INTERFACE_BT
// #endif
// #ifdef INTERFACE_WLAN
// #endif
// #ifdef INTERFACE_LAN
// #endif
#if defined(PROTOCOL_MQTT)
    // !!!!!!!!!!!!!!!!!!!!!!! MqttCommand.WriteEvent(MASK_STATECOMMAND, StateText(FState));
#endif
// #if defined(PROTOCOL_SDCARD)
// #endif
  }
}
//
//#########################################################
//  Command - Helper
//#########################################################
const char* CCommand::StateText(EStateCommand state)
{
  switch (state)
  {
    case scError:
      return (const char*)"Error";
    case scUndefined:
      return (const char*)"Undefined";
    case scInit:
      return (const char*)"Init";
    case scIdle:
      return (const char*)"Idle";
    case scBusy:
      return (const char*)"Busy";
    default: // not in list -> Undefined
      return (const char*)"Unknown";
  }
}
//
#if defined(INTERFACE_UART)
char* CCommand::GetUartCommand(void)
{
  return FUartCommand;
}
Int16 CCommand::GetUartParameterCount(void)
{
  return FUartParameterCount;
}
VPCharacter CCommand::GetUartParameters(void)
{
  return FUartParameters;
}
#endif
#if defined(INTERFACE_BT) 
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
char* CCommand::GetMqttCommand(void)
{
  return FMqttCommand;
}
Int16 CCommand::GetMqttParameterCount(void)
{
  return FMqttParameterCount;
}
VPCharacter CCommand::GetMqttParameters(void)
{
  return FMqttParameters;
}
#endif
//
//#########################################################
//  Command - Helper
//#########################################################
//
//#########################################################
//  Command - Handler
//#########################################################
bool CCommand::Open(void)
{
  SetState(scIdle);
  return true;
}
bool CCommand::Close(void)
{
  SetState(scUndefined);
  return true;
}
//
//##################################################################
// Command - Analysis & Execution
//##################################################################
// InterfaceText -> Command/COMMAND, Parameters
bool CCommand::AnalyseInterfaceBlock(void)
{
#if defined(INTERFACE_UART)
  if (0 < UartCommand.GetRXCount())
  {
    if (scIdle != GetState())
    {
      Error.SetCode(ecTimingFailure);
      return false;
    }
    int SizeBlock = INTERFACE_SIZE_RXBLOCK - 1;
    char Block[INTERFACE_SIZE_RXBLOCK];
    // check of whole line received
    if (UartCommand.ReadLine(Block, SizeBlock))
    { // Block has copy of whole line
      SetState(scBusy);
      char *PTerminal = (char*)" \t\r\n";
      char* PCommand = strtok(Block, PTerminal);
      if (PCommand)
      { 
        strupr(PCommand);
        strcpy(FUartCommand, PCommand);
        UartCommand.WriteLine();
        // debug UartCommand.WriteText((char*)"FUartCommand[");
        // debug UartCommand.WriteText(FUartCommand);
        // debug UartCommand.WriteLine((char*)"]");
        FUartParameterCount = 0;
        char *PParameter;
        while (0 != (PParameter = strtok(0, PTerminal)))
        {
          strcpy(FUartParameters[FUartParameterCount], PParameter);
          // debug UartCommand.WriteInt32("P[%i]", FUartParameterCount);
          // debug UartCommand.WriteLine("<%s>", FUartParameters[FUartParameterCount]);
          FUartParameterCount++;
          if (UART_PARAMETERCOUNT < FUartParameterCount)
          {
            Error.SetCode(ecToManyParameters);
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }
#endif
#if defined(INTERFACE_BT)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(PROTOCOL_MQTT)
// access to Mqtt Received Block
// analyse Topic, Command, Parameters
#endif
  return false;
}
//
