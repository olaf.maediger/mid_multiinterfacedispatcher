//
#ifndef Definition_h
#define Definition_h
//
//----------------------------------------------------------------------
//  Definition
//----------------------------------------------------------------------
//
const long unsigned MQTT_INTERVAL_ACTIVE = 60000L; // [ms]
//
#define WLAN_SSID                 "TPL1300SA7AZ"
// #define WLAN_SSID              "FritzBoxSA7"
#define WLAN_PASSWORD             "01234567890123456789"
#define MQTT_BROKER_IPADDRESS     "192.168.178.53"
#define MQTT_BROKER_PORT          1883
#define MQTT_LOCAL_DEVICEID       "Esp32PubSubClient"
//
#define MQTT_TOPIC_EVENT          "Esp32PubSubClient/Event"
#define MQTT_EVENT_CONNECTED      "IsConnected"
#define MQTT_EVENT_ACTIVE         "IsActive"
//
#define MQTT_TOPIC_COMMAND        "Esp32PubSubClient/Command"
#define MQTT_COMMAND_LEDSYSTEMON  "LSO"
#define MQTT_COMMAND_LEDSYSTEMOFF "LSF"
//#define MQTT_COMMANDMASK_SETVALUE "STV %u"
//
const int MQTT_SIZE_TOPIC         = 64; // Enough space for Topic!!!
const int MQTT_SIZE_MESSAGE       = 64;


//
#endif // Definition_h
