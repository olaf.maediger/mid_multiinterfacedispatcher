#include "DefinitionSystem.h"
//
#ifdef INTERFACE_BT
//
#ifndef InterfaceBt_h
#define InterfaceBt_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <BluetoothSerial.h>
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const Boolean BT_INIT_RXECHO = false;
//
//----------------------------------------------------
//  Type
//----------------------------------------------------
class CInterfaceBtBase : public CInterfaceBase
{
  public:
  CInterfaceBtBase(const char* deviceid);
  //
  virtual Boolean Open(int parameter) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual Boolean Write(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  virtual UInt8 GetRXCount(void) = 0;
  virtual Boolean Read(char &character) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};

class CInterfaceBtUart : public CInterfaceBtBase
{
//   protected:
//   BluetoothSerial* FPSerial;
  
  public:
  CInterfaceBtUart(const char* deviceid, BluetoothSerial* pserial);
  // Handler
  virtual Boolean Open(int parameter);
  virtual Boolean Close(void);
  // Read
  virtual UInt8 GetRXCount(void);
  virtual Boolean Read(char &rxcharacter);
  virtual Boolean ReadLine(char* prxline, int &rxsize);
  // Write
  virtual Boolean Write(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
};

class CInterfaceBt
{
  private:
  CInterfaceBtBase* FPBtBase; 
  //
  // Property
  //!!!!!!!!!!
  // char* GetRXBlock(void)
  // {
  //   return FPBtBase->GetRXBlock();
  // }
  // char* GetTXBlock(void)
  // {
  //   return FPBtBase->GetTXBlock();
  // }
  void SetRXEcho(Boolean rxecho);
  Boolean GetRXEcho(void);
  //
  public:
  CInterfaceBt(const char* devicename, BluetoothSerial* pserial);
  //  
  // Handler
  Boolean Open(int baudrate);
  Boolean Close(void);
   // Read
  UInt8 GetRXCount(void);
  //Boolean LineDetected(void);
  Boolean Read(char &character);
  Boolean ReadText(char* ptext);
  Boolean ReadLine(char* rxline, int &rxsize);
  // Write
  Boolean WriteCharacter(char character);
  Boolean WriteText(const char* ptext);
  Boolean WriteText(const char* mask, const char* ptext);
  Boolean WriteLine(const char* pline);
  Boolean WriteLine(const char* mask, const char* pline);
  Boolean WriteCharacter(const char* mask, char character);
  Boolean WritePChar(const char* mask, char* pvalue);
  Boolean WriteString(const char* mask, String value);
  Boolean WriteByte(const char* mask, Byte value);
  Boolean WriteDual(const char* mask, UInt16 value);
  Boolean WriteQuad(const char* mask, UInt32 value);
  Boolean WriteInt16(const char* mask, Int16 value);
  Boolean WriteUInt16(const char* mask, UInt16 value);
  Boolean WriteInt32(const char* mask, Int32 value);
  Boolean WriteUInt32(const char* mask, UInt32 value);
  Boolean WriteFloat32(const char* mask, Float32 value);
  Boolean WriteDouble64(const char* mask, Double64 value);
  Boolean WriteLine(void);
  // Boolean WritePrompt(void);
  // Boolean WriteLinePrompt(void);
  Boolean WriteComment(void);
  Boolean WriteComment(String comment);
  Boolean WriteComment(const char* mask, String comment);
  Boolean WriteEvent(const char* event);
  Boolean WriteEvent(const char* mask, const char* event);
  Boolean WriteResponse(String comment);
  Boolean WriteResponse(const char* mask, String comment);
  //
  // ??? Boolean Execute(void);
};
//
#endif // InterfaceBt_h
//
#endif // INTERFACE_BT
//
//













/*//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const byte UART_SIZE_RXBLOCK = 32;
const byte UART_SIZE_TXBLOCK = 32;
const Boolean UART_INIT_RXECHO    = true;
//
//----------------------------------------------------
//  CUartBase
//----------------------------------------------------
class CUartBase : public CInterfaceBase
{
  protected:
  char FRXBlock[UART_SIZE_RXBLOCK];
  char FTXBlock[UART_SIZE_TXBLOCK];
  Boolean FRXEcho;
  //
  public:
  CUartBase(void);
  //
  char* GetRXBlock(void)
  {
    return FRXBlock;
  }
  char* GetTXBlock(void)
  {
    return FTXBlock;
  }
  void SetRXEcho(Boolean rxecho);
  Boolean GetRXEcho(void);
  //
  virtual Boolean Open(int baudrate) = 0;
  virtual Boolean Close(void) = 0;
  //
  virtual Boolean WriteCharacter(char character) = 0;
  virtual Boolean WriteText(char* ptext) = 0;
  virtual Boolean WriteLine(void) = 0;
  virtual Boolean WriteLine(char* pline) = 0;
  //
  virtual UInt8 GetRxCount(void) = 0;
  virtual Boolean ReadCharacter(char &rxcharacter) = 0;
  virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
};
//
//----------------------------------------------------
//  CUartHS
//----------------------------------------------------
class CUartHS : public CUartBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CUartHS(HardwareSerial *serial);
  //
  virtual Boolean Open(int baudrate);
  virtual Boolean Close(void);
  //
  virtual Boolean WriteCharacter(char character);
  virtual Boolean WriteText(char* ptext);
  virtual Boolean WriteLine(void);
  virtual Boolean WriteLine(char* pline);
  //
  virtual UInt8 GetRxCount(void);
  virtual Boolean ReadCharacter(char &rxcharacter);
  virtual Boolean ReadLine(char* prxline, int &rxsize);
};

// #if (defined(PROCESSOR_TEENSY32) || defined(PROCESSOR_TEENSY36))
// class CUartUS : public CUartBase
// {
//   protected:
//   usb_serial_class* FPSerial;
//   //  
//   public:
//   CUartU(usb_serial_class *serial)
//   {
//     FPSerial = serial;
//   } 
// };
// #endif
*/