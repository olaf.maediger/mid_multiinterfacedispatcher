//
#if defined(NTP_ISPLUGGED)
//
#ifndef DefinitionNtp_h
#define DefinitionNtp_h
//------------------------------------------------------
// Segment - Global Constant - Ntp
//------------------------------------------------------
#ifndef INCLUDE_NTP
  #define INCLUDE_NTP
  #define IPADDRESS_NTPSERVER "de.pool.ntp.org"
#endif 
//
#endif // DefinitionNtp_h
//
#endif // NTP_ISPLUGGED

