//
#include "DefinitionSystem.h"
//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Automation.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(COMMAND_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(COMMAND_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(COMMAND_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(COMMAND_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(COMMAND_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(COMMAND_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
#include "Dispatcher.h"
//-------------------------------
//
//
//
//
//--------------------------------
#if defined(INTERFACE_UART)
extern CInterfaceUart UartCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan Wlan;
#endif
#if defined(INTERFACE_LAN)
extern CLan Lan;
#endif
#if defined(INTERFACE_BT)
extern CInterfaceBt BtCommand;
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
extern CSDCard SDCard;
#endif
#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!extern CMqtt Mqtt;
#endif
//
//-------------------------------
#if defined(COMMAND_COMMON)
extern CDispatcherCommon DispatcherCommon;
#endif
#if defined(COMMAND_SYSTEM)
//extern CDispatcherSystem DispatcherSystem;
#endif
#if defined(COMMAND_LEDSYSTEM)
extern CLed LedSystem;
#endif
#if defined(COMMAND_RTC)
extern CRtc RtcLocal;
#endif
#if defined(COMMAND_NTP)
extern CNtp NtpClient;
#endif
#if defined(COMMAND_WATCHDOG)
extern CWatchDog WatchDog;
#endif
#if defined(COMMAND_I2CDISPLAY)
extern 
#endif
//-------------------------------
//
//----------------------------------------------------------------
// Segment - Automation
//----------------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimePreset = 0L;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}
//
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  FTimePreset = 0L;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(void)
{ 
  delay(100);  
}

void CAutomation::WriteEvent(String text)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteText(text.c_str());
  UartCommand.WriteLine();
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
}

void CAutomation::WriteEvent(String mask, int value)
{
#if defined(INTERFACE_UART)
  UartCommand.WriteInt32(mask.c_str(), value);
  UartCommand.WriteLine();
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
}

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(void)
{  //----------------------------
#if defined(INTERFACE_UART)
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif
#if defined(INTERFACE_BT)
#endif
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#endif
#if defined(PROTOCOL_MQTT)
//!!!!!!!!!!!!!!!!!!!!!!!!!!  MQTTClient.RefreshConnection();
//!!!!!!!!!!!!!!!!!!!!!!!!!!  MQTTClient.Execute();  
#endif
//-------------------------------
#if defined(COMMAND_COMMON)
#endif
#if defined(COMMAND_SYSTEM)
#endif
#if defined(COMMAND_LEDSYSTEM)
#endif
#if defined(COMMAND_RTC)
#endif
#if defined(COMMAND_NTP)
#endif
#if defined(COMMAND_WATCHDOG)
#endif
#if defined(COMMAND_I2CDISPLAY)
#endif
//-------------------------------
}
//
void CAutomation::HandleReset(void)
{
  delay(100);
  SetState(saIdle);
}
//
void CAutomation::Handle(void)
{
#if defined(COMMAND_WATCHDOG)
  WatchDog.Trigger();    
#endif
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle();
      break;
    case saReset:
      HandleReset();
      break;
    default: // saUndefined
      HandleUndefined();
      break;
  }  
}
