//
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//
#include "DefinitionSystem.h"
#include "Error.h"
#include "Automation.h"
#include "Command.h"
#include "DefinitionPin.h"
#include "TimeSpan.h"
//
//--------------------------------
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//-------------------------------
#if defined(PROTOCOL_SDCARD)
#include "ProtocolSDCard.h"
#include "XmlFile.h"
#include "DispatcherFile.h"
#endif
#if defined(PROTOCOL_MQTT)
#include "ProtocolMqtt.h"
#endif
//
//-------------------------------
#include "Dispatcher.h"
#if defined(DISPATCHER_COMMON)
#include "DispatcherCommon.h"
#endif
#if defined(DISPATCHER_SYSTEM)
#include "DispatcherSystem.h"
#endif
#if defined(DISPATCHER_UART)
#include "DispatcherUart.h"
#endif
#if defined(DISPATCHER_BT)
#include "DispatcherBt.h"
#endif
#if defined(DISPATCHER_WLAN)
#include "DispatcherWlan.h"
#endif
#if defined(DISPATCHER_LAN)
#include "DispatcherLan.h"
#endif
#if defined(DISPATCHER_SDCARD)
#include "DispatcherSDCard.h"
#endif
#if defined(DISPATCHER_MQTT)
#include "DispatcherMqtt.h"
#endif
#if defined(DISPATCHER_LEDSYSTEM)
#include "DispatcherLedSystem.h"
#endif
#if defined(DISPATCHER_RTC)
#include "DispatcherRtc.h"
#endif
#if defined(DISPATCHER_NTP)
#include "DispatcherNtp.h"
#endif
#if defined(DISPATCHER_WATCHDOG)
#include "DispatcherWatchDog.h"
#endif
#if defined(DISPATCHER_I2CDISPLAY)
#include "DispatcherI2CDisplay.h"
// !!!!! #include "Menu.h"
#endif
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
//-----------------------------------------------------------
// Segment - Global Variables - Interface / Protocol
//-----------------------------------------------------------
#if defined(INTERFACE_UART)
CInterfaceUart UartCommand("IUC", &Serial);
// CInterfaceUart UartProgram(&Serial1, PIN_UART0_RXD, PIN_UART0_TXD);
#endif
#if defined(INTERFACE_BT)
//!!!!!!!! BluetoothSerial BtSerial;
//!!!!!!!! CInterfaceBt BtCommand("IBC", &BtSerial);
#endif
#if defined(INTERFACE_WLAN)
CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
CLan LanCommand;
#endif
#if defined(PROTOCOL_SDCARD)
CSDCard SDCardProtocol;
// CSDCard SDCardProtocol(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
#if defined(PROTOCOL_MQTT)
void MqttOnSetup(CProtocolMqtt* pmqtt);
void MqttOnTryConnection(CProtocolMqtt* pmqtt);
void MqttOnConnectionSuccess(CProtocolMqtt* pmqtt);
void MqttOnConnectionFailed(CProtocolMqtt* pmqtt);
bool MqttOnReceive(CProtocolMqtt* pmqtt, const char* topic, const char* message);
WiFiClient WifiClient;
CProtocolMqtt MqttCommand(&WifiClient, MqttOnSetup, 
                          MqttOnTryConnection, MqttOnConnectionSuccess, 
                          MqttOnConnectionFailed, MqttOnReceive);
long unsigned MillisPreset;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Dispatcher
//-----------------------------------------------------------
CError        Error;
CAutomation   Automation;
CCommand      Command;
//
#if defined(DISPATCHER_COMMON)
CDispatcherCommon DispatcherCommon;
#endif
#if defined(DISPATCHER_SYSTEM)
CTimeRelative TimeRelativeSystem(TIMERELATIVE_ID, TIMERELATIVE_MESSAGE_ON);
CDispatcherSystem DispatcherSystem;
#endif
#if defined(DISPATCHER_UART)
CDispatcherUart DispatcherUart;
#endif
#if defined(DISPATCHER_BT)
CDispatcherBt DispatcherBt;
#endif
#if defined(DISPATCHER_WLAN)
CDispatcherWlan DispatcherWlan;
#endif
#if defined(DISPATCHER_LAN)
CDispatcherLan DispatcherLan;
#endif
#if defined(DISPATCHER_SDCARD)
CDispatcherSDCard DispatcherSDCard;
#endif
#if defined(DISPATCHER_MQTT)
CDispatcherMqtt DispatcherMqtt;
#endif
#if defined(DISPATCHER_LEDSYSTEM)
CLed LedSystem("LEDS", PIN_LEDSYSTEM, LEDSYSTEM_INVERTED); 
CDispatcherLedSystem DispatcherLedSystem;
#endif
#if defined(DISPATCHER_RTC)
CRTCInternal RTCInternal; // not here
CDispatcherRtc DispatcherRtc;
#endif
#if defined(DISPATCHER_NTP)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
CDispatcherNtp DispatcherNtp;
#endif
#if defined(DISPATCHER_WATCHDOG)
CDispatcherWatchDog DispatcherWatchDog;
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
CDispatcherI2CDisplay DispatcherI2CDisplay;
C2CDisplay I2CDisplay;
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
CMenu MenuSystem("MS");                       
#endif
//
//#################################################################
//  Global - Event
//#################################################################
void OnWriteHelp(void)
{
#if defined(INTERFACE_UART)
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine();
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_COMMON);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_H, SHORT_H); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GPH, SHORT_GPH); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GSV, SHORT_GSV); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GHV, SHORT_GHV); 
  #endif
  #if defined(DISPATCHER_SYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_APE, SHORT_APE); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_RSS, SHORT_RSS); 
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WTR, SHORT_WTR); 
  #endif
  #if defined(DISPATCHER_UART)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_UART);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCU, SHORT_WCU);
  #endif
  #if defined(DISPATCHER_BT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_BT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCB, SHORT_WCB);
  #endif
  #if defined(DISPATCHER_WLAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WLAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_LAN)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LAN);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_SDCARD)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_SDCARD);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_MQTT)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_MQTT);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_WCM, SHORT_WCM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_STM, SHORT_STM);
  #endif
  #if defined(DISPATCHER_LEDSYSTEM)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_LEDSYSTEM);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_GLS, SHORT_GLS);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSO, SHORT_LSO);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSF, SHORT_LSF);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSB, SHORT_LSB);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_LSA, SHORT_LSA);
  #endif
  #if defined(DISPATCHER_RTC)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_RTC);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_NTP)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_NTP);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_WATCHDOG)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_WATCHDOG);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif
  #if defined(DISPATCHER_I2CDISPLAY)
  UartCommand.WriteComment(); UartCommand.WriteLine(HELP_COMMAND_I2CDISPLAY);
  UartCommand.WriteComment(); UartCommand.WriteLine(MASK_, SHORT_);
  #endif  
  UartCommand.WriteLine(MASK_ENDLINE);
#endif
#if defined(INTERFACE_BT)
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
#if defined(INTERFACE_WLAN) 
  ...
#endif
#if defined(INTERFACE_LAN) 
  ...
#endif
#if defined(PROTOCOL_MQTT)
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
#if defined(PROTOCOL_SDCARD)
  ...
#endif
}
//
//#################################################################
//  Global - Interrupt-Handler
//#################################################################
// void OnEventTimeSpan(CTimeSpan* timespan, char* event)
// {
//   #if defined(INTERFACE_UART)
//   UartCommand.WriteEvent(event);
//   #endif  
// }
// CTimeSpan TimeSpan("TSS", OnEventTimeSpan);
//
//#################################################################
//  Global - Callback - Mqtt
//#################################################################
#if defined(PROTOCOL_MQTT)
bool MqttError(const int errorcode)
{
  Serial.print("!Error[");
  Serial.print(errorcode);
  Serial.print("]: ");
  Serial.print(ERROR_MQTT[errorcode]);
  Serial.println("!");
  return false;
}
void MqttOnSetup(CProtocolMqtt* pmqtt)
{  
  Serial.println("!Mqtt: Wifi-Setup");
  Serial.print("!Mqtt: Wifi-Connection to: ");
  Serial.print(WLAN_SSID);
  Serial.print(" ");
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  while (WL_CONNECTED != WiFi.status())
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.print("\r\n!Mqtt: Wifi-Connection to LocalIPAddress[");
  Serial.print(WiFi.localIP());
  Serial.println("]");
}
void MqttOnTryConnection(CProtocolMqtt* pmqtt)
{  
  Serial.print("!Mqtt: Try Connection to Broker[");
  Serial.print(MQTT_BROKER_IPADDRESS);    
  Serial.println("]");
}
void MqttOnConnectionSuccess(CProtocolMqtt* pmqtt)
{  
  Serial.println("!Mqtt: Broker connected");      
  //  Subscribe: Event
  pmqtt->Subscribe(MQTT_TOPIC_EVENT);
  Serial.print("!Mqtt: Subscribe Topic[");
  Serial.print(MQTT_TOPIC_EVENT);
  Serial.println("]");
  //  Subscribe: Command
  pmqtt->Subscribe(MQTT_TOPIC_COMMAND);
  Serial.print("!Mqtt: Subscribe Topic[");
  Serial.print(MQTT_TOPIC_COMMAND);
  Serial.println("]");
  //  Publish: Connected
  pmqtt->Publish(MQTT_TOPIC_EVENT, MQTT_EVENT_CONNECTED);
  Serial.print("!Mqtt: Connection published[");
  Serial.print(MQTT_TOPIC_EVENT);
  Serial.print("][");
  Serial.print(MQTT_EVENT_CONNECTED);
  Serial.println("]");  
}
void MqttOnConnectionFailed(CProtocolMqtt* pmqtt)
{  
  Serial.print("!Mqtt: State[");
  Serial.print(pmqtt->GetState());
  Serial.println("]");
  MqttError(pmqtt->GetErrorCode());
  Serial.println("!Mqtt: Retry after 5s...");
  delay(5000);
}
bool MqttOnReceive(CProtocolMqtt* pmqtt, const char* ptopic, const char* pmessage)
{
  return true;
}
#endif
//
//#######################################################################################
//  Setup - Interface/Protocol
//#######################################################################################
void SetupInterfaceProtocol(void)
{//---------------------------------------------------
// Interface - Uart
//----------------------------------------------------
#if defined(INTERFACE_UART)
  UartCommand.Open(115200);
  delay(300);
  UartCommand.SetRXEcho(UART_INIT_RXECHO);
  UartCommand.WriteText("\r\n!Uart-Connection[");
  if (UartCommand.GetRXEcho())
  {
    UartCommand.WriteText("RXEcho=true");  
  }
  else
  {
    UartCommand.WriteText("RXEcho=false");  
  }
  UartCommand.WriteLine("] established.");  
#endif
//----------------------------------------------------
// Interface - Bt
//----------------------------------------------------
#if defined(INTERFACE_BT)
  //!!!!!!!!!!!!!!!!!!BtCommand.Open(0);
  delay(300);
#endif
//----------------------------------------------------
// Interface - Wlan
//----------------------------------------------------
#if defined(INTERFACE_WLAN)
#endif
//----------------------------------------------------
// Interface - Lan
//----------------------------------------------------
#if defined(INTERFACE_LAN)
#endif
//----------------------------------------------------
// Protocol - SDCard
//----------------------------------------------------
#if defined(PROTOCOL_SDCARD)
  SDCard.Open();
#endif
//----------------------------------------------------
// Protocol - Mqtt
//----------------------------------------------------
#if defined(PROTOCOL_MQTT)
  MqttCommand.Setup(); 
  MqttCommand.Connect(); // also possible in loop()
#endif
}
//
//#######################################################################################
//  Setup - Dispatcher
//#######################################################################################
void SetupDispatcher(void)
{
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.SetOnWriteHelp(OnWriteHelp);
  DispatcherCommon.Open();
#endif
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Open();
  DispatcherSystem.Open();
#endif
#if defined(DISPATCHER_UART)
#endif
#if defined(DISPATCHER_BT)
#endif
#if defined(DISPATCHER_WLAN)
#endif
#if defined(DISPATCHER_LAN)
#endif
#if defined(DISPATCHER_SDCARD)
#endif
#if defined(DISPATCHER_MQTT)
  DispatcherMqtt.Open();
#endif
#if defined(DISPATCHER_LEDSYSTEM)
  LedSystem.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedSystem.SetOff();
    delay(50);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff(); 
#endif
#if defined(DISPATCHER_RTC)
  RTCInternal.Open();
#endif
#if defined(DISPATCHER_NTP)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  TimeAbsoluteSystem.Open();  
#endif
#if defined(DISPATCHER_WATCHDOG)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
#if defined(DISPATCHER_I2CDISPLAY)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  MenuSystem.Open();
#endif
}
//
//#######################################################################################
//  InterfaceProtocolDispatcherPeriodic
//#######################################################################################
void InterfaceProtocolDispatcherPeriodic(void)
{//-----------------------------------------
// Interface
//------------------------------------------
#if defined(INTERFACE_UART)
  // NC UartCommand.Execute();
#endif
#if defined(INTERFACE_BT)
  // NC BtCommand.Execute();
#endif
#if defined(INTERFACE_LAN)
  LanCommand.Execute();
#endif
#if defined(INTERFACE_WLAN)
  WlanCommand.Execute();
#endif
//------------------------------------------
// Protocol
//------------------------------------------
#if defined(PROTOCOL_MQTT)
  if (!MqttCommand.IsConnected()) 
  { // -> open
    MqttCommand.Connect();
  }
  else
  { // -> execute
    MqttCommand.Execute();
    if (MillisPreset <= millis())
    {
      MillisPreset = millis() + MQTT_INTERVAL_ACTIVE;
      Serial.print("!Mqtt: Publish[");
      Serial.print(MQTT_TOPIC_EVENT);
      Serial.print("][");
      Serial.print(MQTT_EVENT_ACTIVE);
      Serial.println("]");        
      if (!MqttCommand.Publish(MQTT_TOPIC_EVENT, MQTT_EVENT_ACTIVE))
      {
        MqttError(MqttCommand.GetErrorCode());
      }
    }
  }
#endif
//------------------------------------------
// Dispatcher
//------------------------------------------
#if defined(DISPATCHER_SYSTEM)
  TimeRelativeSystem.Wait_Execute();
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
  LedSystem.Blink_Execute(Command.GetBuffer());
#endif
}
//
//#######################################################################################
//  InterfaceProtocolDispatcher
//#######################################################################################
bool InterfaceProtocolDispatcher(char* command, int parametercount, char** parameters)
{
  if (!command) return false;
  if (Error.IsActive()) return false;
#if defined(DISPATCHER_COMMON)
  if (DispatcherCommon.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_SYSTEM)
  if (DispatcherSystem.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_UART)
  if (DispatcherUart.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_BT)
  if (DispatcherBt.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_WLAN)
  if (DispatcherWlan.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_LAN)
  if (DispatcherLan.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_SDCARD)
  if (DispatcherSDCard.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_MQTT)
  if (DispatcherMqtt.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_LEDSYSTEM)    
  if (DispatcherLedSystem.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_RTC)
  if (DispatcherRtc.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_NTP)
  if (DispatcherNtp.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_WATCHDOG)
  if (DispatcherWatchdog.HandleInterface(command, parametercount, parameters)) return true;
#endif
#if defined(DISPATCHER_I2CDISPLAY)
  if (DispatcherI2CDisplay.HandleInterface(command, parametercount, parameters)) return true;
#endif
  // NOT CORRECT FOR ALL CASES!!! Error.SetCode(ecUnknownCommand);  
  return false;
}
//
//#######################################################################################
//  CommandDispatcherPeriodic
//#######################################################################################
void CommandDispatcherPeriodic(void)
{
  if (Command.AnalyseInterfaceBlock())
  { 
#if defined(INTERFACE_UART)
    if (InterfaceProtocolDispatcher(Command.GetUartCommand(), 
                                    Command.GetUartParameterCount(), 
                                    Command.GetUartParameters()))
    {
      Command.ResetUartCommand();
    }
#endif
#if defined(INTERFACE_BT)
    if (InterfaceProtocolDispatcher(Command.GetBtCommand(), 
                                    Command.GetBtParameterCount(), 
                                    Command.GetBtParameters()))
    {
      Command.ResetBtCommand();
    }
#endif
#if defined(INTERFACE_WLAN)
#endif
#if defined(INTERFACE_LAN)
#endif    
#if defined(PROTOCOL_MQTT)
    if (InterfaceProtocolDispatcher(Command.GetMqttCommand(),
                                    Command.GetMqttParameterCount(),
                                    Command.GetMqttParameters()))
    {
      Command.ResetMqttCommand();
    }
#endif
  }
  Command.SetState(scIdle);  
}
//
//#######################################################################################
//#######################################################################################
//#######################################################################################
//-----------------------------------------------------------------
//  Global - Main - Setup
//-----------------------------------------------------------------
void setup() 
{ 
  Error.Open();
  SetupInterfaceProtocol();
  SetupDispatcher();
  Command.Open();
  Automation.Open();
  // Init Application
#if defined(DISPATCHER_COMMON)
  DispatcherCommon.WriteProgramHeader();
  DispatcherCommon.WriteHelp();
#endif
} 
//
//-----------------------------------------------------------------
//  Global - Main - Loop
//-----------------------------------------------------------------
void loop()
{
  InterfaceProtocolDispatcherPeriodic();
  CommandDispatcherPeriodic();
  Error.Handle();
}
//
