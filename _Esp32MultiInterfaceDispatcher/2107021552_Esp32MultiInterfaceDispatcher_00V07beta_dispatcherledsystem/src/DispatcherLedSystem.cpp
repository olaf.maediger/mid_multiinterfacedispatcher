//
#include "Defines.h"
//
#if defined(DISPATCHER_LEDSYSTEM)
//
#include "Error.h"
#include "DispatcherLedSystem.h"
//
#if defined(INTERFACE_UART)
#include "InterfaceUart.h"
#endif
#if defined(INTERFACE_BT)
#include "InterfaceBt.h"
#endif
#if defined(INTERFACE_WLAN)
#include "InterfaceWlan.h"
#endif
#if defined(INTERFACE_LAN)
#include "InterfaceLan.h"
#endif
//
//
extern CError Error;
extern CCommand Command;
// 
#if defined(INTERFACE_UART)
extern CUart UartCommand;
#endif
#if defined(INTERFACE_BT)
extern CBt BtCommand;
#endif
#if defined(INTERFACE_WLAN)
extern CWlan WlanCommand;
#endif
#if defined(INTERFACE_LAN)
extern CLan LanCommand;
#endif
//
extern CLed LedSystem;
//
//#########################################################
//  Dispatcher - LedSystem - Constructor
//#########################################################
CDispatcherLedSystem::CDispatcherLedSystem(void)
{
}
//
//#########################################################
//  Dispatcher - LedSystem - Execution
//#########################################################

bool CDispatcherLedSystem::ExecuteGetLedSystem(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  bool State = LedSystem.GetState();
  sprintf(Command.GetBuffer(), "%s %i", command, State);
  ExecuteResponse(Command.GetBuffer());
  // Action: -  
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLedSystem::ExecuteSetLedSystemOn(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  LedSystem.SetOn();
  ExecuteEnd();
  return true;
}
//
bool CDispatcherLedSystem::ExecuteSetLedSystemOff(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  LedSystem.SetOff();
  ExecuteEnd();
  return true;

}
bool CDispatcherLedSystem::ExecuteLedSystemBlink(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: <n> <p> <w>
  if (parametercount < 3)
  {
    Error.SetCode(ecMissingParameter);
    return false;
  }
  int PC = atoi(parameters[0]);
  int PP = atoi(parameters[1]);
  int PW = atoi(parameters[2]);
  // Response:
  sprintf(Command.GetBuffer(), "%s %i %i %i", command, PC, PP, PW);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  LedSystem.Blink_Start(PC, PP, PW);
  ExecuteEnd();
  return true;
}
bool CDispatcherLedSystem::ExecuteLedSystemAbort(char* command, int parametercount, char** parameters)
{
  ExecuteBegin();
  // Analyse parameters: -
  // Response:
  sprintf(Command.GetBuffer(), "%s", command);
  ExecuteResponse(Command.GetBuffer());
  // Action:
  LedSystem.Blink_Abort();
  ExecuteEnd();
  return true;
}
//
//#########################################################
//  Dispatcher - LedSystem - Handler
//#########################################################
bool CDispatcherLedSystem::HandleInterface(char* command, int parametercount, char** parameters) 
{
  if (!strcmp(SHORT_GLS, command))
  {
    return ExecuteGetLedSystem(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_LSO, command))
  {
    return ExecuteSetLedSystemOn(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_LSF, command))
  {
    return ExecuteSetLedSystemOff(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_LSB, command))
  {
    return ExecuteLedSystemBlink(command, parametercount, parameters);
  }
  if (!strcmp(SHORT_LSA, command))
  {
    return ExecuteLedSystemAbort(command, parametercount, parameters);
  }
  return false;
}
//
#endif // DISPATCHER_LEDSYSTEM
//
//####################################################################
//####################################################################
//####################################################################
//
