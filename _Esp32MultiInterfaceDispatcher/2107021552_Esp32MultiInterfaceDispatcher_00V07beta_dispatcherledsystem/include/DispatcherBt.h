//
#include "Defines.h"
//
#if defined(DISPATCHER_UART)
//
#ifndef DispatcherUart_h
#define DispatcherUart_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Uart - SHORT
//----------------------------------------------------------------
#define SHORT_   ""                   //  - 
//
//----------------------------------------------------------------
// Dispatcher - Uart - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_UART       " Help (Uart):"
#define MASK_                  " %-3s                  : <a>bort <p>rocess <e>xecution"
//
//----------------------------------------------------------------
// Dispatcher - Uart
//----------------------------------------------------------------
class CDispatcherUart : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherSystem(void);
  //
  bool ExecuteAbortProcessExecution(char* command, int parametercount, char** parameters);
  bool ExecuteResetSystem(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTimeRelative(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
};
//
#endif // DispatcherUart_h
//
#endif // DISPATCHER_UART
//
//###################################################################################
//###################################################################################
//###################################################################################

// //
// #include "Defines.h"
// //
// #if defined(COMMAND_BT)
// //
// #ifndef DispatcherBt_h
// #define DispatcherBt_h
// //
// #include "Dispatcher.h"
// //
// //----------------------------------------------------------------
// // Help - Dispatcher - SHORT - Definition
// //----------------------------------------------------------------
// #define SHORT_WCB   "WCB"                   // WCB - Write <c>ommand Bt
// #define SHORT_RCB   "RCB"                   // RCB - Read Dispatcher Bt
// // 
// //----------------------------------------------------------------
// // Help - Dispatcher - MASK - Definition
// //----------------------------------------------------------------
// #define HELP_COMMAND_BT          " Help (Bt):"
// #define MASK_WCB                  " %-3s <c>              : Write <c>ommand Bt"
// #define MASK_RCB                  " %-3s                  : Read Dispatcher Bt"
// //
// class CDispatcherBt : public CDispatcher
// {

// };
// //
// #endif // DispatcherBt_h
// //
// #endif // COMMAND_BT
// //