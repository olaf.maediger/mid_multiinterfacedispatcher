//
#include "Defines.h"
//
#if defined(DISPATCHER_UART)
//
#ifndef DispatcherUart_h
#define DispatcherUart_h
//
#include "Dispatcher.h"
//
//----------------------------------------------------------------
// Dispatcher - Uart - SHORT
//----------------------------------------------------------------
#define SHORT_   ""                   //  - 
//
//----------------------------------------------------------------
// Dispatcher - Uart - MASK
//----------------------------------------------------------------
#define HELP_COMMAND_UART       " Help (Uart):"
#define MASK_                  " %-3s                  : <a>bort <p>rocess <e>xecution"
//
//----------------------------------------------------------------
// Dispatcher - Uart
//----------------------------------------------------------------
class CDispatcherUart : public CDispatcher
{
  protected:
  //
  public:
  CDispatcherSystem(void);
  //
  bool ExecuteAbortProcessExecution(char* command, int parametercount, char** parameters);
  bool ExecuteResetSystem(char* command, int parametercount, char** parameters);
  bool ExecuteWaitTimeRelative(char* command, int parametercount, char** parameters);
  //
  bool virtual HandleInterface(char* command, int parametercount, char** parameters);
};
//
#endif // DispatcherUart_h
//
#endif // DISPATCHER_UART
//
//###################################################################################
//###################################################################################
//###################################################################################

//
//

// //----------------------------------------------------------------
// // Help - Dispatcher - SHORT - Definition
// //----------------------------------------------------------------
// #if defined(COMMAND_COMMON)
// #define SHORT_H     "H"                     // H - (this) Help
// #endif
// //
// #if defined(COMMAND_SYSTEM)
// #define SHORT_GPH   "GPH"                   // GPH - Get Program Header
// #define SHORT_GSV   "GSV"                   // GSV - Get Software Version
// #define SHORT_GHV   "GHV"                   // GHV - Get Hardware Version
// #define SHORT_APE   "APE"                   // APE - Abort Process Execution
// #define SHORT_RSS   "RSS"                   // RSS - Reset System
// #define SHORT_WTR   "WTR"                   // WTR T        - WaitTimeRelative - overload from NTPClient/RTCInternal
// #define SHORT_WTA   "WTA"                   // WTA HH MM SS - WaitTimeAbsolute - overload from NTPClient/RTCInternal
// #endif
// //
// // HELP_COMMAND_UART
// #if defined(COMMAND_UART)
// #define SHORT_WCU   "WCU"                   // WCU - Write <c>ommand Uart 
// #define SHORT_RCU   "RCU"                   // RCU - Read Dispatcher Uart 
// #endif
// //
// // HELP_COMMAND_WLAN
// #if defined(COMMAND_WLAN)
// #define SHORT_WCW   "WCW"                   // WCW - Write <c>ommand Wlan
// #define SHORT_RCW   "RCW"                   // RCW - Read Dispatcher Wlan
// #endif
// //
// // HELP_COMMAND_LAN
// #if defined(COMMAND_LAN)
// #define SHORT_WCL   "WCL"                   // WCL - Write <c>ommand Lan
// #define SHORT_RCL   "RCL"                   // RCL - Read Dispatcher Lan
// #endif
// //
// // HELP_COMMAND_BT
// #if defined(COMMAND_BT)
// #define SHORT_WCB   "WCB"                   // WCB - Write <c>ommand Bt
// #define SHORT_RCB   "RCB"                   // RCB - Read Dispatcher Bt
// #endif
// //
// // HELP_COMMAND_SDCard
// #if defined(COMMAND_SDCARD)
// #define SHORT_OCF   "OCF"                   // OCF - Open Dispatcher<f>ile for writing"
// #define SHORT_WCF   "WCF"                   // WCF - Write <c>ommand with <p>arameter(s) to File"
// #define SHORT_CCF   "CCF"                   // CCF - Close Dispatcher<f>ile end writing"
// #define SHORT_ECF   "ECF"                   // ECF - Execute Dispatcher<f>ile"
// #define SHORT_ACF   "ACF"                   // ACF - Abort Execution Dispatcherfile"
// #endif
// //
// // HELP_COMMAND_MQTT
// #if defined(COMMAND_MQTT)
// #define SHORT_WCM   "WCM"                   // WCM - Write <c>ommand Mqtt
// #define SHORT_RCM   "RCM"                   // RCM - Read Dispatcher Mqtt
// #endif
// //
// // HELP_LEDSYSTEM
// #if defined(COMMAND_LEDSYSTEM)
// #define SHORT_GLS   "GLS"                   // GLS - Get LedSystem
// #define SHORT_LSH   "LSH"                   // LSH - Set LedSystem High
// #define SHORT_LSL   "LSL"                   // LSL - Set LedSystem Low
// #define SHORT_BLS   "BLS"                   // BLS - Blink LedSystem <p>eriod{ms} <c>ount{1}
// #endif
// //
// #if defined(COMMAND_RTC)
// #define SHORT_SRD   "SRD"                   // SRD - Set <r>tc-Date <yy><mm><dd>
// #define SHORT_GRD   "GRD"                   // GRD - Get <r>tc-Date <yy><mm><dd>
// #define SHORT_SRT   "SRT"                   // SRT - Set <r>tc-Time <hh><mm><ss>
// #define SHORT_GRT   "GRT"                   // GRT - Get <r>tc-Time <hh><mm><ss>
// #endif
// //
// #if defined(COMMAND_NTPCLIENT)
// #define SHORT_GND   "GND"                   // GND - Get Ntp-Date
// #define SHORT_GNT   "GNT"                   // GNT - Get Ntp-Time
// #endif
// //
// #if defined(COMMAND_WATCHDOG)
// #define SHORT_PWD   "PWD"                   // PWD - Pulse WatchDog
// #endif
// //
// // HELP_IC2DISPLAY
// #if defined(COMMAND_I2CDISPLAY)
// #define SHORT_CLD   "CLD"                   // CLD - Clear Display
// #define SHORT_STD   "STD"                   // STD - Set Text Display <r>ow <c>olumn <t>ext
// #endif
// //
// // 
// //----------------------------------------------------------------
// // Help - Dispatcher - MASK - Definition
// //----------------------------------------------------------------
// #define TITLE_LINE                "--------------------------------------------------"
// #define MASK_PROJECT              "- Project:   %-35s -"
// #define MASK_SOFTWARE             "- Version:   %-35s -"
// #define MASK_HARDWARE             "- Hardware:  %-35s -"
// #define MASK_DATE                 "- Date:      %-35s -"
// #define MASK_TIME                 "- Time:      %-35s -"
// #define MASK_AUTHOR               "- Author:    %-35s -"
// #define MASK_PORT                 "- Port:      %-35s -"
// #define MASK_PARAMETER            "- Parameter: %-35s -"
// //
// //-----------------------------------------------------------------------------------------
// //
// #if defined(COMMAND_COMMON)
// #define HELP_COMMAND_COMMON       " Help (Common):"
// #define MASK_H                    " %-3s                  : This Help"
// #endif
// //

// #if defined(COMMAND_SYSTEM)
// #define HELP_COMMAND_SYSTEM       " Help (System):"
// #define MASK_GPH                  " %-3s                  : Get Program Header"
// #define MASK_GSV                  " %-3s                  : Get Software Version"
// #define MASK_GHV                  " %-3s                  : Get Hardware Version"
// #define MASK_APE                  " %-3s                  : <a>bort <p>rocess <e>xecution"
// #define MASK_RSS                  " %-3s                  : <r>e<s>et <s>ystem"
// #define MASK_WTR                  " %-3s <t>              : Wait Time Relative <t>ime{ms}"
// #define MASK_WTA                  " %-3s <hh> <mm> <ss>   : Wait Time Absolute <hh> <mm> <ss>"
// #endif
// //
// #if defined(COMMAND_UART)
// #define HELP_COMMAND_UART         " Help (Uart):"
// #define MASK_WCU                  " %-3s <c>              : Write <c>ommand Uart"
// #define MASK_RCU                  " %-3s                  : Read Dispatcher Uart"
// #endif
// //
// #if defined(COMMAND_WLAN)
// #define HELP_COMMAND_WLAN         " Help (Wlan):"
// #define MASK_WCW                  " %-3s <c>              : Write <c>ommand Wlan"
// #define MASK_RCW                  " %-3s                  : Read Dispatcher Wlan"
// #endif
// //
// #if defined(COMMAND_LAN)
// #define HELP_COMMAND_LAN          " Help (Lan):"
// #define MASK_WCL                  " %-3s <c>              : Write <c>ommand Lan"
// #define MASK_RCL                  " %-3s                  : Read Dispatcher Lan"
// #endif
// //
// #if defined(COMMAND_BT)
// #define HELP_COMMAND_BT          " Help (Bt):"
// #define MASK_WCB                  " %-3s <c>              : Write <c>ommand Bt"
// #define MASK_RCB                  " %-3s                  : Read Dispatcher Bt"
// #endif
// //
// #if defined(COMMAND_SDCARD)
// #define HELP_COMMAND_SDCARD       " Help (SDCard):"
// #define MASK_OCF                  " %-3s <f>              : Open Dispatcher<f>ile for writing"
// #define MASK_WCF                  " %-3s <c> <p> ..       : Write <c>ommand with <p>arameter(s) to File"
// #define MASK_CCF                  " %-3s                  : Close Dispatcher<f>ile end writing"
// #define MASK_ECF                  " %-3s <f>              : Execute Dispatcher<f>ile"
// #define MASK_ACF                  " %-3s                  : Abort Execution Dispatcherfile"
// #endif
// //
// #if defined(COMMAND_MQTT)
// #define HELP_COMMAND_MQTT         " Help (Dispatcher Mqtt):"
// #define MASK_WCM                  " %-3s <l>              : Write Dispatcher Mqtt <l>ine"
// #define MASK_RCM                  " %-3s                  : Read Dispatcher Mqtt"
// #endif
// //
// #if defined(COMMAND_LEDSYSTEM)
// #define HELP_COMMAND_LEDSYSTEM    " Help (LedSystem):"
// #define MASK_GLS                  " %-3s                  : Get State LedSystem"
// #define MASK_LSH                  " %-3s                  : Switch LedSystem On"
// #define MASK_LSL                  " %-3s                  : Switch LedSystem Off"
// #define MASK_BLS                  " %-3s <n> <p> <w>      : Blink LedSystem <n>times <p>eriod{ms}/<w>ith{ms}"
// #endif
// //
// #if defined(COMMAND_RTC)
// #define HELP_COMMAND_RTC          " Help (Rtc):"
// #define MASK_SRD                  " %-3s <yy> <mm> <dd>   : Set <r>tc-Date <yy><mm><dd>"
// #define MASK_GRD                  " %-3s                  : Get <r>tc-Date"
// #define MASK_SRT                  " %-3s <hh> <mm> <ss>   : Set <r>tc-Time <hh><mm><ss>"
// #define MASK_GRT                  " %-3s                  : Get <r>tc-Date"
// #endif
// //
// #if defined(COMMAND_NTPCLIENT)
// #define HELP_COMMAND_NTPCLIENT    " Help (NtpClient):"
// #define MASK_GND                  " %-3s                  : Get NTPClient Date {yy.mm.dd}"
// #define MASK_GNT                  " %-3s                  : Get NTPClient Time {hh:mm:ss}"
// #endif
// //
// #if defined(COMMAND_WATCHDOG)
// #define HELP_COMMAND_WATCHDOG     " Help (WatchDog):"
// #define MASK_PWD                  " %-3s                  : Pulse WatchDog"
// #endif
// //
// #if defined(COMMAND_I2CDISPLAY)
// #define HELP_COMMAND_I2CDISPLAY   " Help (I2CDisplay):"
// #define MASK_CLD                  " %-3s                  : <cl>ear <d>isplay"
// #define MASK_STD                  " %-3s <r> <c> <t>      : Show <t>ext at <r>ow <c>olumn"
// #endif
// //
// //
// #define MASK_ENDLINE " ###"
// //
// //-----------------------------------------------------------------------------------------
// //
// // Dispatcher-Parameter
// const int SIZE_TXBUFFER = 128;
// const int SIZE_RXBUFFER = 128;
// const int COUNT_TEXTPARAMETERS = 5;
// const int SIZE_TEXTPARAMETER = 16;
// //
// #define MASK_STATEPROCESS         "STP %i %i"
// //
// //-----------------------------------------------------------------------------------------
// // const int COUNT_SOFTWAREVERSION = 1;
// // const int COUNT_HARDWAREVERSION = 1;
// //
// // #define MASK_SOFTWAREVERSION      "IRQ-Version %s"
// // #define MASK_HARDWAREVERSION      "Hardware-Version %s"
// //
// //
// //-----------------------------------------------------------------------------------------
// enum EStateDispatcher
// {
//   scError     = -2,
//   scUndefined = -1,
//   scInit      = 0,
//   scIdle      = 1,
//   scBusy      = 2
// };
// //
// class CDispatcher
// {
//   private:
//   EStateDispatcher FState;
//   //
// #if defined(COMMAND_UART)
//   Character FRxBufferUart[SIZE_RXBUFFER];
//   Int16 FRxdBufferIndexUart = 0;
//   PCharacter GetRxBufferUart(void)
//   {
//     return FRxBufferUart;
//   }
//   Character FTxBufferUart[SIZE_TXBUFFER];
//   PCharacter GetTxBufferUart(void)
//   {
//     return FTxBufferUart;
//   }
//   //
//   //Character FDispatcherTextUart[SIZE_RXDBUFFER];
//   //PCharacter FPDispatcherUart;
//   //PCharacter FPParametersUart[COUNT_TEXTPARAMETERS];
//   //Int16 FParameterCountUart = 0;
// #endif
// #if defined(COMMAND_LAN)
//   Character FRxdBufferLan[SIZE_RXDBUFFER];
//   Int16 FRxdBufferIndexLan = 0;
// #endif
//   //    
//   public:
//   CDispatcher(void);
//   ~CDispatcher(void);
//   //
//   const char* StateText(EStateDispatcher state);
//   //
//   EStateDispatcher GetState(void);
//   void SetState(EStateDispatcher state);
//   //
//   bool Open(void);
//   bool Close(void);
//   //
//   void TransmitDispatcher(String command);
//   void TransmitEvent(String event);
//   void TransmitComment(String comment);
//   void TransmitWarning(String warning);
//   void TransmitError(String error);
//   void ReceiveAnswer(void);// NC  - internal !!! String &answer);
//   // 
// //
//   void AbortProcessExecution(void);
//   //
//   //  Segment - Execution - Helper
//   void WriteHelp(void);
//   void WriteProgramHeader(void);
//   void WriteSoftwareVersion(void);
//   void WriteHardwareVersion(void);
//   //
//   //  Segment - Execution - Common
//   bool ExecuteGetHelp(void);
//   //
//   //  Segment - Execution - System
//   bool ExecuteGetProgramHeader(void);
//   bool ExecuteGetSoftwareVersion(void);
//   bool ExecuteGetHardwareVersion(void);
//   //
//   bool ExecuteAbortProcessExecution(void);
//   bool ExecuteResetSystem(void);
//   bool ExecuteWaitTimeRelative(void);
//   bool ExecuteWaitTimeAbsolute(void);
//   //
//   //  Segment - Execution - Uart
//   bool ExecuteWriteDispatcherUart(void);
//   bool ExecuteReadDispatcherUart(void);
//   //
//   bool ExecuteWriteDispatcherWlan(void);
//   bool ExecuteReadDispatcherWlan(void);
//   //
//   bool ExecuteWriteDispatcherLan(void);
//   bool ExecuteReadDispatcherLan(void);
//   //
//   bool ExecuteWriteDispatcherBt(void);
//   bool ExecuteReadDispatcherBt(void);
//   //
//   //  Segment - Execution - SDCard
//   bool ExecuteOpenDispatcherFile(void);
//   bool ExecuteWriteDispatcherFile(void);
//   bool ExecuteCloseDispatcherFile(void);
//   bool ExecuteExecuteDispatcherFile(void);
//   bool ExecuteAbortDispatcherFile(void);
//   //
//   //  Segment - Execution - Mqtt
//   //
//   //  Segment - Execution - LedSystem
//   bool ExecuteGetLedSystem(void);
//   bool ExecuteLedSystemOn(void);
//   bool ExecuteLedSystemOff(void);
//   bool ExecuteBlinkLedSystem(void);
//   //
//   //  Segment - Execution - Rtc
//   bool ExecuteSetRtcDate(void);
//   bool ExecuteGetRtcDate(void);
//   bool ExecuteSetRtcTime(void);
//   bool ExecuteGetRtcTime(void);
//   //
//   //  Segment - Execution - NTPClient
//   bool ExecuteGetNTPClientDate(void);
//   bool ExecuteGetNTPClientTime(void);
//   //
//   //  Segment - Execution - Watchdog
//   bool ExecutePulseWatchDog(void);
//   //
//   //  Segment - Execution - I2CDisplay
//   bool ExecuteClearI2CDisplay(void);
//   bool ExecuteShowTextI2CDisplay(void);
//   //
//   //------------------------------------------------------
//   // Main
//   //------------------------------------------------------
//   void Init();
//   //
//   bool AnalyseDispatcherText(void);
//   void WriteEvent(String line);
//   void WriteResponse(String line);
//   void WriteComment(String line);
//   void WriteComment(String mask, String line);
//   void DistributeDispatcher(void);
//   bool DetectRxdLine(void);
//   bool DetectSDCardLine(void);
//   bool DetectMQTTClientLine(void);
//   bool Handle(void);
//   bool Execute(void);
// };
//
#endif // Dispatcher_h







//####################################################################################
































  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);
  // inline PCharacter GetRxdBuffer(void)
  // {
  //   return FRxdBuffer;
  // }
  // inline PCharacter GetTxdBuffer(void)
  // {
  //   return FTxdBuffer;
  // }
  // //
  // inline PCharacter GetPDispatcher(void)
  // {
  //   return FPDispatcher;
  // }
  // inline Byte GetParameterCount(void)
  // {
  //   return FParameterCount;
  // }
  // inline PCharacter GetPParameters(UInt8 index)
  // {
  //   return FPParameters[index];
  // } 
  // //
  // void ZeroRxdBuffer(void);
  // void ZeroTxdBuffer(void);
  // void ZeroDispatcherText(void);



  