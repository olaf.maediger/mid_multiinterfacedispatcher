﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
//using UdpProtocol;
//
namespace UdpMinimal
{
  public partial class FormUdpMinimal : Form
  {
    private CUdpProtocol FServer;
    private CUdpProtocol FClient;
    public FormUdpMinimal()
    {
      InitializeComponent();

      FServer = new CUdpProtocol();
      FServer.Server("127.0.0.1", 27000);

      FClient = new CUdpProtocol();
      FClient.Client("127.0.0.1", 27000);
      FClient.Send("TEST!");

    }
  }
}
