﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

// https://gist.github.com/darkguy2008/413a6fea3a5b4e67e5e0d96f750088a9

namespace UdpMinimal
{
  public class CBuffer
  {
    public const int SIZE_BUFFER = 8 * 1024;
    //
    public byte[] Data = new byte[SIZE_BUFFER];
  }

  public class CUdpProtocol
  {
    //
    private Socket FSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    private CBuffer FBuffer = new CBuffer();
    private EndPoint FEndPointSource = new IPEndPoint(IPAddress.Any, 0);
    private AsyncCallback CBOnReceived = null;


    public void Server(string address, int port)
    {
      FSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
      FSocket.Bind(new IPEndPoint(IPAddress.Parse(address), port));
      Receive();
    }

    public void Client(string address, int port)
    {
      FSocket.Connect(IPAddress.Parse(address), port);
      Receive();
    }

    public void Send(string text)
    {
      byte[] TXData = Encoding.ASCII.GetBytes(text);
      FSocket.BeginSend(TXData, 0, TXData.Length, SocketFlags.None, (asyncresult) =>
      {
        CBuffer so = (CBuffer)asyncresult.AsyncState;
        int bytes = FSocket.EndSend(asyncresult);
        Console.WriteLine("SEND: {0}, {1}", bytes, text);
      }, FBuffer);
    }

    private void Receive()
    {
      FSocket.BeginReceiveFrom(FBuffer.Data, 0, CBuffer.SIZE_BUFFER, 
                               SocketFlags.None, ref FEndPointSource, CBOnReceived = (asyncresult) =>
      {
        CBuffer ReceiveBuffer = (CBuffer)asyncresult.AsyncState;
        int ByteCount = FSocket.EndReceiveFrom(asyncresult, ref FEndPointSource);
        FSocket.BeginReceiveFrom(ReceiveBuffer.Data, 0, CBuffer.SIZE_BUFFER, 
                                 SocketFlags.None, ref FEndPointSource, CBOnReceived, ReceiveBuffer);
        Console.WriteLine("RECV: {0}: {1}, {2}", FEndPointSource.ToString(), 
                          ByteCount, Encoding.ASCII.GetString(ReceiveBuffer.Data, 0, ByteCount));
      }, FBuffer);
    }
  }

}
