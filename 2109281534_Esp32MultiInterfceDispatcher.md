<h2>MID_Esp32MultiInterfaceDispatcher</h2>

<h3>ToDo</h3>

* <b>O</b> arduino -> Wlan/Lan-CommandDispatcher

---


<h3>2109281538 : Esp32UdpWlan</h3>

* erste funktionierende UdpCommunication zwischen PC und Esp32 (Wlan)
* <b>2109281531_Esp32WlanBasicClass_01V06</b>
* <b>2109281531_WCsUdpTcpClientServer_01V06</b>


<h3>2109271037 : Esp32Lan</h3>

* <b>2109271133_Esp32LanMinimal</b>
* Esp32LanBasicClass
* Esp32LanBasic
* leider keinen Wlan-Stick im IFN, daher Versuch mit Lan


<h3>2109260724 : Transmit-/Receive-Line in Esp32WlanBasicClass</h3>

* Integration WlanUDPST(WlanUdpStation) / WlanTCPST(WlanTCPStation) aus MID
* Esp32Wlan mit Udp Transmit-/Receive-Line
* Basis (unter anderem): https://www.nikolaus-lueneburg.de/2015/01/arduino-udp-kommunikation/


<h3>2109250754 : Integration Wlan in Arduino-MultiInterfaceDispatcher</h3>

* Esp32Wlan mit Udp:
* Vorbereitung <b>2109181851_Esp32WlanBasicClass_01V02</b> mit WlanReadLine/WriteLine

<h3>2109240743 : Bidirektionale UDP/TCP-Kommunikation</h3>

* <b>2109242230_WCsUdpTcpClientServer_01V05</b> funktioniert mit 1xServer/1xClient pro PC !!!
* zwei Ports bei bidirektionaleer Kommunikation sind notwendig!!!
* Multiple Clients können gleichberechtigt auf mehrere Server zugreifen!
* Multiple Server können gleichberechtigt auf mehrere Clients zugreifen!
* <b>!!!</b> Udp-Library UdpClient-/UdpServer-Tasks jeweils in MainApplication(UdpServer&-Client) eingebaut
* Software verallgemeinern - IPAddress Server MUSS immer der PC sein, auf dem die Server-Software läuft - daher: automatische LocalIP-Bestimmung:
* d.h. auch unter C# funktionieren alle Udp-Server-Client-Kombinationen!!!
!!! sogar mit zwei Clients auf unterschiedlichen Rechnern kann ich auf einen Server zugreifen!!!!!
Auch mit zwei Clients auf zwei verschiedenen Rechnern!!!!
* mit Python auf Win10Laptop funktionieren sowohl senden(zum PCServer) als auch empfangen(vom PCClient) auch mit Firewall !!!!
* Client -> Server: IPAddress-Server muss korrekt sein
* Server -> Client: senden mit Broadcast Broadcast
* <b>2109240917_WCsUdpTcpClientServer_01V03</b> Port-Defines nach UDProtocol-Dll ausgelagert
* <b>2109240856_WCsUdpTcpClientServer_01V02_ServerClientDualPort</b>
* erst einmal zwei Ports: PORT_COMMAND_SERVER/CLIENT
* Zwei Clients senden eigene Message zu einem Server
* Client/Server.Udp/Tcp-Tabsheets Grösse anpassen
* Versuch: WPcTcpClient/WPcTcpServer: jeweils einen Task mit RxPort und einen Task mit TxPort, temporäre Connections, werden immer wieder neu eröffnet und geschlossen...


<h3>2109230839 : Neuer Versuch - Bidirektionale UDP/TCP-Kommunikation</h3>

* <b>2109232058_WCsUdpTcpClientServer_01V01</b> : Wiederholtes korrektes Senden vom UdpClient zum UdpServer mit Udp-Library, UdpClient-/UdpServer-Tasks!
* <b>2109231007_UdpMinimal_kompliziert</b>
* Neuanfang: Windows, TcpListener, TcpClient, ohne/mit Handshake, bidirektional
* <b>2109230905_WCsUdpTcpClientServer_00V01_base</b>

<h3>2109220700 : WLan-MinimalClientServer</h3>

* deleted!
* Versuch TcpClient(Client) / TcpListener(Server) OHNE Handshake während der gesamten Kommunikation nur einmal (auf einem Port!) zu öffne mit bidirektionaler Kommunikationsmöglichkeit:
gescheitert!
* <b>2109221650_WPcTcpClientServer_01V03_ClientServerHandshake</b>
* <b>2109221536_WPcTcpClientServer_01V02</b>
* <b>2109220954_WPcTcpClientServer_01V01_ClientServer</b>
* Symmetrisierung <b>TcpClient</b> / <b>TcpListener</b>
* <b>2109220954_WPcTcpClientServer_01V01_ClientServer</b>
Sendet Data von Client to Server

<h3>2109210833 : WLan-MinimalClientServer</h3>

* Blocking-Problem (Application no response)


<h3>2109190733 : WLan-CommandDispatcher</h3>

* Erweiterung <b>CONNECTIONWIFI / CWifiBase</b> mit <b>char* deviceid</b>


<h3>2109180800 : WLan-CommandDispatcher</h3>

* Teilweise Einbau <b>CConnectionWifi.cpp/.h</b>, Reconfigure <b>Wlan</b> in MID


<h3>2109180800 : Wlan OOP</h3>

* Namensänderung <b>CWifi</b> nach <b>CConnectionWifi</b> notwendig, sonst Compile-Error
* <b>2109181851_Esp32WlanBasicClass_01V02</b> mit WiFi-Reference, CConnectionWifi
* <b>2109181842_Esp32WlanBasicClass_01V01</b>
* Basisprogramm mit OOP


<h3>2109171834 : Wlan</h3>

Einfache Basisprogramme ohne OOP:
* erweiterte <b>platformio.ini</b> mit 3MB und 115200bps
* Wlan-Version <b>2109172136_Esp32WlanBasic_01V01</b>
* Wlan-Version <b>2109171920_Esp32WlanScan_01V01</b>

<h3>2109170700 : Speichererweiterung ESP32</h3>

* Minimalprojekt <b>2109170906_Esp32Flash4MB_01V01</b>
* modifizierte Datei <b>default_4MB.csv</b> erzeugt bei Compilierung 3MB Flash!!!
<code># Name,   Type, SubType, Offset,  Size, Flags
nvs,      data, nvs,     0x009000,0x005000,
otadata,  data, ota,     0x00e000,0x002000,
app0,     app,  ota_0,   0x010000,0x2E0000,
app1,     app,  ota_1,   0x3E0000,0x010000,
spiffs,   data, spiffs,  0x3F0000,0x010000,</code>

* Erweiterung <b>platformio.ini</b>:
<code>board_upload.flash_size = 4MB
board_upload.maximum_size = 4194304
board_build.partitions = default_4MB.csv</code>

* Original Partition-Files unter:
<a href="https://github.com/espressif/arduino-esp32/tree/master/tools/partitions">Espressif Esp32 Partition</a>

<h3>2109160800 : Speichererweiterung ESP32</h3>

* Stichwort: Partition-Table (als <b>default.csv</b> Datei unter x:/)
* default Flash-Memory: 1.3MB, obwohl 4MB vorhanden sind


<h3>210915xxxx : InterfaceBt ohne Mqtt</h3>

* <b>XX</b> Bt-CommandDispatcher verhält sich identisch zu Uart-/Mqtt-Dispatcher
* <b>X</b> Bluetooth-Version: <b>2109161457_Esp32MultiInterfaceDispatcher_00V35_UartBtorMqtt</b>

<h3>210913xxxx : InterfaceBt Exception</h3>

* Bt erst einmal mit Mqtt disabled programmieren
* <b>X</b> Exception liegt an dem zu geringen Flash-Speicher

<h3>2109121018</h3>

* <b>???</b> Instanzierung der CInterfaceBt-Class löst Exception aus
* <b>X</b> <b>DefinitionSystem.h</b> für Bluetooth freischalten
* <b>...</b> Mqtt-Ausgaben identisch zu Uart-Ausgaben (auch Hilfe...) erweitern!
* <b>X</b> Eigene Bt-Class-Library suchen, Reste gefunden und neu formuliert

<h3>2109111002</h3>

* <b>X</b> <b>2109111648_Esp32MultiInterfaceDispatcher_00V33</b>
* <b>X</b> Übertragung von <b>Definition.h</b>(Mqtt) nach <b>DefinitionSystem.h</b>
* <b>X</b> Jede Device erhält eigene <b>Definition<device>.h</b> Datei
* <b>X</b> Mqtt-Uart-Ausgaben normalisieren
* <b>X</b> Mqtt-Commands werden bearbeitet
* <b>X</b> Exception in <b>Command.cpp</b> (aktuell unkommentiert)
> for (int MI = 0; MI < MQTT_PARAMETERCOUNT; MI++)
> {
> // &nbsp;&nbsp;MqttParameters[0] = 0x00;
> &nbsp;&nbsp;MqttParameters[MI][0] = 0x00;
> }

* <b>X</b> main.cpp: <b>void InterfaceProtocolDispatcherPeriodic(void)</b>
* <b>X</b> main.cpp: Dispatcher Erweiterung mit Mqtt, neue Command-Interface/Protocol/Dispatcher Function: <b>bool InterfaceProtocolDispatcher(char* command, int parametercount, char** parameters)</b>
* <b>X</b> main.cpp: <b>void CommandDispatcherPeriodic(void)</b>
* <b>X</b> Entschlackung von main-<b>setup()</b> und -<b>loop()</b>:
  <b>SetupInterfaceProtocol()</b>
  <b>SetupDispatcher()</b>


<h3>2109100430</h3>

* <b>X</b> bool CCommand::AnalyseInterfaceBlock(void) mit Mqtt erweitern
* <b>O</b> Erweiterung DISPATCHER_MQTT
* <b>X</b> <b>2109101607_Esp32MultiInterfaceDispatcher_00V32</b>
  mit Uart- und (teilweiser)Mqtt-Kommunikation(ohne Dispatcher)
* <b>X</b> Zyklisches Reconnecten funktioniert
* <b>X</b> Integration von <b>2109100958_Esp32PointerPointer_01V01</b> und <b>CProtocolMqtt</b>
* <b>X</b> Demo-Programm zur Lösung des PointerPointerProblems:
  <b>2109100958_Esp32PointerPointer_01V01</b>,
  Übertragung der Pointer PCharacter, VPCharacter in <b>DefinitionSystem.h</b>
* Boolean CInterfaceUartHS::ReadLine(char* prxline, int &rxsize)
  reads a whole line with CR/LF,
  if <line then rxdata is buffered in FRXBlock,
  return true: found whole line
  prxline points to extern buffer with copy of line

<h3>2109090700</h3>

* <b>O</b> Help-Ausgabe main.cpp von uart auf Mqtt erweitern!
* <b>O</b> Bearbeitung der Command-Dispatcher-Files
  DispatcherMqtt.h/.cpp entsprechend DispatcherUart.h/.cpp
* <b>X</b> Basis-Interface-Files: Uart.h/.cpp // Mqtt.h/.cpp
  (Um)Benennung auf InterfaceUart.h/.cpp mit CUart -> CInterfaceUart
  (Um)Benennung auf Mqtt.h/.cpp mit CMqtt -> CProtocolMqtt
* <b>O</b> Topic-Buffer: Reduction auf 32 Byte
* <b>O</b> Mqtt-Instanz in main.cpp
* <b>O</b>
* <b>///</b> Integration von CProtocolMqtt MqttCommand in main.cpp
* <b>X</b> ReadCommandUart/Mqtt geben keinen Sinn, da automatische Hintergrund-Bearbeitung
  (Receiving und Dispatching) der eingehenden RXData
* <b>O</b> Integration von OOP-Version <b>2109081625_Esp32PubSubClient_02V01</b>,
  speziell (Protocol)Mqtt.h, (Protocol)Mqtt.cpp, Definition.h

<h3>2109081800</h3>

  * Restart Projekt <b>Esp32MultiInterfceDispatcher</b> auf der Grundlage von   <b>2108311537_Esp32MultiInterfaceDispatcher_NoErrorRXBuffer</b>
  * Fertigstellung Version: 2109081625_Esp32PubSubClient_02V01




<!--
<a href="https://pubsubclient.knolleary.net/api">Api PubSubClient</a>
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<h8></h8>
<b></b>
<i></i>
<></>
<></>

<b><i>Beispiel LaTex in Markdown:</i></b>
$A = \dfrac{\pi r^2}{2} = \frac{1}{2} \pi r^2$
-->
