#include "DefinitionSystem.h"
//
#ifdef INTERFACE_WLAN
//
#ifndef InterfaceWlan_h
#define InterfaceWlan_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
#include "ConnectionWifi.h"
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  Constant
//----------------------------------------------------
const byte WLAN_SIZE_RXBLOCK = 32;
const byte WLAN_SIZE_TXBLOCK = 32;
const bool WLAN_INIT_RXECHO    = true;
//
//----------------------------------------------------
//  CInterfceWlanBase
//----------------------------------------------------
class CInterfaceWlanBase : public CInterfaceBase
{
  public:
  CInterfaceWlanBase(const char* deviceid);
  // Handler
  virtual bool Open(int parameter) = 0;
  virtual bool Close(void) = 0;
  // Read
  virtual byte GetRxCount(void) = 0;
  virtual bool Read(char &character) = 0;
  virtual bool ReadLine(char* pline, int &size) = 0;
  // Write
  virtual bool Write(char character) = 0;
  virtual bool WriteText(char* ptext) = 0;
  virtual bool WriteLine(void) = 0;
  virtual bool WriteLine(char* pline) = 0;
};
//
//----------------------------------------------------
//  CInterfaceWlanUDPST - UdpStation
//----------------------------------------------------
class CInterfaceWlanUDPST : public CInterfaceWlanBase
{
  protected:
  CWifiStation* FPWifiStation;
  
  public:
  CInterfaceWlanUDPST(const char* deviceid, 
                      CWifiStation* pwifistation);
 // Handler
  virtual bool Open(int parameter);
  virtual bool Close(void);
  // Read
  virtual byte GetRxCount(void);
  virtual bool Read(char &character);
  virtual bool ReadLine(char* pline, int &size);
  // Write
  virtual bool Write(char character);
  virtual bool WriteText(char* ptext);
  virtual bool WriteLine(void);
  virtual bool WriteLine(char* pline);
};
//
//----------------------------------------------------
//  CInterfaceWlanTCPST - TcpStation
//----------------------------------------------------
class CInterfaceWlanTCPST : public CInterfaceWlanBase
{
  protected:
  CWifiStation* FPWifiStation;
  
  public:
  CInterfaceWlanTCPST(const char* deviceid, 
                      CWifiStation* pwifistation);
 // Handler
  virtual bool Open(int parameter);
  virtual bool Close(void);
  // Read
  virtual byte GetRxCount(void);
  virtual bool Read(char &character);
  virtual bool ReadLine(char* pline, int &size);
  // Write
  virtual bool Write(char character);
  virtual bool WriteText(char* ptext);
  virtual bool WriteLine(void);
  virtual bool WriteLine(char* pline);
};
//
//----------------------------------------------------
//  CInterfaceWlan
//----------------------------------------------------
class CInterfaceWlan
{
  protected:
  CInterfaceWlanBase* FPWlanBase;
  //
  public:
  // Constructor
  CInterfaceWlan(const char* deviceid, 
                 CWifiStation* pwifistation);
  // Handler
  bool Open(int parameter);
  bool Close(void);
  // Property
  char* GetRxBlock(void)
  {
    return FPWlanBase->GetRxBlock();
  }
  char* GetTxBlock(void)
  {
    return FPWlanBase->GetTxBlock();
  }
  bool GetRxEcho(void)
  {
    return FPWlanBase->GetRxEcho();
  }
  void SetRxEcho(bool rxecho)
  {
    FPWlanBase->SetRxEcho(rxecho);
  }
  // Read
  byte GetRxCount(void);
  bool Read(char &rxcharacter);
  bool ReadText(char* ptext);
  bool ReadLine(char* rxline, int &rxsize);
  //
  // Write
  bool Write(char character);
  bool WriteText(char* ptext);
  bool WriteLine(void);
  bool WriteLine(char* pline);
  //
  bool Execute(void);
};
//
#endif // InterfaceWlan_h
//
#endif // INTERFACE_WLAN
//
//
