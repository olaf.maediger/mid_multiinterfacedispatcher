//
#include "DefinitionSystem.h"
//
#if defined(CONNECTION_WIFI)
//
#include "ConnectionWifi.h"
//
//-------------------------------------------------------------
//  WifiBase
//-------------------------------------------------------------
CWifiBase::CWifiBase(WiFiClass* pwifi, 
                     const char* ssid, const char* password)
{
  FPWiFi = pwifi;
  FSSID = ssid;
  FPassword = password;
}
bool CWifiBase::IsConnected(void)
{
  return (WL_CONNECTED == FPWiFi->status());
}
const char* CWifiBase::GetSSID(void)
{
  return FSSID;
}
const char* CWifiBase::GetPassword(void)
{
  return FPassword;
}
IPAddress CWifiBase::GetIPAddress(void)
{
  return FPWiFi->localIP();
}
//
bool CWifiBase::Open(void)
{
  FPWiFi->begin(FSSID, FPassword);
  for (int CI = 0; CI < WIFI_COUNT_TRYCONNECTION; CI++)
  {
    if (WL_CONNECTED == WiFi.status())
    {
      return true;
    }
    delay(1000);
  }
  return false;
}
bool CWifiBase::Close(void)
{
  FPWiFi->disconnect();
  return true;
}
bool CWifiBase::Reconnect(void)
{
  if (!(WL_CONNECTED == FPWiFi->status()))
  {
    FPWiFi->disconnect();
    FPWiFi->reconnect();  
    delay(3000);
  }
  return IsConnected();
}
//
bool CWifiBase::WriteLine(char* pline, int countwrite)
{
  return false;
}
bool CWifiBase::ReadLine(char* pline, int& countpresetread)
{
  return false;
}
//
//-------------------------------------------------------------
//  WifiStation
//-------------------------------------------------------------
CWifiStation::CWifiStation(WiFiClass* pwifi, 
                           const char* ssid, const char* password)
  : CWifiBase(pwifi, ssid, password)
{
}
bool CWifiStation::Open(void)
{
  Serial.print("Wifi connecting[");
  Serial.print(GetSSID());
  Serial.print("|");
  Serial.print(GetPassword());
  Serial.println("] ...");
  FPWiFi->mode(WIFI_STA);
  if (!CWifiBase::Open())
  {
    Serial.println("Error: Cannot connect to Wifi!");
    return false;
  }
  Serial.println("Wifi connected ok");
  Serial.print("Wifi IPAddress[");
  Serial.print(GetIPAddress());
  Serial.println("]");
  Serial.print("Wifi RSSI[");
  Serial.print(WiFi.RSSI());
  Serial.println("dBm]");
  return true;
}
bool CWifiStation::Close(void)
{
  return CWifiBase::Close();
}
bool CWifiStation::Reconnect(void)
{
  return CWifiBase::Reconnect();
}
bool CWifiStation::WriteLine(char* pline, int countwrite)
{
  return CWifiBase::WriteLine(pline, countwrite);
}
bool CWifiStation::ReadLine(char* pline, int& countpresetread)
{
  return CWifiBase::ReadLine(pline, countpresetread);
}
// //
// //-------------------------------------------------------------
// //  WifiAccessPoint
// //-------------------------------------------------------------
// CWifiAccessPoint::CWifiAccessPoint(WiFiClass* pwifi,
//                                    const char* ssid, const char* password)
//   : CWifiBase(pwifi, ssid, password)
// {  
// }
// bool CWifiAccessPoint::IsConnected(void)
// { 
//   return CWifiBase::IsConnected();
// }
// bool CWifiAccessPoint::Open(void)
// {
//    FPWiFi->mode(WIFI_STA...);
//   return CWifiBase::Open();
// }
// bool CWifiAccessPoint::Close(void)
// {
//   return CWifiBase::Close();
// }
// bool CWifiAccessPoint::Reconnect(void)
// {
//   return CWifiBase::Reconnect();
// }
// bool CWifiAccessPoint::WriteLine(char* pline, int countwrite)
// {
//   return CWifiBase::WriteLine(pline, countwrite);
// }
// bool CWifiAccessPoint::ReadLine(char* pline, int& countpresetread)
// {
//   return CWifiBase::ReadLine(pline, countpresetread);
// }
//
#endif // CONNECTION_WIFI
//