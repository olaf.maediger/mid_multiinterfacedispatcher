//
#include "DefinitionSystem.h"
//
#if defined(INTERFACE_WLAN)
//
#include "InterfaceWlan.h"
//
//####################################################
//  CInterfaceWlanBase
//####################################################
//----------------------------------------------------
//  CInterfaceWlanBase - Constructor
//----------------------------------------------------
// CInterfaceBtBase(abstract!) - CInterfaceBase(abstract!)
CInterfaceWlanBase::CInterfaceWlanBase(const char* deviceid)
  : CInterfaceBase(deviceid)
{
  FRxEcho = WLAN_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBtBase - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Handler
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Write
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBtBase - Read
//----------------------------------------------------


//
//----------------------------------------------------
//  CInterfaceWlanWIS
//----------------------------------------------------
CInterfaceWlanUDPST::CInterfaceWlanUDPST(const char* deviceid, 
                                         CWifiStation* pwifistation)
  : CInterfaceWlanBase(deviceid)
{
  FPWifiStation = pwifistation;
}                                     
// Handler
bool CInterfaceWlanUDPST::Open(int parameter)
{
  if (!FPWifiStation->IsConnected())
  {
    if (FPWifiStation->Open())
    {
    }
  }
  return (FPWifiStation->IsConnected());
}
bool CInterfaceWlanUDPST::Close(void)
{
  return false;
}
// Read
byte CInterfaceWlanUDPST::GetRxCount(void)
{
  return 0;
}
bool CInterfaceWlanUDPST::Read(char &character)
{
  return false;
}
bool CInterfaceWlanUDPST::ReadLine(char* pline, int &size)
{
  size = 0;
  return false;
}
// Write
bool CInterfaceWlanUDPST::Write(char character)
{
  return false;
}
bool CInterfaceWlanUDPST::WriteText(char* ptext)
{
  return false;
}
bool CInterfaceWlanUDPST::WriteLine(void)
{
  return false;
}
bool CInterfaceWlanUDPST::WriteLine(char* pline)
{
  return false;
}
//
//####################################################
//  CInterfaceWlan
//####################################################
//----------------------------------------------------
//  CInterfaceWlan - Constructor
//----------------------------------------------------
CInterfaceWlan::CInterfaceWlan(const char* deviceid,  
                               CWifiStation* pwifistation)
{
  FPWlanBase = new CInterfaceWlanUDPST(deviceid, pwifistation);
}
//
//----------------------------------------------------
//  CInterfaceBt - Property
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBt - Handler
//----------------------------------------------------
bool CInterfaceWlan::Open(int parameter)
{
  return FPWlanBase->Open(parameter);
}
bool CInterfaceWlan::Close(void)
{
  return FPWlanBase->Close();
}
//
//----------------------------------------------------
//  CInterfaceBt - Read
//----------------------------------------------------
byte CInterfaceWlan::GetRxCount(void)
{
  return FPWlanBase->GetRxCount();
}
bool CInterfaceWlan::Read(char &character)
{
  return FPWlanBase->Read(character);
}
// true: whole line with cr/lf found
bool CInterfaceWlan::ReadLine(char* prxline, int &rxsize)
{
  return FPWlanBase->ReadLine(prxline, rxsize);
}
//
//----------------------------------------------------
// CInterfaceUart - Write
//----------------------------------------------------
bool CInterfaceWlan::Write(char character)
{
  return FPWlanBase->Write(character);
}
bool CInterfaceWlan::WriteText(char* ptext)
{
  return FPWlanBase->WriteText((char*)ptext);
}
bool CInterfaceWlan::WriteLine(void)
{
  return FPWlanBase->WriteLine();
}
bool CInterfaceWlan::WriteLine(char* pline)
{
  return FPWlanBase->WriteLine((char*)pline);
}
//
//------------------------------------------------------------------------
// Bt - Write - Specials
//------------------------------------------------------------------------

//
//-------------------------------------------------------------------
//  Bt - Execute
//-------------------------------------------------------------------
bool CInterfaceWlan::Execute(void)
{
  return true;
}
//
#endif // INTERFACE_BT
//
