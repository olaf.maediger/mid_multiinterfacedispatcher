//
#include "DefinitionSystem.h"
//
#include "InterfaceBase.h"
//
//----------------------------------------------------
//  CInterfaceBase - Constructor
//----------------------------------------------------
CInterfaceBase::CInterfaceBase(const char* deviceid)
{
  strncpy(FDeviceID, deviceid, INTERFACE_SIZE_DEVICEID - 1);
  // NC FDeviceID[INTERFACE_SIZE_DEVICEID - 1] = 0x00;
  FRxIndex = 0;
  FRxBlock[FRxIndex] = 0x00;
  FRxEcho = INTERFACE_INIT_RXECHO;
}
//
//----------------------------------------------------
//  CInterfaceBase - Property
//----------------------------------------------------

//
//----------------------------------------------------
//  CInterfaceBase - Helper
//----------------------------------------------------
//
//----------------------------------------------------
//  CInterfaceBase - Handler
//----------------------------------------------------
// virtual Boolean Open(int parameter) = 0;
// virtual Boolean Close(void) = 0;
//
//----------------------------------------------------
//  CInterfaceBase - Read
//----------------------------------------------------
// virtual UInt8 GetRXCount(void) = 0;
// virtual Boolean Read(char &character) = 0;
// virtual Boolean ReadLine(char* prxline, int &rxsize) = 0;
//  
//----------------------------------------------------
//  CInterfaceBase - Write
//----------------------------------------------------
// virtual Boolean Write(char character) = 0;
// virtual Boolean WriteText(char* ptext) = 0;
// virtual Boolean WriteLine(void) = 0;
// virtual Boolean WriteLine(char* pline) = 0;
// 