//
// Basic Wlan with OOP
//
#include "ConnectionWifi.h"
#include "InterfaceWlan.h"
//
const char* WIFI_SSID     = "TPL1300SA7AZ";
const char* WIFI_PASSWORD = "01234567890123456789";
//
unsigned long MillisPreset = 0;
unsigned long TIME_INTERVAL = 30000;
//
CWifiStation WifiStation(&WiFi, WIFI_SSID, WIFI_PASSWORD);
CInterfaceWlan InterfaceWlan("IWL", &WifiStation);
//
void setup() 
{
  Serial.begin(115200);
  delay(333);
  Serial.println("\r\n");
  Serial.println("***********************");
  Serial.println("* Esp32WlanBasicClass *");
  Serial.println("* Version: 01V04      *");
  Serial.println("* Date:    210926     *");
  Serial.println("* Time:    0831       *");
  Serial.println("* Author:  OMdevelop  *");
  Serial.println("***********************");
  //
  // if (!WifiStation.Open())
  // {
  //   // Error...
  //   return;
  // }
  //
  if (!InterfaceWlan.Open(0))
  {
    // Error...
    return;
  }
}

void loop() 
{
  if (!WifiStation.IsConnected())
  {
    Serial.println("WiFi reconnecting...");
    if (WifiStation.Reconnect())
    {
      Serial.println("Wifi connected");
    }
  } 
  else
  { // look for received messages:
    // after each Time-Interval send a Measurement-Value:
  }
}