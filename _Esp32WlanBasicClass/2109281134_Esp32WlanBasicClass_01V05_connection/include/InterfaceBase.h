#include "DefinitionSystem.h"
//
#ifndef InterfaceBase_h
#define InterfaceBase_h
//
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
const int INTERFACE_SIZE_DEVICEID   =  6;
const int INTERFACE_SIZE_RXBLOCK    = 256;
const int INTERFACE_SIZE_TXBLOCK    = 256;
//
const int INTERFACE_SIZE_CONVERSION = 256;
//
const bool INTERFACE_INIT_RXECHO   = false;
//
//
//----------------------------------------------------
//  CInterfaceBase
//----------------------------------------------------
class CInterfaceBase
{
  protected:
  char FDeviceID[INTERFACE_SIZE_DEVICEID];
  char FRxBlock[INTERFACE_SIZE_RXBLOCK];
  bool FRxEcho;
  int  FRxIndex;
  char FTxBlock[INTERFACE_SIZE_TXBLOCK];
  char FCVBuffer[INTERFACE_SIZE_CONVERSION];
  //
  public:
  //
  // Constructor
  CInterfaceBase(const char* deviceid);
  //
  // Property
  const char* GetDevideID(void)
  {
    return FDeviceID;
  }
  char* GetRxBlock(void)
  {
    return FRxBlock;
  }
  char* GetTxBlock(void)
  {
    return FTxBlock;
  }
  bool GetRxEcho(void)
  {
    return FRxEcho;    
  }
  void SetRxEcho(bool rxecho)
  {
    FRxEcho = rxecho;
  }
  //
  // Helper
  // Handler
  virtual bool Open(int parameter) = 0;
  virtual bool Close(void) = 0;
  // Read
  virtual byte GetRxCount(void) = 0;
  virtual bool Read(char &character) = 0;
  virtual bool ReadLine(char* pline, int &size) = 0;
  // Write
  virtual bool Write(char character) = 0;
  virtual bool WriteText(char* ptext) = 0;
  virtual bool WriteLine(void) = 0;
  virtual bool WriteLine(char* pline) = 0;
};
//
#endif // InterfaceBase_h
//
//