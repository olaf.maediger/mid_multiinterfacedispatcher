//
// Basic Wlan with OOP
//
#include "ConnectionWifi.h"
#include "InterfaceWlan.h"
//
// // SA7
// const char* WIFI_SSID     = "TPL1300SA7AZ";
// const char* WIFI_PASSWORD = "01234567890123456789";
//
// IFN
const char* WIFI_SSID     = "IFNANO-Guest";
const char* WIFI_PASSWORD = "ifnano-2020";
//
unsigned long MillisPreset = 0;
unsigned long TIME_INTERVAL = 30000;
//
CWifiStation WifiStation(&WiFi, WIFI_SSID, WIFI_PASSWORD);
CInterfaceWlan InterfaceWlan("IWL", &WifiStation);
//
void Error(const char* text)
{
  Serial.print("Error: ");
  Serial.print(text);
  Serial.println("!");
}
//
bool OpenConnectWifiWlan()
{
  while ((!WifiStation.IsConnected()) || (!InterfaceWlan.IsConnected()))
  {
    if (!WifiStation.IsConnected())
    {
      Serial.print("Wifi connecting[");
      Serial.print(WifiStation.GetSSID());
      Serial.print("|");
      Serial.print(WifiStation.GetPassword());
      Serial.println("] ...");
      while (!WifiStation.Open())
      {
        Error("Cannot connect to Wifi");
        delay(3000);
        Serial.println("Try Reconnection...");
      }
      Serial.println("Wifi connected ok");
      Serial.print("Wifi IPAddress[");
      Serial.print(WifiStation.GetIPAddress());
      Serial.println("]");
      Serial.print("Wifi RSSI[");
      Serial.print(WifiStation.GetRSSI());
      Serial.println("dBm]");  
    }
    if (!InterfaceWlan.IsConnected())
    {
      Error("Cannot open InterfaceWlan");
    }
  }
  return true;
}

bool CheckReconnectionWifiWlan(void)
{
  while (!WifiStation.IsConnected())
  {
    Serial.println("Warning: WiFi reconnecting...");
    if (WifiStation.Reconnect())
    {
      Serial.println("Wifi reconnected");
      return true;
    }
    delay(3000);
  }
  return true;
}
//
void setup() 
{
  Serial.begin(115200);
  delay(333);
  Serial.println("\r\n");
  Serial.println("***********************");
  Serial.println("* Esp32WlanBasicClass *");
  Serial.println("* Version: 01V05      *");
  Serial.println("* Date:    210928     *");
  Serial.println("* Time:    0844       *");
  Serial.println("* Author:  OMdevelop  *");
  Serial.println("***********************");
  //
  OpenConnectWifiWlan();
}

void loop() 
{
  if (!CheckReconnectionWifiWlan()) return;
  //
  //...
}
