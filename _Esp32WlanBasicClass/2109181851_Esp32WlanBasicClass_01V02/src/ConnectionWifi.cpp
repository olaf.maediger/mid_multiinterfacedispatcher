//
#include "DefinitionSystem.h"
//
#if defined(CONNECTION_WIFI)
//
#include "ConnectionWifi.h"
//
//-------------------------------------------------------------
//  WifiBase
//-------------------------------------------------------------
CWifiBase::CWifiBase(WiFiClass* pwifi, 
                     const char* ssid, const char* password)
{
  FPWiFi = pwifi;
  FSSID = ssid;
  FPassword = password;
}
bool CWifiBase::IsConnected(void)
{
  return (WL_CONNECTED == FPWiFi->status());
}
const char* CWifiBase::GetSSID(void)
{
  return FSSID;
}
const char* CWifiBase::GetPassword(void)
{
  return FPassword;
}
IPAddress CWifiBase::GetIPAddress(void)
{
  return FPWiFi->localIP();
}
bool CWifiBase::Open(void)
{
  FPWiFi->mode(WIFI_STA);
  FPWiFi->begin(FSSID, FPassword);
  for (int CI = 0; CI < WIFI_COUNT_TRYCONNECTION; CI++)
  {
    if (WL_CONNECTED == WiFi.status())
    {
      return true;
    }
    delay(1000);
  }
  return false;
}

bool CWifiBase::Close(void)
{
  FPWiFi->disconnect();
  return true;
}
bool CWifiBase::Reconnect(void)
{
  FPWiFi->disconnect();
  FPWiFi->reconnect();  
  delay(1000);
  return IsConnected();
}
//
//-------------------------------------------------------------
//  WifiStation
//-------------------------------------------------------------
CWifiStation::CWifiStation(WiFiClass* pwifi, 
                           const char* ssid, const char* password)
  : CWifiBase(pwifi, ssid, password)
{
}
bool CWifiStation::Open(void)
{
  return CWifiBase::Open();
}
bool CWifiStation::Close(void)
{
  return CWifiBase::Close();
}
bool CWifiStation::Reconnect(void)
{
  return CWifiBase::Reconnect();
}
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
CWifiAccessPoint::CWifiAccessPoint(WiFiClass* pwifi,
                                   const char* ssid, const char* password)
  : CWifiBase(pwifi, ssid, password)
{  
}
bool CWifiAccessPoint::Open(void)
{
  return CWifiBase::Open();
}
bool CWifiAccessPoint::Close(void)
{
  return CWifiBase::Close();
}
bool CWifiAccessPoint::Reconnect(void)
{
  return CWifiBase::Reconnect();
}
//
#endif // CONNECTION_WIFI
//