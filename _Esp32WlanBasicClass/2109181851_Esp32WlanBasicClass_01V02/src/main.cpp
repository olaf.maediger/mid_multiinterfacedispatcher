//
// Basic Wlan with OOP
//
#include "ConnectionWifi.h"
//
const char* WIFI_SSID     = "TPL1300SA7AZ";
const char* WIFI_PASSWORD = "01234567890123456789";
//
unsigned long MillisPreset = 0;
unsigned long TIME_INTERVAL = 30000;
//
CWifiStation WifiStation(&WiFi, WIFI_SSID, WIFI_PASSWORD);
//
void setup() 
{
  Serial.begin(115200);
  delay(333);
  Serial.println("\r\n");
  Serial.println("***********************");
  Serial.println("* Esp32WlanBasicClass *");
  Serial.println("* Version: 01V02      *");
  Serial.println("* Date:    210918     *");
  Serial.println("* Time:    1851       *");
  Serial.println("* Author:  OMdevelop  *");
  Serial.println("***********************");
  //
  Serial.print("Wifi connecting[");
  Serial.print(WifiStation.GetSSID());
  Serial.print("|");
  Serial.print(WifiStation.GetPassword());
  Serial.println("] ...");
  if (!WifiStation.Open())
  {
    Serial.println("Error: Cannot connect to Wifi!");
    return;
  }
  Serial.println("Wifi connected ok");
  Serial.print("Wifi IPAddress[");
  Serial.print(WifiStation.GetIPAddress());
  Serial.println("]");
  Serial.print("Wifi RSSI[");
  Serial.print(WiFi.RSSI());
  Serial.println("dBm]");
}

void loop() 
{
  if (!WifiStation.IsConnected())
  {
    Serial.println("WiFi reconnecting...");
    if (WifiStation.Reconnect())
    {
      Serial.println("Wifi connected");
    }
  }  
}