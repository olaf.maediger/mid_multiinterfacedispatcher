//
#include "DefinitionSystem.h"
//
#if defined(CONNECTION_WIFI)
//
#ifndef Wifi_h
#define Wifi_h
//
#include <Arduino.h>
#include "WiFi.h"
//
const int WIFI_COUNT_TRYCONNECTION = 10;
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
class CWifiBase
{
  protected:
  WiFiClass*  FPWiFi;
  const char* FSSID;
  const char* FPassword;
  public:
  CWifiBase(WiFiClass* pwifi, const char* ssid, const char* password);
  //
  bool IsConnected(void);
  const char* GetSSID(void);  
  const char* GetPassword(void);  
  IPAddress GetIPAddress(void);
  int GetRSSI(void);
  //
  virtual bool Open(void);
  virtual bool Close(void);
  virtual bool Reconnect(void);
  //
  virtual bool WriteLine(char* pline, int countwrite);
  virtual bool ReadLine(char* pline, int& countpresetread);
};
//
//-------------------------------------------------------------
//  WifiAccessPoint
//-------------------------------------------------------------
class CWifiStation : public CWifiBase
{
  protected:
  public:
  CWifiStation(WiFiClass* pwifi, const char* ssid, const char* password);
  //
  bool Open(void);
  bool Close(void);
  bool Reconnect(void);
  //
  bool WriteLine(char* pline, int countwrite);
  bool ReadLine(char* pline, int& countpresetread);
};
// //
// //-------------------------------------------------------------
// //  WifiAccessPoint
// //-------------------------------------------------------------
// class CWifiAccessPoint : public CWifiBase
// {
//   private:
//   protected:
//   public:
//   CWifiAccessPoint(WiFiClass* pwifi, const char* ssid, const char* password);
//   //
//   bool IsConnected(void);
//   //
//   bool Open(void);
//   bool Close(void);
//   bool Reconnect(void);
//   //
//   bool WriteLine(char* pline, int countwrite);
//   bool ReadLine(char* pline, int& countpresetread);
// };
// //
// //-------------------------------------------------------------
// //  WifiStationAccessPoint
// //-------------------------------------------------------------
// //...
//
// //
// //-------------------------------------------------------------
// //  WifiAccessPoint
// //-------------------------------------------------------------
// class CWifiAccessPointStation : public CWifiBase
// {
//   private:
//   protected:
//   public:
//   CWifiAccessPointStation(WiFiClass* pwifi, const char* ssid, const char* password);
//   //
//   bool IsConnected(void);
//   //
//   bool Open(void);
//   bool Close(void);
//   bool Reconnect(void);
//   //
//   bool WriteLine(char* pline, int countwrite);
//   bool ReadLine(char* pline, int& countpresetread);
// };
// //
// //-------------------------------------------------------------
// //  WifiStationAccessPoint
// //-------------------------------------------------------------
// //...
//
#endif // DefinitionSystem_h
//
#endif // INTERFACE_WLAN
//