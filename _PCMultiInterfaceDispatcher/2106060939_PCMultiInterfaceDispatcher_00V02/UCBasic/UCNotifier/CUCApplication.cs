﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCNotifier
{ //
  //-----------------------------------------------------
  //  Segment - Type
  //-----------------------------------------------------
  //
  public delegate void DOnApplicationCancel(RApplicationData data);
  public delegate void DOnApplicationConfirm(RApplicationData data);
  //
  public partial class CUCApplication : UserControl
  { //
    //-----------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------
    //
    private const String STRING_EMPTY = "";
    private Color COLOR_READONLY = SystemColors.Info;
    private Color COLOR_READWRITE = SystemColors.Window;
    //
    //-----------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------
    //
    private CApplicationlist FApplicationlist;
    private DOnApplicationCancel FOnApplicationCancel;
    private DOnApplicationConfirm FOnApplicationConfirm;
    private Boolean FModeDelete;
    private RApplicationData FApplicationData;
    //
    //-----------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------
    //
    public void SetApplicationlist(CApplicationlist value)
    {
      FApplicationlist = value;
    }

    public void SetOnApplicationCancel(DOnApplicationCancel value)
    {
      FOnApplicationCancel = value;
    }

    public void SetOnApplicationConfirm(DOnApplicationConfirm value)
    {
      FOnApplicationConfirm = value;
    }

    public String Title
    {
      get { return gbxApplication.Text; }
      set { gbxApplication.Text = value; }
    }

    public String ConfirmTitle
    {
      get { return btnConfirm.Text; }
      set { btnConfirm.Text = value; }
    }

    public Boolean ModeDelete
    {
      get { return FModeDelete; }
      set { FModeDelete = value; }
    }
    //
    //-----------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------
    //
    public CUCApplication()
    {
      InitializeComponent();
      FApplicationData = new RApplicationData(0);
    }
    //
    //-----------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------
    //
    private void tbxTextBoxInput_TextChanged(object sender, EventArgs e)
    {
      if (FModeDelete)
      { // Delete - Name
        tbxName.BackColor = COLOR_READONLY;
        tbxName.Enabled = false;
        btnCopyApplicationName.Enabled = false;
        btnPasteApplicationName.Enabled = false;
        // Delete - ProductKey
        tbxProductKey.BackColor = COLOR_READONLY;
        tbxProductKey.Enabled = false;
        btnCopyProductKey.Enabled = false;
        btnPasteProductKey.Enabled = false;
      } else
      { // Add / Edit - Name
        tbxName.BackColor = COLOR_READWRITE;
        tbxName.Enabled = true;
        btnCopyApplicationName.Enabled = true;
        btnPasteApplicationName.Enabled = true;
        // Add / Edit - ProductKey
        tbxProductKey.BackColor = COLOR_READWRITE;
        tbxProductKey.Enabled = true;
        btnCopyProductKey.Enabled = true;
        btnPasteProductKey.Enabled = true;
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------
    //
    public Boolean SetData(RApplicationData data)
    {
      FApplicationData = data;
      tbxName.Text = data.TransformData.Name;
      tbxProductKey.Text = data.TransformData.ProductKey;
      return true;
    }

    public Boolean GetData(ref RApplicationData data)
    {
      data = FApplicationData;
      data.TransformData.Name = tbxName.Text;
      data.TransformData.ProductKey = tbxProductKey.Text;
      return true;
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu
    //-----------------------------------------------------
    //
    private void btnCancel_Click(object sender, EventArgs e)
    {
      if (FOnApplicationCancel is DOnApplicationCancel)
      {
        GetData(ref FApplicationData);
        FOnApplicationCancel(FApplicationData);
      }
    }

    private void btnConfirm_Click(object sender, EventArgs e)
    {
      if (FOnApplicationCancel is DOnApplicationCancel)
      {
        GetData(ref FApplicationData);
        FOnApplicationConfirm(FApplicationData);
      }
    }

    private void btnPasteName_Click(object sender, EventArgs e)
    {
      tbxName.Text = Clipboard.GetText();
    }
    private void btnCopyName_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(tbxName.Text);
    }

    private void btnCopyProductKey_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(tbxProductKey.Text);
    }
    private void btnPasteProductKey_Click(object sender, EventArgs e)
    {
      tbxProductKey.Text = Clipboard.GetText();
    }
    //
    //-----------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------
    //
    public void RefreshControls()
    {
      tbxTextBoxInput_TextChanged(this, null);
    }

  }
}
