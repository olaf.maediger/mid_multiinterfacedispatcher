﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCTextEditor
{
  public partial class CUCRichTextBox : RichTextBox
  {
    public CUCRichTextBox()
    {
      InitializeComponent();
    }

    public event EventHandler CursorPositionChanged;

    protected override void OnSelectionChanged(EventArgs e)
    {
      if (CursorPositionChanged is EventHandler)
      {
        CursorPositionChanged(this, e);
      }
    }

    public Int32 CursorRow
    {
      get { return CCursorPosition.Row(this, SelectionStart); }
    }

    public Int32 CursorCol
    {
      get { return CCursorPosition.Col(this, SelectionStart); }
    }



    internal class CCursorPosition
    {
      [System.Runtime.InteropServices.DllImport("user32")]
      public static extern int GetCaretPos(ref Point point);

      private static int GetCorrection(RichTextBox richtextbox, int index)
      {
        Point PA = Point.Empty;
        GetCaretPos(ref PA);
        Point PB = richtextbox.GetPositionFromCharIndex(index);
        if (PA != PB)
        {
          return 1; 
        }
        return 0;
      }

      public static int Row(RichTextBox richtextbox, int index)
      {
        int Correction = GetCorrection(richtextbox, index);
        return 1 + richtextbox.GetLineFromCharIndex(index) - Correction;
      }

      public static int Col(RichTextBox richtextbox, int index)
      {
        int Correction = GetCorrection(richtextbox, index);
        Point P = richtextbox.GetPositionFromCharIndex(index - Correction);
        if (1 == P.X)
        {
          return 1;
        }
        P.X = 0;
        int Index = richtextbox.GetCharIndexFromPosition(P);
        return 1 + index - Index;
      }

    }


  }
}
