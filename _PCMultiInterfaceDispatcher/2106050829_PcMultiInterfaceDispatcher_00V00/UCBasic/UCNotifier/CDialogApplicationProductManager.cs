﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCNotifier
{
  public partial class CDialogApplicationProductManager : Form
  {
    private CUCApplicationManager FUCApplicationManager;

    public CDialogApplicationProductManager()
    {
      InitializeComponent();
    }

    public void SetUCApplicationManager(CUCApplicationManager ucmanager)
    {
      FUCApplicationManager = ucmanager;
      this.Controls.Add(FUCApplicationManager); // ??? is this correct ???
      FUCApplicationManager.Dock = DockStyle.Fill;
    }

    private void CDialogApplicationManager_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.Controls.Remove(FUCApplicationManager);
    }

    private void CDialogApplicationManager_Load(object sender, EventArgs e)
    {
      FUCApplicationManager.ForceResize();
    }
		

  }
}
