﻿namespace UCNotifier
{
  partial class CUCApplication
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxApplication = new System.Windows.Forms.GroupBox();
      this.btnConfirm = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnPasteProductKey = new System.Windows.Forms.Button();
      this.btnCopyProductKey = new System.Windows.Forms.Button();
      this.btnPasteApplicationName = new System.Windows.Forms.Button();
      this.btnCopyApplicationName = new System.Windows.Forms.Button();
      this.tbxName = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.tbxProductKey = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label17 = new System.Windows.Forms.Label();
      this.gbxApplication.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxApplication
      // 
      this.gbxApplication.Controls.Add(this.btnConfirm);
      this.gbxApplication.Controls.Add(this.btnCancel);
      this.gbxApplication.Controls.Add(this.btnPasteProductKey);
      this.gbxApplication.Controls.Add(this.btnCopyProductKey);
      this.gbxApplication.Controls.Add(this.btnPasteApplicationName);
      this.gbxApplication.Controls.Add(this.btnCopyApplicationName);
      this.gbxApplication.Controls.Add(this.tbxName);
      this.gbxApplication.Controls.Add(this.label12);
      this.gbxApplication.Controls.Add(this.tbxProductKey);
      this.gbxApplication.Controls.Add(this.label7);
      this.gbxApplication.Controls.Add(this.label17);
      this.gbxApplication.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbxApplication.Location = new System.Drawing.Point(0, 0);
      this.gbxApplication.Name = "gbxApplication";
      this.gbxApplication.Size = new System.Drawing.Size(592, 123);
      this.gbxApplication.TabIndex = 0;
      this.gbxApplication.TabStop = false;
      this.gbxApplication.Text = "<title>";
      // 
      // btnConfirm
      // 
      this.btnConfirm.Location = new System.Drawing.Point(299, 90);
      this.btnConfirm.Name = "btnConfirm";
      this.btnConfirm.Size = new System.Drawing.Size(75, 23);
      this.btnConfirm.TabIndex = 178;
      this.btnConfirm.Text = "<confirm>";
      this.btnConfirm.UseVisualStyleBackColor = true;
      this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Location = new System.Drawing.Point(218, 90);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(76, 23);
      this.btnCancel.TabIndex = 177;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnPasteProductKey
      // 
      this.btnPasteProductKey.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteProductKey.Location = new System.Drawing.Point(533, 32);
      this.btnPasteProductKey.Name = "btnPasteProductKey";
      this.btnPasteProductKey.Size = new System.Drawing.Size(36, 18);
      this.btnPasteProductKey.TabIndex = 176;
      this.btnPasteProductKey.Text = "Paste";
      this.btnPasteProductKey.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteProductKey.UseVisualStyleBackColor = true;
      this.btnPasteProductKey.Click += new System.EventHandler(this.btnPasteProductKey_Click);
      // 
      // btnCopyProductKey
      // 
      this.btnCopyProductKey.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopyProductKey.Location = new System.Drawing.Point(497, 32);
      this.btnCopyProductKey.Name = "btnCopyProductKey";
      this.btnCopyProductKey.Size = new System.Drawing.Size(36, 18);
      this.btnCopyProductKey.TabIndex = 175;
      this.btnCopyProductKey.Text = "Copy";
      this.btnCopyProductKey.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopyProductKey.UseVisualStyleBackColor = true;
      this.btnCopyProductKey.Click += new System.EventHandler(this.btnCopyProductKey_Click);
      // 
      // btnPasteApplicationName
      // 
      this.btnPasteApplicationName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteApplicationName.Location = new System.Drawing.Point(253, 32);
      this.btnPasteApplicationName.Name = "btnPasteApplicationName";
      this.btnPasteApplicationName.Size = new System.Drawing.Size(36, 18);
      this.btnPasteApplicationName.TabIndex = 173;
      this.btnPasteApplicationName.Text = "Paste";
      this.btnPasteApplicationName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteApplicationName.UseVisualStyleBackColor = true;
      this.btnPasteApplicationName.Click += new System.EventHandler(this.btnPasteName_Click);
      // 
      // btnCopyApplicationName
      // 
      this.btnCopyApplicationName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopyApplicationName.Location = new System.Drawing.Point(217, 32);
      this.btnCopyApplicationName.Name = "btnCopyApplicationName";
      this.btnCopyApplicationName.Size = new System.Drawing.Size(36, 18);
      this.btnCopyApplicationName.TabIndex = 172;
      this.btnCopyApplicationName.Text = "Copy";
      this.btnCopyApplicationName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopyApplicationName.UseVisualStyleBackColor = true;
      this.btnCopyApplicationName.Click += new System.EventHandler(this.btnCopyName_Click);
      // 
      // tbxName
      // 
      this.tbxName.Location = new System.Drawing.Point(23, 52);
      this.tbxName.Multiline = true;
      this.tbxName.Name = "tbxName";
      this.tbxName.Size = new System.Drawing.Size(267, 20);
      this.tbxName.TabIndex = 170;
      this.tbxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbxName.TextChanged += new System.EventHandler(this.tbxTextBoxInput_TextChanged);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(25, 36);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(90, 13);
      this.label12.TabIndex = 171;
      this.label12.Text = "Name Application";
      // 
      // tbxProductKey
      // 
      this.tbxProductKey.BackColor = System.Drawing.SystemColors.Window;
      this.tbxProductKey.Location = new System.Drawing.Point(304, 52);
      this.tbxProductKey.Name = "tbxProductKey";
      this.tbxProductKey.Size = new System.Drawing.Size(266, 20);
      this.tbxProductKey.TabIndex = 169;
      this.tbxProductKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbxProductKey.TextChanged += new System.EventHandler(this.tbxTextBoxInput_TextChanged);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(305, 36);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(65, 13);
      this.label7.TabIndex = 168;
      this.label7.Text = "Product-Key";
      // 
      // label17
      // 
      this.label17.BackColor = System.Drawing.SystemColors.Info;
      this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label17.Location = new System.Drawing.Point(12, 20);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(567, 61);
      this.label17.TabIndex = 174;
      this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCApplication
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxApplication);
      this.Name = "CUCApplication";
      this.Size = new System.Drawing.Size(592, 123);
      this.gbxApplication.ResumeLayout(false);
      this.gbxApplication.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxApplication;
    private System.Windows.Forms.Button btnConfirm;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnPasteProductKey;
    private System.Windows.Forms.Button btnCopyProductKey;
    private System.Windows.Forms.Button btnPasteApplicationName;
    private System.Windows.Forms.Button btnCopyApplicationName;
    private System.Windows.Forms.TextBox tbxName;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox tbxProductKey;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label17;

  }
}
