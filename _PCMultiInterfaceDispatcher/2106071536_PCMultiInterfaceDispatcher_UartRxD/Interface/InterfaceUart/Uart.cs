﻿using System;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Data;
using System.Runtime.Versioning;

namespace InterfaceUart
{
  public delegate void DOnLineReceived(String devicename, String rxline);
  public delegate void DOnErrorReceived(String devicename, SerialErrorReceivedEventArgs data);
  public delegate void DOnPinChanged(String devicename, SerialPinChangedEventArgs data);
  //
  public class CUart
  {
    //
    //------------------------------------------------------------------------
    //  Field
    //------------------------------------------------------------------------
    private SerialPort FSerialPort;
    private String FDeviceName;
    private String FError;
    private String FRxLine;
    private DOnLineReceived FOnLineReceived;
    private DOnErrorReceived FOnErrorReceived;
    private DOnPinChanged FOnPinChanged;
    //
    //------------------------------------------------------------------------
    //  Constructor
    //------------------------------------------------------------------------
    public CUart(DOnLineReceived onlinereceived, 
                 DOnErrorReceived onerrorreceived, 
                 DOnPinChanged onpinchanged)
    {
      FOnLineReceived = onlinereceived;
      FOnErrorReceived = onerrorreceived;
      FOnPinChanged = onpinchanged;
      FSerialPort = new SerialPort();
      FError = "";
      FRxLine = "";
    }
    //
    //------------------------------------------------------------------------
    //  Propery
    //------------------------------------------------------------------------
    public String GetDeviceName()
    {
      return FDeviceName;
    }
    public String GetDevicePort()
    {
      return FSerialPort.PortName;
    }
    public Boolean IsError()
    {
      return (0 < FError.Length);
    }
    public String GetError()
    {
      String Result = FError;
      FError = "";
      return Result;
    }
    public String GetRxLine()
    {
      String Result = FRxLine;
      FRxLine = "";
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Helper
    //------------------------------------------------------------------------

    //
    //------------------------------------------------------------------------
    //  Manager - 
    //------------------------------------------------------------------------
    public Boolean Open(String devicename, String deviceport,   // "Controller", "COM18", 
                        int baudrate, Parity parity,          // 115200, Parity.None
                        int databits, StopBits stopbits,      // 8, StopBits.One
                        Handshake handshake)                  // Handshake.None
    {
      try
      {
        FDeviceName = devicename;
        FSerialPort.PortName = deviceport;
        FSerialPort.BaudRate = baudrate;
        FSerialPort.Parity = parity;
        FSerialPort.DataBits = databits;
        FSerialPort.StopBits = stopbits;
        FSerialPort.Handshake = handshake;
        //
        FSerialPort.DataReceived += OnDataReceived;
        FSerialPort.ErrorReceived += OnErrorReceived;
        FSerialPort.PinChanged += OnPinChanged;
        //
        FSerialPort.Open();
        return true;
      }
      catch (Exception)
      {
        FError = String.Format("{0}: Cannot open ComPort", FDeviceName);
        return false;
      }
    }
    public Boolean Close()
    {
      try
      {
        if (FSerialPort.IsOpen)
        {
          //
          FSerialPort.DataReceived -= OnDataReceived;
          FSerialPort.ErrorReceived -= OnErrorReceived;
          FSerialPort.PinChanged -= OnPinChanged;
          //
          FSerialPort.Close();
          return true;
        }
        FError = String.Format("{0}: ComPort already closed", FDeviceName);
        return false;
      }
      catch (Exception)
      {
        FError = String.Format("{0}: Cannot close ComPort", FDeviceName);
        return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Manager - Event
    //------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------
    //  Callback
    //------------------------------------------------------------------------
    //private static void OnDataReceived(object sender)
    private void OnDataReceived(object sender, SerialDataReceivedEventArgs data)
    {
      try
      {
        while (0 < FSerialPort.BytesToRead)
        {
          byte B = (byte)FSerialPort.ReadByte();
          switch (B)
          {
            case 0x0A: case 0x0D:
              if (0 < FRxLine.Length)
              {
                if (FOnLineReceived is DOnLineReceived)
                {
                  FOnLineReceived(FDeviceName, GetRxLine());
                }
                FRxLine = "";
                return;
              }
              break;
            default:
              FRxLine += (Char)B;
              break;
          }
        }
      }
      catch (Exception)
      {
        FError = String.Format("{0}: OnDataReceived", FDeviceName);
      }
    }
    private void OnErrorReceived(object sender, SerialErrorReceivedEventArgs data)
    {
      try
      {
      }
      catch (Exception)
      {
        FError = String.Format("{0}: OnErrorReceived", FDeviceName);
      }
    }
    private void OnPinChanged(object sender, SerialPinChangedEventArgs data)
    {
      if (FOnPinChanged is DOnPinChanged)
      {
        FOnPinChanged(FDeviceName, data);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Manager - Write
    //------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------
    //  Manager - Read
    //------------------------------------------------------------------------


  }
}
