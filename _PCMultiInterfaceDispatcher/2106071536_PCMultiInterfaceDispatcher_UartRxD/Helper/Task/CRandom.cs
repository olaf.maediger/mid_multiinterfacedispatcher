﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task
{
  public class CRandom
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const Int32 INIT_MINIMUM = 1;
    private const Int32 INIT_MAXIMUM = 1000;
    private const Int32 INIT_SCALEFACTOR = 1;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private Random FRandom;
    private Int32 FTicksOffset;
    private Int32 FMinimum, FMaximum;
    private Int32 FScaleFactor;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CRandom(Int32 minimum, Int32 maximum, Int32 scalefactor, Int32 ticksoffset)
    {
      FTicksOffset = ticksoffset;
      Int32 RT = (Int32)DateTime.Now.Ticks + FTicksOffset;
      FRandom = new Random(RT);
      FMinimum = minimum;
      FMaximum = maximum;
      FScaleFactor = Math.Max(INIT_SCALEFACTOR, scalefactor);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetInterval(Int32 minimum, Int32 maximum, Int32 scalefactor)
    {
      FMinimum = minimum;
      FMaximum = maximum;
      FScaleFactor = Math.Max(INIT_SCALEFACTOR, scalefactor);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    
    
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Int32 NextInteger()
    {
      return FRandom.Next(FMinimum, FMaximum) / FScaleFactor;
    }

    public Int32 NextInteger(Int32 minimum, Int32 maximum)
    {
      return FRandom.Next(minimum, maximum);
    }

  }
}
