﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceBase
{
  public abstract class CInterfaceBase
  {
    public abstract Boolean WriteLine(String line);
  }
}
