﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using Task;
using TextFile;
using UCNotifier;
using InterfaceUart;
using static InterfaceUart.Common;
using InterfaceBt;
using InterfaceWlan;
using InterfaceLan;
using UCInterfaceUart;
using UCInterfaceBt;
using UCInterfaceWlan;
using UCInterfaceLan;
using System.IO.Ports;
//
namespace MultiInterfaceDispatcher
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    private const String NAME_DEBUG = "Debug";
    private const Boolean INIT_DEBUG = true;
    private const String NAME_SHOWPAGE = "ShowPage";
    private const Int32 INIT_SHOWPAGE = 1;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    private CUart FUartController;
    // 
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //!!!!!!!!!!!!!!!!!!!!!!tbcMultiInterfaceDispatcher.SelectedIndex = 0;
      //
      FUartController = new CUart(UartOnLineReceived, UartOnErrorReceived, UartOnPinChanged);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      FUartController.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Initdata
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      Int32 IValue = INIT_AUTOINTERVAL;
      Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      nudAutoIntervalms.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      BValue = INIT_DEBUG;
      Result &= initdata.ReadBoolean(NAME_DEBUG, out BValue, BValue);
      cbxDebug.Checked = BValue;
      //
      IValue = INIT_SHOWPAGE;
      Result &= initdata.ReadInt32(NAME_SHOWPAGE, out IValue, IValue);
      tbcMultiInterfaceDispatcher.SelectedIndex = IValue;
      //

      //Result &= FUartController.Open("Controller", "COM18", 115200, Parity.None, 8, StopBits.One, Handshake.None);
      //if (FUartController.IsError())
      //{
      //  Error(FUartController.GetError());
      //}

      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      Result &= initdata.WriteBoolean(NAME_DEBUG, cbxDebug.Checked);
      Result &= initdata.WriteInt32(NAME_SHOWPAGE, tbcMultiInterfaceDispatcher.SelectedIndex);
      Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoIntervalms.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------


    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    private void UartOnLineReceived(String devicename, String rxline)
    {
      Message(String.Format("{0}>>> {1}", devicename, rxline));
    }
    private void UartOnErrorReceived(String devicename, SerialErrorReceivedEventArgs data)
    {
      switch (data.EventType)
      {
        case SerialError.Frame:
          Error("OnErrorReceived - Frame");
          break;
        case SerialError.Overrun:
          Error("OnErrorReceived - Overrun");
          break;
        case SerialError.RXOver:
          Error("OnErrorReceived - RXOver");
          break;
        case SerialError.RXParity:
          Error("OnErrorReceived - RXParity");
          break;
        case SerialError.TXFull:
          Error("OnErrorReceived - TXFull");
          break;
        default:
          Error("OnErrorReceived - Unknown");
          break;
      }
    }
    private void UartOnPinChanged(String devicename, SerialPinChangedEventArgs data)
    {
      switch (data.EventType)
      {
        case SerialPinChange.Break:
          Event("OnPinChanged - Break");
          break;
        case SerialPinChange.CDChanged:
          Event("OnPinChanged - CD");
          break;
        case SerialPinChange.CtsChanged:
          Event("OnPinChanged - CTS");
          break;
        case SerialPinChange.DsrChanged:
          Event("OnPinChanged - DSR");
          break;
        case SerialPinChange.Ring:
          Event("OnPinChanged - Ring");
          break;
        default:
          Event("OnPinChanged - Unknown");
          break;
      }
    }


    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    private Boolean OnStartupTick(Int32 startupstate)
    {
      Message(String.Format("[{0:000}] ----------------------------", FStartupState));
      switch (FStartupState)
      {
        case 0:
          tbcMultiInterfaceDispatcher.SelectedIndex = 0;
          return true;
        case 1:
          String[] CPE = GetExistingComPorts();
          if (null != CPE)
          {
            Message("Existing ComPorts:");
            for (int I = 0; I < CPE.Length; I++)
            {
              Message(CPE[I]);
            }
          }
          else
          {
            Error("No existing ComPorts");
          }
          return true;
        case 2:
          String[] CPA = GetAvailableComPorts();
          if (null != CPA)
          {
            Message("Available ComPorts:");
            for (int I = 0; I < CPA.Length; I++)
            {
              Message(CPA[I]);
            }
          }
          else
          {
            Error("No available ComPorts");
          }
          return true;
        case 3:

          Boolean Result = FUartController.Open("UartController", "COM18", 115200, 
                                                Parity.None, 8, StopBits.One, Handshake.None);
          if (Result)
          {
            Message(String.Format("{0}[{1}] correct opened", 
                                  FUartController.GetDeviceName(), FUartController.GetDevicePort()));
            return true;
          }
          Error(FUartController.GetError());
          return true;
        case 4:
          String[] CPL = GetAvailableComPorts();
          if (null != CPL)
          {
            Message("Available ComPorts:");
            for (int I = 0; I < CPL.Length; I++)
            {
              Message(CPL[I]);
            }
          }
          else
          {
            Error("No available ComPorts");
          }
          return false;
        case 5:
          return true;
        case 6:
          //Application.Exit();
          //return false;
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          //Application.Exit();
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}


//
//####################################################################################################
//####################################################################################################
//####################################################################################################
