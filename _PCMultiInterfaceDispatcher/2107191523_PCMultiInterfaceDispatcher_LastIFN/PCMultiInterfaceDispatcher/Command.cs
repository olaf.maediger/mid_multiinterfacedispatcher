﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCMultiInterfaceDispatcher
{

  public class CArgumentList : List<String>
  {
  }

  public class CCommand
  {
    private String FText;
    private CArgumentList FArguments;
    public CCommand(String text)
    {
      FText = text;
      FArguments = new CArgumentList();
    }
    public CCommand(String text, String argument0)
    {
      FText = text;
      FArguments = new CArgumentList();
      FArguments.Add(argument0);
    }
    public CCommand(String text,
                    String argument0, String argument1)
    {
      FText = text;
      FArguments = new CArgumentList();
      FArguments.Add(argument0);
      FArguments.Add(argument1);
    }
    public CCommand(String text, String argument0, 
                    String argument1, String argument2)
    {
      FText = text;
      FArguments = new CArgumentList();
      FArguments.Add(argument0);
      FArguments.Add(argument1);
      FArguments.Add(argument2);
    }
    public CCommand(String text,
                    String argument0, String argument1,
                    String argument2, String argument3)
    {
      FText = text;
      FArguments = new CArgumentList();
      FArguments.Add(argument0);
      FArguments.Add(argument1);
      FArguments.Add(argument2);
      FArguments.Add(argument3);
    }
    public String GetText()
    {
      return FText;
    }
    public String GetArgument(int index)
    {
      return FArguments[index];
    }

  }


}
