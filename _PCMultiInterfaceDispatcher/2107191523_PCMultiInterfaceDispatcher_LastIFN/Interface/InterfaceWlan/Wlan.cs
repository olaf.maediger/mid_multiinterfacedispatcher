﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using InterfaceBase;
//
namespace InterfaceWlan
{
  public class CWlan : CInterfaceBase
  {
    //
    //------------------------------------------------------------------------
    //  Manager - Write
    //------------------------------------------------------------------------
    public override Boolean WriteLine(String line)
    {
      return false;
    }
    //
    //------------------------------------------------------------------------
    //  Manager - Read
    //------------------------------------------------------------------------
    public override Boolean ReadLine(out String line)
    {
      line = null;
      return false;
    }
  }
}
