﻿namespace UCInterfaceUart
{
    partial class CUCInterfaceUart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.lblTitle = new System.Windows.Forms.Label();
      this.tbcInterfaceUart = new System.Windows.Forms.TabControl();
      this.tbpUartConfiguration = new System.Windows.Forms.TabPage();
      this.FUCUartConfiguration = new UCInterfaceUart.CUCUartConfiguration();
      this.tbpUartTerminal = new System.Windows.Forms.TabPage();
      this.FUCUartTerminal = new UCInterfaceUart.CUCUartTerminal();
      this.tbcInterfaceUart.SuspendLayout();
      this.tbpUartConfiguration.SuspendLayout();
      this.tbpUartTerminal.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.SystemColors.Info;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(798, 13);
      this.lblTitle.TabIndex = 2;
      this.lblTitle.Text = "Interface UART";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tbcInterfaceUart
      // 
      this.tbcInterfaceUart.Controls.Add(this.tbpUartConfiguration);
      this.tbcInterfaceUart.Controls.Add(this.tbpUartTerminal);
      this.tbcInterfaceUart.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcInterfaceUart.Location = new System.Drawing.Point(0, 13);
      this.tbcInterfaceUart.Name = "tbcInterfaceUart";
      this.tbcInterfaceUart.SelectedIndex = 0;
      this.tbcInterfaceUart.Size = new System.Drawing.Size(798, 435);
      this.tbcInterfaceUart.TabIndex = 3;
      // 
      // tbpUartConfiguration
      // 
      this.tbpUartConfiguration.Controls.Add(this.FUCUartConfiguration);
      this.tbpUartConfiguration.Location = new System.Drawing.Point(4, 22);
      this.tbpUartConfiguration.Name = "tbpUartConfiguration";
      this.tbpUartConfiguration.Size = new System.Drawing.Size(790, 409);
      this.tbpUartConfiguration.TabIndex = 0;
      this.tbpUartConfiguration.Text = "UART - Configuration";
      // 
      // FUCUartConfiguration
      // 
      this.FUCUartConfiguration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCUartConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCUartConfiguration.Location = new System.Drawing.Point(0, 0);
      this.FUCUartConfiguration.Name = "FUCUartConfiguration";
      this.FUCUartConfiguration.Size = new System.Drawing.Size(790, 409);
      this.FUCUartConfiguration.TabIndex = 0;
      // 
      // tbpUartTerminal
      // 
      this.tbpUartTerminal.Controls.Add(this.FUCUartTerminal);
      this.tbpUartTerminal.Location = new System.Drawing.Point(4, 22);
      this.tbpUartTerminal.Name = "tbpUartTerminal";
      this.tbpUartTerminal.Size = new System.Drawing.Size(790, 409);
      this.tbpUartTerminal.TabIndex = 1;
      this.tbpUartTerminal.Text = "UART - Terminal";
      // 
      // FUCUartTerminal
      // 
      this.FUCUartTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCUartTerminal.Location = new System.Drawing.Point(0, 0);
      this.FUCUartTerminal.Name = "FUCUartTerminal";
      this.FUCUartTerminal.Size = new System.Drawing.Size(790, 409);
      this.FUCUartTerminal.TabIndex = 0;
      // 
      // CUCInterfaceUart
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.tbcInterfaceUart);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCInterfaceUart";
      this.Size = new System.Drawing.Size(798, 448);
      this.tbcInterfaceUart.ResumeLayout(false);
      this.tbpUartConfiguration.ResumeLayout(false);
      this.tbpUartTerminal.ResumeLayout(false);
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.TabControl tbcInterfaceUart;
    private System.Windows.Forms.TabPage tbpUartConfiguration;
    private System.Windows.Forms.TabPage tbpUartTerminal;
    private UCInterfaceUart.CUCUartConfiguration FUCUartConfiguration;
    private CUCUartTerminal FUCUartTerminal;
  }
}
