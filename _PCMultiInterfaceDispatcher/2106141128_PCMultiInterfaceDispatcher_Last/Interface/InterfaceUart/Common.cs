﻿using System;
using System.Collections.Generic;
using System.Linq;
//
using System.IO.Ports;
//
namespace InterfaceUart
{
  public static class Common
  {
    //
    //------------------------------------------------------------------------
    //  Global Function
    //------------------------------------------------------------------------
    public static String[] GetExistingComPorts()
    {
      try
      {
        List<String> CPL = new List<String>();
        foreach (String PN in SerialPort.GetPortNames())
        { // debug Console.WriteLine("   {0}", PN);
          CPL.Add(PN);
        }
        return CPL.ToArray();
      }
      catch (Exception)
      {
        return null;
      }
    }
    public static String[] GetAvailableComPorts()
    {
      try
      {
        List<String> CPL = new List<String>();
        CUart UT = new CUart(null, null, null);
        foreach (String PN in SerialPort.GetPortNames())
        {
          if (UT.Open("test", PN, 9600, Parity.None, 8, StopBits.One, Handshake.None))
          { // debug Console.WriteLine("   {0}", PN);
            UT.Close();
            CPL.Add(PN);
          }
        }
        return CPL.ToArray();
      }
      catch (Exception)
      {
        return null;
      }
    }

  }
}






//

//  Console.WriteLine("Available Ports:");
//        for (int CI = 1; CI <= 32; CI++)
//        {
//          String DeviceName = String.Format("COM{0}", CI);
//CUart UartTest = new CUart();
//          if (UartTest.Open("test", DeviceName, 9600, Parity.None, 8, StopBits.One, Handshake.None))
//          {
//            Console.WriteLine("   {0}", DeviceName);
//            UartTest.Close();
//          }
//        }
