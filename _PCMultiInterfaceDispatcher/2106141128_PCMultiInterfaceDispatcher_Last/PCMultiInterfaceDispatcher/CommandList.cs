﻿using InterfaceUart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using Task;
//
namespace PCMultiInterfaceDispatcher
{
  class CCommandList : Stack<CCommand>
  {

    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (0 < Count)
      { 
        CCommand Command = Peek();
        if (Command is CCommand)
        {
          String CommandText = Command.GetText();
        }
      }
    }
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      return false;
    }
    private void CommandListOnExecutionEnd(RTaskData data)
    {
    }
    private void CommandListOnExecutionAbort(RTaskData data)
    {
    }
  }
}


//
//#########################################################################
//  Segment - Callback - CommandList
//#########################################################################

//private CUart FUart;
//public CCommandList(CUart uart)
//{
//  FUart = uart;
//}
// NC FUart.WriteLine(CommandText);

//private delegate void CBCommandListOnExecutionStart(RTaskData data);
//private void CommandListOnExecutionStart(RTaskData data)
//{
//  if (this.InvokeRequired)
//  {
//    CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
//    Invoke(CB, new object[] { data });
//  }
//  else
//  {
//    if (cbxDebug.Checked)
//    {
//      // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
//    }
//    //if (0 < FCommandList.Count)
//    //{ 
//    //  CCommand Command = FCommandList.Peek();
//    //  if (Command is CCommand)
//    //  {
//    //    String CommandText = Command.GetCommandText();
//    //    FUart.WriteLine(CommandText);
//    //  }
//    //}
//  }
//}
