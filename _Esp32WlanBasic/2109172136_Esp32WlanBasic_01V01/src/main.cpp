//
// https://randomnerdtutorials.com/esp32-useful-wi-fi-functions-arduino/
//
#include <Arduino.h>
#include <WiFi.h>
//
const char* WLAN_SSID     = "TPL1300SA7AZ";
const char* WLAN_PASSWORD = "01234567890123456789";
//
unsigned long MillisPreset = 0;
unsigned long TIME_INTERVAL = 30000;

void setup() 
{
  Serial.begin(115200);
  delay(333);
  Serial.println("\r\n*** Esp32WlanBasic");
  //
  WiFi.mode(WIFI_STA);
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  Serial.print("Connecting to WiFi: ");
  while (WL_CONNECTED != WiFi.status()) 
  {
    Serial.print('.');
    delay(1000);
  }
  Serial.println("\r\nWiFi: connected");
  Serial.print("WiFi: IPAddress[");
  Serial.print(WiFi.localIP());
  Serial.println("]");
  Serial.print("WiFi: ReceivedSignalStrengthIndicator[");
  Serial.print(WiFi.RSSI());
  Serial.println("]");
}

void loop() 
{
  if ((WL_CONNECTED != WiFi.status()) && 
      (TIME_INTERVAL <= millis() - MillisPreset))
  {
    MillisPreset = millis();
    Serial.print(MillisPreset);    
    Serial.println("WiFi: reconnecting...");
    WiFi.disconnect();
    WiFi.reconnect();
  }
}