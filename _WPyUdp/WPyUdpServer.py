#
import socket
#
HostIPPort = 20010
#
HostName = socket.gethostname()
HostIPAddress = socket.gethostbyname(HostName)

print('HostName[{}]'.format(HostName))
print('HostIPAddress[{}]'.format(HostIPAddress))
print('HostIPPort[{}]'.format(HostIPPort))
#
FSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
FSocket.bind((HostIPAddress, HostIPPort))
#
while True:
    Data, IPPoint = FSocket.recvfrom(1024) 
    print('Received[{}:{}]<{}>'.format(IPPoint[0], IPPoint[1], Data))
#

