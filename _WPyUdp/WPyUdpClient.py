#
import socket
#
HostIPPort = 20010
#
#BroadcastIPAddress = "192.168.178.255"
BroadcastIPAddress = "192.168.178.36"
LocalName = socket.gethostname()
LocalIPAddress = socket.gethostbyname(LocalName)
print('LocalName[{}]'.format(LocalName))
print('LocalIPAddress[{}]'.format(LocalIPAddress))
print('BroadcastIPAddress[{}]'.format(BroadcastIPAddress))
print('HostIPPort[{}]'.format(HostIPPort))
#
Message = b'Hello, World!'
#
print('Message<{}>'.format(Message))
FSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
FSocket.sendto(Message, (BroadcastIPAddress, HostIPPort))
#