#include <Arduino.h>
//
const int PIN_LEDSYSTEM = 2;
//
void setup(void) 
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
}

void loop(void) 
{
  digitalWrite(PIN_LEDSYSTEM, HIGH);
  delay(100);
  digitalWrite(PIN_LEDSYSTEM, LOW);
  delay(100);
}